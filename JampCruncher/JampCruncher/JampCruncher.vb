﻿Imports System.Data.SqlClient
Imports System.IO
Imports MySql.Data.MySqlClient

Public Class JampCruncher

#Region "Main"

    Public Sub New()
        InitializeComponent()
        Me.Text = "Contrôleur d'encans Version" & My.Application.Info.Version.ToString & " (" & Me.Name.ToString & ")"
        MonthActive = Date.Now.AddMonths(-1).Month
        YearActive = Date.Now.AddMonths(-1).Year
        Me.lblCurrentMonth.Text = "Mise à jour: " & MonthDesc(MonthToUpdate - 1)
        Me.Show()
        hide_all()
        cbMonth.SelectedItem = MonthDesc(MonthActive - 1)
        cbYear.Items.Clear()
        For year As Integer = Date.Now.Year - 1 To Date.Now.Year
            cbYear.Items.Add(year.ToString)
        Next
        cbYear.SelectedItem = YearActive.ToString
        cbYear.Visible = True
        cbYear.Enabled = True
        cbMonth.Visible = True
        cbMonth.Enabled = True
        Choose_Compagny()
    End Sub

    Private Sub lblCurrentMonth_Click(sender As Object, e As EventArgs) Handles lblCurrentMonth.Click
        'MonthToUpdate = Date.Now.AddMonths(-2).Month
        'lblCurrentMonth.Text = "Mise à jour: " & MonthDesc(MonthToUpdate - 1)
    End Sub

    Private Sub lblCurrentMonth_DoubleClick(sender As Object, e As EventArgs) Handles lblCurrentMonth.DoubleClick
        'MonthToUpdate = Date.Now.AddMonths(-1).Month
        'lblCurrentMonth.Text = "Mise à jour: " & MonthDesc(MonthToUpdate - 1)
    End Sub

    Sub Choose_Compagny()
        Dim datareader As SqlDataReader
        Dim Query As String
        Dim Company As String
        Dim db_command As SqlCommand
        Dim db_connection As SqlConnection

        db_connection = New SqlConnection(GSS_Connection)
        db_connection.Open()
        lbCompany.Visible = True
        Query = "SELECT sDepName FROM gss_Cie LEFT OUTER JOIN gss_Dep ON gss_Cie.iCieCode = gss_Dep.iCieCode WHERE LEN(sCieDepGuid) > 0 ORDER BY sDepName"
        db_command = New SqlCommand(Query, db_connection)
        datareader = db_command.ExecuteReader
        db_command.Dispose()
        lbCompany.Items.Clear()
        While datareader.Read
            Company = datareader("sDepName").ToString
            lbCompany.Items.Add(Company)
        End While
        datareader.Close()
        db_connection.Close()
        db_connection.Dispose()
        If lbCompany.Items.Count = 1 Then
            lbCompany.SetSelected(0, True)
            ChooseCompany()
        End If
        Application.DoEvents()
    End Sub

    Private Sub lbCompany_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lbCompany.Click
        ChooseCompany()
    End Sub

    Private Sub ChooseCompany()
        Dim db_command As SqlCommand
        Dim datareader As SqlDataReader
        Dim Query As String
        Dim db_connection As SqlConnection

        If lbCompany.SelectedItem IsNot Nothing Then
            CompagnyName = lbCompany.SelectedItem.ToString
            db_connection = New SqlConnection(GSS_Connection)
            db_connection.Open()
            Query = "SELECT iCieCode, sDepName FROM gss_Dep Where sDepName = '" & lbCompany.SelectedItem.ToString.Replace("'", "''") & "'"
            db_command = New SqlCommand(Query, db_connection)
            datareader = db_command.ExecuteReader
            db_command.Dispose()
            datareader.Read()
            CompagnyID = CInt(datareader("iCieCode").ToString)
            CompagnyName = datareader("sDepName")
            datareader.Close()
            lblCompagny.Text = CompagnyName
            lbCompany.Visible = False
            lblCompagny.Visible = True
            Query = "SELECT * FROM gss_UPC"
            db_command = New SqlCommand(Query, db_connection)
            datareader = db_command.ExecuteReader
            db_command.Dispose()
            lstUPC.Clear()
            lstUPCOther.Clear()
            While datareader.Read
                If CInt(datareader("iCieCode").ToString) = CompagnyID Then
                    lstUPC.Add(datareader("UPCVendeur").ToString)
                Else
                    lstUPCOther.Add(datareader("UPCVendeur").ToString)
                End If
            End While
            datareader.Close()
            db_connection.Close()
            db_connection.Dispose()
            DateActive = New Date(YearActive, MonthActive, 15)
            show_command()
        End If
    End Sub

    Private Sub cbMonth_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbMonth.SelectedIndexChanged
        MonthActive = Array.IndexOf(MonthDesc, cbMonth.SelectedItem) + 1
        DateActive = New Date(YearActive, MonthActive, 15)
        If ImportSales = True Then
            ShowDist()
        End If
    End Sub

    Private Sub cbYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbYear.SelectedIndexChanged
        YearActive = cbYear.Text
        DateActive = New Date(YearActive, MonthActive, 15)
        If ImportSales = True Then
            ShowDist()
        End If
    End Sub

    Private Sub hide_all()
        BtnImportDist.Enabled = False
        'btnPharma.Visible = False
        'btnProduit.Visible = False
        'BtnFactures.Enabled = False
        'btnCME.Enabled = False
        'btnBV.Enabled = False
        'BtnUpdate.Enabled = False
        'btnUpdateCRM.Enabled = False
        lblDistributeur.Visible = False
        lbCompany.Visible = False
        'lvDist.Visible = False
        txtRecordInfo.Visible = False
        btnStatus.Text = ""
        btnStatus.Visible = False
        cbMonth.Enabled = False
        cbYear.Enabled = False
        lblCurrentMonth.Enabled = False
        'btn_bio_mol.Enabled = False
        'btn_Bio_Pharm.Enabled = False
        'btnPharmDiscount.Enabled = False
        'btnImportPharm.Visible = False
        'btnExportExcel.Visible = False
        'btnAjustementRistourne.Visible = False
    End Sub

    Private Sub show_command()
        'BtnFactures.Enabled = True
        'btn_bio_mol.Visible = False
        'btn_Bio_Pharm.Visible = True
        'BtnFactures.Visible = True
        'BtnFactures.Enabled = False
        'btn_bio_mol.Enabled = True
        'btn_Bio_Pharm.Enabled = True
        BtnImportDist.Enabled = True
        'btnPharma.Visible = True
        'btnProduit.Visible = True
        'btnCME.Enabled = True
        'btnBV.Enabled = True
        'BtnUpdate.Enabled = True
        'btnUpdateCRM.Enabled = True
        ImportSales = False
        cbMonth.Enabled = True
        cbYear.Enabled = True
        lblCurrentMonth.Enabled = True
        'btnPharmDiscount.Enabled = True
        'btnAjustementRistourne.Visible = True
        cbMonth.Select()
    End Sub

#End Region

#Region "Importation Distributeur"

    Private Sub BtnImportDist_Click(sender As Object, e As EventArgs) Handles BtnImportDist.Click
        ImportSales = True
        hide_all()
        ShowDist()
    End Sub

    Sub ShowDist()
        Dim db_command As SqlCommand
        Dim datareader As SqlDataReader
        Dim db_command_sale As SqlCommand
        Dim datareader_sale As SqlDataReader
        Dim Query As String
        Dim IntCount As Integer
        Dim Vente As Decimal
        Dim Vente_Totale As Decimal = 0
        Dim db_connection As SqlConnection
        Dim UpdateNeeded As Integer
        Dim LastDist As String = "90pafwjo;f"
        Dim Dist As String

        db_connection = New SqlConnection(GSS_Connection)
        db_connection.Open()

        Query = "SELECT sDistGrossName, Date, Actif, S.iDistCode, iCieCode FROM gss_Dist D RIGHT JOIN gss_DistSetup S ON D.iDistCode = S.iDistCode ORDER BY sDistGrossName, Date DESC "
        db_command = New SqlCommand(Query, db_connection)
        datareader = db_command.ExecuteReader
        db_command.Dispose()
        lvDist.Items.Clear()
        lvDist.Columns.Clear()
        lvDist.View = View.Details
        lvDist.Columns.Add("", 1, HorizontalAlignment.Center)
        lvDist.Columns.Add("Distributeur", 200, HorizontalAlignment.Left)
        lvDist.Columns.Add("Nb lignes", 82, HorizontalAlignment.Center)
        lvDist.Columns.Add("Total", 138, HorizontalAlignment.Right)
        lvDist.Columns.Add("Pass", 50, HorizontalAlignment.Center)
        IntCount = 0
        While datareader.Read
            If YearActive * 100 + MonthActive >= CInt(datareader("Date").ToString) Then
                Dist = datareader("sDistGrossName").ToString
                If datareader("iCieCode").ToString.Contains(CompagnyID.ToString) Then
                    If Dist <> LastDist AndAlso CBool(datareader("Actif").ToString) = True Then
                        iDistCode = datareader("iDistCode").ToString
                        If iDistCode = 0 Then Dist = "JAMP Direct"
                        lvDist.Items.Add(iDistCode)
                        Dim subItem1 As New ListViewItem.ListViewSubItem With {
                        .Name = "Dist",
                        .Text = Dist
                    }
                        lvDist.Items(IntCount).SubItems.Add(subItem1)
                        Query = "SELECT count(*) AS ligne, sum(fNetSales) AS total, sum(iSkip) as PassPharm FROM gss_Trans WHERE iDistCode = '" & iDistCode &
                        "' AND iCieCode = '" & CompagnyID & "' AND dClosedate = convert(datetime,'" & DateActive & "',120)"
                        db_command_sale = New SqlCommand(Query, db_connection)
                        datareader_sale = db_command_sale.ExecuteReader
                        datareader_sale.Read()
                        db_command_sale.Dispose()
                        If datareader_sale.HasRows Then
                            Dim subItem2 As New ListViewItem.ListViewSubItem With {
                            .Name = "nbLigne",
                            .Text = datareader_sale("Ligne").ToString
                        }
                            lvDist.Items(IntCount).SubItems.Add(subItem2)
                            Dim subItem3 As New ListViewItem.ListViewSubItem With {
                            .Name = "Total"
                        }
                            Decimal.TryParse(datareader_sale("Total").ToString, Vente)
                            If Vente = 0 Then
                                subItem3.Text = "$0,00"
                            Else
                                subItem3.Text = Vente.ToString("$0,0.00")
                            End If
                            lvDist.Items(IntCount).SubItems.Add(subItem3)
                            Int32.TryParse(datareader_sale("passPharm").ToString, UpdateNeeded)
                            Dim subItem4 As New ListViewItem.ListViewSubItem With {
                            .Name = "Pass"
                        }
                            If UpdateNeeded Then
                                subItem4.Text = UpdateNeeded
                            Else
                                subItem4.Text = ""
                            End If
                            lvDist.Items(IntCount).SubItems.Add(subItem4)
                            Vente_Totale += Vente
                        End If
                        IntCount = IntCount + 1
                    End If
                    LastDist = Dist
                End If
            End If
        End While
        datareader.Close()
        lvDist.Visible = True
        db_connection.Close()
        db_connection.Dispose()
        Application.DoEvents()

    End Sub

    Private Sub lvDist_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvDist.ItemActivate
        Dim nbItem As Integer
        Dim Vente As Decimal
        Dim nbItemCredit As Integer
        Dim Credit As Decimal
        Dim db_command As SqlCommand
        Dim datareader As SqlDataReader
        Dim Query As String
        Dim db_connection As SqlConnection
        Dim SetupDate As Integer
        Dim Skip As Integer = 0
        Dim SkipPharm As Integer = 0
        Dim SkipDate As Integer = 0
        Dim ExitImport As Boolean = False

        If lvDist.SelectedItems IsNot Nothing AndAlso lvDist.SelectedItems.Count > 0 Then
            db_connection = New SqlConnection(GSS_Connection)
            db_connection.Open()
            lvDist.Visible = False
            cbMonth.Visible = False
            cbYear.Visible = False
            iDistCode = CInt(lvDist.SelectedItems.Item(0).SubItems(0).Text)

            dtClientD.Reset()
            Query = "SELECT DISTINCT * FROM gss_vClientD WHERE iActive = 1 AND iClientTypeCode <> 6 AND iClientTypeCode <> 7"
            Using cmd As New SqlCommand(Query, db_connection)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtClientD)
                End Using
            End Using
            dvClientD = New DataView(dtClientD)

            dtClientC.Reset()
            Query = "SELECT iCLientCode, iCieCode, iTerrCode, iBanCode, sBanName, iGrpCode " _
                   & "FROM gss_vClientC WHERE iCurrentInfoC = 1 AND iActive = 1 AND iCieCode = " & CompagnyID
            Using cmd As New SqlCommand(Query, db_connection)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtClientC)
                End Using
            End Using
            dvClientC = New DataView(dtClientC)

            dtProdM.Reset()
            Query = "SELECT iProdCode, iCieCode, iUnivCode, sProdUPC, sProdName_F, sProdName_E, sProdInfos FROM gss_PRODM WHERE iActive = 1"
            Using cmd As New SqlCommand(Query, db_connection)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtProdM)
                End Using
            End Using
            dvProdM = New DataView(dtProdM)

            dtProdP.Reset()
            Query = "SELECT iProdCode, iProdProv, fProdSalesPrice FROM gss_ProdP WHERE iCurrentInfoP = 1"
            Using cmd As New SqlCommand(Query, db_connection)
                Using sa As New SqlDataAdapter(cmd)
                    sa.Fill(dtProdP)
                End Using
            End Using
            dvProdP = New DataView(dtProdP)

            dtDep.Reset()
            Query = "SELECT iCieCode, sDepName FROM gss_Dep"
            Using cmd As New SqlCommand(Query, db_connection)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtDep)
                End Using
            End Using
            dvDep = New DataView(dtDep)

            dtTerr.Reset()
            Query = "SELECT iTerrCode, iRepCode, iCoorCode, iGestCode FROM gss_TerrD WHERE iCurrentInfo = 1"
            Using cmd As New SqlCommand(Query, db_connection)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtTerr)
                End Using
            End Using
            dvTerr = New DataView(dtTerr)

            dSkip.Clear() 'Empty Skip dictionnary

            dtMapClient.Reset()
            Query = "SELECT iClientCode, iNoClient, NoClient, iDoNotProcess FROM gss_MapClient WHERE iDistCode = " & iDistCode
            Using cmd As New SqlCommand(Query, db_connection)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtMapClient)
                End Using
            End Using

            dtMapProd.Reset()
            Query = "SELECT iProdCode, iNoProduit, NoProduit, iDoNotProcess FROM gss_MapProd WHERE iDistCode = " & iDistCode
            Using cmd As New SqlCommand(Query, db_connection)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtMapProd)
                End Using
            End Using

            iSessionCode = DB_Get_Next_Index("iSessionCode", "gss_Trans_Mapping", db_connection)
            DistributorName = lvDist.SelectedItems.Item(0).SubItems("Dist").Text
            nbItem = CInt(lvDist.SelectedItems.Item(0).SubItems("nbLigne").Text)
            Vente = lvDist.SelectedItems.Item(0).SubItems("total").Text
            SetupDate = MonthActive + YearActive * 100
            Query = "SELECT * FROM gss_Dist D RIGHT JOIN gss_DistSetup S ON D.iDistCode = S.iDistCode " _
                        & "Where iCieCode like '%" & CompagnyID & "%' AND  S.iDistCode = '" & iDistCode & "' AND date <= '" & SetupDate & "' ORDER BY date DESC"
            db_command = New SqlCommand(Query, db_connection)
            datareader = db_command.ExecuteReader
            db_command.Dispose()
            datareader.Read()
            DistFile.MultiCie = CInt(datareader("iMultiCie").ToString)
            DistFile.Cie = datareader("iCieCode").ToString
            DistFile.FichierClient = datareader("FichierClient").ToString
            DistFile.MultiCie = CInt(datareader("iMultiCie").ToString)
            DistFile.Cie = datareader("iCieCode").ToString
            DistFile.FileNameStart = datareader("FileNameStart")
            DistFile.FormatFichier = CInt(datareader("FormatFichier").ToString)
            DistFile.PremiereLigne = CInt(datareader("PremiereLigne").ToString)
            If DistFile.PremiereLigne < 1 Then DistFile.PremiereLigne = 1
            DistFile.ConserveColonne = CInt(datareader("ConserveColonne").ToString)
            DistFile.NoPharmaPos = CInt(datareader("NoPharmaciePos").ToString)
            DistFile.NoPharmaLen = CInt(datareader("NoPharmacieLen").ToString)
            DistFile.NoProduitPos = CInt(datareader("NoProduitPos").ToString)
            DistFile.NoProduitLen = CInt(datareader("NoProduitLen").ToString)
            DistFile.NoProduitPrefixe = datareader("NoProduitPrefixe")
            DistFile.DatePos = CInt(datareader("DatePos").ToString)
            DistFile.DateLen = CInt(datareader("DateLen").ToString)
            DistFile.DateFormat = datareader("DateFormat").ToString
            DistFile.BannierePos = CInt(datareader("BannierePos").ToString)
            DistFile.BanniereLen = CInt(datareader("BanniereLen").ToString)
            DistFile.BanniereOverride1 = datareader("BanniereOverride1").ToString
            DistFile.banniereOverride2 = datareader("BanniereOverride2").ToString
            DistFile.PharmaciePos = CInt(datareader("PharmaciePos").ToString)
            DistFile.Pharmacie2Pos = CInt(datareader("Pharmacie2Pos").ToString)
            DistFile.PharmacieLen = CInt(datareader("PharmacieLen").ToString)
            DistFile.AdressePos = CInt(datareader("AdressePos").ToString)
            DistFile.AdresseLen = CInt(datareader("AdresseLen").ToString)
            DistFile.VillePos = CInt(datareader("VillePos").ToString)
            DistFile.VilleLen = CInt(datareader("VilleLen").ToString)
            DistFile.CodePostalPos = CInt(datareader("CodePostalPos").ToString)
            DistFile.CodePostalLen = CInt(datareader("CodePostalLen").ToString)
            DistFile.UPCClientPos = CInt(datareader("UPCClientPos").ToString)
            DistFile.UPCClientLen = CInt(datareader("UPCClientLen").ToString)
            DistFile.UPCProduitPos = CInt(datareader("UPCProduitPos").ToString)
            DistFile.UPCProduitLen = CInt(datareader("UPCProduitLen").ToString)
            DistFile.ProduitPos = CInt(datareader("ProduitPos").ToString)
            DistFile.Produit2Pos = CInt(datareader("Produit2Pos").ToString)
            DistFile.ProduitLen = CInt(datareader("ProduitLen").ToString)
            DistFile.QtePos = CInt(datareader("QtePos").ToString)
            DistFile.QteLen = CInt(datareader("QteLen").ToString)
            DistFile.MontantPos = CInt(datareader("MontantPos").ToString)
            DistFile.MontantLen = CInt(datareader("MontantLen").ToString)
            DistFile.Diviseur = CInt(datareader("Diviseur").ToString)
            DistFile.FacturePos = CInt(datareader("FacturePos").ToString)
            DistFile.FactureLen = CInt(datareader("FactureLen").ToString)
            If DistFile.DateFormat.Length > 1 Then
                If DistFile.DateFormat.Contains("AAAA") Then
                    DistFile.DateFormat = DistFile.DateFormat.Replace("AAAA", YearActive)
                End If
                If DistFile.DateFormat.Contains("AA") Then
                    DistFile.DateFormat = DistFile.DateFormat.Replace("AA", YearActive - 2000)
                End If
                If DistFile.DateFormat.Contains("MM") Then
                    DistFile.DateFormat = DistFile.DateFormat.Replace("MM", (MonthActive + 100).ToString.Substring(1, 2))
                End If
                'If DistFile.DateFormat.Contains("DD") Then
                '    DistFile.DateFormat = DistFile.DateFormat.Replace("DD", "01")
                'End If
            End If
            datareader.Close()
            lblDistributeur.Text = DistributorName & " - " & MonthDesc(MonthActive - 1) & " " & YearActive
            Application.DoEvents()
            DB_Exec_NonQuery("TRUNCATE TABLE gss_Trans_Temp", db_connection) 'Erase previous Import
            If nbItem > 0 Then 'Erase previous Import
                lblDistributeur.Text = lblDistributeur.Text & vbLf & Vente.ToString("$0,0.00")
                Dim reply As DialogResult = MessageBox.Show("Vous avez déjà des ventes d'importées" & vbLf & "Désirez-vous les effacer?", DistributorName & " " & Vente.ToString("$0,0.00"),
                  MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
                If reply = DialogResult.Yes Then 'Effacer les ventes
                    If DistFile.MultiCie = 1 Then
                        Query = "DELETE FROM gss_Trans WHERE iDistCode = '" & iDistCode & "' AND dCloseDate = '" & DateActive & "'"
                    Else
                        Query = "DELETE FROM gss_Trans WHERE iCieCode = '" & CompagnyID & "' AND iDistCode = '" & iDistCode & "' AND dCloseDate = '" & DateActive & "'"
                    End If
                    DB_Exec_NonQuery(Query, db_connection)
                End If
                If reply = DialogResult.Cancel Then 'Go out
                    ExitImport = True
                End If
            End If
            lblDistributeur.Visible = True
            If Not ExitImport Then
                import_data(Skip, SkipPharm, SkipDate) 'Read data and save in Trans_Temp
                Query = "SELECT count(*) AS ligne, sum(fNetSales) AS total FROM gss_Trans_Temp"
                db_command = New SqlCommand(Query, db_connection)
                datareader = db_command.ExecuteReader
                db_command.Dispose()
                datareader.Read()
                nbItem = CInt(datareader("ligne").ToString)
                Decimal.TryParse(datareader("total").ToString, Vente)
                datareader.Close()
                Query = "SELECT count(*) AS ligne, sum(fNetSales) AS total FROM gss_Trans_Temp WHERE fNetSales < '0'"
                db_command = New SqlCommand(Query, db_connection)
                datareader = db_command.ExecuteReader
                db_command.Dispose()
                datareader.Read()
                nbItemCredit = CInt(datareader("ligne").ToString)
                Decimal.TryParse(datareader("total").ToString, Credit)
                If nbItem > 0 Then 'Some sales present
                    Dim sCredit As String = "Aucun crédit"
                    Dim sSkip As String = ""
                    Dim sSkipPharm As String = ""
                    Dim SSkipDate As String = ""

                    If Credit < 0 Then sCredit = nbItemCredit & " Crédit(s) : " & Credit.ToString("$0,0.00")
                    If Skip > 0 Then sSkip = vbLf & Skip & " Produit(s) non traité(s)"
                    If SkipPharm > 0 Then sSkipPharm = vbLf & SkipPharm & " CLient(s) non traité(s)"
                    If SkipDate > 0 Then SSkipDate = vbLf & SkipDate & " Date(s) non traité(s)"
                    Dim reply As DialogResult = MessageBox.Show(DistributorName & " " & MonthDesc(MonthActive - 1) _
                        & " " & YearActive & vbLf & vbLf & nbItem & " Ligne(s) : " & Vente.ToString("$0,0.00") _
                        & vbLf & sCredit & sSkip & sSkipPharm & SSkipDate _
                        & vbLf & vbLf & "Importer le résutat ?", DistributorName & " " & Vente.ToString("$0,0.00"),
                      MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
                    If reply = DialogResult.Yes Then 'Importer les ventes
                        DB_Exec_NonQuery("INSERT INTO gss_Trans SELECT * FROM gss_Trans_Temp", db_connection)
                        If UpdateGrabb Then
                            Dim db_connection_GRABB = New SqlConnection(GRABB_Connection)
                            db_connection_GRABB.Open()
                            DB_Exec_NonQuery("DELETE FROM gss_Trans WHERE dCloseDate = '" & DateActive & "' AND iDistCode = " & iDistCode, db_connection_GRABB)
                            db_connection_GRABB.Close()
                            db_connection_GRABB.Dispose()
                            Data_Transfert("SELECT * FROM gss_Trans WHERE dCloseDate = '" & DateActive & "' AND iDistCode = " & iDistCode, db_connection, "gss_Trans", GRABB_Connection)
                        End If
                    End If
                    'Export_Excel_Trans_Mapping_Client()
                    'Export_Excel_Trans_Mapping_Produit()
                End If
                datareader.Close()
                DB_Exec_NonQuery("TRUNCATE TABLE gss_Trans_Temp", db_connection) 'Erase previous Import
            End If
            db_connection.Close()
            db_connection.Dispose()
            lvDist.Visible = True
            'btGRABBUpdate.Visible = True
            cbMonth.Visible = True
            cbYear.Visible = True
            lblDistributeur.Visible = False
            txtRecordInfo.Visible = False
            ShowDist()
        End If
    End Sub

    Sub import_data(ByRef Skip As Integer, ByRef SkipPharm As Integer, ByRef SkipDate As Integer)
        Dim fileDialog As New OpenFileDialog()
        Dim db_connection As SqlConnection
        Dim iLoop As Integer = 0
        Dim jloop As Integer = 0
        Dim Amount As String = ""
        Dim Qtee As String = ""
        Dim LastCheck As Boolean = False
        Dim LastPharm As String = ""
        Dim LastProduit As String = ""
        Dim LastSkip As Integer
        Dim Pharmacie1 As String = ""
        Dim Pharmacie2 As String = ""
        Dim Produit1 As String = ""
        Dim Produit2 As String = ""

        db_connection = New SqlConnection(GSS_Connection)
        db_connection.Open()
        DistData = New FieldExtract
        skipProduct.Clear()
        skipPharma.Clear()
        Select Case DistFile.FormatFichier
            Case 0
                Dim result As DataTable
                Dim DataLine As DataRow

                result = getDataTable(DistFile.PremiereLigne, DistFile.DatePos, "", DistFile.FileNameStart)
                If result IsNot Nothing AndAlso result.Rows.Count > 0 Then
                    For jloop = 0 To result.Rows.Count - 1
                        DataLine = result.Rows(jloop)
                        Extract_Data_Excel(DataLine, DistFile.NoPharmaPos, DistData.NoPharma)
                        Extract_Data_Excel(DataLine, DistFile.NoProduitPos, DistData.noProduit)
                        Extract_Data_Excel(DataLine, DistFile.DatePos, DistData.TransDate)
                        Extract_Data_Excel(DataLine, DistFile.BannierePos, DistData.Banniere)
                        If DistFile.BanniereOverride1.Length > 0 Then DistData.Banniere = DistFile.BanniereOverride1
                        Extract_Data_Excel(DataLine, DistFile.PharmaciePos, Pharmacie1)
                        Extract_Data_Excel(DataLine, DistFile.Pharmacie2Pos, Pharmacie2)
                        DistData.Pharmacie = (Pharmacie1 & " " & Pharmacie2).Trim
                        Extract_Data_Excel(DataLine, DistFile.AdressePos, DistData.Adresse)
                        Extract_Data_Excel(DataLine, DistFile.VillePos, DistData.Ville)
                        Extract_Data_Excel(DataLine, DistFile.CodePostalPos, DistData.CodePostal)
                        Extract_Data_Excel(DataLine, DistFile.UPCClientPos, DistData.UPCClient)
                        Extract_Data_Excel(DataLine, DistFile.UPCProduitPos, DistData.UPCProduit)
                        Extract_Data_Excel(DataLine, DistFile.ProduitPos, Produit1)
                        Extract_Data_Excel(DataLine, DistFile.Produit2Pos, Produit2)
                        DistData.Produit = (Produit1 & " " & Produit2).Trim
                        Extract_Data_Excel(DataLine, DistFile.QtePos, Qtee)
                        Double.TryParse(Qtee, DistData.Qte)
                        Extract_Data_Excel(DataLine, DistFile.MontantPos, Amount)
                        If DistData.TransDate.Length > 10 Then DistData.TransDate = DistData.TransDate.Substring(0, 10)
                        DistData.TransDate = DistData.TransDate.Replace("M", "-") 'Patch pour McKesson Date
                        Amount = Amount.Replace("$", "")
                        Decimal.TryParse(Amount, DistData.Montant)
                        If DistData.Montant = 0D Then Decimal.TryParse(Amount.Replace(".", ","), DistData.Montant)
                        If DistFile.Diviseur > 0 Then DistData.Montant = DistData.Montant / DistFile.Diviseur
                        Extract_Data_Excel(DataLine, DistFile.FacturePos, DistData.NoFacture)
                        If DistData.NoFacture.StartsWith("Facture #") Then
                            Dim noFact() As String = Split(DistData.NoFacture)
                            DistData.NoFacture = noFact(1).Substring(1, noFact(1).Length - 1)
                        End If
                        If DistData.noProduit.Length > 0 Then
                            LastSkip = Skip
                            Check_Data(db_connection, iLoop, Skip, SkipPharm, SkipDate, LastPharm, LastProduit)
                            If LastSkip = Skip Then
                                LastPharm = DistData.NoPharma
                                LastProduit = DistData.noProduit
                            End If
                        End If
                        If iLoop = -1 Then Exit For
                    Next
                End If

            Case 1 'CSV
                Dim result As DataSet
                Dim DataLine As DataRow
                Dim ClientInfo As DataSet
                Dim dvClientInfo As DataView = Nothing
                Dim UseClientInfo As Boolean = False

                If DistFile.FichierClient.Length > 0 Then
                    ClientInfo = getDatasetCSV("", DistFile.FichierClient)
                    dvClientInfo = New DataView(ClientInfo.Tables(0))
                    UseClientInfo = True
                End If
                result = getDatasetCSV("", DistFile.FileNameStart)
                If result IsNot Nothing AndAlso result.Tables.Count > 0 Then
                    For jloop = DistFile.PremiereLigne - 1 To result.Tables(0).Rows.Count - 1
                        DataLine = result.Tables(0).Rows(jloop)
                        If UseClientInfo = True Then
                            Extract_Data_Excel(DataLine, DistFile.NoPharmaPos, DistData.NoPharma)
                            dvClientInfo.RowFilter = "Column" & DistFile.NoPharmaLen & " = '" & DistData.NoPharma & "'"
                            DistData.Banniere = ""
                            Pharmacie1 = ""
                            Pharmacie2 = ""
                            DistData.Adresse = ""
                            DistData.Ville = ""
                            DistData.CodePostal = ""

                            If dvClientInfo.Count > 0 Then
                                If DistFile.BannierePos > 0 Then DistData.Banniere = dvClientInfo(0)("Column" & DistFile.BannierePos)
                                If DistFile.PharmaciePos > 0 Then Pharmacie1 = dvClientInfo(0)("Column" & DistFile.PharmaciePos)
                                If DistFile.Pharmacie2Pos > 0 Then Pharmacie2 = dvClientInfo(0)("Column" & DistFile.Pharmacie2Pos)
                                If DistFile.AdressePos > 0 Then DistData.Adresse = dvClientInfo(0)("Column" & DistFile.AdressePos)
                                If DistFile.VillePos > 0 Then DistData.Ville = dvClientInfo(0)("Column" & DistFile.VillePos)
                                If DistFile.CodePostalPos > 0 Then DistData.CodePostal = dvClientInfo(0)("Column" & DistFile.CodePostalPos)
                            Else
                                Dim Trouble = True
                            End If
                        Else
                            Extract_Data_Excel(DataLine, DistFile.NoPharmaPos, DistData.NoPharma)
                            Extract_Data_Excel(DataLine, DistFile.BannierePos, DistData.Banniere)
                            Extract_Data_Excel(DataLine, DistFile.PharmaciePos, Pharmacie1)
                            Extract_Data_Excel(DataLine, DistFile.Pharmacie2Pos, Pharmacie2)
                            Extract_Data_Excel(DataLine, DistFile.AdressePos, DistData.Adresse)
                            Extract_Data_Excel(DataLine, DistFile.VillePos, DistData.Ville)
                            Extract_Data_Excel(DataLine, DistFile.CodePostalPos, DistData.CodePostal)
                        End If

                        Extract_Data_Excel(DataLine, DistFile.NoProduitPos, DistData.noProduit)
                        Extract_Data_Excel(DataLine, DistFile.DatePos, DistData.TransDate)
                        If DistFile.BanniereOverride1.Length > 0 Then DistData.Banniere = DistFile.BanniereOverride1
                        DistData.Pharmacie = (Pharmacie1 & " " & Pharmacie2).Trim
                        Extract_Data_Excel(DataLine, DistFile.UPCClientPos, DistData.UPCClient)
                        Extract_Data_Excel(DataLine, DistFile.UPCProduitPos, DistData.UPCProduit)
                        Extract_Data_Excel(DataLine, DistFile.ProduitPos, Produit1)
                        Extract_Data_Excel(DataLine, DistFile.Produit2Pos, Produit2)
                        DistData.Produit = (Produit1 & " " & Produit2).Trim
                        Extract_Data_Excel(DataLine, DistFile.QtePos, Qtee)
                        Double.TryParse(Qtee, DistData.Qte)
                        Extract_Data_Excel(DataLine, DistFile.MontantPos, Amount)
                        If DistData.TransDate.Length > 10 Then DistData.TransDate = DistData.TransDate.Substring(0, 10)
                        Amount = Amount.Replace("$", "")
                        Decimal.TryParse(Amount, DistData.Montant)
                        If DistData.Montant = 0D Then Decimal.TryParse(Amount.Replace(".", ","), DistData.Montant)
                        If DistFile.Diviseur > 0 Then DistData.Montant = DistData.Montant / DistFile.Diviseur
                        Extract_Data_Excel(DataLine, DistFile.FacturePos, DistData.NoFacture)
                        If DistData.NoFacture.StartsWith("Facture #") Then
                            Dim noFact() As String = Split(DistData.NoFacture)
                            DistData.NoFacture = noFact(1).Substring(1, noFact(1).Length - 1)
                        End If
                        If DistData.noProduit.Length > 0 Then
                            LastSkip = Skip
                            Check_Data(db_connection, iLoop, Skip, SkipPharm, SkipDate, LastPharm, LastProduit)
                            If LastSkip = Skip Then
                                LastPharm = DistData.NoPharma
                                LastProduit = DistData.noProduit
                            End If
                        End If
                        If iLoop = -1 Then Exit For
                    Next
                End If

            Case 2 'Flat file
                Dim DataLine As String
                Dim stream As StreamReader
                Dim Proceed As Boolean = True

                If DistFile.FileNameStart.Length > 0 Then
                    fileDialog.Filter = "Files (" & DistFile.FileNameStart & "*.*)|" & DistFile.FileNameStart & "*.*|All Files (*.*)|*.*"
                Else
                    fileDialog.Filter = "Files (*.*)|*.*|All Files (*.*)|*.*"
                End If
                Dim result = fileDialog.ShowDialog()
                If fileDialog.FileName.Length = 0 Then Proceed = False
                If DistFile.FileNameStart.Length > 0 Then
                    If Not fileDialog.SafeFileName.ToUpper.StartsWith(DistFile.FileNameStart.ToUpper) Then
                        If MsgBox("Le fichier n'a pas le bon nom, désirez-vous l'importer quand même?", MsgBoxStyle.OkCancel, "Mauvais nom de fichier") = MsgBoxResult.Cancel Then
                            Proceed = False
                        End If
                    End If
                End If

                ImportFile = fileDialog.SafeFileName
                If Proceed = True Then
                    Try
                        stream = New StreamReader(fileDialog.FileName)
                    Catch
                        Exit Select
                    End Try
                    While (Not stream.EndOfStream)
                        jloop += 1
                        If jloop >= DistFile.PremiereLigne Then
                            DataLine = stream.ReadLine
                            DistData.NoPharma = Extract_Data_Flat(DataLine, DistFile.NoPharmaPos, DistFile.NoPharmaLen)
                            DistData.noProduit = Extract_Data_Flat(DataLine, DistFile.NoProduitPos, DistFile.NoProduitLen)
                            DistData.TransDate = Extract_Data_Flat(DataLine, DistFile.DatePos, DistFile.DateLen)
                            DistData.Banniere = Extract_Data_Flat(DataLine, DistFile.BannierePos, DistFile.BanniereLen)
                            If DistFile.BanniereOverride1.Length > 0 Then DistData.Banniere = DistFile.BanniereOverride1
                            DistData.Pharmacie = Extract_Data_Flat(DataLine, DistFile.PharmaciePos, DistFile.PharmacieLen)
                            DistData.Adresse = Extract_Data_Flat(DataLine, DistFile.AdressePos, DistFile.AdresseLen)
                            DistData.Ville = Extract_Data_Flat(DataLine, DistFile.VillePos, DistFile.VilleLen)
                            DistData.CodePostal = Extract_Data_Flat(DataLine, DistFile.CodePostalPos, DistFile.CodePostalLen)
                            DistData.UPCClient = Extract_Data_Flat(DataLine, DistFile.UPCClientPos, DistFile.UPCClientLen)
                            DistData.UPCProduit = Extract_Data_Flat(DataLine, DistFile.UPCProduitPos, DistFile.UPCProduitLen)
                            DistData.Produit = Extract_Data_Flat(DataLine, DistFile.ProduitPos, DistFile.ProduitLen)
                            Double.TryParse(Extract_Data_Flat(DataLine, DistFile.QtePos, DistFile.QteLen), DistData.Qte)
                            Amount = Extract_Data_Flat(DataLine, DistFile.MontantPos, DistFile.MontantLen)
                            Decimal.TryParse(Amount, DistData.Montant)
                            If DistData.Montant = 0D Then Decimal.TryParse(Amount.Replace(".", ","), DistData.Montant)
                            If DistFile.Diviseur > 0 Then DistData.Montant = DistData.Montant / DistFile.Diviseur
                            DistData.NoFacture = Extract_Data_Flat(DataLine, DistFile.FacturePos, DistFile.FactureLen)
                            Check_Data(db_connection, iLoop, Skip, SkipPharm, SkipDate, LastPharm, LastProduit)
                            LastPharm = DistData.NoPharma
                            LastProduit = DistData.noProduit
                            If iLoop = -1 Then Exit While
                        End If
                    End While
                End If

        End Select
        db_connection.Close()
        db_connection.Dispose()
    End Sub

    Sub Check_Data(db_connection As SqlConnection, ByRef iLoop As Integer, ByRef Skip As Integer, ByRef SkipPharm As Integer, ByRef SkipDate As Integer, ByRef LastPharm As String, ByRef LastProduit As String)
        Dim db_command As SqlCommand
        Dim datareader As SqlDataReader = Nothing
        Dim Query As String
        Dim SubQuery As String
        Dim iClientCode As Integer = 0
        Dim sClientCode As String = ""
        Dim Codepostal As String = "       "
        Dim AdresseString As String = ""
        Dim Adresse As Integer = 0
        Dim DataString As String()
        Dim SearchName As New List(Of String)
        Dim PharmaID As String
        Dim iProdCode As Integer = 0
        Dim UPC As String
        Dim Produit As String = ""
        Dim iTerrCode As Integer = 0
        Dim iProvCode As Integer = 0
        Dim iRepCode As Integer = 0
        Dim iCoorCode As Integer = 0
        Dim iGestCode As Integer = 0
        Dim iBanCode As Integer = 0
        Dim iGrpCode As Integer = 0
        Dim iChnlCode As Integer = 0
        Dim iUnivCode As Integer = 0
        Dim DateValidate As Boolean = False
        Dim FamilyProductValidate As Boolean = False
        Dim SearchPos As Integer = 0
        Dim Montant As Double
        Dim Prix As Double = 0
        Dim index As Integer
        Dim iCieCode As Integer = 0
        Dim noProduit As Integer
        Dim noClient As Integer
        Dim ForceSelection As Boolean = False
        Dim AutoMapping As Boolean = True

        If DistFile.DatePos = 0 Or
                (DistData.TransDate.Length >= DistFile.DateFormat.Length AndAlso DistFile.DateFormat = DistData.TransDate.Substring(0, DistFile.DateFormat.Length)) Then
            DateValidate = True
        Else
            If DistData.TransDate.Length > 0 Then
                Dim WorkDate As Date
                Dim ExcelDate As Integer

                Int32.TryParse(DistData.TransDate, ExcelDate)
                WorkDate = New Date(1899, 12, 30).AddDays(ExcelDate)
                If WorkDate.Year = YearActive And WorkDate.Month Then
                    DistData.TransDate = DistFile.DateFormat
                    DateValidate = True
                Else
                    Try
                        WorkDate = DateTime.Parse(DistData.TransDate)
                        If WorkDate.Year = YearActive And WorkDate.Month = MonthActive Then
                            'DistData.TransDate = DistFile.DateFormat
                            DateValidate = True
                        End If
                    Catch
                    End Try
                End If
            End If
        End If
        If iLoop = 0 Then
            If Validate_Data(DateValidate) = False Then
                iLoop = -1
                Exit Sub
            End If
            txtRecordInfo.Visible = True
        End If
        iLoop += 1
        Application.DoEvents()
        If DateValidate = False Then
            SkipDate += 1
        Else
            If DistData.Qte <> 0 Then
                'Look for product
                UPC = ""
                Integer.TryParse(DistData.noProduit, noProduit)
                If noProduit > 0 Then
                    drMap = dtMapProd.Select("iNoProduit = " & noProduit)
                Else
                    drMap = dtMapProd.Select("NoProduit = '" & DistData.noProduit.Replace("'", "''").Trim & "'")
                End If

                If drMap.Count = 0 Then
                    dvProdM.RowFilter = "sProdName_F = 'azvvazwef'"  'reset dataview
                    If DistFile.UPCProduitPos > 0 Then
                        If DistFile.UPCClientPos > 0 Then
                            UPC = (CDec(DistData.UPCClient) * 100000 + CDec(DistData.UPCProduit)).ToString
                        Else
                            UPC = DistData.UPCProduit.Replace(" ", "") 'Remove space for NQ
                        End If
                        If iDistCode = 0 And UPC.Length = 13 Then UPC = UPC.Substring(0, 12) 'Patch pour Jamp Direct
                        UPC = UPC.Trim
                        dvProdM.RowFilter = "sProdUPC = '" & UPC & "'"
                        If dvProdM.Count = 0 And UPC.Length < 12 Then
                            dvProdM.RowFilter = "sProdUPC LIKE '%" & UPC & "%'"
                        End If
                    End If
                    If dvProdM.Count = 0 Then
                        ForceSelection = True
                        Dim nb_search As Integer = 0

                        If UPC.Length > 10 And UPC.Length < 12 Then
                            'May be missing first or last number
                            dvProdM.RowFilter = "sProdUPC LIKE '%" & UPC & "%'"
                        End If

                        If dvProdM.Count = 0 Or dvProdM.Count > 6 Then
                            DataString = Split(DistData.Produit.ToUpper.Trim.Replace("*", " ").Replace("-", " ").Replace("/", " ").Replace("MG", "MG ").Replace("&", " & "), " ") 'Add space to help search around MG and &
                            SearchName.Clear()
                            For jloop As Integer = 1 To DataString.Length - 1
                                If DataString(jloop).Length > 2 Then
                                    SearchName.Add(DataString(jloop))
                                    nb_search += 1
                                    If nb_search = 3 Then Exit For
                                End If
                            Next
                            Query = ""
                            If SearchName.Count Then
                                For jloop = 0 To SearchName.Count - 1
                                    If jloop > 0 Then Query = Query & " AND "
                                    Query = Query & "(sProdName_F LIKE '%" & SearchName(jloop).Replace("'", "''") & "%' OR sProdInfos LIKE '%" & SearchName(jloop).Replace("'", "''") & "%'" & ")"
                                Next
                            End If
                            dvProdM.RowFilter = Query
                        End If

                        If dvProdM.Count = 0 Or dvProdM.Count > 6 Then
                            DataString = Split(DistData.Produit.ToUpper.Replace("*", " ").Replace("-", " ").Replace("/", " ").Replace("MG", "MG ").Replace("&", " & "), " ")
                            SearchName.Clear()
                            For jloop As Integer = 0 To DataString.Length - 1
                                If DataString(jloop).Length > 3 AndAlso Not DataString(jloop).Contains(".") Then
                                    SearchName.Add(DataString(jloop))
                                End If
                            Next
                            If SearchName.Count Then
                                For jloop = 0 To SearchName.Count - 1
                                    dvProdM.RowFilter = "sProdName_F LIKE '%" & SearchName(jloop).Replace("'", "''") & "%' OR sProdName_E LIKE '%" & SearchName(jloop).Replace("'", "''") & "%'"
                                    If dvProdM.Count > 0 And dvProdM.Count < 7 Then
                                        Exit For
                                    End If
                                Next
                            End If
                        End If

                        If (dvProdM.Count = 0 Or dvProdM.Count > 6) And UPC.Length > 9 Then
                            'Still not find, so try middle of UPC
                            dvProdM.RowFilter = "sProdUPC LIKE '%" & UPC.Substring(2, 8) & "%'"
                        End If

                        'If dvProdM.Count > 6 Then
                        '    dvProdM.RowFilter = "sProdName_F = 'azvvazwef'"  'reset dataview too many result
                        'End If

                        If dSkip.ContainsKey("p" & DistData.noProduit) Then
                            dvProdM.RowFilter = "sProdName_F = 'azvvazwef'"  'reset dataview too many result
                        Else
                            dSkip.Add("p" & DistData.noProduit, 0)
                            'Show search result
                            Find_Prod.lblCompanie.Text = CompagnyName
                            Find_Prod.lblDistri.Text = DistributorName
                            Find_Prod.lblLigne.Text = "Ligne: " & iLoop + DistFile.PremiereLigne
                            If UPC.Length > 0 Then
                                Find_Prod.tbCode.Text = UPC
                            Else
                                Find_Prod.tbCode.Text = DistData.noProduit.Replace("'", "''").Trim
                            End If
                            Find_Prod.tbFR.Text = DistData.Produit.Trim
                            Find_Prod.ToolTip1.SetToolTip(Find_Prod.tbFR, DistData.Produit.Trim)
                            Find_Prod.cbSkip.Checked = False
                            Find_Prod.Location = Me.Location
                            SearchPos = 0
                            For jLoop As Integer = 0 To 6
                                Find_Prod.Controls("tbSearchProdUPC" & jLoop).Text = ""
                                Find_Prod.Controls("tbSearchProdFR" & jLoop).Text = ""
                                Find_Prod.ToolTip1.SetToolTip(Find_Prod.Controls("tbSearchProdFR" & jLoop), "")
                                Find_Prod.Controls("tbSearchProdEN" & jLoop).Text = ""
                                Find_Prod.ToolTip1.SetToolTip(Find_Prod.Controls("tbSearchProdEN" & jLoop), "")
                            Next
                            Find_Prod.cb1.Checked = False
                            Find_Prod.cb2.Checked = False
                            Find_Prod.cb3.Checked = False
                            Find_Prod.cb4.Checked = False
                            Find_Prod.cb5.Checked = False
                            Find_Prod.cb6.Checked = False
                            Find_Prod.tbProduit.Text = ""
                            If dvProdM.Count > 0 Then
                                For SearchPos = 0 To dvProdM.Count - 1
                                    Find_Prod.Controls("tbSearchProdUPC" & SearchPos + 1).Text = dvProdM(SearchPos)("sProdUPC")
                                    Find_Prod.Controls("tbSearchProdFR" & SearchPos + 1).Text = dvProdM(SearchPos)("sProdName_F").ToString.Trim & " " & dvProdM(SearchPos)("sProdInfos").ToString.Trim
                                    Find_Prod.ToolTip1.SetToolTip(Find_Prod.Controls("tbSearchProdFR" & SearchPos + 1), dvProdM(SearchPos)("sProdName_F").ToString.Trim & " " & dvProdM(SearchPos)("sProdInfos").ToString.Trim)
                                    Find_Prod.Controls("tbSearchProdEN" & SearchPos + 1).Text = dvProdM(SearchPos)("sProdName_E").ToString.Trim & " " & dvProdM(SearchPos)("sProdInfos").ToString.Trim
                                    Find_Prod.ToolTip1.SetToolTip(Find_Prod.Controls("tbSearchProdEN" & SearchPos + 1), dvProdM(SearchPos)("sProdName_E").ToString.Trim & " " & dvProdM(SearchPos)("sProdInfos").ToString.Trim)
                                    If SearchPos = 5 Then Exit For 'Show first 6 result
                                Next
                                If SearchPos = 1 Then
                                    Find_Prod.tbProduit.Text = Find_Prod.tbSearchProdUPC1.Text
                                    Find_Prod.Search_Prod()
                                End If
                            End If

                            If SearchPos > 1 Then
                                Find_Prod.Width = 750 + (194 * SearchPos)
                            Else
                                Find_Prod.Width = 695
                            End If
                            Find_Prod.cb1.Visible = (SearchPos > 1)
                            If ForceSelection = True Or iDistCode <> 0 Or SearchPos > 1 Then
                                Find_Prod.Show()
                                Find_Prod.Select()
                                Me.Hide()
                                While Find_Prod.Visible = True
                                    Threading.Thread.Sleep(500)
                                    Application.DoEvents()
                                End While
                                'Find_Prod_location = Find_Prod.Location
                                Me.Show()
                                Me.Select()
                                AutoMapping = False
                            End If
                            UPC = Find_Prod.tbProduit.Text
                            If UPC.Length = 0 Then LogTrans(iLoop, "SKIP", 0, -1, db_connection)
                            Application.DoEvents()
                            'If Find_Prod.cbSkip.Checked = True Then
                            '    index = DB_Get_Next_Index("iMapProdCode", "gss_MapProd", db_connection)
                            '    Query = "INSERT INTO gss_MapProd VALUES (" _
                            '        & index & ", " _
                            '        & iDistCode & ", " _
                            '        & "0" & ", " _
                            '        & noProduit & ", " _
                            '        & SFilter(DistData.noProduit.Replace("'", "''")) & ", " _
                            '        & "1" & ", " _
                            '        & SFilter(Date.Now) & ")"
                            '    'DB_Exec_NonQuery(Query, db_connection)
                            '    Exit Sub
                            'End If
                            dvProdM.RowFilter = "sProdUPC = '" & UPC & "'"
                        End If
                    End If

                    If dvProdM.Count > 0 Then
                        'Product Exist, create mapping

                        Dim R = dtMapProd.NewRow
                        R("iProdCode") = dvProdM(0)("iProdCode")
                        R("iNoProduit") = noProduit
                        R("NoProduit") = DistData.noProduit.Replace("'", "''")
                        R("iDoNotProcess") = 0
                        dtMapProd.Rows.Add(R)
                        index = DB_Get_Next_Index("iMapProdCode", "gss_MapProd", db_connection)
                        iProdCode = dvProdM(0)("iProdCode")
                        Query = "INSERT INTO gss_MapProd VALUES (" _
                                & index & ", " _
                                & iDistCode & ", " _
                                & iProdCode & ", " _
                                & noProduit & ", " _
                                & SFilter(DistData.noProduit.Replace("'", "''")) & ", " _
                                & "0" & ", " _
                                & SFilter(Date.Now) & ")"
                        If UpdateGrabb Then
                            Dim db_connection_GRABB = New SqlConnection(GRABB_Connection)
                            db_connection_GRABB.Open()
                            Dim db_connection_CRM = New MySqlConnection(CRM_Connection)
                            db_connection_CRM.Open()
                            DB_Exec_NonQuery(Query, db_connection_GRABB)

                            DB_Exec_NonQuery_MySQL(Query, db_connection_CRM)
                            db_connection_GRABB.Close()
                            db_connection_GRABB.Dispose()
                            db_connection_CRM.Close()
                            db_connection_CRM.Dispose()
                        End If
                        DB_Exec_NonQuery(Query, db_connection)
                        LogTrans(iLoop, "Nouveau mapping produit" & IIf(AutoMapping = True, " (AUTO)", ""), 0, iProdCode, db_connection)
                    End If
                    drMap = dtMapProd.Select("NoProduit = '" & DistData.noProduit.Replace("'", "''").Trim & "'")
                End If
                iCieCode = 0
                If drMap.Count > 0 Then
                    iProdCode = drMap(0)("iProdCode")
                    If drMap(0)("iDoNotProcess") = 1 Then
                        Exit Sub
                    End If
                    dvProdM.RowFilter = "iProdCode = " & iProdCode
                    If dvProdM.Count > 0 Then
                        iCieCode = dvProdM(0)("iCieCode")
                    End If
                    If (DistFile.MultiCie = 1 AndAlso Not DistFile.Cie.Contains(iCieCode)) Or (DistFile.MultiCie = 0 AndAlso iCieCode <> CompagnyID) Then
                        If Not dSkip.ContainsKey("p" & DistData.noProduit) Then
                            dSkip.Add("p" & DistData.noProduit, 0)
                            dvDep.RowFilter = "iCieCode = " & iCieCode
                            Dim CieName As String = ""
                            If dvDep.Count > 0 Then CieName = dvDep(0)("sDepName")
                            MsgBox(DistributorName & vbLf & "n'est pas configuré pour importer les molécules de" & vbLf & CieName & vbLf & dvProdM(0)("sProdUPC") & " " & dvProdM(0)("sProdName_F"))
                            LogTrans(iLoop, DistributorName & "n'est pas configuré pour importer les molécules de " & CieName, 0, iProdCode, db_connection)
                        End If
                        iProdCode = 0
                        iCieCode = CompagnyID
                    End If
                End If

                'Look for Pharmacie
                'Check if we already know this client
                Integer.TryParse(DistData.NoPharma, noClient)
                If noClient > 0 Then
                    drMap = dtMapClient.Select("iNoClient = " & noClient)
                Else
                    If Char.IsLetter(DistData.NoPharma.Substring(0, 1)) Then
                        'Partial check if first char is letter  For K&F
                        drMap = dtMapClient.Select("NoClient LIKE '%" & DistData.NoPharma.Replace("'", "''").Trim & "%'")
                    Else
                        drMap = dtMapClient.Select("NoClient = '" & DistData.NoPharma.Replace("'", "''").Trim & "'")
                    End If
                End If

                PharmaID = ""
                AutoMapping = True
                ForceSelection = False
                If drMap.Count > 0 Then
                    iClientCode = drMap(0)("iClientCode")
                    If drMap(0)("iDoNotProcess") = True Then
                        Exit Sub
                    End If
                    dvClientD.RowFilter = "iClientCode = " & iClientCode
                Else
                    dvClientD.RowFilter = "sClientGuid = 'azvvazwef'"  'reset dataview
                    If iDistCode = 0 Then
                        'JAMP Direct
                        dvClientD.RowFilter = "sPreviousCode = '" & DistData.NoPharma & "'"
                        If dvClientD.Count = 0 Then ForceSelection = True
                    End If
                    If Not dSkip.ContainsKey("c" & DistData.NoPharma) Then
                        dSkip.Add("c" & DistData.NoPharma, 0)

                        If (dvClientD.Count < 1 Or dvClientD.Count > 6) Then
                            'Remove Space
                            If DistData.CodePostal.Length > 0 Then
                                Codepostal = DistData.CodePostal.Replace(" ", "")
                            End If

                            'Put it back
                            If Codepostal.Length = 6 Then
                                Codepostal = Codepostal.Substring(0, 3) & " " & Codepostal.Substring(3, 3)
                            End If

                            'Replace O with 0
                            Codepostal = Codepostal.Replace("O", "0").Replace("'", "''")

                            If DistFile.CodePostalPos > 0 And DistFile.BanniereOverride1.Length Then
                                'PostalCode and banniere override available  Familiprix
                                dvClientD.RowFilter = "(sClientPC = '" & Codepostal & "')"
                                SubQuery = ""
                                If dvClientD.Count > 0 Then
                                    If DistFile.BanniereOverride1.Length Then
                                        If DistFile.banniereOverride2.Length Then
                                            SubQuery = "(sBanName = '" & DistData.Banniere.Replace("'", "''") & "' OR sBanName = '" & DistFile.banniereOverride2.Replace("'", "''") & "')"
                                        Else
                                            SubQuery = "sBanName = '" & DistData.Banniere.Replace(" '", "''") & "'"
                                        End If
                                        dvClientD.RowFilter &= " AND " & SubQuery
                                    End If
                                End If
                            End If
                        End If

                        If (dvClientD.Count < 1 Or dvClientD.Count > 6) And DistFile.BanniereOverride1.Length Then
                            'Nothing available for search, try store number  Jean-Coutu
                            dvClientD.RowFilter = "(sClientName LIKE '%" & DistData.NoPharma.Replace("'", "''") & "%' OR sClientSocialReason LIKE '%" & DistData.NoPharma.Replace("'", "''") & "%')"
                            SubQuery = ""
                            If dvClientD.Count > 0 Then
                                If DistFile.BanniereOverride1.Length Then
                                    If DistFile.banniereOverride2.Length Then
                                        SubQuery = "(sBanName = '" & DistData.Banniere.Replace("'", "''") & "' OR sBanName = '" & DistFile.banniereOverride2.Replace("'", "''") & "')"
                                    Else
                                        SubQuery = "sBanName = '" & DistData.Banniere.Replace(" '", "''") & "'"
                                    End If
                                    dvClientD.RowFilter &= " AND " & SubQuery
                                End If
                            End If
                        End If

                        If (dvClientD.Count < 1 Or dvClientD.Count > 6) And DistFile.CodePostalPos > 0 And DistFile.AdressePos > 0 Then
                            'PostalCode and Address available to search, start with postal code
                            Query = "sClientPC = '" & Codepostal & "'"
                            dvClientD.RowFilter = Query
                            If dvClientD.Count > 1 Then
                                'More than one result, check address too to shrink result
                                AdresseString = Split(DistData.Adresse, " ")(0).Replace(",", "")
                                Int32.TryParse(AdresseString, Adresse)
                                If Adresse > 0 Then
                                    dvClientD.RowFilter = Query & " AND sClientAddress LIKE '" & Adresse & "%'"
                                    If dvClientD.Count = 0 Then
                                        dvClientD.RowFilter = Query
                                    End If
                                End If
                            End If
                        End If

                        If (dvClientD.Count < 1 Or dvClientD.Count > 6) And DistFile.CodePostalPos = 0 And DistFile.PharmaciePos And DistFile.AdressePos Then
                            'No postal code available, need to work with name and adresse
                            DataString = Split(DistData.Pharmacie.Replace(".", " ").Replace("*", " ").Replace("/", " ").Replace("-", " "), " ")
                            SearchName.Clear()
                            For jloop As Integer = 0 To DataString.Length - 1
                                If DataString(jloop).Length > 3 AndAlso Not DataString(jloop).Contains(".") AndAlso DataString(jloop).Substring(0, 2).ToUpper <> "PH" Then
                                    SearchName.Add(DataString(jloop))
                                End If
                            Next
                            DataString = Split(DistData.Adresse, " ")
                            For jloop As Integer = 0 To DataString.Length - 1
                                Int32.TryParse(DataString(jloop).Replace(",", ""), Adresse)
                                If Adresse > 0 Then Exit For
                            Next
                            Query = "sClientAddress LIKE '%" & Adresse & "%'"
                            If SearchName.Count Then
                                For jloop = 0 To SearchName.Count - 1
                                    dvClientD.RowFilter = Query & " AND (sClientName LIKE '%" & SearchName(jloop).Replace("'", "''") & "%' OR sClientSocialReason LIKE '%" & SearchName(jloop).Replace("'", "''") & "%')"
                                    If dvClientD.Count > 0 And dvClientD.Count < 7 Then
                                        Exit For
                                    End If
                                Next
                            End If
                            db_command = New SqlCommand(Query, db_connection)
                            datareader = db_command.ExecuteReader
                            db_command.Dispose()
                        End If

                        If (dvClientD.Count < 1 Or dvClientD.Count > 6) And DistFile.AdressePos > 0 Then
                            'Try with only with adresse
                            DataString = Split(DistData.Adresse, " ")
                            For jloop As Integer = 0 To DataString.Length - 1
                                Int32.TryParse(DataString(jloop).Replace(",", ""), Adresse)
                                If Adresse > 0 Then Exit For
                            Next
                            dvClientD.RowFilter = "sClientAddress = '%" & Adresse & "%'"
                        End If

                        If (dvClientD.Count < 1 Or dvClientD.Count > 6) And DistFile.VillePos > 0 Then
                            'Try with only with city
                            dvClientD.RowFilter = "sClientCity LIKE '%" & DistData.Ville.Replace("'", "''") & "%'"
                        End If

                        If (dvClientD.Count < 1 Or dvClientD.Count > 10) And DistFile.PharmaciePos > 0 Then
                            'Nothing work, last try with only with pharmacie name
                            dvClientD.RowFilter = "sClientGuid = 'azvvazwef'"  'reset dataview
                            DataString = Split(DistData.Pharmacie.Replace(".", " ").Replace("*", " ").Replace("/", " ").Replace("-", " "), " ") 'Help search by replacing space , -, * and / by space
                            SearchName.Clear()
                            For jloop As Integer = 0 To DataString.Length - 1
                                If DataString(jloop).Length > 3 AndAlso Not DataString(jloop).Contains(".") AndAlso DataString(jloop).Substring(0, 2).ToUpper <> "PH" Then
                                    SearchName.Add(DataString(jloop))
                                End If
                            Next
                            If SearchName.Count Then
                                'First try 2 at a time
                                If SearchName.Count > 1 Then
                                    For jloop = 0 To SearchName.Count - 2
                                        dvClientD.RowFilter = "(sClientName LIKE '%" & SearchName(jloop).Replace("'", "''") & "%' AND sClientName LIKE '%" & SearchName(jloop + 1).Replace("'", "''") & "%') OR (sClientSocialReason LIKE '%" & SearchName(jloop).Replace("'", "''") & "%' AND sClientSocialReason LIKE '%" & SearchName(jloop + 1).Replace("'", "''") & "%')"
                                        If dvClientD.Count > 0 And dvClientD.Count < 7 Then
                                            Exit For
                                        End If
                                    Next
                                End If
                                If dvClientD.Count = 0 Then
                                    'Try one word at a time
                                    For jloop = 0 To SearchName.Count - 1
                                        dvClientD.RowFilter = "sClientName LIKE '%" & SearchName(jloop).Replace("'", "''") & "%' OR sClientSocialReason LIKE '%" & SearchName(jloop).Replace("'", "''") & "%'"
                                        If dvClientD.Count > 0 And dvClientD.Count < 7 Then
                                            Exit For
                                        End If
                                    Next
                                End If

                            End If
                        End If

                        'If dvClientD.Count > 6 Then
                        '    'Query an illegal PharmaID to reset previous result
                        '    dvClientD.RowFilter = "sClientGuid = 'azvvazwef'"  'reset dataview
                        'End If

                        If Find_Pharm_location.X <> 0 Then
                            Find_Pharm.Location = Find_Pharm_location
                        End If
                        'Show search result
                        Find_Pharm.lblCompanie.Text = CompagnyName
                        Find_Pharm.lblDistri.Text = DistributorName
                        Find_Pharm.lblLigne.Text = "Ligne: " & iLoop + DistFile.PremiereLigne
                        Find_Pharm.tbCode.Text = DistData.NoPharma.Trim
                        Find_Pharm.tbPharm.Text = DistData.Pharmacie.Trim
                        Find_Pharm.ToolTip1.SetToolTip(Find_Pharm.tbPharm, DistData.Pharmacie.Trim)
                        Find_Pharm.tbBan.Text = DistData.Banniere.Trim
                        Find_Pharm.tbAdress.Text = DistData.Adresse.Trim
                        Find_Pharm.ToolTip1.SetToolTip(Find_Pharm.tbAdress, DistData.Adresse.Trim)
                        Find_Pharm.tbVille.Text = DistData.Ville.Trim
                        Find_Pharm.tbCodePostal.Text = DistData.CodePostal
                        Find_Pharm.cbSkip.Checked = False
                        Find_Pharm.Location = Me.Location
                        SearchPos = 0
                        For jLoop As Integer = 0 To 6
                            Find_Pharm.Controls("tbSearchPharmaID" & jLoop).Text = ""
                            Find_Pharm.Controls("tbSearchPharm" & jLoop).Text = ""
                            Find_Pharm.ToolTip1.SetToolTip(Find_Pharm.Controls("tbsearchpharm" & jLoop), "")
                            Find_Pharm.Controls("tbSearchPharmS" & jLoop).Text = ""
                            Find_Pharm.ToolTip1.SetToolTip(Find_Pharm.Controls("tbsearchpharmS" & jLoop), "")
                            Find_Pharm.Controls("tbSearchBan" & jLoop).Text = ""
                            Find_Pharm.Controls("tbSearchAdress" & jLoop).Text = ""
                            Find_Pharm.ToolTip1.SetToolTip(Find_Pharm.Controls("tbsearchadress" & jLoop), "")
                            Find_Pharm.Controls("tbSearchVille" & jLoop).Text = ""
                            Find_Pharm.Controls("tbSearchCodePostal" & jLoop).Text = ""
                        Next
                        Find_Pharm.cb1.Checked = False
                        Find_Pharm.cb2.Checked = False
                        Find_Pharm.cb3.Checked = False
                        Find_Pharm.cb4.Checked = False
                        Find_Pharm.cb5.Checked = False
                        Find_Pharm.cb6.Checked = False
                        Find_Pharm.tbPharmaID.Text = ""
                        If dvClientD.Count > 0 Then
                            Dim sBanName As String = ""
                            For SearchPos = 0 To dvClientD.Count - 1
                                dvClientC.RowFilter = "iClientCode = '" & dvClientD(SearchPos)("iClientCode") & "' AND iCieCode = " & iCieCode
                                If dvClientC.Count > 0 Then
                                    sBanName = dvClientC(0)("sBanName").ToString.Trim
                                Else
                                    sBanName = ""
                                End If
                                Find_Pharm.Controls("tbSearchPharmaID" & SearchPos + 1).Text = dvClientD(SearchPos)("sClientGuid")
                                Find_Pharm.Controls("tbSearchPharm" & SearchPos + 1).Text = dvClientD(SearchPos)("sClientName").ToString.Trim
                                Find_Pharm.ToolTip1.SetToolTip(Find_Pharm.Controls("tbsearchpharm" & SearchPos + 1), dvClientD(SearchPos)("sClientName").ToString.Trim)
                                Find_Pharm.Controls("tbSearchPharmS" & SearchPos + 1).Text = dvClientD(SearchPos)("sClientSocialReason").ToString.Trim
                                Find_Pharm.ToolTip1.SetToolTip(Find_Pharm.Controls("tbsearchpharmS" & SearchPos + 1), dvClientD(SearchPos)("sClientSocialReason").ToString.Trim)
                                Find_Pharm.Controls("tbSearchBan" & SearchPos + 1).Text = sBanName
                                Find_Pharm.Controls("tbSearchAdress" & SearchPos + 1).Text = dvClientD(SearchPos)("sClientAddress").ToString.Trim
                                Find_Pharm.ToolTip1.SetToolTip(Find_Pharm.Controls("tbsearchadress" & SearchPos + 1), dvClientD(SearchPos)("sClientAddress").ToString.Trim)
                                Find_Pharm.Controls("tbSearchVille" & SearchPos + 1).Text = dvClientD(SearchPos)("sClientCity").ToString.Trim
                                Find_Pharm.Controls("tbSearchCodePostal" & SearchPos + 1).Text = dvClientD(SearchPos)("sClientPC").ToString.Trim
                                If SearchPos = 5 Then Exit For 'Show first 6 result
                            Next
                            If SearchPos = 1 Then
                                Find_Pharm.tbPharmaID.Text = Find_Pharm.tbSearchPharmaID1.Text
                                Find_Pharm.Search_Pharm()
                            End If
                        End If

                        If SearchPos > 1 Then
                            Find_Pharm.Width = 750 + (194 * SearchPos)
                        Else
                            Find_Pharm.Width = 695
                        End If
                        Find_Pharm.cb1.Visible = (SearchPos > 1)
                        If ForceSelection = True Or iDistCode <> 0 Or SearchPos > 1 Then
                            Find_Pharm.Show()
                            Find_Pharm.Select()
                            Me.Hide()
                            While Find_Pharm.Visible = True
                                Threading.Thread.Sleep(500)
                                Application.DoEvents()
                            End While
                            Me.Show()
                            Me.Select()
                            AutoMapping = False
                        End If
                        Find_Pharm_location = Find_Pharm.Location
                        PharmaID = Find_Pharm.tbPharmaID.Text
                        If PharmaID.Length = 0 Then LogTrans(iLoop, "SKIP", -1, 0, db_connection)
                        Application.DoEvents()
                        'If Find_Pharm.cbSkip.Checked = True Then
                        '    index = DB_Get_Next_Index("iMapClientCode", "gss_MapClient", db_connection)
                        '    Query = "INSERT INTO gss_MapClient VALUES (" _
                        '        & index & ", " _
                        '        & iDistCode & ", " _
                        '        & "0" & ", " _
                        '        & noClient & ", " _
                        '        & SFilter(DistData.NoPharma) & ", " _
                        '        & "1" & ", " _
                        '        & SFilter(Date.Now) & ")"
                        '    'DB_Exec_NonQuery(Query, db_connection)
                        '    Exit Sub
                        'End If

                        dvClientD.RowFilter = "sClientGuid = '" & PharmaID & "'"
                        If dvClientD.Count > 0 Then
                            iClientCode = dvClientD(0)("iClientCode")

                            Dim iNoClient As Integer
                            Integer.TryParse(noClient, iNoClient)

                            Dim R = dtMapClient.NewRow
                            R("iClientCode") = iClientCode
                            R("iNoClient") = iNoClient
                            R("NoClient") = DistData.NoPharma.Replace("'", "''")
                            R("iDoNotProcess") = 0
                            dtMapClient.Rows.Add(R)

                            index = DB_Get_Next_Index("iMapClientCode", "gss_MapClient", db_connection)
                            Query = "INSERT INTO gss_MapClient VALUES (" _
                                    & index & ", " _
                                    & iDistCode & ", " _
                                    & iClientCode & ", " _
                                    & iNoClient & ", " _
                                    & SFilter(noClient) & ", " _
                                    & 0 & ", " _
                                    & SFilter(Date.Now) & ")"

                            If UpdateGrabb Then
                                Dim db_connection_GRABB = New SqlConnection(GRABB_Connection)
                                db_connection_GRABB.Open()
                                Dim db_connection_CRM = New MySqlConnection(CRM_Connection)
                                db_connection_CRM.Open()

                                DB_Exec_NonQuery(Query, db_connection_GRABB)
                                DB_Exec_NonQuery_MySQL(Query, db_connection_CRM)

                                db_connection_GRABB.Close()
                                db_connection_GRABB.Dispose()
                                db_connection_CRM.Close()
                                db_connection_CRM.Dispose()
                            End If
                            DB_Exec_NonQuery(Query, db_connection)

                            LogTrans(iLoop, "Nouveau mapping client" & IIf(AutoMapping = True, " (AUTO)", ""), iClientCode, 0, db_connection)
                        End If
                    End If
                End If

                Dim Pass As Integer = 0
                If iClientCode = 0 Or iProdCode = 0 Then
                    SkipPharm += 1
                    Pass = 1
                Else
                    iProvCode = dvClientD(0)("iProvCode")
                    sClientCode = dvClientD(0)("sClientGuid")

                    dvClientC.RowFilter = "iCieCode = '" & iCieCode & "' AND iClientCode = " & dvClientD(0)("iClientCode")
                    If dvClientC.Count > 0 Then
                        iTerrCode = dvClientC(0)("iTerrCode")
                        iBanCode = dvClientC(0)("iBanCode")
                        iGrpCode = dvClientC(0)("iGrpCode")
                        iChnlCode = -(iDistCode = 0)
                        dvTerr.RowFilter = "iTerrCode = " & iTerrCode
                        If dvTerr.Count > 0 Then
                            iRepCode = dvTerr(0)("iRepCode")
                            iCoorCode = dvTerr(0)("iCoorCode")
                            iGestCode = dvTerr(0)("iGestCode")
                        Else
                            Dim debug = True
                        End If
                    Else
                        If Not dSkip.ContainsKey("c" & DistData.NoPharma) Then
                            dSkip.Add("c" & DistData.NoPharma, 0)
                            dvDep.RowFilter = "iCieCode = " & iCieCode
                            Dim CieName As String = ""
                            If dvDep.Count > 0 Then CieName = dvDep(0)("sDepName")
                            Dim ProdName As String = ""
                            If dvProdM.Count > 0 Then ProdName = dvProdM(0)("sProdUPC") & " - " & dvProdM(0)("sProdName_F")
                            MsgBox(dvClientD(0)("sClientGUID") & " - " & dvClientD(0)("sClientName") & vbLf & "n'est pas assigné à un territoire pour " & vbLf & CieName & vbLf & ProdName)
                            LogTrans(iLoop, dvClientD(0)("sClientGUID") & " - " & dvClientD(0)("sClientName") & " n'est pas assigné à un territoire pour " & " " & CieName, iClientCode, 0, db_connection)
                        End If
                        Pass = 1
                    End If

                    dvProdM.RowFilter = "iProdCode = " & iProdCode
                    If dvProdM.Count > 0 Then
                        iUnivCode = dvProdM(0)("iUnivCode")
                    End If

                    dvProdP.RowFilter = "iProdCode = " & iProdCode & " AND iProdProv = " & iProvCode
                    If dvProdP.Count > 0 Then
                        Prix = dvProdP(0)("fProdSalesPrice")
                    End If
                    Montant = Prix * DistData.Qte
                End If

                If DistData.TransDate.Length < 9 Then DistData.TransDate = DateActive
                Query = "INSERT INTO gss_Trans_Temp VALUES (" _
                       & iCieCode & ", " _
                       & iClientCode & ", " _
                       & iTerrCode & ", " _
                       & iProvCode & ", " _
                       & iRepCode & ", " _
                       & iCoorCode & ", " _
                       & iGestCode & ", " _
                       & iBanCode & ", " _
                       & iGrpCode & ", " _
                       & iDistCode & ", " _
                       & iChnlCode & ", " _
                       & iProdCode & ", " _
                       & iUnivCode & ", " _
                       & DistData.Qte & ", " _
                       & Montant & ", " _
                       & SFilter(DistData.TransDate) & ", " _
                       & SFilter(DateActive) & ", " _
                       & Pass & ")"
                DB_Exec_NonQuery(Query, db_connection)
            End If
            If DistData.Qte <> 0 Then
                txtRecordInfo.Text = "(" & sClientCode & ") Fact:  " & DistData.NoFacture & " Ligne: " & iLoop & " Quantitée:  " & DistData.Qte & " Montant:  " & DistData.Montant.ToString("$0.00")
            End If
        End If
    End Sub

    Sub LogTrans(iloop As Integer, Message As String, iClientCode As Integer, iProdCode As Integer, db_connection As SqlConnection)
        Dim Query As String
        Dim noClient As String = ""
        Dim noProduit As String = ""
        Dim Client As String = ""
        Dim Adresse As String = ""
        Dim Ville As String = ""
        Dim CodePostal As String = ""
        Dim Produit As String = ""
        Dim UPC As String = ""

        If iClientCode <> 0 Then
            noClient = DistData.NoPharma
            Client = DistData.Pharmacie
            Adresse = DistData.Adresse
            Ville = DistData.Ville
            CodePostal = DistData.CodePostal
            If iClientCode = -1 Then iClientCode = 0
        Else
            If DistFile.UPCClientPos <> 0 Then
                UPC = (CDec(DistData.UPCClient) * 100000 + CDec(DistData.UPCProduit)).ToString
            Else
                UPC = DistData.UPCProduit
            End If
            Produit = DistData.Produit
            noProduit = DistData.noProduit
            If iProdCode = -1 Then iProdCode = 0
        End If

        Query = "INSERT INTO gss_Trans_Mapping (iSessionCode, iCieCode, iDistCode, Fichier, Ligne, sMessage, noClient, noProduit, Client, Adresse, Ville, CodePostal, Produit, UPC, iClientCode, iProdCode, Date) VALUES (" _
                    & iSessionCode & ", " _
                    & CompagnyID & ", " _
                    & iDistCode & ", " _
                    & SFilter(ImportFile) & ", " _
                    & iloop & ", " _
                    & SFilter(Message) & ", " _
                    & SFilter(noClient) & ", " _
                    & SFilter(noProduit) & ", " _
                    & SFilter(Client) & ", " _
                    & SFilter(Adresse) & ", " _
                    & SFilter(Ville) & ", " _
                    & SFilter(CodePostal) & ", " _
                    & SFilter(Produit) & ", " _
                    & SFilter(UPC) & ", " _
                    & iClientCode & ", " _
                    & iProdCode & ", " _
                    & SFilter(Date.Now) & ")"
        DB_Exec_NonQuery(Query, db_connection)

    End Sub
    Function Validate_Data(DateValidate As Boolean) As Boolean
        Dim RecordInfo As String = ""

        If DistData.NoPharma.Length Then RecordInfo &= (vbLf & "Numéro de pharmacie : " & DistData.NoPharma)
        If DistData.Pharmacie.Length Then RecordInfo &= (vbLf & "Pharmacie : " & DistData.Pharmacie)
        If DistData.Banniere.Length Then RecordInfo &= (vbLf & "Bannière : " & DistData.Banniere)
        If DistData.Adresse.Length Then RecordInfo &= (vbLf & "Adresse : " & DistData.Adresse)
        If DistData.Ville.Length Then RecordInfo &= (vbLf & "Ville : " & DistData.Ville)
        If DistData.CodePostal.Length Then RecordInfo &= (vbLf & "Code postal : " & DistData.CodePostal)
        If DistData.noProduit.Length Then RecordInfo &= (vbLf & "Numéro de produit : " & DistData.noProduit)
        If DistFile.UPCProduitPos > 0 Then
            If DistFile.UPCClientPos > 0 Then
                RecordInfo &= (vbLf & "UPC : " & (CDec(DistData.UPCClient) * 100000 + CDec(DistData.UPCProduit)).ToString)
            Else
                RecordInfo &= (vbLf & "UPC : " & DistData.UPCProduit)
            End If
        End If
        If DistData.Produit.Length Then RecordInfo &= (vbLf & "Produit : " & DistData.Produit)
        If DistData.Qte Then RecordInfo &= (vbLf & "Quantitée : " & DistData.Qte.ToString)
        If DistData.Montant Then RecordInfo &= (vbLf & "Montant : " & DistData.Montant.ToString("$0.00"))
        If DistData.NoFacture.Length Then RecordInfo &= (vbLf & "Numéro de facture : " & DistData.NoFacture)
        RecordInfo &= vbLf & vbLf
        If DateValidate Then
            RecordInfo &= "Date validée"
        Else
            RecordInfo &= "**** Date invalide ****"
        End If
        RecordInfo &= (vbLf & vbLf & "Continuer l'importation?")
        Dim reply As DialogResult = MessageBox.Show(RecordInfo, DistributorName,
                       MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
        Validate_Data = (reply = DialogResult.Yes)
    End Function

#End Region

#Region "Transfert"

#Region "CRM"

    Private Sub BtnUpdate_Click(sender As Object, e As EventArgs)
        Dim db_connection As SqlConnection
        db_connection = New SqlConnection(GSS_Connection) 'Connect to local DB
        db_connection.Open()
        hide_all()
        btnStatus.Visible = True
        'Load_Banniere_CRM(db_connection)
        'Load_Groupe_CRM(db_connection)
        'Load_Utilisateur_CRM(db_connection)
        'Load_Produit_CRM(db_connection)
        'Load_Pharmacie_CRM(db_connection)
        'Update_ERP(db_connection)
        btnStatus.Visible = False
        show_command()
        db_connection.Close()
        db_connection.Dispose()
    End Sub

    Private Sub btnUpdateCRM_Click(sender As Object, e As EventArgs)
        hide_all()
        btnStatus.Visible = True
        'Upload_Cumul_CRM_Bio()
        btnStatus.Visible = False
        show_command()
    End Sub


#End Region

#End Region


End Class