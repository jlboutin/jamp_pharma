﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Net
Imports MySql.Data.MySqlClient

Module CRM

    Sub Load_Banniere_CRM(db_connection As SqlConnection)
        'Dim db_connectionCRM As New MySqlConnection(My.Settings.BiomedCRMConnector) 'Connect to CRM
        'db_connectionCRM.Open()

        'Dim Result As New DataTable

        'Using cmd As New MySqlCommand("SELECT BanniereDesc, BanniereID FROM Banniere", db_connectionCRM)
        '    Using sda As New MySqlDataAdapter(cmd)
        '        sda.Fill(Result)
        '    End Using
        'End Using

        'JampCruncher.btnStatus.Text = "Synchronisation du CRM" & vbLf & "Bannières"
        'Application.DoEvents()

        'DB_Exec_NonQuery("TRUNCATE TABLE Banniere", db_connection) 'Erase previous data
        'For Each ResultList In Result.Rows
        '    DB_Exec_NonQuery("INSERT INTO Banniere (CompagnieID, BanniereDesc, BanniereID) Values ('" _
        '                    & CompagnyID & "', '" _
        '                    & ResultList("BanniereDesc").replace("'", "''") & "', '" _
        '                    & ResultList("BanniereID") & "');" _
        '                    , db_connection)
        'Next

        'db_connectionCRM.Close()
        'db_connectionCRM.Dispose()
    End Sub

    Sub Load_Groupe_CRM(db_connection As SqlConnection)
        'Dim db_connectionCRM As New MySqlConnection(My.Settings.BiomedCRMConnector) 'Connect to CRM
        'db_connectionCRM.Open()

        'Dim Result As New DataTable

        'Using cmd As New MySqlCommand("SELECT GroupeDesc, GroupeID FROM Groupe", db_connectionCRM)
        '    Using sda As New MySqlDataAdapter(cmd)
        '        sda.Fill(Result)
        '    End Using
        'End Using

        'JampCruncher.btnStatus.Text = "Synchronisation du CRM" & vbLf & "Groupes"
        'Application.DoEvents()

        'DB_Exec_NonQuery("TRUNCATE TABLE Groupe", db_connection) 'Erase previous data
        'For Each ResultList In Result.Rows
        '    DB_Exec_NonQuery("INSERT INTO Groupe (CompagnieID, GroupeNom, GroupeID) Values ('" _
        '                    & CompagnyID & "', '" _
        '                    & ResultList("GroupeDesc").replace("'", "''") & "', '" _
        '                    & ResultList("GroupeID") & "');" _
        '                    , db_connection)
        'Next

        'db_connectionCRM.Close()
        'db_connectionCRM.Dispose()

    End Sub

    Sub Load_Utilisateur_CRM(db_connection As SqlConnection)

        'Dim db_connectionCRM As New MySqlConnection(My.Settings.BiomedCRMConnector) 'Connect to CRM
        'db_connectionCRM.Open()

        'Dim Result As New DataTable

        'Using cmd As New MySqlCommand("SELECT name, RepID, RepIDERP FROM sec_users", db_connectionCRM)
        '    Using sda As New MySqlDataAdapter(cmd)
        '        sda.Fill(Result)
        '    End Using
        'End Using

        'JampCruncher.btnStatus.Text = "Synchronisation du CRM" & vbLf & "Représentants"
        'Application.DoEvents()

        'DB_Exec_NonQuery("TRUNCATE TABLE Representant", db_connection) 'Erase previous data
        'For Each ResultList In Result.Rows
        '    DB_Exec_NonQuery("INSERT INTO Representant (CompagnieID, Rep_Nom, RepID, RepIDERP) Values ('" _
        '                & CompagnyID & "', '" _
        '                & ResultList("name").replace("'", "''") & "', " _
        '                & ResultList("RepID") & ", " _
        '                & ResultList("RepIDERP") & ");" _
        '                , db_connection)
        'Next

        'db_connectionCRM.Close()
        'db_connectionCRM.Dispose()
    End Sub

    Sub Load_Pharmacie_CRM(db_connection As SqlConnection)
        'Dim LoopCount As Integer = 0
        'Dim Read_name As String
        'Dim Read_billing_address_street As String
        'Dim Read_billing_address_city As String
        'Dim Read_billing_address_state As String
        'Dim Read_billing_address_postalcode As String
        'Dim Read_assigned_user_id As String
        'Dim Read_next_user_id As String
        'Dim Read_transfer_id_bannieres_c As String
        'Dim Read_transfer_id_groupes_c As String
        'Dim Read_no_client_c As String
        'Dim Read_phone_office As String
        'Dim Read_phone_fax As String
        'Dim Read_email As String
        'Dim Read_changt_nom_adresse As String
        'Dim Read_changt_banniere As String
        'Dim Read_changt_groupe As String
        'Dim Read_changt_rep As String
        'Dim Read_rabais As Integer
        'Dim LastUpdate As Date
        'Dim sLastUpdate As String
        'Dim nbRecord As Integer
        'Dim nbRecordLeft As Integer
        'Dim ClientArray As New ArrayList()
        'Dim CL As Object
        'Dim FoundPharm As Boolean
        'Dim PharmaID As String
        'Dim PharmacieID As Integer
        'Dim CurMonth As Integer
        'Dim dtUser As New DataTable
        'Dim AncienRepID As Integer
        'Dim NeedUpdate As Boolean

        ''Get Data From CRM
        'LastUpdate = CDate(DB_Exec_Scalar("Select LastUpdatePharm FROM setup WHERE CompagnieID =" & CompagnyID, db_connection))
        'sLastUpdate = LastUpdate.ToString("yyyy-MM-dd ") & (LastUpdate.Hour + 100).ToString.Substring(1, 2) & LastUpdate.ToString(":mm:ss")

        'Dim db_connectionCRM As New MySqlConnection(My.Settings.BiomedCRMConnector) 'Connect to CRM
        'db_connectionCRM.Open()
        'Dim Result As New DataTable
        'Using cmd As New MySqlCommand("SELECT PharmaID, Raison_sociale, Adresse, Ville, Province, Code_postal, Rabais, RepID, BanniereID, " _
        '                            & "GroupeID, NextRepID, PharmacieID, ModifiedDate, NoClient, Telephone, Fax, Email, DateChangNom, DateChangBan, " _
        '                            & "DateChangGrp, DateChangRep FROM Pharmacie WHERE ModifiedDate >= '" & sLastUpdate & "' ORDER BY PharmaID", db_connectionCRM)
        '    Using sda As New MySqlDataAdapter(cmd)
        '        sda.Fill(Result)
        '    End Using
        'End Using

        'JampCruncher.btnStatus.Text = "Synchronisation du CRM" & vbLf & "Pharmacie(s)"
        'Application.DoEvents()

        ''Load working data
        'Load_Array("SELECT * FROM Pharmacie ORDER BY PharmaID", db_connection, ClientArray)

        ''Create dictionnary key to access Pharmacie and Rep
        'Dim dtPharmIndex As New DataTable
        'Using cmd As New SqlCommand("SELECT PharmaID, CONVERT(INT, ROW_NUMBER() OVER (ORDER BY PharmaID)) as line, PharmacieID FROM Pharmacie", db_connection)
        '    Using da As New SqlDataAdapter(cmd)
        '        da.Fill(dtPharmIndex)
        '    End Using
        'End Using

        'Dim iFs As Func(Of String, Func(Of DataRow, String)) = Function(s) Function(dr) dr.Field(Of Integer)(s)
        'Dim sFs As Func(Of String, Func(Of DataRow, String)) = Function(s) Function(dr) dr.Field(Of String)(s)
        'Dim dicPharmaID = dtPharmIndex.Rows.OfType(Of DataRow)().ToDictionary(sFs("PharmaID"), iFs("line"))

        'nbRecord = Result.Rows.Count
        'nbRecordLeft = nbRecord

        'For Each ResultList In Result.Rows
        '    nbRecordLeft -= 1

        '    'Get data from Row
        '    PharmaID = ResultList("PharmaID")

        '    'If PharmaID = "H1K2237" Then
        '    '    nbRecord = nbRecord
        '    'End If


        '    JampCruncher.btnStatus.Text = "Synchronisation du CRM (" & PharmaID & ")" & vbLf & "Pharmacies " & nbRecordLeft
        '    Application.DoEvents()
        '    Read_name = WebUtility.HtmlDecode(ResultList("Raison_sociale").Trim)
        '    Read_billing_address_street = WebUtility.HtmlDecode(ResultList("Adresse").Trim)
        '    Read_billing_address_city = WebUtility.HtmlDecode(ResultList("Ville").Trim)
        '    Read_billing_address_state = WebUtility.HtmlDecode(ResultList("Province").Trim)
        '    Read_billing_address_postalcode = WebUtility.HtmlDecode(ResultList("Code_postal").Trim)
        '    Read_assigned_user_id = ResultList("RepID")
        '    Read_next_user_id = ResultList("NextRepID")
        '    If Read_next_user_id = 0 Then Read_next_user_id = Read_assigned_user_id
        '    Read_transfer_id_bannieres_c = ResultList("BanniereID")
        '    Read_transfer_id_groupes_c = ResultList("GroupeID")
        '    PharmacieID = ResultList("PharmacieID")
        '    Read_no_client_c = ResultList("NoClient")
        '    Read_phone_office = ResultList("Telephone")
        '    Read_phone_fax = ResultList("Fax")
        '    Read_email = ResultList("Email")
        '    Read_rabais = ResultList("Rabais")
        '    If IsNothing(ResultList("DateChangNom")) OrElse ResultList("DateChangNom") < "2010-01-01" Then
        '        Read_changt_nom_adresse = "2017-01-01"
        '    Else
        '        Read_changt_nom_adresse = ResultList("DateChangNom")
        '    End If
        '    If IsNothing(ResultList("DateChangBan")) OrElse ResultList("DateChangBan") < "2010-01-01" Then
        '        Read_changt_banniere = "2017-01-01"
        '    Else
        '        Read_changt_banniere = ResultList("DateChangBan")
        '    End If
        '    If IsNothing(ResultList("DateChangGrp")) OrElse ResultList("DateChangGrp") < "2010-01-01" Then
        '        Read_changt_groupe = "2017-01-01"
        '    Else
        '        Read_changt_groupe = ResultList("DateChangGrp")
        '    End If
        '    If IsNothing(ResultList("DateChangRep")) OrElse ResultList("DateChangRep") < "2010-01-01" Then
        '        Read_changt_rep = "2017-01-01"
        '    Else
        '        Read_changt_rep = ResultList("DateChangRep")
        '    End If

        '    'Check if PharmaID is right Format
        '    If PharmaID.Length > 6 AndAlso Not IsNumeric(PharmaID.Substring(0, 1)) _
        '                           AndAlso IsNumeric(PharmaID.Substring(1, 1)) _
        '                           AndAlso Not IsNumeric(PharmaID.Substring(2, 1)) _
        '                           AndAlso IsNumeric(PharmaID.Substring(3, 4)) Then
        '        FoundPharm = False
        '        NeedUpdate = False
        '        If dicPharmaID.ContainsKey(PharmaID) Then
        '            'Load current Info
        '            CL = ClientArray(dicPharmaID(PharmaID) - 1)
        '            If CL("PharmacieID") = PharmacieID Then
        '                FoundPharm = True

        '                'If New data not yet updated
        '                If CL("New_Raison_sociale") = "" Then
        '                    CL("New_Raison_sociale") = CL("Raison_sociale")
        '                    CL("New_Adresse") = CL("Adresse")
        '                    CL("New_Ville") = CL("Ville")
        '                    CL("New_Province") = CL("Province")
        '                    CL("New_Code_postal") = CL("Code_postal")
        '                    CL("New_Telephone") = CL("Telephone")
        '                    CL("New_Fax") = CL("Fax")
        '                    CL("New_Courriel") = CL("Courriel")
        '                    CL("New_BanniereID") = CL("BanniereID")
        '                    CL("New_GroupeID") = CL("GroupeID")
        '                    CL("New_RepID") = CL("RepID")

        '                    'Find previous Rep
        '                    AncienRepID = 0
        '                    Using cmd2 As New SqlCommand("SELECT * FROM RepBanGrp_Mapping WHERE PharmacieID = '" & PharmacieID & "'", db_connection)
        '                        Using da2 As New SqlDataAdapter(cmd2)
        '                            Dim dtRepMonth As New DataTable
        '                            da2.Fill(dtRepMonth)
        '                            If dtRepMonth.Rows.Count > 0 Then
        '                                For xMonth As Integer = 1 To 11
        '                                    CurMonth = MonthToUpdate - xMonth
        '                                    If CurMonth < 1 Then CurMonth += 12
        '                                    If dtRepMonth.Rows(0)("RepID_Mois_" & CurMonth).ToString <> CL("New_RepID") Then
        '                                        AncienRepID = dtRepMonth.Rows(0)("RepID_Mois_" & CurMonth).ToString
        '                                        Exit For
        '                                    End If
        '                                Next
        '                            End If
        '                        End Using
        '                    End Using

        '                    'Update DB
        '                    DB_Exec_NonQuery("UPDATE Pharmacie SET " _
        '                                & "New_BanniereID = '" & CL("BanniereID") & "', " _
        '                                & "New_GroupeID = '" & CL("GroupeID") & "', " _
        '                                & "New_RepID = '" & CL("RepID") & "', " _
        '                                & "AncienRepID = '" & AncienRepID & "'" _
        '                                & " WHERE PharmacieID = '" & PharmacieID & "'" _
        '                                , db_connection)
        '                    'Save to CRM
        '                    NeedUpdate = True
        '                Else
        '                    'Check if update is needed
        '                    If CL("New_Raison_sociale") <> Read_name OrElse
        '                                CL("PharmaID") <> PharmaID OrElse
        '                                CL("New_Adresse") <> Read_billing_address_street OrElse
        '                                CL("New_Ville") <> Read_billing_address_city OrElse
        '                                CL("New_Province") <> Read_billing_address_state OrElse
        '                                CL("New_Code_postal") <> Read_billing_address_postalcode OrElse
        '                                CL("NoClient") <> Read_no_client_c OrElse
        '                                CL("New_Telephone") <> Read_phone_office OrElse
        '                                CL("New_Fax") <> Read_phone_fax OrElse
        '                                CL("New_Courriel") <> Read_email OrElse
        '                                CL("Date_Change_Adresse") <> Read_changt_nom_adresse OrElse
        '                                CL("Date_Change_Ban") <> Read_changt_banniere OrElse
        '                                CL("Date_Change_Groupe") <> Read_changt_groupe OrElse
        '                                CL("Date_Change_Rep") <> Read_changt_rep OrElse
        '                                CL("New_BanniereID") <> Read_transfer_id_bannieres_c OrElse
        '                                CL("New_GroupeID") <> Read_transfer_id_groupes_c OrElse
        '                                CL("New_RepID") <> Read_next_user_id Then
        '                        NeedUpdate = True
        '                    End If
        '                End If

        '                If NeedUpdate = True Then
        '                    'Update Pharmastat
        '                    DB_Exec_NonQuery("UPDATE Pharmacie SET " _
        '                            & "PharmaID = '" & PharmaID & "', " _
        '                            & "NoClient = '" & Read_no_client_c & "', " _
        '                            & "New_Raison_sociale = '" & Read_name.Trim.Replace("'", "''") & "', " _
        '                            & "New_Adresse = '" & Read_billing_address_street.Trim.Replace("'", "''") & "', " _
        '                            & "New_Ville = '" & Read_billing_address_city.Trim.Replace("'", "''") & "', " _
        '                            & "New_Province = '" & Read_billing_address_state.Trim & "', " _
        '                            & "New_Code_postal = '" & Read_billing_address_postalcode.Trim & "', " _
        '                            & "New_Telephone = '" & Read_phone_office.Trim & "', " _
        '                            & "New_Fax = '" & Read_phone_fax.Trim & "', " _
        '                            & "New_Courriel = '" & Read_email.Trim & "', " _
        '                            & "Date_Change_Adresse = '" & Read_changt_nom_adresse & "', " _
        '                            & "Date_Change_Ban = '" & Read_changt_banniere & "', " _
        '                            & "Date_Change_Groupe = '" & Read_changt_groupe & "', " _
        '                            & "Date_Change_Rep = '" & Read_changt_rep & "', " _
        '                            & "New_BanniereID = '" & Read_transfer_id_bannieres_c & "', " _
        '                            & "New_GroupeID = '" & Read_transfer_id_groupes_c & "', " _
        '                            & "New_RepID = '" & Read_next_user_id & "', " _
        '                            & "Rabais = '" & Read_rabais & "' " _
        '                            & "WHERE PharmacieID = '" & PharmacieID & "'", db_connection)
        '                End If
        '            Else
        '                MsgBox("Erreur: Problème de doublon" & vbLf & "( " & PharmaID & " - " & Read_name & " )")
        '                UpdateTimeStamp = False
        '                PharmaID = ""
        '            End If
        '        End If

        '        'Add new pharm
        '        If Not FoundPharm Then 'New Pharm
        '            If Count_Row(db_connection, "Select * FROM Pharmacie WHERE PharmaID = '" & PharmaID & "'") > 0 Then
        '                MsgBox("Erreur: PharmaID déjà existant ( " & PharmaID & " )")
        '                UpdateTimeStamp = False
        '            Else
        '                If Count_Row(db_connection, "Select * FROM Pharmacie WHERE NoClient = '" & Read_no_client_c.Trim.Replace("'", "''") & "'") > 0 Then
        '                    MsgBox("Erreur: NoClient déjà existant ( " & Read_no_client_c & " - " & PharmaID & " )")
        '                    UpdateTimeStamp = False
        '                Else
        '                    DB_Exec_NonQuery("INSERT INTO Pharmacie (CompagnieID, PharmaID, PharmacieID, NoCLient, New_Raison_sociale, New_Adresse, " _
        '                                       & "New_Ville, New_Province, New_Code_postal, New_Telephone, New_Fax, New_Courriel, Rabais, " _
        '                                       & "Raison_sociale, Adresse, Ville, Province, Code_postal, Telephone, Fax, Courriel, " _
        '                                       & "Date_Change_Adresse, Date_Change_Ban, Date_Change_Groupe, Date_Change_Rep, " _
        '                                       & "New_BanniereID, New_GroupeID, New_RepID, Update_ERP) Values ('" _
        '                                       & CompagnyID & "', '" _
        '                                       & PharmaID.Trim.Replace("'", "''") & "', '" _
        '                                       & PharmacieID & "', '" _
        '                                       & Read_no_client_c.Trim.Replace("'", "''") & "', '" _
        '                                       & Read_name.Trim.Replace("'", "''") & "', '" _
        '                                       & Read_billing_address_street.Replace("'", "''") & "', '" _
        '                                       & Read_billing_address_city.Replace("'", "''") & "', '" _
        '                                       & Read_billing_address_state.Replace("'", "''") & "', '" _
        '                                       & Read_billing_address_postalcode.Replace("'", "''") & "', '" _
        '                                       & Read_phone_office.Replace("'", "''") & "', '" _
        '                                       & Read_phone_fax.Replace("'", "''") & "', '" _
        '                                       & Read_email.Replace("'", "''") & "', '" _
        '                                       & Read_rabais & "', '" _
        '                                       & Read_name.Trim.Replace("'", "''") & "', '" _
        '                                       & Read_billing_address_street.Replace("'", "''") & "', '" _
        '                                       & Read_billing_address_city.Replace("'", "''") & "', '" _
        '                                       & Read_billing_address_state.Replace("'", "''") & "', '" _
        '                                       & Read_billing_address_postalcode.Replace("'", "''") & "', '" _
        '                                       & Read_phone_office.Replace("'", "''") & "', '" _
        '                                       & Read_phone_fax.Replace("'", "''") & "', '" _
        '                                       & Read_email.Replace("'", "''") & "', '" _
        '                                       & Read_changt_nom_adresse & "', '" _
        '                                       & Read_changt_banniere & "', '" _
        '                                       & Read_changt_groupe & "', '" _
        '                                       & Read_changt_rep & "', '" _
        '                                       & Read_transfer_id_bannieres_c & "', '" _
        '                                       & Read_transfer_id_groupes_c & "', '" _
        '                                       & Read_assigned_user_id & "', 1)", db_connection)

        '                    'Create RepBanGrp empty entry
        '                    If Count_Row(db_connection, "SELECT * FROM RepBanGrp_Mapping WHERE PharmacieID = " & PharmacieID) = 0 Then
        '                        DB_Exec_Scalar("INSERT INTO RepBanGrp_Mapping (CompagnieID, PharmacieID) VALUES ('" & CompagnyID & "', '" & PharmacieID & "')", db_connection)
        '                    End If
        '                End If
        '            End If
        '        End If
        '    Else
        '        MsgBox("Erreur: PharmaID invalide " & vbLf & "( " & PharmaID & " - " & Read_name & " )")
        '        UpdateTimeStamp = False
        '    End If
        'Next

        ''Check dated updated value
        'Dim Acceo_Erp As Integer
        'Using cmd As New SqlCommand("SELECT * FROM Pharmacie", db_connection)
        '    Using da As New SqlDataAdapter(cmd)
        '        dtUser.Reset()
        '        da.Fill(dtUser)
        '        Acceo_Erp = dtUser.Rows.Count - 1
        '        For Each Row As DataRow In dtUser.Rows
        '            JampCruncher.btnStatus.Text = "Synchronisation du CRM" & vbLf & "Pharmacies" & vbLf & "Check Acceo ERP for Update (" & Acceo_Erp & ")"
        '            Application.DoEvents()
        '            Acceo_Erp -= 1

        '            'If data is available
        '            If Row("Date_Change_Adresse").ToString > "2010-01-01" Then
        '                If Row("Date_Change_Adresse").ToString.Trim <= Now And Row("New_Raison_sociale").ToString.Trim <> "" And (
        '                   Row("New_Raison_sociale").ToString.Trim <> Row("Raison_sociale").ToString.Trim OrElse
        '                   Row("New_Adresse").ToString.Trim <> Row("Adresse").ToString.Trim OrElse
        '                   Row("New_Ville").ToString.Trim <> Row("Ville").ToString.Trim OrElse
        '                   Row("New_Province").ToString.Trim <> Row("Province").ToString.Trim OrElse
        '                   Row("New_Code_postal").ToString.Trim <> Row("Code_postal").ToString.Trim OrElse
        '                   Row("New_Telephone").ToString.Trim <> Row("Telephone").ToString.Trim OrElse
        '                   Row("New_Fax").ToString.Trim <> Row("Fax").ToString.Trim OrElse
        '                   Row("New_Courriel").ToString.Trim <> Row("Courriel").ToString.Trim) Then
        '                    DB_Exec_NonQuery("UPDATE Pharmacie SET " _
        '                                    & "Raison_sociale = '" & Row("New_Raison_sociale").replace("'", "''") & "', " _
        '                                    & "Adresse = '" & Row("New_Adresse").replace("'", "''") & "', " _
        '                                    & "Ville = '" & Row("New_Ville").replace("'", "''") & "', " _
        '                                    & "Province = '" & Row("New_Province").replace("'", "''") & "', " _
        '                                    & "Code_postal = '" & Row("New_Code_postal") & "', " _
        '                                    & "Telephone = '" & Row("New_Telephone") & "', " _
        '                                    & "Fax = '" & Row("New_Fax") & "', " _
        '                                    & "Courriel = '" & Row("New_Courriel") & "', " _
        '                                    & "Update_ERP = '1' " _
        '                                    & "WHERE PharmacieID = '" & Row("PharmacieID") & "'", db_connection)
        '                End If

        '                Dim xDate As Integer

        '                If Row("Date_Change_Ban") <= Now And Row("New_BanniereID").ToString <> Row("BanniereID").ToString Then
        '                    DB_Exec_NonQuery("UPDATE Pharmacie SET BanniereID = '" & Row("New_BanniereID").ToString & "' " _
        '                                                 & "WHERE PharmacieID = '" & Row("PharmacieID") & "'", db_connection)
        '                    xDate = Row("Date_Change_Ban").month
        '                    If xDate > Now.Month Then xDate = 1
        '                    For xloop = xDate To Now.Month
        '                        DB_Exec_NonQuery("UPDATE RepBanGrp_Mapping SET " _
        '                                                 & "RepBanGrp_Mapping.BanID_Mois_" & xloop & " = '" & Row("New_BanniereID").ToString & "' " _
        '                                                 & "WHERE PharmacieID = '" & Row("PharmacieID") & "'", db_connection)
        '                    Next
        '                End If

        '                If Row("Date_Change_Groupe") <= Now And Row("New_GroupeID").ToString <> Row("GroupeID").ToString Then
        '                    DB_Exec_NonQuery("UPDATE Pharmacie SET GroupeID = '" & Row("New_GroupeID").ToString & "' " _
        '                                                 & "WHERE PharmacieID = '" & Row("PharmacieID") & "'", db_connection)
        '                    xDate = Row("Date_Change_Groupe").month
        '                    If xDate > Now.Month Then xDate = 1
        '                    For xloop = xDate To Now.Month
        '                        DB_Exec_NonQuery("UPDATE RepBanGrp_Mapping SET " _
        '                                                 & "RepBanGrp_Mapping.GrpID_Mois_" & xloop & " = '" & Row("New_GroupeID").ToString & "' " _
        '                                                 & "WHERE PharmacieID = '" & Row("PharmacieID") & "'", db_connection)
        '                    Next

        '                End If

        '                If Row("Date_Change_Rep") <= Now And Row("New_RepID").ToString <> Row("RepID").ToString Then
        '                    xDate = Row("Date_Change_rep").month
        '                    If xDate > Now.Month Then xDate = 1
        '                    For xloop = xDate To Now.Month
        '                        DB_Exec_NonQuery("UPDATE RepBanGrp_Mapping SET " _
        '                                                 & "RepBanGrp_Mapping.RepID_Mois_" & xloop & " = '" & Row("New_RepID").ToString & "' " _
        '                                                 & " WHERE PharmacieID = '" & Row("PharmacieID") & "'", db_connection)
        '                    Next
        '                    AncienRepID = 0
        '                    Using cmd2 As New SqlCommand("SELECT * FROM RepBanGrp_Mapping WHERE PharmacieID = '" & Row("PharmacieID") & "'", db_connection)
        '                        Using da2 As New SqlDataAdapter(cmd2)
        '                            Dim dtRepMonth As New DataTable
        '                            da2.Fill(dtRepMonth)
        '                            If dtRepMonth.Rows.Count > 0 Then
        '                                For xMonth As Integer = 1 To 11
        '                                    CurMonth = xDate - xMonth
        '                                    If CurMonth < 1 Then CurMonth += 12
        '                                    If dtRepMonth.Rows(0)("RepID_Mois_" & CurMonth).ToString <> Row("New_RepID") Then
        '                                        AncienRepID = dtRepMonth.Rows(0)("RepID_Mois_" & CurMonth).ToString
        '                                        Exit For
        '                                    End If
        '                                Next
        '                            End If
        '                        End Using
        '                    End Using

        '                    'Save to CRM and DB
        '                    DB_Exec_NonQuery("UPDATE Pharmacie SET RepID = '" & Row("New_RepID").ToString & "', Update_ERP = '1', " _
        '                                        & "AncienRepID = '" & AncienRepID & "' WHERE PharmacieID = '" & Row("PharmacieID") & "'", db_connection)
        '                    DB_Exec_NonQuery_MySQL("UPDATE Pharmacie SET RepID = '" & Row("New_RepID").ToString & "', AncienRepID = '" & AncienRepID & "' " _
        '                                        & "WHERE PharmacieID = '" & Row("PharmacieID") & "'", db_connectionCRM)
        '                End If
        '            End If
        '        Next
        '    End Using
        'End Using

        ''Update Rep, Groupe and Banniere Mapping per month
        'DB_Exec_NonQuery("UPDATE RepBanGrp_Mapping SET " _
        '                         & "RepBanGrp_Mapping.RepID_Mois_" & Now.Month & " = Pharmacie.repID, " _
        '                         & "RepBanGrp_Mapping.BanID_Mois_" & Now.Month & " = Pharmacie.BanniereID, " _
        '                         & "RepBanGrp_Mapping.GrpID_Mois_" & Now.Month & " = Pharmacie.GroupeID " _
        '                         & "FROM RepBanGrp_Mapping INNER JOIN Pharmacie ON " _
        '                         & "RepBanGrp_Mapping.PharmacieID  = Pharmacie.PharmacieID", db_connection)

        'If UpdateTimeStamp = True Then
        '    DB_Exec_NonQuery("UPDATE setup SET LastUpdatePharm = '" & Now.AddMinutes(-30) & "'", db_connection)
        'End If

        'db_connectionCRM.Close()
        'db_connectionCRM.Dispose()

    End Sub

    Sub Update_ERP(db_connection As SqlConnection)
        '    If Count_Row(db_connection, "SELECT * FROM Pharmacie WHERE Update_ERP = 1") > 0 Then

        '        JampCruncher.btnStatus.Text = "Synchronisation du CRM" & vbLf & "Update Acceo_Erp"
        '        Application.DoEvents()

        '        Dim dtUser As New DataTable
        '        Dim Acceo_Erp As Integer
        '        Dim PharmaID As String
        '        Dim AncienRepID As Integer
        '        Dim AcceoErr As String = ""
        '        Dim sRep As String

        '        Dim dtRep As New DataTable
        '        Using cmd As New SqlCommand("SELECT RepID, RepIDERP FROM Representant ORDER BY RepID", db_connection)
        '            Using sda As New SqlDataAdapter(cmd)
        '                sda.Fill(dtRep)
        '            End Using
        '        End Using

        '        Dim iFs As Func(Of String, Func(Of DataRow, String)) = Function(s) Function(dr) dr.Field(Of Integer)(s)
        '        Dim sFs As Func(Of String, Func(Of DataRow, String)) = Function(s) Function(dr) dr.Field(Of String)(s)
        '        Dim dicRep = dtRep.Rows.OfType(Of DataRow)().ToDictionary(iFs("RepID"), iFs("RepIDERP"))

        '        Using cmd As New SqlCommand("SELECT * FROM Pharmacie WHERE Update_ERP = 1 ORDER BY PharmaID", db_connection)
        '            Using da As New SqlDataAdapter(cmd)
        '                da.Fill(dtUser)
        '                Dim ERPconn = Acceo_Connect("Biomed")
        '                Dim ERPRepList As New Dictionary(Of String, String)
        '                Dim ERPCustList As New Dictionary(Of String, Guid)
        '                Dim dtERPCustEmpList As New DataTable
        '                Dim SearchAll As Boolean = Count_Row(db_connection, "SELECT * FROM Pharmacie WHERE Update_ERP = 1") > 100
        '                Dim nbSearch = IIf(SearchAll, 26, Count_Row(db_connection, "SELECT * FROM Pharmacie WHERE Update_ERP = 1"))

        '                dtERPCustEmpList.Columns.Add("PharmaID", GetType(String))
        '                dtERPCustEmpList.Columns.Add("RepID", GetType(Integer))
        '                dtERPCustEmpList.Columns.Add("AncienRepID", GetType(Integer))

        '                If Not IsNothing(ERPconn) Then
        '                    Dim custlist() As Object
        '                    Dim Customer As AR303000_Customers
        '                    Dim searchCust As New StringSearch With {
        '                        .Condition = IIf(SearchAll, StringCondition.StartsWith, StringCondition.Equal)
        '                    }
        '                    Dim TimeBegin = Now
        '                    Dim RepID As Integer

        '                    Acceo_Erp = nbSearch - 1

        '                    'Get current Rep assignment in ERP
        '                    For BeginChar = 0 To nbSearch - 1
        '                        JampCruncher.btnStatus.Text = "Get ERP Rep Mapping" & vbLf & "Biomed" & vbLf & "Update Acceo_Erp (" & Acceo_Erp & ")"
        '                        Application.DoEvents()
        '                        Acceo_Erp -= 1
        '                        Customer = New AR303000_Customers
        '                        searchCust.Value = IIf(SearchAll, Encoding.ASCII.GetString(New Byte() {(65 + BeginChar)}), dtUser.Rows(BeginChar)("PharmaID").ToString)
        '                        Customer.CustomerID = searchCust
        '                        Customer.Salespersons = New AR303000_Salespersons() {New AR303000_Salespersons With {.ReturnBehavior = ReturnBehavior.All}}
        '                        Customer.ReturnBehavior = ReturnBehavior.OnlySpecified
        '                        custlist = ERPconn.GetList(Customer)
        '                        Dim sRow As DataRow
        '                        For xloop = 0 To custlist.Length - 1
        '                            ERPCustList.Add(custlist(xloop).CustomerID.Value, custlist(xloop).ID)
        '                            If custlist(xloop).salespersons.length > 0 Then
        '                                sRow = dtERPCustEmpList.NewRow
        '                                PharmaID = custlist(xloop).customerid.value
        '                                If custlist(xloop).salespersons.length = 1 Then
        '                                    RepID = custlist(xloop).salespersons(0).salespersonID.value
        '                                    AncienRepID = 0
        '                                End If
        '                                If custlist(xloop).salespersons.length = 2 Then
        '                                    If custlist(xloop).salespersons(0).default.value = True Then
        '                                        RepID = custlist(xloop).salespersons(0).salespersonID.value
        '                                        AncienRepID = custlist(xloop).salespersons(1).salespersonID.value
        '                                    Else
        '                                        RepID = custlist(xloop).salespersons(1).salespersonID.value
        '                                        AncienRepID = custlist(xloop).salespersons(0).salespersonID.value
        '                                    End If
        '                                End If
        '                                sRow(0) = PharmaID
        '                                sRow(1) = RepID
        '                                sRow(2) = AncienRepID
        '                                dtERPCustEmpList.Rows.Add(sRow)
        '                            End If
        '                        Next
        '                    Next
        '                    Dim timeEnd = Now

        '                    Dim RepList() As Object
        '                    Dim Rep As New AR205000_Salespersons
        '                    Dim searchRep As New StringSearch With {
        '                          .Condition = StringCondition.IsGreaterThan,
        '                          .Value = ""
        '                    }
        '                    Rep.SalespersonID = searchRep
        '                    Rep.Name = searchRep
        '                    Rep.ReturnBehavior = ReturnBehavior.OnlySpecified
        '                    RepList = ERPconn.GetList(Rep)
        '                    For xloop = 0 To RepList.Length - 1
        '                        ERPRepList.Add(RepList(xloop).SalespersonID.value, RepList(xloop).Name.value)
        '                    Next

        '                End If

        '                Acceo_Erp = dtUser.Rows.Count - 1
        '                Dim ModRep As Integer = 0
        '                Dim dtCustRep() As DataRow

        '                For Each Row As DataRow In dtUser.Rows
        '                    JampCruncher.btnStatus.Text = "Synchronisation du CRM" & vbLf & "Biomed" & vbLf & "Update Acceo_Erp (" & Acceo_Erp & ")"
        '                    Application.DoEvents()
        '                    Acceo_Erp -= 1
        '                    If Not IsNothing(ERPconn) Then
        '                        ModRep = -1
        '                        Dim CustMain As New AR303000_Customers
        '                        PharmaID = Row("PharmaID").ToString
        '                        If ERPCustList.ContainsKey(PharmaID) Then
        '                            CustMain.ID = ERPCustList(PharmaID)
        '                            dtCustRep = dtERPCustEmpList.Select("PharmaID = '" & Row("PharmaID") & "'")
        '                            If dtCustRep.Length = 0 Then
        '                                ModRep = 0
        '                            Else
        '                                If (Row("RepID") <> dtCustRep(0)("RepID") Or Row("AncienRepID") <> dtCustRep(0)("AncienRepID")) Then
        '                                    If dtCustRep(0)("RepID") <> "0" Or dtCustRep(0)("AncienRepID") <> (0) Then
        '                                        CustMain.ReturnBehavior = ReturnBehavior.All
        '                                        CustMain = ERPconn.Get(CustMain)
        '                                    End If
        '                                    If IsNothing(CustMain.Salespersons) Then
        '                                        ModRep = 0
        '                                    Else
        '                                        ModRep = CustMain.Salespersons.Length
        '                                    End If
        '                                    For xloop = 0 To ModRep - 1
        '                                        CustMain.Salespersons(xloop).Delete = True
        '                                    Next
        '                                End If
        '                            End If
        '                        Else
        '                            'New Customer
        '                            CustMain.Status = New StringValue With {.Value = "On Hold"}
        '                            ModRep = 0
        '                        End If
        '                        If ModRep >= 0 Then
        '                            If ModRep = 0 Then
        '                                CustMain.Salespersons = New AR303000_Salespersons() {}
        '                            End If
        '                            Array.Resize(CustMain.Salespersons, ModRep + 1)
        '                            CustMain.Salespersons(ModRep) = New AR303000_Salespersons
        '                            CustMain.Salespersons(ModRep).Default = New BooleanValue With {.Value = True}
        '                            CustMain.Salespersons(ModRep).LocationID = New StringValue With {.Value = "MAIN"}
        '                            CustMain.Salespersons(ModRep).SalespersonID = New StringValue With {.Value = dicRep(Row("RepID").ToString)}
        '                            If Row("AncienRepID").ToString <> "0" Then
        '                                Array.Resize(CustMain.Salespersons, ModRep + 2)
        '                                CustMain.Salespersons(ModRep + 1) = New AR303000_Salespersons
        '                                CustMain.Salespersons(ModRep + 1).Default = New BooleanValue With {.Value = False}
        '                                CustMain.Salespersons(ModRep + 1).LocationID = New StringValue With {.Value = "MAIN"}
        '                                CustMain.Salespersons(ModRep + 1).SalespersonID = New StringValue With {.Value = dicRep(Row("AncienRepID").ToString)}
        '                            End If
        '                        End If
        '                        CustMain.CustomerName = New StringValue With {.Value = Row("Raison_sociale").ToString}
        '                        CustMain.CustomerID = New StringValue With {.Value = PharmaID}
        '                        CustMain.MainContact = New AR303000_Contact
        '                        CustMain.MainContact.CompanyName = New StringValue With {.Value = Row("Raison_sociale").ToString}
        '                        CustMain.MainContact.Phone1 = New StringValue With {.Value = Row("Telephone").ToString}
        '                        CustMain.MainContact.Fax = New StringValue With {.Value = Row("Fax").ToString}
        '                        CustMain.MainContact.Email = New StringValue With {.Value = Row("Courriel").ToString}
        '                        CustMain.MainContact.Address = New AR303000_Address
        '                        CustMain.MainContact.Address.AddressLine1 = New StringValue With {.Value = Row("Adresse").ToString}
        '                        CustMain.MainContact.Address.City = New StringValue With {.Value = Row("Ville").ToString}
        '                        CustMain.MainContact.Address.State = New StringValue With {.Value = Row("Province").ToString}
        '                        If Row("Province").ToString.ToUpper = "QUÉBEC" Or Row("Province").ToString.ToUpper = "QUEBEC" Then CustMain.MainContact.Address.State = New StringValue With {.Value = "QC"}
        '                        CustMain.MainContact.Address.PostalCode = New StringValue With {.Value = Row("Code_Postal").ToString}
        '                        CustMain.MainContact.Address.Country = New StringValue With {.Value = "CA"}
        '                        CustMain.ReturnBehavior = ReturnBehavior.OnlySpecified
        '                        Try
        '                            CustMain = ERPconn.Put(CustMain)
        '                            DB_Exec_NonQuery("UPDATE Pharmacie SET Update_ERP = '0' WHERE PharmacieID = '" & Row("PharmacieID") & "'", db_connection)
        '                        Catch ex As Exception
        '                            Dim AncienRep As Integer = Row("AncienRepID")
        '                            If AncienRep > 0 Then AncienRep = dicRep(AncienRep)
        '                            If ex.Message.Contains("'SalesPersonID' cannot be found in the system.") Then
        '                                sRep = DB_Exec_Scalar("SELECT Rep_Nom FROM representant WHERE RepID = " & Row("RepID"), db_connection)
        '                                AcceoErr = AcceoErr & Row("PharmaID") & " - " & sRep & " non configuré(e) dans AcceoERP, veuillez l'ajouter" & vbLf
        '                            Else
        '                                If ex.Message.Contains("Email address must be specified") Then
        '                                    AcceoErr = AcceoErr & Row("PharmaID") & " - L'addresse courriel doit être spécifiée pour AcceoERP" & vbLf
        '                                Else
        '                                    If ex.Message.Contains("'State' cannot be found in the system") Then
        '                                        AcceoErr = AcceoErr & Row("PharmaID") & " - Province inconnue, veuillez corriger dans le CRM" & vbLf
        '                                    Else
        '                                        MsgBox("PharmaID = " & Row("PharmaID") & vbLf & vbLf & ex.Message, MsgBoxStyle.Critical)
        '                                    End If
        '                                End If
        '                            End If
        '                        End Try
        '                    End If
        '                Next
        '                ERP_Logout(ERPconn)
        '            End Using
        '        End Using
        '        If AcceoErr.Length > 0 Then
        '            MsgBox(AcceoErr, MsgBoxStyle.Critical)
        '        End If
        '    End If

    End Sub

    Sub Load_Produit_CRM(db_connection As SqlConnection)
        'Dim db_connectionCRM As New MySqlConnection(My.Settings.BiomedCRMConnector) 'Connect to CRM
        'db_connectionCRM.Open()

        'JampCruncher.btnStatus.Text = "Synchronisation du CRM" & vbLf & "Familles de produits"
        'Application.DoEvents()

        'Dim Result As New DataTable
        'Using cmd As New MySqlCommand("SELECT FamProdID, FamProdNom, BrandID FROM ProduitFamille", db_connectionCRM)
        '    Using sda As New MySqlDataAdapter(cmd)
        '        sda.Fill(Result)
        '    End Using
        'End Using
        'DB_Exec_NonQuery("TRUNCATE TABLE ProduitFamille", db_connection) 'Erase previous data
        'For Each ResultList In Result.Rows
        '    DB_Exec_NonQuery("INSERT INTO ProduitFamille (CompagnieID, FamProdID, FamProdNom, BrandID) Values ('" _
        '                    & CompagnyID & "', '" _
        '                    & ResultList("FamProdID") & "', '" _
        '                    & ResultList("FamProdNom").replace("'", "''") & "', '" _
        '                    & ResultList("BrandID") & "');" _
        '                    , db_connection)
        'Next

        'Result.Clear()
        'Using cmd As New MySqlCommand("SELECT ProduitID, ProduitCode, ProduitUPC, ProduitDesc, FamProdNewID, CodeUniversel, Prix_CRM, Actif FROM Produit", db_connectionCRM)
        '    Using sda As New MySqlDataAdapter(cmd)
        '        sda.Fill(Result)
        '    End Using
        'End Using

        'JampCruncher.btnStatus.Text = "Synchronisation du CRM" & vbLf & "Produits"
        'Application.DoEvents()

        'DB_Exec_NonQuery("TRUNCATE TABLE Produit", db_connection) 'Erase previous data
        'For Each ResultList In Result.Rows
        '    DB_Exec_NonQuery("INSERT INTO Produit (CompagnieID, ProduitID, ProduitCode, ProduitUPC, ProduitDesc, FamProdNewID, Prix_CRM, CodeUniversel, Actif) Values ('" _
        '                    & CompagnyID & "', '" _
        '                    & ResultList("ProduitID") & "', '" _
        '                    & ResultList("ProduitCode") & "', '" _
        '                    & ResultList("ProduitUPC") & "', '" _
        '                    & ResultList("ProduitDesc").replace("'", "''") & "', '" _
        '                    & ResultList("FamProdNewID") & "', '" _
        '                    & CDec(ResultList("Prix_Crm")).ToString("N2", CultureInfo.CreateSpecificCulture("en-US")).Replace(",", "") & "', '" _
        '                    & ResultList("CodeUniversel") & "', '" _
        '                    & ResultList("Actif") & "');" _
        '                    , db_connection)
        'Next
        'db_connectionCRM.Close()
        'db_connectionCRM.Dispose()

        'JampCruncher.btnStatus.Text = "Synchronisation du CRM" & vbLf & "Codes Universel"
        'Application.DoEvents()

        'Dim CUID As Integer
        'Dim dtProduit As New DataTable
        'Using cmd As New SqlCommand("SELECT * FROM Produit", db_connection)
        '    Using sda As New SqlDataAdapter(cmd)
        '        sda.Fill(dtProduit)
        '    End Using
        'End Using

        'For Each Row In dtProduit.Rows
        '    If Count_Row(db_connection, "SELECT * FROM CodeUniversel WHERE CodeUniversel = '" & Row("CodeUniversel").replace("'", "''") & "'") = 0 Then
        '        DB_Exec_NonQuery("INSERT INTO CodeUniversel (CieID, CodeUniversel) Values ('" _
        '                          & CompagnyID & "', '" _
        '                          & Row("CodeUniversel").replace("'", "''") & "')", db_connection)
        '    End If
        '    DB_Exec_NonQuery("UPDATE CodeUniversel SET " _
        '                      & "ProduitDesc = '" & Row("ProduitDesc").replace("'", "''") & "', " _
        '                      & "Prix_CRM = '" & CDec(Row("Prix_Crm")).ToString("N2", CultureInfo.CreateSpecificCulture("en-US")).Replace(",", "") & "' " _
        '                                                   & "WHERE CodeUniversel = '" & Row("CodeUniversel").replace("'", "''") & "'", db_connection)
        '    CUID = CInt(DB_Exec_Scalar("SELECT CUId FROM CodeUniversel WHERE CodeUniversel = '" & Row("CodeUniversel").replace("'", "''") & "'", db_connection))
        '    DB_Exec_NonQuery("UPDATE Produit Set CUID = " & CUID & " WHERE ProduitID = " & Row("ProduitID"), db_connection)
        'Next

        ''Update Monthly price of product
        'DB_Exec_NonQuery("UPDATE CodeUniversel SET Mois_" & MonthToUpdate & " = Prix_CRM", db_connection)

    End Sub

    Function Check_Date(InputDate As String, Begin As Boolean) As String
        Dim tempdate As Date
        Date.TryParse(InputDate, tempdate)
        Check_Date = tempdate.ToShortDateString
        If Check_Date = "0001-01-01" Or Check_Date = "0001-01-01 00:00:00" Then
            If Begin = True Then
                Check_Date = "2000-01-01"
            Else
                Check_Date = "2999-12-31"
            End If
        End If
        If tempdate.Day > 1 And tempdate.Day < 28 Then
            tempdate = New Date(tempdate.Year, tempdate.Month, 1)
            Check_Date = tempdate.ToShortDateString
        End If
    End Function

    Sub Upload_Cumul_CRM_Bio()

        'Dim db_connection As SqlConnection
        'Dim db_command As SqlCommand
        'Dim datareader As SqlDataReader
        'Dim Annee As Integer
        'Dim Mois As Integer
        'Dim PharmacieID As Integer
        'Dim TotSales As Decimal
        'Dim TotRistournes As Decimal
        'Dim PharmaID As String
        'Dim BanniereID As Integer
        'Dim GroupeID As Integer
        'Dim RepID As Integer
        'Dim PharmastatTotal As Decimal = 0
        'Dim GRABB_Total As Decimal = 0
        'Dim Ristourne As Decimal = 0
        'Dim scurMonth As String
        'Dim Insert As String

        'JampCruncher.btnStatus.Text = "Synchronisation Data" & vbLf & "Vers le CRM"
        'Application.DoEvents()

        'db_connection = New SqlConnection(GSS_Connection) 'Connect to local DB
        'db_connection.Open()

        ''Calcul des ristournes Taux fixe de 15%
        'scurMonth = New Date(YearActive, MonthActive, 15).ToShortDateString
        'Dim dtTrans As New DataTable

        ''Update transaction Data to the latest Price, Rep, Ban and GRP in case they were modify after the importation, save result to a temporary table (Trans_Month)
        'DB_Exec_NonQuery("TRUNCATE TABLE Trans_Month", db_connection)
        'Insert = "INSERT INTO Trans_Month SELECT " _
        '                   & "Trans.TransactionID, Trans.CompagnieID, Trans.DistID, Trans.NoFacture, Trans.Date, Trans.PharmacieID, Trans.ProduitID, " _
        '                   & "Trans.Qtee, (CodeUniversel.Mois_" & MonthActive & " * Trans.Qtee) AS Montant, RepBanGrp_Mapping.RepID_Mois_" & MonthActive & ", " _
        '                   & "RepBanGrp_Mapping.banID_Mois_" & MonthActive & ", RepBanGrp_Mapping.GrpID_Mois_" & MonthActive & ", Trans.Skip, " _
        '                   & "Produit.CUId, CASE Trans.Overwrite WHEN 1 THEN Trans.Ristourne ELSE Pharmacie.Rabais END, Trans.Overwrite FROM Trans " _
        '                   & "INNER JOIN Produit ON Trans.ProduitID = Produit.ProduitID AND Trans.CompagnieID = Produit.CompagnieID " _
        '                   & "INNER JOIN CodeUniversel ON Produit.CUID = CodeUniversel.CUID AND Trans.CompagnieID = CodeUniversel.CieID " _
        '                   & "INNER JOIN Pharmacie ON Trans.PharmacieID = Pharmacie.PharmacieID AND Trans.CompagnieID = Pharmacie.CompagnieID " _
        '                   & "INNER JOIN RepBanGrp_Mapping ON Trans.CompagnieID = RepBanGrp_Mapping.CompagnieID AND Trans.PharmacieID = RepBanGrp_Mapping.PharmacieID " _
        '                   & "WHERE Trans.Date = '" & DateActive & "'"
        'DB_Exec_NonQuery(Insert, db_connection)

        ''Update Trans table for Excel access and Transfert
        'DB_Exec_NonQuery("DELETE FROM Trans WHERE date = '" & DateActive & "' AND Skip = 0", db_connection)
        'DB_Exec_NonQuery("INSERT INTO Trans SELECT CompagnieID, DistID, NoFacture, Date, PharmacieID, " _
        '              & "ProduitID, Qtee, Montant, RepID, BanniereID, GroupeID, Skip, CUId, Ristourne, Overwrite " _
        '              & "FROM Trans_Month", db_connection)

        'Using cmd As New SqlCommand("SELECT * FROM vTrans WHERE [Date] = '" & scurMonth & "'", db_connection)
        '    Using sda As New SqlDataAdapter(cmd)
        '        sda.Fill(dtTrans)
        '    End Using
        'End Using

        'DB_Exec_NonQuery("DELETE FROM grabb_calc_cur_mth_tot WHERE dCloseDate = '" & scurMonth & "'", db_connection)

        'For Each row In dtTrans.Rows
        '    Ristourne = (row("Amount") * row("Ristourne") / 100)
        '    Insert = "INSERT INTO grabb_calc_cur_mth_tot (CompanieID, sRepName, iRepID, sClientCode, sProductCode, dCloseDate, fNetSales, iQty, " _
        '               & "sDistriName, BanniereDesc, Year, [Fisc# Mth], [Ristourne $], [Rebate5 rate], [Final rebate], iProdId) Values ('" _
        '               & CompagnyID & "', '" _
        '               & row("Rep_Nom").replace("'", "''") & "', '" _
        '               & row("RepID") & "', '" _
        '               & row("PharmaID") & "', '" _
        '               & row("ProduitDesc") & "', '" _
        '               & row("Date") & "', '" _
        '               & CDec(row("Amount")).ToString("N2", CultureInfo.CreateSpecificCulture("en-US")).Replace(",", "") & "', '" _
        '               & row("Qty") & "', '" _
        '               & row("DistributeurNom").replace("'", "''") & "', '" _
        '               & row("BanniereDesc").replace("'", "''") & "', '" _
        '               & YearActive & "', '" _
        '               & MonthActive & "', '" _
        '               & Ristourne.ToString("N2", CultureInfo.CreateSpecificCulture("en-US")).Replace(",", "") & "', '" _
        '               & CDec(row("Ristourne") / 100).ToString("N2", CultureInfo.CreateSpecificCulture("en-US")).Replace(",", "") & "', '" _
        '               & CDec(row("Ristourne") / 100).ToString("N2", CultureInfo.CreateSpecificCulture("en-US")).Replace(",", "") & "', '" _
        '               & row("ProduitID") & "')"
        '    DB_Exec_NonQuery(Insert, db_connection)
        'Next

        'DB_Exec_NonQuery("TRUNCATE TABLE CME_Temp", db_connection)
        'DB_Exec_NonQuery("INSERT INTO CME_Temp SELECT * FROM vCME_temp", db_connection)

        ''Update CME

        'db_command = New SqlCommand("SELECT * FROM vCME WHERE Annee = '" & YearActive & "' and Mois = '" _
        '                            & MonthActive & "' ORDER BY PharmacieID", db_connection)
        'datareader = db_command.ExecuteReader 'Get the info
        'db_command.Dispose()
        'While datareader.Read
        '    PharmacieID = datareader("PharmacieID")
        '    Annee = datareader("Annee")
        '    Mois = datareader("Mois")
        '    CompagnyID = datareader("CieID")
        '    TotSales = datareader("TotSales") + 0.004
        '    TotRistournes = datareader("TotRistournes") + 0.004
        '    PharmaID = datareader("PharmaID")

        '    BanniereID = DB_Exec_Scalar("SELECT BanID_Mois_" & MonthActive & " FROM RepBanGrp_Mapping WHERE PharmacieID = " & PharmacieID, db_connection)
        '    GroupeID = DB_Exec_Scalar("SELECT GrpID_Mois_" & MonthActive & " FROM RepBanGrp_Mapping WHERE PharmacieID = " & PharmacieID, db_connection)
        '    RepID = DB_Exec_Scalar("SELECT RepID_Mois_" & MonthActive & " FROM RepBanGrp_Mapping WHERE PharmacieID = " & PharmacieID, db_connection)

        '    If Count_Row(db_connection, "SELECT * FROM CME WHERE PharmacieID = '" & PharmacieID &
        '              "' AND Annee = '" & Annee & "' AND Mois = '" & Mois & "'") > 0 Then
        '        DB_Exec_Scalar("UPDATE CME SET CompagnyID = '" & CompagnyID &
        '                       "', TotSales = '" & TotSales.ToString("N2", CultureInfo.CreateSpecificCulture("en-US")).Replace(",", "") &
        '                       "', TotRistournes = '" & TotRistournes.ToString("N2", CultureInfo.CreateSpecificCulture("en-US")).Replace(",", "") &
        '                       "', PharmaID = '" & PharmaID &
        '                       "', BanniereID = '" & BanniereID &
        '                       "', GroupeID = '" & GroupeID &
        '                       "', RepID = '" & RepID &
        '                       "' WHERE PharmacieID = '" & PharmacieID &
        '                       "' AND Annee = '" & Annee & "' AND Mois = '" & Mois & "'", db_connection)
        '    Else
        '        DB_Exec_Scalar("INSERT INTO CME (CompagnyID, PharmacieID, Annee, Mois, TotSales, TotRistournes, PharmaID, BanniereID, GroupeID, RepID) Values ('" _
        '                               & CompagnyID & "', '" _
        '                               & PharmacieID & "', '" _
        '                               & Annee & "', '" _
        '                               & Mois & "', '" _
        '                               & TotSales.ToString("N2", CultureInfo.CreateSpecificCulture("en-US")).Replace(",", "") & "', '" _
        '                               & TotRistournes.ToString("N2", CultureInfo.CreateSpecificCulture("en-US")).Replace(",", "") & "', '" _
        '                               & PharmaID & "', '" _
        '                               & BanniereID & "', '" _
        '                               & GroupeID & "', '" _
        '                               & RepID & "');" _
        '                               , db_connection)
        '    End If
        'End While
        'datareader.Close()

        'Dim db_connectionCRM As New MySqlConnection(My.Settings.BiomedCRMConnector) 'Connect to CRM
        'db_connectionCRM.Open()

        'Upload_Cumul_Pharma_CRM(db_connection, db_connectionCRM)
        'Upload_Cumul_Groupe_CRM(db_connection, db_connectionCRM)
        'Upload_Cumul_Banniere_CRM(db_connection, db_connectionCRM)

        'db_connectionCRM.Close()
        'db_connectionCRM.Dispose()

        ''Dim OldData As Date = New Date(YearActive - 1, MonthActive, 1)
        '''Dim OldDataLocal As Date = New Date(YearActive - 2, MonthActive, 1)

        ''Pharmastat.btnStatus.Text = "Synchronisation de GRABB" & vbLf & "CME"
        ''Application.DoEvents()
        '''DB_Exec_NonQuery("DELETE FROM grabb_calc_cur_mth_tot WHERE dclosedate < '" & OldDataLocal & "'", db_connection)
        '''DB_Exec_NonQuery("DELETE FROM pp.grabb_calc_mth_det_temp_new WHERE dclosedate < '" & OldData & "'", db_connection_GRABB)
        ''DB_Exec_NonQuery("DELETE FROM grabb_calc_cur_mth_tot WHERE Year = '" & YearActive &
        ''                            "' AND [Fisc# Mth] = '" & MonthActive & "'", db_connection)
        ''Data_Transfert("SELECT * FROM bio.grabb_calc_mth_det_temp_new WHERE Year = '" & YearActive &
        ''                            "' AND [Fisc# Mth] = '" & MonthActive & "'", db_connection_GRABB,
        ''                            "grabb_calc_cur_mth_tot", GSS_Connection)

        'db_connection.Close()
        'db_connection.Dispose()

    End Sub

    Sub Upload_Cumul_Pharma_CRM(db_connection As SqlConnection, db_connectionCRM As MySqlConnection)
        'Dim db_command As SqlCommand
        'Dim datareader As SqlDataReader
        'Dim Query As String
        'Dim PharmacieID As Integer
        'Dim nbRecord As Integer
        'Dim Ventes As Decimal
        'Dim Ristournes As Decimal

        'nbRecord = Count_Count_Row(db_connection, "SELECT * FROM CME WHERE PharmacieID > 0 AND Annee = '" _
        '                           & YearActive & "' GROUP BY PharmacieID")
        'Query = "SELECT PharmaID, PharmacieID, COALESCE(SUM(TotSales),0) AS Vente, COALESCE(SUM(TotRistournes),0) AS Ristourne " _
        '            & "FROM CME WHERE PharmacieID IS NOT NULL AND Annee = '" & YearActive & "' " _
        '            & "GROUP BY PharmaID, PharmacieID ORDER BY PharmacieID"

        'db_command = New SqlCommand(Query, db_connection)
        'datareader = db_command.ExecuteReader 'Get the info
        'While datareader.Read
        '    nbRecord -= 1
        '    JampCruncher.btnStatus.Text = "Synchronisation du CRM" & vbLf & "Ventes par Pharmacie" & vbLf & nbRecord
        '    Application.DoEvents()

        '    PharmacieID = datareader("PharmacieID")
        '    If Count_Row_MySQL(db_connectionCRM, "SELECT * FROM Pharmacie WHERE PharmacieID = " & PharmacieID) > 0 Then
        '        Decimal.TryParse(datareader("Vente"), Ventes)
        '        Decimal.TryParse(datareader("Ristourne"), Ristournes)
        '        DB_Exec_NonQuery_MySQL("UPDATE Pharmacie SET " _
        '                                    & "Achat" & IIf(YearActive <> Now.Year And Now.Month > 1, "1", "") & " = " & Ventes.ToString("N2", CultureInfo.CreateSpecificCulture("en-US")).Replace(",", "") & ", " _
        '                                    & "Ristourne" & IIf(YearActive <> Now.Year And Now.Month > 1, "1", "") & " = " & Ristournes.ToString("N2", CultureInfo.CreateSpecificCulture("en-US")).Replace(",", "") & " " _
        '                                    & "WHERE PharmacieID = " & PharmacieID, db_connectionCRM)
        '    End If
        'End While
        'datareader.Close()

    End Sub

    Sub Upload_Cumul_Groupe_CRM(db_connection As SqlConnection, db_connectionCRM As MySqlConnection)
        'Dim db_command As SqlCommand
        'Dim datareader As SqlDataReader
        'Dim Query As String
        'Dim GroupeID As Integer
        'Dim nbRecord As Integer
        'Dim Ventes As Decimal
        'Dim Ristournes As Decimal

        'nbRecord = Count_Count_Row(db_connection, "SELECT * FROM CME WHERE GroupeID > 0 AND Annee = '" _
        '                                    & YearActive & "' GROUP BY GroupeID")
        'Query = "SELECT GroupeID, COALESCE(SUM(TotSales),0) AS Vente, COALESCE(SUM(TotRistournes),0) AS Ristourne " _
        '            & "FROM CME WHERE groupeID IS NOT NULL AND GroupeID > 0 AND Annee = '" & YearActive & "' " _
        '            & "GROUP BY GroupeID ORDER BY GroupeID" 'Select the info

        'db_command = New SqlCommand(Query, db_connection)
        'datareader = db_command.ExecuteReader 'Get the info
        'While datareader.Read
        '    nbRecord -= 1
        '    JampCruncher.btnStatus.Text = "Synchronisation du CRM" & vbLf & "Ventes par Groupe" & vbLf & nbRecord
        '    Application.DoEvents()

        '    GroupeID = datareader("GroupeID")
        '    If Count_Row_MySQL(db_connectionCRM, "SELECT * FROM Groupe WHERE GroupeID = " & GroupeID) > 0 Then
        '        Decimal.TryParse(datareader("Vente"), Ventes)
        '        Decimal.TryParse(datareader("Ristourne"), Ristournes)
        '        DB_Exec_NonQuery_MySQL("UPDATE Groupe SET " _
        '                                    & "Achat" & IIf(YearActive <> Now.Year And Now.Month > 1, "1", "") & " = " & Ventes.ToString("N2", CultureInfo.CreateSpecificCulture("en-US")).Replace(",", "") & ", " _
        '                                    & "Ristourne" & IIf(YearActive <> Now.Year And Now.Month > 1, "1", "") & " = " & Ristournes.ToString("N2", CultureInfo.CreateSpecificCulture("en-US")).Replace(",", "") & " " _
        '                                    & "WHERE GroupeID = " & GroupeID, db_connectionCRM)
        '    End If
        'End While
        'datareader.Close()
    End Sub

    Sub Upload_Cumul_Banniere_CRM(db_connection As SqlConnection, db_connectionCRM As MySqlConnection)
        'Dim db_command As SqlCommand
        'Dim datareader As SqlDataReader
        'Dim Query As String
        'Dim BanniereID As Integer
        'Dim nbRecord As Integer
        'Dim Ventes As Decimal
        'Dim Ristournes As Decimal

        'nbRecord = Count_Count_Row(db_connection, "SELECT * FROM CME WHERE BanniereID > 0 AND Annee = '" _
        '                                    & YearActive & "' GROUP BY BanniereID")
        'Query = "SELECT BanniereID, COALESCE(SUM(TotSales),0) AS Vente, COALESCE(SUM(TotRistournes),0) AS Ristourne " _
        '            & "FROM CME WHERE BanniereID IS NOT NULL AND BanniereID > 0 AND Annee = '" & YearActive & "' " _
        '            & "GROUP BY BanniereID ORDER BY BanniereID" 'Select the info

        'db_command = New SqlCommand(Query, db_connection)
        'datareader = db_command.ExecuteReader 'Get the info
        'While datareader.Read
        '    nbRecord -= 1
        '    JampCruncher.btnStatus.Text = "Synchronisation du CRM" & vbLf & "Ventes par Banniere" & vbLf & nbRecord
        '    Application.DoEvents()

        '    BanniereID = datareader("BanniereID")
        '    If Count_Row_MySQL(db_connectionCRM, "SELECT * FROM Banniere WHERE BanniereID = " & BanniereID) > 0 Then
        '        Decimal.TryParse(datareader("Vente"), Ventes)
        '        Decimal.TryParse(datareader("Ristourne"), Ristournes)
        '        DB_Exec_NonQuery_MySQL("UPDATE Banniere SET " _
        '                                    & "Achat" & IIf(YearActive <> Now.Year And Now.Month > 1, "1", "") & " = " & Ventes.ToString("N2", CultureInfo.CreateSpecificCulture("en-US")).Replace(",", "") & ", " _
        '                                    & "Ristourne" & IIf(YearActive <> Now.Year And Now.Month > 1, "1", "") & " = " & Ristournes.ToString("N2", CultureInfo.CreateSpecificCulture("en-US")).Replace(",", "") & " " _
        '                                    & "WHERE BanniereID = " & BanniereID, db_connectionCRM)
        '    End If
        'End While
        'datareader.Close()
    End Sub

End Module
