﻿Public Class Find_Pharm

    Private Sub btnQuit_Click(sender As Object, e As EventArgs) Handles btnQuit.Click
        tbPharmaID.Text = ""
        Me.Visible = False
    End Sub

    Private Sub btUse_Click(sender As Object, e As EventArgs) Handles btUse.Click
        If tbSearchPharm0.Text.Length < 7 And cbSkip.Checked = False Then
            Dim reply As DialogResult = MessageBox.Show("Veuillez faire une recherche S.V.P.",
                        "Veuillez faire une recherche", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Me.Visible = False
        End If
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Search_Pharm()
    End Sub

    Public Sub Search_Pharm()
        If tbPharmaID.Text.Length < 7 Then
            Dim reply As DialogResult = MessageBox.Show("Veuillez mettre un code PharmaID",
                    "Code nécessaire", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        'Update Local Database

        dvClientD.RowFilter = "sClientGuid = '" & tbPharmaID.Text & "'"
        If dvClientD.Count > 0 Then
            tbSearchPharmaID0.Text = dvClientD(0)("sClientGuid").ToString.Trim
            tbSearchPharm0.Text = dvClientD(0)("sClientName").ToString.Trim
            ToolTip1.SetToolTip(tbSearchPharm0, tbSearchPharm0.Text.Trim)
            tbSearchPharmS0.Text = dvClientD(0)("sClientSocialReason").ToString.Trim
            ToolTip1.SetToolTip(tbSearchPharmS0, tbSearchPharmS0.Text.Trim)
            dvClientC.RowFilter = "iClientCode = " & dvClientD(0)("iClientCode")
            tbSearchBan0.Text = dvClientC(0)("sBanName").ToString.Trim
            tbSearchAdress0.Text = dvClientD(0)("sClientAddress").ToString.Trim
            ToolTip1.SetToolTip(tbSearchAdress0, tbSearchAdress0.Text.Trim)
            tbSearchVille0.Text = dvClientD(0)("sClientCity").ToString.Trim
            tbSearchCodePostal0.Text = dvClientD(0)("sClientPC").ToString.Trim
        Else
            Dim reply As DialogResult = MessageBox.Show("Code client inexistant",
                                "Code Client inexistant", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub


    Private Sub cb1_CheckedChanged(sender As Object, e As EventArgs) Handles cb1.Click
        cb1.Checked = True
        cb2.Checked = False
        cb3.Checked = False
        cb4.Checked = False
        cb5.Checked = False
        cb6.Checked = False
        tbPharmaID.Text = tbSearchPharmaID1.Text
        Search_Pharm()
    End Sub

    Private Sub cb2_CheckedChanged(sender As Object, e As EventArgs) Handles cb2.Click
        cb1.Checked = False
        cb2.Checked = True
        cb3.Checked = False
        cb4.Checked = False
        cb5.Checked = False
        cb6.Checked = False
        tbPharmaID.Text = tbSearchPharmaID2.Text
        Search_Pharm()
    End Sub

    Private Sub cb3_CheckedChanged(sender As Object, e As EventArgs) Handles cb3.Click
        cb1.Checked = False
        cb2.Checked = False
        cb3.Checked = True
        cb4.Checked = False
        cb5.Checked = False
        cb6.Checked = False
        tbPharmaID.Text = tbSearchPharmaID3.Text
        Search_Pharm()
    End Sub

    Private Sub cb4_CheckedChanged(sender As Object, e As EventArgs) Handles cb4.Click
        cb1.Checked = False
        cb2.Checked = False
        cb3.Checked = False
        cb4.Checked = True
        cb5.Checked = False
        cb6.Checked = False
        tbPharmaID.Text = tbSearchPharmaID4.Text
        Search_Pharm()
    End Sub

    Private Sub cb5_CheckedChanged(sender As Object, e As EventArgs) Handles cb5.Click
        cb1.Checked = False
        cb2.Checked = False
        cb3.Checked = False
        cb4.Checked = False
        cb5.Checked = True
        cb6.Checked = False
        tbPharmaID.Text = tbSearchPharmaID5.Text
        Search_Pharm()
    End Sub

    Private Sub cb6_CheckedChanged(sender As Object, e As EventArgs) Handles cb6.Click
        cb1.Checked = False
        cb2.Checked = False
        cb3.Checked = False
        cb4.Checked = False
        cb5.Checked = False
        cb6.Checked = True
        tbPharmaID.Text = tbSearchPharmaID6.Text
        Search_Pharm()
    End Sub

End Class