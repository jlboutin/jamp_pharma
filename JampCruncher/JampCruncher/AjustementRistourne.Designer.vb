﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class AjustementRistourne
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.dgvTransactions = New System.Windows.Forms.DataGridView()
        Me.cbMonth = New System.Windows.Forms.ComboBox()
        Me.tbPharmaID = New System.Windows.Forms.TextBox()
        Me.lbPharmaID = New System.Windows.Forms.Label()
        Me.cbYear = New System.Windows.Forms.ComboBox()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        CType(Me.dgvTransactions, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvTransactions
        '
        Me.dgvTransactions.AllowUserToAddRows = False
        Me.dgvTransactions.AllowUserToDeleteRows = False
        Me.dgvTransactions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTransactions.Location = New System.Drawing.Point(25, 124)
        Me.dgvTransactions.Name = "dgvTransactions"
        Me.dgvTransactions.Size = New System.Drawing.Size(794, 407)
        Me.dgvTransactions.TabIndex = 23
        '
        'cbMonth
        '
        Me.cbMonth.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbMonth.FormattingEnabled = True
        Me.cbMonth.ItemHeight = 20
        Me.cbMonth.Items.AddRange(New Object() {"Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"})
        Me.cbMonth.Location = New System.Drawing.Point(254, 33)
        Me.cbMonth.Name = "cbMonth"
        Me.cbMonth.Size = New System.Drawing.Size(146, 28)
        Me.cbMonth.TabIndex = 17
        '
        'tbPharmaID
        '
        Me.tbPharmaID.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbPharmaID.Location = New System.Drawing.Point(399, 83)
        Me.tbPharmaID.Name = "tbPharmaID"
        Me.tbPharmaID.Size = New System.Drawing.Size(100, 26)
        Me.tbPharmaID.TabIndex = 21
        '
        'lbPharmaID
        '
        Me.lbPharmaID.AutoSize = True
        Me.lbPharmaID.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbPharmaID.Location = New System.Drawing.Point(310, 89)
        Me.lbPharmaID.Name = "lbPharmaID"
        Me.lbPharmaID.Size = New System.Drawing.Size(83, 17)
        Me.lbPharmaID.TabIndex = 19
        Me.lbPharmaID.Text = "PharmaID:"
        '
        'cbYear
        '
        Me.cbYear.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbYear.FormattingEnabled = True
        Me.cbYear.Location = New System.Drawing.Point(441, 33)
        Me.cbYear.Name = "cbYear"
        Me.cbYear.Size = New System.Drawing.Size(121, 28)
        Me.cbYear.TabIndex = 18
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(277, 569)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(123, 33)
        Me.btnSave.TabIndex = 24
        Me.btnSave.Text = "Sauvegarder"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(441, 569)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(114, 33)
        Me.btnExit.TabIndex = 25
        Me.btnExit.Text = "Quitter"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'AjustementRistourne
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(846, 635)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.dgvTransactions)
        Me.Controls.Add(Me.tbPharmaID)
        Me.Controls.Add(Me.lbPharmaID)
        Me.Controls.Add(Me.cbYear)
        Me.Controls.Add(Me.cbMonth)
        Me.Name = "AjustementRistourne"
        Me.Text = "AjustementRistourne"
        CType(Me.dgvTransactions, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvTransactions As DataGridView
    Friend WithEvents cbMonth As ComboBox
    Friend WithEvents tbPharmaID As TextBox
    Friend WithEvents lbPharmaID As Label
    Friend WithEvents cbYear As ComboBox
    Friend WithEvents btnSave As Button
    Friend WithEvents btnExit As Button
End Class
