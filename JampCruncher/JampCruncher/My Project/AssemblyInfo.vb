﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Jamp Cruncher")>
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Connexxium")>
<Assembly: AssemblyProduct("Jamp Cruncher")>
<Assembly: AssemblyCopyright("Copyright © Connexxium 2018")>
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("6380ef36-bb07-4712-b903-bfafe39fd25a")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.1.14.0")>
<Assembly: AssemblyFileVersion("1.1.14.0")>
