﻿Imports System.Data.SqlClient
Imports MySql.Data.MySqlClient

Module DBAcess
    Function GRABB_Connection() As String
        Dim ConnectionString As String

        'If My.Computer.Name = "CONNEXXIUM-DEV" Then
        '    ConnectionString = My.Settings.BiomedERP_ConnectionStringConnexxium
        '    Return ConnectionString
        'End If

        ConnectionString = My.Settings.GRABB_CS
        Return ConnectionString

    End Function

    Function GSS_Connection() As String
        Dim ConnectionString As String

        'If My.Computer.Name = "CONNEXXIUM-DEV" Then
        '    ConnectionString = My.Settings.BiomedERP_ConnectionStringConnexxium
        '    Return ConnectionString
        'End If

        ConnectionString = My.Settings.GSS_CS
        Return ConnectionString

        'Dim CipherText() As Byte = Obviex.CipherLite.Encoder.HexDecode(ConnectionString)
        'Return Obviex.CipherLite.Rijndael.Decrypt(CipherText, Application.CompanyName, "", 256, 1, "", "MD5")
    End Function

    Function CRM_Connection() As String
        Dim ConnectionString As String

        'If My.Computer.Name = "CONNEXXIUM-DEV" Then
        '    ConnectionString = My.Settings.BiomedERP_ConnectionStringConnexxium
        '    Return ConnectionString
        'End If

        ConnectionString = My.Settings.CRM_CS
        Return ConnectionString

    End Function

    Function SFilter(field As String) As String
        Return ("'" & field.Trim.Replace("'", "''") & "'")
    End Function

    Function DB_Get_Next_Index(Field As String, Table As String, db_connection As SqlConnection) As Integer
        Dim sIndex As String
        Dim Index As Integer

        sIndex = DB_Exec_Scalar("SELECT TOP(1) " & Field & " FROM " & Table & " ORDER BY " & Field & " DESC", db_connection)
        Integer.TryParse(sIndex, Index)
        DB_Get_Next_Index = Index + 1
    End Function

    Function DB_Exec_Scalar(Query As String, db_connection As SqlConnection) As Object
        Dim db_command As SqlCommand = New SqlCommand(Query, db_connection)
        DB_Exec_Scalar = db_command.ExecuteScalar()
        db_command.Dispose()
        If DB_Exec_Scalar Is Nothing Or IsDBNull(DB_Exec_Scalar) Then
            DB_Exec_Scalar = ""
        End If
    End Function

    Function DB_Exec_Scalar_MySQL(Query As String, db_connection As MySqlConnection) As Object
        Dim db_command As MySqlCommand = New MySqlCommand(Query, db_connection)
        DB_Exec_Scalar_MySQL = db_command.ExecuteScalar()
        db_command.Dispose()
        If DB_Exec_Scalar_MySQL Is Nothing Or IsDBNull(DB_Exec_Scalar_MySQL) Then
            DB_Exec_Scalar_MySQL = ""
        End If
    End Function

    Sub DB_Exec_NonQuery(Query As String, db_connection As SqlConnection)
        If Query.Substring(0, 6) = "Select" Then
            Query = "Trouble"
        End If
        Dim db_command As SqlCommand = New SqlCommand(Query, db_connection)
        db_command.ExecuteNonQuery()
        db_command.Dispose()
    End Sub

    Sub DB_Exec_NonQuery_MySQL(Query As String, db_connection As MySqlConnection)
        If Query.Substring(0, 6) = "Select" Then
            Query = "Trouble"
        End If
        Dim db_command As MySqlCommand = New MySqlCommand(Query, db_connection)
        db_command.ExecuteNonQuery()
        db_command.Dispose()
    End Sub

    Function Extract_DataField(ByRef DataLine As DataRow, ByRef Field As String) As String
        Extract_DataField = ""
        Try
            If Not (DataLine(Field) Is Nothing Or IsDBNull(DataLine(Field))) Then
                Extract_DataField = DataLine(Field).ToString.Trim
            End If
        Catch
            If Not DataLine.Table.Columns.Contains(Field) Then
                Dim Result As DialogResult
                Result = MessageBox.Show("Undefined Field " & Field, Extract_DataField, MessageBoxButtons.OKCancel)
                If Result = System.Windows.Forms.DialogResult.Cancel Then Environment.Exit(0)
            End If
        End Try
    End Function

    Function Extract_DataReader(DataReader As SqlDataReader, ByRef Field As String)
        Extract_DataReader = ""
        Try
            If Not (DataReader(Field) Is Nothing Or IsDBNull(DataReader(Field))) Then
                Extract_DataReader = DataReader(Field).ToString.Trim
            End If
        Catch
        End Try
    End Function

    Function Data_Transfert(Query As String, db_connection_Source As SqlConnection, Destination_table As String, db_connection_Destination_String As String) As Boolean
        Dim db_connection_Destination As SqlConnection
        Dim Done As Boolean = False
        Dim nbRecordSource As Integer
        Dim nbRecordDestination As Integer = 0
        Dim nbRecordTransfert As Integer = 0
        Dim nbRetry As Integer = 0


        db_connection_Destination = New SqlConnection(db_connection_Destination_String)
        db_connection_Destination.Open()
        nbRecordSource = Count_Row(db_connection_Source, Query)
        nbRecordDestination = Count_Row(db_connection_Destination, "SELECT * FROM " & Destination_table)

        While Done = False
            Dim db_command As SqlCommand
            Dim datareader As SqlDataReader

            ' Get data from the source table as a SqlDataReader.
            db_command = New SqlCommand(Query, db_connection_Source)
            datareader = db_command.ExecuteReader
            db_command.Dispose()
            If datareader.HasRows Then
                Using bulkCopy As SqlBulkCopy = New SqlBulkCopy(db_connection_Destination_String)
                    bulkCopy.DestinationTableName = Destination_table
                    bulkCopy.BulkCopyTimeout = 300
                    bulkCopy.BatchSize = 1000
                    Try
                        ' try to Write from the source to the destination.
                        bulkCopy.WriteToServer(datareader)
                        Data_Transfert = True
                    Catch ex As Exception
                        MsgBox("Problème de mise à jour avec GRABB" & vbLf & ex.Message, MsgBoxStyle.Critical)
                    End Try
                End Using
            Else
                Data_Transfert = True
            End If
            datareader.Close()
            nbRecordTransfert = Count_Row(db_connection_Destination, "SELECT * FROM " & Destination_table)
            If nbRecordSource = nbRecordTransfert - nbRecordDestination Then Done = True
            nbRetry += 1
            If nbRetry = 10 Then
                Done = True
                MsgBox("Impossible de synchroniser avec GRABB", MsgBoxStyle.Critical)
            End If
        End While
        Data_Transfert = False
        db_connection_Destination.Close()
        db_connection_Destination.Dispose()

    End Function

    Function Extract_Field(ByVal Field As String, ByRef Datareader As SqlDataReader) As String
        If Datareader(Field) Is Nothing Or IsDBNull(Datareader(Field)) Then
            Extract_Field = ""
        Else
            Extract_Field = Datareader(Field).ToString
        End If
    End Function

    Function Count_Row(db_connection As SqlConnection, Query As String) As Integer
        Count_Row = CInt(DB_Exec_Scalar("SELECT COUNT(*) " &
                                         Query.Substring(Query.IndexOf("FROM")), db_connection))
    End Function

    Function Count_Row_MySQL(db_connection As MySqlConnection, Query As String) As Integer
        Count_Row_MySQL = CInt(DB_Exec_Scalar_MySQL("SELECT COUNT(*) " &
                                         Query.Substring(Query.IndexOf("FROM")), db_connection))
    End Function


    Function Count_Count_Row(db_connection As SqlConnection, Query As String) As Integer
        Count_Count_Row = CInt(DB_Exec_Scalar("SELECT Count(*) FROM (SELECT COUNT(*) As cnt " &
                                         Query.Substring(Query.IndexOf("FROM")) &
                                         ") AS Counts", db_connection))
    End Function

    Function GenerateFTPPath(ByRef PharmaID) As String
        Dim wrapper As New Simple3Des(Application.CompanyName)
        Dim cipherText As String
        Dim iLoop As Integer

        cipherText = wrapper.EncryptData(PharmaID).Replace("/", "_").Replace("+", "_")
        GenerateFTPPath = ""
        For iLoop = 0 To PharmaID.Length
            GenerateFTPPath &= cipherText.Substring(iLoop * 3, 1)
        Next
    End Function

    Sub Load_Array(Query As String, db_connection As SqlConnection, ByRef arList As ArrayList)
        Dim db_command As SqlCommand
        Dim datareader As SqlDataReader
        db_command = New SqlCommand(Query, db_connection)
        datareader = db_command.ExecuteReader()
        db_command.Dispose()
        While datareader.Read()
            Dim dict As New Dictionary(Of String, Object)
            For count As Integer = 0 To (datareader.FieldCount - 1)
                dict.Add(datareader.GetName(count), datareader(count))
            Next
            arList.Add(dict)
        End While
        datareader.Close()
    End Sub

End Module
