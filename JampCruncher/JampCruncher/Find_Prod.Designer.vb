﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Find_Prod
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblCompanie = New System.Windows.Forms.Label()
        Me.btnQuit = New System.Windows.Forms.Button()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tbCode = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.tbFR = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblDistri = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.tbProduit = New System.Windows.Forms.TextBox()
        Me.btUse = New System.Windows.Forms.Button()
        Me.tbSearchProdFR1 = New System.Windows.Forms.TextBox()
        Me.tbSearchProdEN1 = New System.Windows.Forms.TextBox()
        Me.tbSearchProdUPC1 = New System.Windows.Forms.TextBox()
        Me.tbSearchProdUPC2 = New System.Windows.Forms.TextBox()
        Me.tbSearchProdEN2 = New System.Windows.Forms.TextBox()
        Me.tbSearchProdFR2 = New System.Windows.Forms.TextBox()
        Me.tbSearchProdUPC3 = New System.Windows.Forms.TextBox()
        Me.tbSearchProdEN3 = New System.Windows.Forms.TextBox()
        Me.tbSearchProdFR3 = New System.Windows.Forms.TextBox()
        Me.tbSearchProdUPC4 = New System.Windows.Forms.TextBox()
        Me.tbSearchProdEN4 = New System.Windows.Forms.TextBox()
        Me.tbSearchProdFR4 = New System.Windows.Forms.TextBox()
        Me.tbSearchProdUPC5 = New System.Windows.Forms.TextBox()
        Me.tbSearchProdEN5 = New System.Windows.Forms.TextBox()
        Me.tbSearchProdFR5 = New System.Windows.Forms.TextBox()
        Me.tbSearchProdUPC6 = New System.Windows.Forms.TextBox()
        Me.tbSearchProdEN6 = New System.Windows.Forms.TextBox()
        Me.tbSearchProdFR6 = New System.Windows.Forms.TextBox()
        Me.cb1 = New System.Windows.Forms.CheckBox()
        Me.cb2 = New System.Windows.Forms.CheckBox()
        Me.cb3 = New System.Windows.Forms.CheckBox()
        Me.cb4 = New System.Windows.Forms.CheckBox()
        Me.cb5 = New System.Windows.Forms.CheckBox()
        Me.cb6 = New System.Windows.Forms.CheckBox()
        Me.tbSearchProdEN0 = New System.Windows.Forms.TextBox()
        Me.tbSearchProdFR0 = New System.Windows.Forms.TextBox()
        Me.tbSearchProdUPC0 = New System.Windows.Forms.TextBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cbSkip = New System.Windows.Forms.CheckBox()
        Me.lblLigne = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lblCompanie
        '
        Me.lblCompanie.AutoSize = True
        Me.lblCompanie.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompanie.Location = New System.Drawing.Point(12, 36)
        Me.lblCompanie.Name = "lblCompanie"
        Me.lblCompanie.Size = New System.Drawing.Size(99, 20)
        Me.lblCompanie.TabIndex = 8
        Me.lblCompanie.Text = "Compagnie"
        '
        'btnQuit
        '
        Me.btnQuit.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnQuit.Location = New System.Drawing.Point(471, 243)
        Me.btnQuit.Name = "btnQuit"
        Me.btnQuit.Size = New System.Drawing.Size(74, 29)
        Me.btnQuit.TabIndex = 9
        Me.btnQuit.Text = "Passer"
        Me.btnQuit.UseVisualStyleBackColor = True
        '
        'btnSearch
        '
        Me.btnSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSearch.Location = New System.Drawing.Point(252, 244)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(110, 29)
        Me.btnSearch.TabIndex = 7
        Me.btnSearch.Text = "Rechercher"
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(209, 53)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(182, 20)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Recherche de produit"
        '
        'tbCode
        '
        Me.tbCode.Enabled = False
        Me.tbCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCode.Location = New System.Drawing.Point(16, 103)
        Me.tbCode.Name = "tbCode"
        Me.tbCode.Size = New System.Drawing.Size(230, 21)
        Me.tbCode.TabIndex = 10
        Me.tbCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(265, 104)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(81, 17)
        Me.Label4.TabIndex = 15
        Me.Label4.Text = "Code UPC"
        '
        'tbFR
        '
        Me.tbFR.Enabled = False
        Me.tbFR.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbFR.Location = New System.Drawing.Point(16, 137)
        Me.tbFR.Name = "tbFR"
        Me.tbFR.Size = New System.Drawing.Size(230, 20)
        Me.tbFR.TabIndex = 12
        Me.tbFR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(288, 150)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(29, 17)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "EN"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(289, 126)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(28, 17)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "FR"
        '
        'lblDistri
        '
        Me.lblDistri.AutoSize = True
        Me.lblDistri.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDistri.Location = New System.Drawing.Point(12, 60)
        Me.lblDistri.Name = "lblDistri"
        Me.lblDistri.Size = New System.Drawing.Size(103, 20)
        Me.lblDistri.TabIndex = 16
        Me.lblDistri.Text = "Distributeur"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(139, 192)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(107, 17)
        Me.Label8.TabIndex = 23
        Me.Label8.Text = "Code Produit:"
        '
        'tbProduit
        '
        Me.tbProduit.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbProduit.Location = New System.Drawing.Point(249, 192)
        Me.tbProduit.MaxLength = 20
        Me.tbProduit.Name = "tbProduit"
        Me.tbProduit.Size = New System.Drawing.Size(109, 21)
        Me.tbProduit.TabIndex = 24
        '
        'btUse
        '
        Me.btUse.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btUse.Location = New System.Drawing.Point(373, 244)
        Me.btUse.Name = "btUse"
        Me.btUse.Size = New System.Drawing.Size(90, 29)
        Me.btUse.TabIndex = 25
        Me.btUse.Text = "Utiliser"
        Me.btUse.UseVisualStyleBackColor = True
        '
        'tbSearchProdFR1
        '
        Me.tbSearchProdFR1.Enabled = False
        Me.tbSearchProdFR1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchProdFR1.Location = New System.Drawing.Point(736, 126)
        Me.tbSearchProdFR1.Name = "tbSearchProdFR1"
        Me.tbSearchProdFR1.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchProdFR1.TabIndex = 26
        '
        'tbSearchProdEN1
        '
        Me.tbSearchProdEN1.Enabled = False
        Me.tbSearchProdEN1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchProdEN1.Location = New System.Drawing.Point(736, 149)
        Me.tbSearchProdEN1.Name = "tbSearchProdEN1"
        Me.tbSearchProdEN1.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchProdEN1.TabIndex = 27
        '
        'tbSearchProdUPC1
        '
        Me.tbSearchProdUPC1.Enabled = False
        Me.tbSearchProdUPC1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchProdUPC1.Location = New System.Drawing.Point(736, 101)
        Me.tbSearchProdUPC1.MaxLength = 7
        Me.tbSearchProdUPC1.Name = "tbSearchProdUPC1"
        Me.tbSearchProdUPC1.Size = New System.Drawing.Size(188, 21)
        Me.tbSearchProdUPC1.TabIndex = 31
        '
        'tbSearchProdUPC2
        '
        Me.tbSearchProdUPC2.Enabled = False
        Me.tbSearchProdUPC2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchProdUPC2.Location = New System.Drawing.Point(930, 101)
        Me.tbSearchProdUPC2.MaxLength = 7
        Me.tbSearchProdUPC2.Name = "tbSearchProdUPC2"
        Me.tbSearchProdUPC2.Size = New System.Drawing.Size(188, 21)
        Me.tbSearchProdUPC2.TabIndex = 37
        '
        'tbSearchProdEN2
        '
        Me.tbSearchProdEN2.Enabled = False
        Me.tbSearchProdEN2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchProdEN2.Location = New System.Drawing.Point(930, 149)
        Me.tbSearchProdEN2.Name = "tbSearchProdEN2"
        Me.tbSearchProdEN2.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchProdEN2.TabIndex = 33
        '
        'tbSearchProdFR2
        '
        Me.tbSearchProdFR2.Enabled = False
        Me.tbSearchProdFR2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchProdFR2.Location = New System.Drawing.Point(930, 126)
        Me.tbSearchProdFR2.Name = "tbSearchProdFR2"
        Me.tbSearchProdFR2.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchProdFR2.TabIndex = 32
        '
        'tbSearchProdUPC3
        '
        Me.tbSearchProdUPC3.Enabled = False
        Me.tbSearchProdUPC3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchProdUPC3.Location = New System.Drawing.Point(1124, 100)
        Me.tbSearchProdUPC3.MaxLength = 7
        Me.tbSearchProdUPC3.Name = "tbSearchProdUPC3"
        Me.tbSearchProdUPC3.Size = New System.Drawing.Size(188, 21)
        Me.tbSearchProdUPC3.TabIndex = 43
        '
        'tbSearchProdEN3
        '
        Me.tbSearchProdEN3.Enabled = False
        Me.tbSearchProdEN3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchProdEN3.Location = New System.Drawing.Point(1124, 148)
        Me.tbSearchProdEN3.Name = "tbSearchProdEN3"
        Me.tbSearchProdEN3.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchProdEN3.TabIndex = 39
        '
        'tbSearchProdFR3
        '
        Me.tbSearchProdFR3.Enabled = False
        Me.tbSearchProdFR3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchProdFR3.Location = New System.Drawing.Point(1124, 125)
        Me.tbSearchProdFR3.Name = "tbSearchProdFR3"
        Me.tbSearchProdFR3.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchProdFR3.TabIndex = 38
        '
        'tbSearchProdUPC4
        '
        Me.tbSearchProdUPC4.Enabled = False
        Me.tbSearchProdUPC4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchProdUPC4.Location = New System.Drawing.Point(1318, 101)
        Me.tbSearchProdUPC4.MaxLength = 7
        Me.tbSearchProdUPC4.Name = "tbSearchProdUPC4"
        Me.tbSearchProdUPC4.Size = New System.Drawing.Size(188, 21)
        Me.tbSearchProdUPC4.TabIndex = 49
        '
        'tbSearchProdEN4
        '
        Me.tbSearchProdEN4.Enabled = False
        Me.tbSearchProdEN4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchProdEN4.Location = New System.Drawing.Point(1318, 149)
        Me.tbSearchProdEN4.Name = "tbSearchProdEN4"
        Me.tbSearchProdEN4.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchProdEN4.TabIndex = 45
        '
        'tbSearchProdFR4
        '
        Me.tbSearchProdFR4.Enabled = False
        Me.tbSearchProdFR4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchProdFR4.Location = New System.Drawing.Point(1318, 126)
        Me.tbSearchProdFR4.Name = "tbSearchProdFR4"
        Me.tbSearchProdFR4.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchProdFR4.TabIndex = 44
        '
        'tbSearchProdUPC5
        '
        Me.tbSearchProdUPC5.Enabled = False
        Me.tbSearchProdUPC5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchProdUPC5.Location = New System.Drawing.Point(1512, 100)
        Me.tbSearchProdUPC5.MaxLength = 7
        Me.tbSearchProdUPC5.Name = "tbSearchProdUPC5"
        Me.tbSearchProdUPC5.Size = New System.Drawing.Size(188, 21)
        Me.tbSearchProdUPC5.TabIndex = 55
        '
        'tbSearchProdEN5
        '
        Me.tbSearchProdEN5.Enabled = False
        Me.tbSearchProdEN5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchProdEN5.Location = New System.Drawing.Point(1512, 148)
        Me.tbSearchProdEN5.Name = "tbSearchProdEN5"
        Me.tbSearchProdEN5.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchProdEN5.TabIndex = 51
        '
        'tbSearchProdFR5
        '
        Me.tbSearchProdFR5.Enabled = False
        Me.tbSearchProdFR5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchProdFR5.Location = New System.Drawing.Point(1512, 125)
        Me.tbSearchProdFR5.Name = "tbSearchProdFR5"
        Me.tbSearchProdFR5.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchProdFR5.TabIndex = 50
        '
        'tbSearchProdUPC6
        '
        Me.tbSearchProdUPC6.Enabled = False
        Me.tbSearchProdUPC6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchProdUPC6.Location = New System.Drawing.Point(1706, 100)
        Me.tbSearchProdUPC6.MaxLength = 7
        Me.tbSearchProdUPC6.Name = "tbSearchProdUPC6"
        Me.tbSearchProdUPC6.Size = New System.Drawing.Size(188, 21)
        Me.tbSearchProdUPC6.TabIndex = 61
        '
        'tbSearchProdEN6
        '
        Me.tbSearchProdEN6.Enabled = False
        Me.tbSearchProdEN6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchProdEN6.Location = New System.Drawing.Point(1706, 148)
        Me.tbSearchProdEN6.Name = "tbSearchProdEN6"
        Me.tbSearchProdEN6.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchProdEN6.TabIndex = 57
        '
        'tbSearchProdFR6
        '
        Me.tbSearchProdFR6.Enabled = False
        Me.tbSearchProdFR6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchProdFR6.Location = New System.Drawing.Point(1706, 125)
        Me.tbSearchProdFR6.Name = "tbSearchProdFR6"
        Me.tbSearchProdFR6.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchProdFR6.TabIndex = 56
        '
        'cb1
        '
        Me.cb1.AutoSize = True
        Me.cb1.Location = New System.Drawing.Point(821, 197)
        Me.cb1.Name = "cb1"
        Me.cb1.Size = New System.Drawing.Size(15, 14)
        Me.cb1.TabIndex = 68
        Me.cb1.UseVisualStyleBackColor = True
        '
        'cb2
        '
        Me.cb2.AutoSize = True
        Me.cb2.Location = New System.Drawing.Point(1014, 197)
        Me.cb2.Name = "cb2"
        Me.cb2.Size = New System.Drawing.Size(15, 14)
        Me.cb2.TabIndex = 69
        Me.cb2.UseVisualStyleBackColor = True
        '
        'cb3
        '
        Me.cb3.AutoSize = True
        Me.cb3.Location = New System.Drawing.Point(1210, 197)
        Me.cb3.Name = "cb3"
        Me.cb3.Size = New System.Drawing.Size(15, 14)
        Me.cb3.TabIndex = 70
        Me.cb3.UseVisualStyleBackColor = True
        '
        'cb4
        '
        Me.cb4.AutoSize = True
        Me.cb4.Location = New System.Drawing.Point(1404, 199)
        Me.cb4.Name = "cb4"
        Me.cb4.Size = New System.Drawing.Size(15, 14)
        Me.cb4.TabIndex = 71
        Me.cb4.UseVisualStyleBackColor = True
        '
        'cb5
        '
        Me.cb5.AutoSize = True
        Me.cb5.Location = New System.Drawing.Point(1602, 199)
        Me.cb5.Name = "cb5"
        Me.cb5.Size = New System.Drawing.Size(15, 14)
        Me.cb5.TabIndex = 72
        Me.cb5.UseVisualStyleBackColor = True
        '
        'cb6
        '
        Me.cb6.AutoSize = True
        Me.cb6.Location = New System.Drawing.Point(1793, 199)
        Me.cb6.Name = "cb6"
        Me.cb6.Size = New System.Drawing.Size(15, 14)
        Me.cb6.TabIndex = 73
        Me.cb6.UseVisualStyleBackColor = True
        '
        'tbSearchProdEN0
        '
        Me.tbSearchProdEN0.Enabled = False
        Me.tbSearchProdEN0.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchProdEN0.Location = New System.Drawing.Point(357, 149)
        Me.tbSearchProdEN0.Name = "tbSearchProdEN0"
        Me.tbSearchProdEN0.Size = New System.Drawing.Size(315, 20)
        Me.tbSearchProdEN0.TabIndex = 75
        '
        'tbSearchProdFR0
        '
        Me.tbSearchProdFR0.Enabled = False
        Me.tbSearchProdFR0.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchProdFR0.Location = New System.Drawing.Point(357, 126)
        Me.tbSearchProdFR0.Name = "tbSearchProdFR0"
        Me.tbSearchProdFR0.Size = New System.Drawing.Size(315, 20)
        Me.tbSearchProdFR0.TabIndex = 74
        '
        'tbSearchProdUPC0
        '
        Me.tbSearchProdUPC0.Enabled = False
        Me.tbSearchProdUPC0.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchProdUPC0.Location = New System.Drawing.Point(357, 101)
        Me.tbSearchProdUPC0.MaxLength = 7
        Me.tbSearchProdUPC0.Name = "tbSearchProdUPC0"
        Me.tbSearchProdUPC0.Size = New System.Drawing.Size(315, 21)
        Me.tbSearchProdUPC0.TabIndex = 79
        '
        'ToolTip1
        '
        Me.ToolTip1.AutomaticDelay = 100
        Me.ToolTip1.AutoPopDelay = 10000
        Me.ToolTip1.InitialDelay = 100
        Me.ToolTip1.ReshowDelay = 20
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(13, 248)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(120, 18)
        Me.Label9.TabIndex = 80
        Me.Label9.Text = "Ne pas traiter :"
        Me.Label9.Visible = False
        '
        'cbSkip
        '
        Me.cbSkip.AutoSize = True
        Me.cbSkip.Location = New System.Drawing.Point(139, 252)
        Me.cbSkip.Name = "cbSkip"
        Me.cbSkip.Size = New System.Drawing.Size(15, 14)
        Me.cbSkip.TabIndex = 81
        Me.cbSkip.UseVisualStyleBackColor = True
        Me.cbSkip.Visible = False
        '
        'lblLigne
        '
        Me.lblLigne.AutoSize = True
        Me.lblLigne.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLigne.Location = New System.Drawing.Point(413, 53)
        Me.lblLigne.Name = "lblLigne"
        Me.lblLigne.Size = New System.Drawing.Size(53, 20)
        Me.lblLigne.TabIndex = 82
        Me.lblLigne.Text = "Ligne"
        '
        'Find_Prod
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 18.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1900, 291)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblLigne)
        Me.Controls.Add(Me.cbSkip)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.tbSearchProdUPC0)
        Me.Controls.Add(Me.tbSearchProdEN0)
        Me.Controls.Add(Me.tbSearchProdFR0)
        Me.Controls.Add(Me.cb6)
        Me.Controls.Add(Me.cb5)
        Me.Controls.Add(Me.cb4)
        Me.Controls.Add(Me.cb3)
        Me.Controls.Add(Me.cb2)
        Me.Controls.Add(Me.cb1)
        Me.Controls.Add(Me.tbSearchProdUPC6)
        Me.Controls.Add(Me.tbSearchProdEN6)
        Me.Controls.Add(Me.tbSearchProdFR6)
        Me.Controls.Add(Me.tbSearchProdUPC5)
        Me.Controls.Add(Me.tbSearchProdEN5)
        Me.Controls.Add(Me.tbSearchProdFR5)
        Me.Controls.Add(Me.tbSearchProdUPC4)
        Me.Controls.Add(Me.tbSearchProdEN4)
        Me.Controls.Add(Me.tbSearchProdFR4)
        Me.Controls.Add(Me.tbSearchProdUPC3)
        Me.Controls.Add(Me.tbSearchProdEN3)
        Me.Controls.Add(Me.tbSearchProdFR3)
        Me.Controls.Add(Me.tbSearchProdUPC2)
        Me.Controls.Add(Me.tbSearchProdEN2)
        Me.Controls.Add(Me.tbSearchProdFR2)
        Me.Controls.Add(Me.tbSearchProdUPC1)
        Me.Controls.Add(Me.tbSearchProdEN1)
        Me.Controls.Add(Me.tbSearchProdFR1)
        Me.Controls.Add(Me.btUse)
        Me.Controls.Add(Me.tbProduit)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.lblDistri)
        Me.Controls.Add(Me.tbCode)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.tbFR)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblCompanie)
        Me.Controls.Add(Me.btnQuit)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "Find_Prod"
        Me.Text = "Recherche de produit"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblCompanie As System.Windows.Forms.Label
    Friend WithEvents btnQuit As System.Windows.Forms.Button
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tbCode As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tbFR As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblDistri As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents tbProduit As System.Windows.Forms.TextBox
    Friend WithEvents btUse As System.Windows.Forms.Button
    Friend WithEvents tbSearchProdFR1 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchProdEN1 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchProdUPC1 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchProdUPC2 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchProdEN2 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchProdFR2 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchProdUPC3 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchProdEN3 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchProdFR3 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchProdUPC4 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchProdEN4 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchProdFR4 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchProdUPC5 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchProdEN5 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchProdFR5 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchProdUPC6 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchProdEN6 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchProdFR6 As System.Windows.Forms.TextBox
    Friend WithEvents cb1 As System.Windows.Forms.CheckBox
    Friend WithEvents cb2 As System.Windows.Forms.CheckBox
    Friend WithEvents cb3 As System.Windows.Forms.CheckBox
    Friend WithEvents cb4 As System.Windows.Forms.CheckBox
    Friend WithEvents cb5 As System.Windows.Forms.CheckBox
    Friend WithEvents cb6 As System.Windows.Forms.CheckBox
    Friend WithEvents tbSearchProdEN0 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchProdFR0 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchProdUPC0 As System.Windows.Forms.TextBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cbSkip As System.Windows.Forms.CheckBox
    Friend WithEvents lblLigne As Label
End Class
