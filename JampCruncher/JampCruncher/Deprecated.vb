﻿Imports System.Data.SqlClient

Module Deprecated

    Sub Import_Cumul_Groupe()

        Dim result As DataSet
        Dim count As Integer
        Dim db_connection As SqlConnection
        Dim sAmount As String
        Dim GroupeID As Integer
        Dim Groupe_Cumul As Double

        db_connection = New SqlConnection(GSS_Connection)
        db_connection.Open()
        DB_Exec_NonQuery("TRUNCATE TABLE Cumul_Groupe", db_connection)

        result = getDataset()
        count = result.Tables(0).Rows.Count
        For i As Integer = 0 To count - 1
            GroupeID = CInt(result.Tables(0).Rows(i)(0).ToString)
            sAmount = result.Tables(0).Rows(i)(2).ToString
            sAmount = sAmount.Replace("$", "")
            Decimal.TryParse(sAmount, Groupe_Cumul)
            If Groupe_Cumul = 0D Then Decimal.TryParse(sAmount.Replace(".", ","), Groupe_Cumul)
            DB_Exec_NonQuery("INSERT INTO Cumul_Groupe (GroupeID,Groupe_Cumul) VALUES ('" _
                 & GroupeID & "','" _
                 & Groupe_Cumul.ToString.Replace(",", ".") & "');", db_connection)
        Next
        db_connection.Close()
        db_connection.Dispose()
    End Sub

    Sub Import_Cumul_Pharma()

        Dim result As DataSet
        Dim count As Integer
        Dim db_connection As SqlConnection
        Dim sAmount As String
        Dim PharmaID As String
        Dim PharmacieID As Integer
        Dim Pharma_Cumul As Double
        Dim SearchLoop As Integer
        Dim ClientArray As New ArrayList()

        db_connection = New SqlConnection(GSS_Connection)
        db_connection.Open()
        DB_Exec_NonQuery("TRUNCATE TABLE Cumul_Pharma", db_connection)

        Load_Array("SELECT * FROM Pharmacie", db_connection, ClientArray)

        result = getDataset()
        count = result.Tables(0).Rows.Count
        For i As Integer = 0 To count - 1
            PharmaID = result.Tables(0).Rows(i)(0).ToString
            sAmount = result.Tables(0).Rows(i)(1).ToString
            sAmount = sAmount.Replace("$", "")
            Decimal.TryParse(sAmount, Pharma_Cumul)
            If Pharma_Cumul = 0D Then Decimal.TryParse(sAmount.Replace(".", ","), Pharma_Cumul)
            For SearchLoop = 0 To ClientArray.Count - 1
                If ClientArray(SearchLoop)("PharmaID") = PharmaID Then
                    PharmacieID = ClientArray(SearchLoop)("PharmacieID")
                    DB_Exec_NonQuery("INSERT INTO Cumul_Pharma (PharmacieID,Pharma_Cumul) VALUES ('" _
                                         & PharmacieID & "','" _
                                         & Pharma_Cumul.ToString.Replace(",", ".") & "');", db_connection)
                    Exit For
                End If
            Next
        Next
        db_connection.Close()
        db_connection.Dispose()
    End Sub

    Sub Import_Pharma4()

        Dim result As DataSet
        Dim count As Integer
        Dim RaisonSociale As String
        Dim Client As String
        Dim Adresse As String
        Dim Ville As String
        Dim Province As String
        Dim CodePostal As String
        Dim Telephone As String
        Dim Fax As String
        Dim Groupe As String
        Dim GroupeID As Integer
        Dim Banniere As String
        Dim BanniereID As Integer
        Dim Rep As String
        Dim Uniprix As String
        Dim Uniprix_f As Boolean
        Dim RepID As Integer
        Dim CodeClient As String
        Dim db_command As SqlCommand
        Dim datareader As SqlDataReader
        Dim Query As String
        Dim db_connection As SqlConnection

        db_connection = New SqlConnection(GSS_Connection)
        db_connection.Open()
        DB_Exec_NonQuery("TRUNCATE TABLE Pharmacie", db_connection)
        'Importation List Pharm V.4
        result = getDataset()
        count = result.Tables(0).Rows.Count
        For i As Integer = 0 To count - 1
            CodeClient = result.Tables(0).Rows(i)(0).ToString()
            If CodeClient.Length = 7 Then
                RaisonSociale = result.Tables(0).Rows(i)(1).ToString()
                Client = result.Tables(0).Rows(i)(2).ToString()
                Adresse = result.Tables(0).Rows(i)(3).ToString()
                Ville = result.Tables(0).Rows(i)(4).ToString()
                Province = result.Tables(0).Rows(i)(5).ToString()
                CodePostal = result.Tables(0).Rows(i)(6).ToString()
                Telephone = result.Tables(0).Rows(i)(7).ToString()
                Fax = result.Tables(0).Rows(i)(8).ToString()
                Banniere = result.Tables(0).Rows(i)(9).ToString()
                Rep = result.Tables(0).Rows(i)(10).ToString()
                Groupe = result.Tables(0).Rows(i)(11).ToString()
                Uniprix = result.Tables(0).Rows(i)(12).ToString()
                If Uniprix = "X" Then Uniprix_f = True Else Uniprix_f = False

                Query = "SELECT * FROM Banniere WHERE BanniereDesc = '" & Banniere.Replace("'", "''") & "'"
                db_command = New SqlCommand(Query, db_connection)
                datareader = db_command.ExecuteReader
                db_command.Dispose()
                If Not datareader.HasRows Then
                    DB_Exec_NonQuery("INSERT INTO Banniere (BanniereDesc) Values('" &
                                Banniere.Trim.Replace("'", "''") & "');", db_connection)
                    db_command = New SqlCommand(Query, db_connection) 'Redo Query
                    datareader = db_command.ExecuteReader
                    db_command.Dispose()
                End If
                datareader.Read()
                BanniereID = CInt(datareader("BanniereID"))

                If Groupe.Length > 0 Then
                    Query = "SELECT * FROM Groupe WHERE GroupeNom = '" & Groupe.Replace("'", "''") & "'"
                    db_command = New SqlCommand(Query, db_connection)
                    datareader = db_command.ExecuteReader
                    db_command.Dispose()
                    If Not datareader.HasRows Then
                        DB_Exec_NonQuery("INSERT INTO Groupe (GroupeNom) Values('" &
                                Groupe.Trim.Replace("'", "''") & "');", db_connection)
                        db_command = New SqlCommand(Query, db_connection) 'Redo Query
                        datareader = db_command.ExecuteReader
                        db_command.Dispose()
                    End If
                    datareader.Read()
                    GroupeID = CInt(datareader("GroupeID"))
                Else
                    GroupeID = 0
                End If

                If Rep <> "FERMÉ" Then
                    Query = "SELECT * FROM Representant WHERE Rep_Nom LIKE '" & Rep.Replace("'", "''") & "%'"
                    db_command = New SqlCommand(Query, db_connection)
                    datareader = db_command.ExecuteReader
                    db_command.Dispose()
                    If Not datareader.HasRows Then
                        DB_Exec_NonQuery("INSERT INTO Representant (Rep_Nom) Values('" &
                                Rep.Trim.Replace("'", "''") & "');", db_connection)
                        db_command = New SqlCommand(Query, db_connection) 'Redo Query
                        datareader = db_command.ExecuteReader
                        db_command.Dispose()
                    End If
                    datareader.Read()
                    RepID = CInt(datareader("RepID"))
                Else
                    RepID = 0
                End If

                DB_Exec_NonQuery("INSERT INTO Pharmacie (PharmaID,Raison_sociale,Client,Adresse,Ville," _
                     & "Province,Code_Postal,Telephone,Fax,Uniprix_F,BanniereID,GroupeID,RepID) VALUES ('" _
                     & CodeClient & "','" _
                     & RaisonSociale.Trim.Replace("'", "''") & "','" _
                     & Client.Trim.Replace("'", "''") & "','" _
                     & Adresse.Trim.Replace("'", "''") & "','" _
                     & Ville.Trim.Replace("'", "''") & "','" _
                     & Province.Trim.Replace("'", "''") & "','" _
                     & CodePostal & "','" _
                     & Telephone & "','" _
                     & Fax.Trim.Replace("'", "''") & "','" _
                     & Uniprix_f & "','" _
                     & BanniereID & "','" _
                     & GroupeID & "','" _
                     & RepID & "');", db_connection)
            End If
        Next
        db_connection.Close()
        db_connection.Dispose()
    End Sub


    Sub Merge_Pharmapar_list()

        Dim result As DataSet
        Dim count As Integer
        Dim Client As String
        Dim Fax As String
        Dim Groupe As String
        Dim Representant As String
        Dim CodePostal As String
        Dim Telephone As String
        Dim GroupeID As Integer
        Dim RepID As Integer
        Dim PharmacieID As Integer
        Dim CodeClient As String
        Dim LastCode As String = ""
        Dim db_command As SqlCommand
        Dim datareader As SqlDataReader = Nothing
        Dim Query As String
        Dim db_connection As SqlConnection

        db_connection = New SqlConnection(GSS_Connection)
        db_connection.Open()
        'Importation liste Pharmapar
        result = getDataset()
        count = result.Tables(1).Rows.Count
        For i As Integer = 0 To count - 1
            CodePostal = result.Tables(1).Rows(i)(4).ToString()
            Telephone = result.Tables(1).Rows(i)(7).ToString()
            If Telephone.Length > 6 And CodePostal.Length > 5 Then
                CodeClient = CodePostal.Substring(0, 3) & Telephone.Substring(Telephone.Length - 4, 4)
                If CodeClient <> LastCode And CodePostal <> "Code Postal" Then
                    Client = result.Tables(1).Rows(i)(2).ToString()
                    Fax = result.Tables(1).Rows(i)(8).ToString()
                    Groupe = result.Tables(1).Rows(i)(9).ToString()
                    Representant = result.Tables(1).Rows(i)(10).ToString()
                    If Groupe.Length > 5 Then
                        Query = "SELECT * FROM Groupe WHERE GroupeNom = '" & Groupe.Replace("'", "''") & "'"
                        db_command = New SqlCommand(Query, db_connection)
                        datareader = db_command.ExecuteReader
                        db_command.Dispose()
                        If Not datareader.HasRows Then
                            DB_Exec_NonQuery("INSERT INTO Groupe (GroupeNom) Values ('" &
                                    Groupe.Trim.Replace("'", "''") & "');", db_connection)
                            db_command = New SqlCommand(Query, db_connection) 'Redo Query
                            datareader = db_command.ExecuteReader
                            db_command.Dispose()
                        End If
                        datareader.Read()
                        GroupeID = CInt(datareader("GroupeID"))
                    Else
                        GroupeID = 0
                    End If
                    If Representant.Length > 1 Then
                        Query = "SELECT * FROM Representant WHERE Rep_Nom LIKE '" & Representant.Replace("'", "''") & "%'"
                        db_command = New SqlCommand(Query, db_connection)
                        datareader = db_command.ExecuteReader
                        db_command.Dispose()
                        If Not datareader.HasRows Then
                            DB_Exec_NonQuery("INSERT INTO Representant (Rep_Nom) Values ('" &
                                    Representant.Trim.Replace("'", "''") & "');", db_connection)
                            db_command = New SqlCommand(Query, db_connection) 'Redo Query
                            datareader = db_command.ExecuteReader
                            db_command.Dispose()
                        End If
                        datareader.Read()
                        RepID = CInt(datareader("RepID"))
                    Else
                        RepID = 0
                    End If
                    If CodeClient.Length > 6 Then
                        Query = "SELECT * FROM Pharmacie WHERE PharmaID = '" & CodeClient & "'"
                        Application.DoEvents()
                        db_command = New SqlCommand(Query, db_connection)
                        datareader = db_command.ExecuteReader
                        db_command.Dispose()
                        If datareader.HasRows Then
                            datareader.Read()
                            PharmacieID = CInt(datareader("PharmacieID"))
                        Else
                            PharmacieID = 0
                        End If
                    Else
                        PharmacieID = 0
                    End If
                    If PharmacieID <> 0 Then
                        DB_Exec_NonQuery("UPDATE Pharmacie SET Client = '" & Client.Trim.Replace("'", "''") &
                                "', Fax = '" & Fax &
                                "', GroupeID = '" & GroupeID &
                                "', RepID = '" & RepID &
                                "' WHERE PharmacieID = '" & PharmacieID & "';" _
                                , db_connection)
                        Application.DoEvents()
                        LastCode = CodeClient
                    End If

                End If
            End If
        Next
        db_connection.Close()
        db_connection.Dispose()
    End Sub

    Sub Import_Produit()

        Dim result As DataSet
        Dim count As Integer
        Dim ProduitCode As String
        Dim ProduitDesc As String
        Dim db_connection As SqlConnection

        db_connection = New SqlConnection(GSS_Connection)
        db_connection.Open()
        'Importation liste produit actif
        result = getDataset()
        count = result.Tables(2).Rows.Count
        For i As Integer = 0 To count - 1
            ProduitCode = result.Tables(2).Rows(i)(0).ToString()
            If ProduitCode.Length > 2 Then
                ProduitDesc = result.Tables(2).Rows(i)(1).ToString()
                If Not Count_Row(db_connection, "SELECT * FROM produit WHERE ProduitCode = '" _
                            & ProduitCode.Trim.Replace("'", "''") & "'") Then
                    DB_Exec_NonQuery("INSERT INTO produit (ProduitCode,ProduitDesc,Actif) Values ('" _
                            & ProduitCode.Trim.Replace("'", "''") & "','" &
                            ProduitDesc.Trim.Replace("'", "''") & "','true');", db_connection)
                End If
            End If
        Next
        'Importation liste produit innactif
        count = result.Tables(3).Rows.Count
        For i As Integer = 0 To count - 1
            ProduitCode = result.Tables(3).Rows(i)(0).ToString()
            If ProduitCode.Length > 2 Then
                ProduitDesc = result.Tables(3).Rows(i)(1).ToString()
                If Not Count_Row(db_connection, "SELECT * FROM produit WHERE ProduitCode = '" _
                            & ProduitCode.Trim.Replace("'", "''") & "'") Then
                    DB_Exec_NonQuery("INSERT INTO produit (ProduitCode,ProduitDesc,Actif) Values ('" _
                            & ProduitCode.Trim.Replace("'", "''") & "','" &
                            ProduitDesc.Trim.Replace("'", "''") & "','false');", db_connection)
                End If
            End If
        Next
        db_connection.Close()
        db_connection.Dispose()
    End Sub
End Module
