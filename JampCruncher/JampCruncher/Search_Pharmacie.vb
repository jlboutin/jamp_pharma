﻿Imports System.Data.SqlClient

Module Search_Pharmacie

    Function Search_PharmacieID(db_connection As SqlConnection) As Integer
        Dim Query As String
        Dim db_command As SqlCommand
        Dim datareader As SqlDataReader
        Dim PharmacieID As Integer = 0
        Dim Codepostal As String
        Dim SubQuery As String
        Dim AdresseString As String
        Dim Adresse As Integer
        Dim DataString As String()
        Dim SearchName As New List(Of String)
        Dim iCount As Integer
        Dim SearchPos As Integer
        Dim PharmaID As String

        Query = "Select * FROM MappingPharmacie WHERE CompagnieID = '" & CompagnyID & "' AND DistID = '" & iDistCode & "' AND NoClient = '" & DistData.NoPharma.Replace("'", "''") & "'"
        db_command = New SqlCommand(Query, db_connection)
        datareader = db_command.ExecuteReader
        db_command.Dispose()
        If datareader.HasRows Then
            datareader.Read()
            PharmacieID = CInt(datareader("PharmacieID").ToString)
        Else
            If DistData.CodePostal.Length > 0 Then
                Codepostal = DistData.CodePostal.Replace(" ", "")
            Else
                Codepostal = "      "
            End If
            If DistFile.CodePostalPos > 0 And DistFile.BanniereOverride1.Length Then
                'PostalCode and banniere override available
                Query = "Select * FROM vPharmacie WHERE Code_postal = '" & Codepostal.Replace("'", "''") & "'"
                If DistFile.BanniereOverride1.Length Then
                    If DistFile.banniereOverride2.Length Then
                        SubQuery = "(BanniereDesc = '" & DistData.Banniere.Replace("'", "''") & "' OR BanniereDesc = '" & DistFile.banniereOverride2.Replace("'", "''") & "')"
                    Else
                        SubQuery = "BanniereDesc = '" & DistData.Banniere.Replace(" '", "''") & "'"
                    End If
                    Query &= " AND " & SubQuery
                End If
                db_command = New SqlCommand(Query, db_connection)
                datareader = db_command.ExecuteReader
                db_command.Dispose()
            End If
            If (Not datareader.HasRows Or Count_Row(db_connection, Query) > 6) And DistFile.AdressePos > 0 And DistFile.BanniereOverride1.Length Then
                'Adresse and banniere override available
                AdresseString = Split(DistData.Adresse, " ")(0).Replace(",", "")
                Int32.TryParse(AdresseString, Adresse)
                Query = "Select * FROM vPharmacie WHERE Adresse LIKE '%" & Adresse & "%'"
                If DistFile.BanniereOverride1.Length Then
                    If DistFile.banniereOverride2.Length Then
                        SubQuery = "(BanniereDesc = '" & DistData.Banniere.Replace("'", "''") & "' OR BanniereDesc = '" & DistFile.banniereOverride2.Replace("'", "''") & "')"
                    Else
                        SubQuery = "BanniereDesc = '" & DistData.Banniere.Replace("'", "''") & "'"
                    End If
                    Query &= " AND " & SubQuery
                End If
                db_command = New SqlCommand(Query, db_connection)
                datareader = db_command.ExecuteReader
                db_command.Dispose()
            End If
            If (Not datareader.HasRows Or Count_Row(db_connection, Query) > 6) And DistFile.CodePostalPos > 0 And DistFile.AdressePos > 0 Then
                'PostalCode and Adress available to search
                Query = "Select * FROM vPharmacie Where Code_postal = '" & Codepostal.Replace("'", "''") & "'"
                If Count_Row(db_connection, Query) > 1 Then
                    AdresseString = Split(DistData.Adresse, " ")(0).Replace(",", "")
                    Int32.TryParse(AdresseString, Adresse)
                    If Adresse > 0 Then
                        Query = "Select * FROM vPharmacie Where Code_postal = '" & Codepostal.Replace("'", "''") & "' AND Adresse LIKE '" & Adresse & "%'"
                    End If
                End If
                db_command = New SqlCommand(Query, db_connection)
                datareader = db_command.ExecuteReader
                db_command.Dispose()
            End If
            If (Not datareader.HasRows Or Count_Row(db_connection, Query) > 6) And DistFile.CodePostalPos = 0 And DistFile.PharmaciePos And DistFile.AdressePos Then
                'No postal code available, need to work with name and adresse
                DataString = Split(DistData.Pharmacie, " ")
                SearchName.Clear()
                For jloop As Integer = 0 To DataString.Length - 1
                    If DataString(jloop).Length > 3 AndAlso Not DataString(jloop).Contains(".") AndAlso DataString(jloop).Substring(0, 2).ToUpper <> "PH" Then
                        SearchName.Add(DataString(jloop))
                    End If
                Next
                DataString = Split(DistData.Adresse, " ")
                For jloop As Integer = 0 To DataString.Length - 1
                    Int32.TryParse(DataString(jloop).Replace(",", ""), Adresse)
                    If Adresse > 0 Then Exit For
                Next
                SubQuery = "Select * FROM vPharmacie WHERE Adresse LIKE '%" & Adresse & "%'"
                If SearchName.Count Then
                    For jloop = 0 To SearchName.Count - 1
                        Query = SubQuery & " AND Raison_sociale LIKE '%" & SearchName(jloop).Replace("'", "''") & "%'"
                        iCount = Count_Row(db_connection, Query)
                        If iCount > 0 And iCount < 7 Then
                            Exit For
                        End If
                    Next
                End If
                db_command = New SqlCommand(Query, db_connection)
                datareader = db_command.ExecuteReader
                db_command.Dispose()
            End If
            If (Not datareader.HasRows Or Count_Row(db_connection, Query) > 6) And DistFile.AdressePos > 0 Then
                'Try with only with adresse
                DataString = Split(DistData.Adresse, " ")
                For jloop As Integer = 0 To DataString.Length - 1
                    Int32.TryParse(DataString(jloop).Replace(",", ""), Adresse)
                    If Adresse > 0 Then Exit For
                Next
                Query = "Select * FROM vPharmacie WHERE Adresse LIKE '%" & Adresse & "%'"
                db_command = New SqlCommand(Query, db_connection)
                datareader = db_command.ExecuteReader
                db_command.Dispose()
            End If
            If (Not datareader.HasRows Or Count_Row(db_connection, Query) > 6) And DistFile.PharmaciePos > 0 Then
                'Nothing work, last try with only with pharmacie name
                'Query an illegal PharmaID to reset previous result
                Query = "Select * FROM vpharmacie WHERE PharmaID = '9Z.Y7.9'"
                DataString = Split(DistData.Pharmacie, " ")
                SearchName.Clear()
                For jloop As Integer = 0 To DataString.Length - 1
                    If DataString(jloop).Length > 3 AndAlso Not DataString(jloop).Contains(".") AndAlso DataString(jloop).Substring(0, 2).ToUpper <> "PH" Then
                        SearchName.Add(DataString(jloop))
                    End If
                Next
                If SearchName.Count Then
                    For jloop = 0 To SearchName.Count - 1
                        Query = "Select * FROM vPharmacie WHERE Raison_sociale LIKE '%" & SearchName(jloop).Replace("'", "''") & "%'"
                        iCount = Count_Row(db_connection, Query)
                        If iCount > 0 And iCount < 7 Then
                            Exit For
                        End If
                    Next
                End If
                db_command = New SqlCommand(Query, db_connection)
                datareader = db_command.ExecuteReader
                db_command.Dispose()
            End If
            PharmaID = 0
            If Count_Row(db_connection, Query) > 6 Then
                'Query an illegal PharmaID to reset previous result
                Query = "Select * FROM vpharmacie WHERE PharmaID = '9Z.Y7.9'"
                db_command = New SqlCommand(Query, db_connection)
                datareader = db_command.ExecuteReader
                db_command.Dispose()
            End If

            Find_Pharm.lblCompanie.Text = CompagnyName
            Find_Pharm.lblDistri.Text = DistributorName
            Find_Pharm.tbCode.Text = DistData.NoPharma.Trim
            Find_Pharm.tbPharm.Text = DistData.Pharmacie.Trim
            Find_Pharm.ToolTip1.SetToolTip(Find_Pharm.tbPharm, DistData.Pharmacie.Trim)
            Find_Pharm.tbBan.Text = DistData.Banniere.Trim
            If Not DistData.Adresse Is Nothing Then
                Find_Pharm.tbAdress.Text = DistData.Adresse.Trim
            Else
                Find_Pharm.tbAdress.Text = ""
            End If
            Find_Pharm.ToolTip1.SetToolTip(Find_Pharm.tbAdress, Find_Pharm.tbAdress.Text)
            If Not DistData.Adresse Is Nothing Then
                Find_Pharm.tbVille.Text = DistData.Ville.Trim
            Else
                Find_Pharm.tbVille.Text = ""
            End If
            Find_Pharm.tbCodePostal.Text = DistData.CodePostal
            Find_Pharm.cbSkip.Checked = False
            Find_Pharm.Location = JampCruncher.Location
            SearchPos = 0
            For jLoop As Integer = 0 To 6
                Find_Pharm.Controls("tbSearchPharmaID" & jLoop).Text = ""
                Find_Pharm.Controls("tbSearchPharm" & jLoop).Text = ""
                Find_Pharm.ToolTip1.SetToolTip(Find_Pharm.Controls("tbsearchpharm" & jLoop), "")
                Find_Pharm.Controls("tbSearchBan" & jLoop).Text = ""
                Find_Pharm.Controls("tbSearchAdress" & jLoop).Text = ""
                Find_Pharm.ToolTip1.SetToolTip(Find_Pharm.Controls("tbsearchadress" & jLoop), "")
                Find_Pharm.Controls("tbSearchVille" & jLoop).Text = ""
                Find_Pharm.Controls("tbSearchCodePostal" & jLoop).Text = ""
            Next
            Find_Pharm.cb1.Checked = False
            Find_Pharm.cb2.Checked = False
            Find_Pharm.cb3.Checked = False
            Find_Pharm.cb4.Checked = False
            Find_Pharm.cb5.Checked = False
            Find_Pharm.cb6.Checked = False
            Find_Pharm.tbPharmaID.Text = ""
            If datareader.HasRows Then
                While datareader.Read()
                    SearchPos += 1
                    Find_Pharm.Controls("tbSearchPharmaID" & SearchPos).Text = datareader("PharmaID").ToString.Trim
                    Find_Pharm.Controls("tbSearchPharm" & SearchPos).Text = datareader("Raison_sociale").ToString.Trim
                    Find_Pharm.ToolTip1.SetToolTip(Find_Pharm.Controls("tbsearchpharm" & SearchPos), datareader("Raison_sociale").ToString.Trim)
                    Find_Pharm.Controls("tbSearchBan" & SearchPos).Text = datareader("BanniereDesc").ToString.Trim
                    Find_Pharm.Controls("tbSearchAdress" & SearchPos).Text = datareader("Adresse").ToString.Trim
                    Find_Pharm.ToolTip1.SetToolTip(Find_Pharm.Controls("tbsearchadress" & SearchPos), datareader("Adresse").ToString.Trim)
                    Find_Pharm.Controls("tbSearchVille" & SearchPos).Text = datareader("Ville").ToString.Trim
                    Find_Pharm.Controls("tbSearchCodePostal" & SearchPos).Text = datareader("Code_postal").ToString.Trim
                End While
                If SearchPos = 1 Then
                    Find_Pharm.tbPharmaID.Text = Find_Pharm.tbSearchPharmaID1.Text
                    Find_Pharm.Search_Pharm()
                End If
            End If
            If SearchPos > 1 Then
                Find_Pharm.Width = 565 + (194 * SearchPos)
            Else
                Find_Pharm.Width = 565
            End If
            Find_Pharm.cb1.Visible = (SearchPos > 1)
            Find_Pharm.Show()
            Find_Pharm.Select()
            JampCruncher.Hide()
            While Find_Pharm.Visible = True
                Application.DoEvents()
            End While
            PharmaID = Find_Pharm.tbPharmaID.Text
            JampCruncher.Show()
            JampCruncher.Select()
            Application.DoEvents()
            If Find_Pharm.cbSkip.Checked = True Then
                DB_Exec_NonQuery("Insert INTO MappingPharmacie (CompagnieID, DistID, NoClient, DoNotProcess) VALUES ('" _
                         & CompagnyID & "','" & iDistCode & "','" & DistData.NoPharma.Replace("'", "''") _
                         & "', '1');", db_connection)
            Else
                Query = "Select * FROM vpharmacie WHERE PharmaID = '" & PharmaID & "'"
                db_command = New SqlCommand(Query, db_connection)
                datareader = db_command.ExecuteReader
                db_command.Dispose()
                datareader.Read()
                If datareader.HasRows Then
                    PharmacieID = CInt(datareader("PharmacieID").ToString)
                    DB_Exec_NonQuery("Insert INTO MappingPharmacie (CompagnieID, DistID, PharmacieID, NoClient)" _
                               & "VALUES ('" & CompagnyID & "','" & iDistCode & "','" & PharmacieID & "','" & _
                               DistData.NoPharma.Replace("'", "''") & "');", db_connection)
                End If
            End If
        End If
        Search_PharmacieID = PharmacieID
    End Function

End Module
