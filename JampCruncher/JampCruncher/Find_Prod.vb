﻿Public Class Find_Prod

    Private Sub btnQuit_Click(sender As Object, e As EventArgs) Handles btnQuit.Click
        tbProduit.Text = ""
        Me.Visible = False
    End Sub

    Private Sub btUse_Click(sender As Object, e As EventArgs) Handles btUse.Click
        If tbSearchProdFR0.Text.Length < 7 And cbSkip.Checked = False Then
            Dim reply As DialogResult = MessageBox.Show("Veuillez faire une recherche S.V.P.", "Veuillez faire une recherche", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Me.Visible = False
        End If
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Search_Prod()
    End Sub

    Public Sub Search_Prod()
        If tbProduit.Text.Length < 7 Then
            Dim reply As DialogResult = MessageBox.Show("Veuillez mettre un code UPC",
                    "Code nécessaire", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        'Update Local Database

        dvProdM.RowFilter = "sProdUPC = '" & tbProduit.Text & "'"
        If dvProdM.Count > 0 Then
            tbSearchProdUPC0.Text = dvProdM(0)("sProdUPC").ToString.Trim
            tbSearchProdFR0.Text = dvProdM(0)("sProdName_F").ToString.Trim & " " & dvProdM(0)("sProdInfos").ToString.Trim
            tbSearchProdEN0.Text = dvProdM(0)("sProdName_E").ToString.Trim & " " & dvProdM(0)("sProdInfos").ToString.Trim
        Else
            Dim reply As DialogResult = MessageBox.Show("Code UPC inexistant", "Code UPC inexistant", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub


    Private Sub cb1_CheckedChanged(sender As Object, e As EventArgs) Handles cb1.Click
        cb1.Checked = True
        cb2.Checked = False
        cb3.Checked = False
        cb4.Checked = False
        cb5.Checked = False
        cb6.Checked = False
        tbProduit.Text = tbSearchProdUPC1.Text
        Search_Prod()
    End Sub

    Private Sub cb2_CheckedChanged(sender As Object, e As EventArgs) Handles cb2.Click
        cb1.Checked = False
        cb2.Checked = True
        cb3.Checked = False
        cb4.Checked = False
        cb5.Checked = False
        cb6.Checked = False
        tbProduit.Text = tbSearchProdUPC2.Text
        Search_Prod()
    End Sub

    Private Sub cb3_CheckedChanged(sender As Object, e As EventArgs) Handles cb3.Click
        cb1.Checked = False
        cb2.Checked = False
        cb3.Checked = True
        cb4.Checked = False
        cb5.Checked = False
        cb6.Checked = False
        tbProduit.Text = tbSearchProdUPC3.Text
        Search_Prod()
    End Sub

    Private Sub cb4_CheckedChanged(sender As Object, e As EventArgs) Handles cb4.Click
        cb1.Checked = False
        cb2.Checked = False
        cb3.Checked = False
        cb4.Checked = True
        cb5.Checked = False
        cb6.Checked = False
        tbProduit.Text = tbSearchProdUPC4.Text
        Search_Prod()
    End Sub

    Private Sub cb5_CheckedChanged(sender As Object, e As EventArgs) Handles cb5.Click
        cb1.Checked = False
        cb2.Checked = False
        cb3.Checked = False
        cb4.Checked = False
        cb5.Checked = True
        cb6.Checked = False
        tbProduit.Text = tbSearchProdUPC5.Text
        Search_Prod()
    End Sub

    Private Sub cb6_CheckedChanged(sender As Object, e As EventArgs) Handles cb6.Click
        cb1.Checked = False
        cb2.Checked = False
        cb3.Checked = False
        cb4.Checked = False
        cb5.Checked = False
        cb6.Checked = True
        tbProduit.Text = tbSearchProdUPC6.Text
        Search_Prod()
    End Sub

End Class