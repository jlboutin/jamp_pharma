﻿Imports System.Security.Cryptography
Imports System.Text

Module CRMAcess
    Function CRM_Connect() As String
        'login -------------------------------------------------------------------------------------------
        If CompagnyID = 2 Then 'Biomed
            Return ""
        End If
        CRM_Connect = ""
    End Function

    Sub CRM_Deconnect(SessionID As String)

    End Sub

    Function getMD5(PlainText As String) As String
        Dim md5__1 As MD5 = MD5.Create()
        Dim inputBuffer As Byte() = System.Text.Encoding.ASCII.GetBytes(PlainText)
        Dim outputBuffer As Byte() = md5__1.ComputeHash(inputBuffer)

        'Convert the byte[] to a hex-string
        Dim builder As New StringBuilder(outputBuffer.Length)
        For i As Integer = 0 To outputBuffer.Length - 1
            builder.Append(outputBuffer(i).ToString("x2"))
        Next
        Return builder.ToString
    End Function


End Module
