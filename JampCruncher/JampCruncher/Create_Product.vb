﻿Imports System.Data.SqlClient

Public Class Create_Product

    Private Sub btnQuit_Click(sender As Object, e As EventArgs) Handles btnQuit.Click
        Me.Visible = False
    End Sub

    Private Sub btnCreate_Click(sender As Object, e As EventArgs) Handles btnCreate.Click
        Dim FamilyProductValidate As Boolean = False
        Dim Insert As String
        Dim Query As String
        Dim Update As String
        Dim db_connection As SqlConnection
        Dim db_command As SqlCommand
        Dim datareader As SqlDataReader
        Dim Modulo As Integer = 0
        Dim iModulo As Integer
        Dim Prix As Double

        If cbSkip.Checked = True Then
            Me.Visible = False
            Exit Sub
        End If
        If tbUPC.Text.Length = 12 Then
            For jLoop As Integer = 0 To lstUPC.Count - 1 'Check if product sold by company
                If lstUPC(jLoop) = tbUPC.Text.Substring(0, 6) Then FamilyProductValidate = True
            Next
            If FamilyProductValidate = False Then
                Dim reply As DialogResult = MessageBox.Show("Famille de produit inconnue : " & tbUPC.Text.Substring(0, 6),
                       "Famille de produit inconnue", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
        Else
            Dim reply As DialogResult = MessageBox.Show("Le code UPC doit avoir 12 caractères",
                    "Longueur invalide", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If
        For iModulo = 0 To 10 Step 2
            Modulo += CInt(tbUPC.Text.Substring(iModulo, 1))
        Next
        Modulo *= 3
        For iModulo = 1 To 9 Step 2
            Modulo += CInt(tbUPC.Text.Substring(iModulo, 1))
        Next
        Math.DivRem(Modulo, 10, Modulo)
        If Modulo > 0 Then Modulo = 10 - Modulo
        If Modulo <> CInt(tbUPC.Text.Substring(11, 1)) Then
            Dim reply As DialogResult = MessageBox.Show("Code UPC invalide" & vbLf & "Il devrait terminer par : " & Modulo,
                    "Code UPC invalide", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If
        If tbDescription.Text.Length < 1 Then

        End If
        If tbCode.Text.Length < 1 Then
            Dim reply As DialogResult = MessageBox.Show("Veuillez mettre un code produit S.V.P.",
                    "Code nécessaire", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        Me.Visible = False
    End Sub
End Class