﻿Imports System.Data.SqlClient
Imports System.IO
Imports SpreadsheetLight

Friend Module ExcelExport
    Public Sub Export_Excel_Trans_Mapping_Client()
        Dim db_connection As SqlConnection
        Dim db_command As SqlCommand
        Dim datareader As SqlDataReader
        Dim CurRow As Integer = 1
        Dim FileName As String

        FileName = "Sommaire importation distributeur (Client) (" & DistributorName & ") " & CompagnyName & ".xlsx"
        Try
            File.Delete(FileName)
        Catch
        End Try

        db_connection = New SqlConnection(GSS_Connection)
        db_connection.Open()
        JampCruncher.btnStatus.Text = "Exportation du sommaire des importations"
        Application.DoEvents()

        Dim sl As New SLDocument
        Dim ps As New SLPageSettings

        sl.RenameWorksheet(SLDocument.DefaultFirstSheetName, "Data")
        sl.SetPageSettings(ps, "Data")

        sl.SetColumnWidth(1, 15)
        sl.SetColumnWidth(2, 7)
        sl.SetColumnWidth(3, 40)
        sl.SetColumnWidth(4, 8)
        sl.SetColumnWidth(5, 40)
        sl.SetColumnWidth(6, 10)
        sl.SetColumnWidth(7, 40)
        sl.SetColumnWidth(8, 40)
        sl.SetColumnWidth(9, 30)
        sl.SetColumnWidth(10, 15)
        sl.SetColumnWidth(11, 15)
        sl.SetColumnWidth(12, 40)
        sl.SetColumnWidth(13, 40)
        sl.SetColumnWidth(14, 40)
        sl.SetColumnWidth(15, 30)
        sl.SetColumnWidth(16, 15)
        sl.SetColumnWidth(17, 20)

        Dim style As SLStyle = sl.CreateStyle
        style.Font.FontName = "Calibri"
        style.Font.FontSize = 11
        style.Font.Bold = False
        style.Font.FontColor = Color.Black

        sl.SetCellStyle(1, 1, 12, 1, style)
        sl.SetCellValue(1, 1, "Cie")
        sl.SetCellValue(1, 2, "Dist")
        sl.SetCellValue(1, 3, "Fichier")
        sl.SetCellValue(1, 4, "Ligne")
        sl.SetCellValue(1, 5, "Message")
        sl.SetCellValue(1, 6, "no Client")
        sl.SetCellValue(1, 7, "Client")
        sl.SetCellValue(1, 8, "Adresse")
        sl.SetCellValue(1, 9, "Ville")
        sl.SetCellValue(1, 10, "Code postal")
        sl.SetCellValue(1, 11, "no Client")
        sl.SetCellValue(1, 12, "Nom légal")
        sl.SetCellValue(1, 13, "Raison sociale")
        sl.SetCellValue(1, 14, "Adresse")
        sl.SetCellValue(1, 15, "Ville")
        sl.SetCellValue(1, 16, "Code postal")
        sl.SetCellValue(1, 17, "Date")

        style = sl.CreateStyle
        style.Font.FontName = "Calibri"
        style.Font.FontSize = 11
        style.Font.Bold = False

        db_command = New SqlCommand("SELECT * FROM gss_vTrans_Mapping_Client WHERE iSessionCode = " & iSessionCode & " ORDER BY iSessionCode, Ligne", db_connection)
        datareader = db_command.ExecuteReader
        db_command.Dispose()
        While datareader.Read
            CurRow += 1
            style = sl.CreateStyle
            style.Font.FontName = "Calibri"
            style.Font.FontSize = 11
            style.Font.Bold = False
            sl.SetCellStyle(CurRow, 1, CurRow, 16, style)
            style.FormatCode = "yyyy-mm-dd HH:MM:ss"
            sl.SetCellStyle(CurRow, 12, CurRow, 17, style)
            sl.SetCellValue(CurRow, 1, Extract_DataReader(datareader, "sDepName"))
            sl.SetCellValue(CurRow, 2, Extract_DataReader(datareader, "sDistShortName"))
            sl.SetCellValue(CurRow, 3, Extract_DataReader(datareader, "Fichier"))
            sl.SetCellValue(CurRow, 4, Extract_DataReader(datareader, "Ligne"))
            sl.SetCellValue(CurRow, 5, Extract_DataReader(datareader, "sMessage"))
            sl.SetCellValue(CurRow, 6, Extract_DataReader(datareader, "noClient"))
            sl.SetCellValue(CurRow, 7, Extract_DataReader(datareader, "Client"))
            sl.SetCellValue(CurRow, 8, Extract_DataReader(datareader, "Adresse"))
            sl.SetCellValue(CurRow, 9, Extract_DataReader(datareader, "Ville"))
            sl.SetCellValue(CurRow, 10, Extract_DataReader(datareader, "CodePostal"))
            sl.SetCellValue(CurRow, 11, Extract_DataReader(datareader, "sClientGuid"))
            sl.SetCellValue(CurRow, 12, Extract_DataReader(datareader, "sClientName"))
            sl.SetCellValue(CurRow, 13, Extract_DataReader(datareader, "sClientSocialReason"))
            sl.SetCellValue(CurRow, 14, Extract_DataReader(datareader, "sClientAddress"))
            sl.SetCellValue(CurRow, 15, Extract_DataReader(datareader, "sClientCity"))
            sl.SetCellValue(CurRow, 16, Extract_DataReader(datareader, "sclientpc"))
            sl.SetCellValue(CurRow, 17, Extract_DataReader(datareader, "Date"))
        End While

        sl.SaveAs(FileName)
        sl.Dispose()
        datareader.Close()
        db_connection.Close()
        db_connection.Dispose()
        JampCruncher.btnStatus.Text = ""
        Application.DoEvents()
        If CurRow > 1 Then Process.Start(Application.StartupPath & "/" & FileName)
    End Sub

    Public Sub Export_Excel_Trans_Mapping_Produit()
        Dim db_connection As SqlConnection
        Dim db_command As SqlCommand
        Dim datareader As SqlDataReader
        Dim CurRow As Integer = 1
        Dim FileName As String

        FileName = "Sommaire importation distributeur (Produit) (" & DistributorName & ") " & CompagnyName & ".xlsx"
        Try
            File.Delete(FileName)
        Catch
        End Try

        db_connection = New SqlConnection(GSS_Connection)
        db_connection.Open()
        JampCruncher.btnStatus.Text = "Exportation du sommaire des importations"
        Application.DoEvents()

        Dim sl As New SLDocument
        Dim ps As New SLPageSettings

        sl.RenameWorksheet(SLDocument.DefaultFirstSheetName, "Data")
        sl.SetPageSettings(ps, "Data")

        sl.SetColumnWidth(1, 15)
        sl.SetColumnWidth(2, 7)
        sl.SetColumnWidth(3, 40)
        sl.SetColumnWidth(4, 8)
        sl.SetColumnWidth(5, 40)
        sl.SetColumnWidth(6, 10)
        sl.SetColumnWidth(7, 40)
        sl.SetColumnWidth(8, 15)
        sl.SetColumnWidth(9, 15)
        sl.SetColumnWidth(10, 50)
        sl.SetColumnWidth(11, 15)
        sl.SetColumnWidth(12, 20)

        Dim style As SLStyle = sl.CreateStyle
        style.Font.FontName = "Calibri"
        style.Font.FontSize = 11
        style.Font.Bold = False
        style.Font.FontColor = Color.Black

        sl.SetCellStyle(1, 1, 12, 1, style)
        sl.SetCellValue(1, 1, "Cie")
        sl.SetCellValue(1, 2, "Dist")
        sl.SetCellValue(1, 3, "Fichier")
        sl.SetCellValue(1, 4, "Ligne")
        sl.SetCellValue(1, 5, "Message")
        sl.SetCellValue(1, 6, "no Produit")
        sl.SetCellValue(1, 7, "UPC")
        sl.SetCellValue(1, 8, "Produit")
        sl.SetCellValue(1, 9, "no Produit")
        sl.SetCellValue(1, 10, "Description")
        sl.SetCellValue(1, 11, "UPC")
        sl.SetCellValue(1, 12, "Date")

        style = sl.CreateStyle
        style.Font.FontName = "Calibri"
        style.Font.FontSize = 11
        style.Font.Bold = False

        db_command = New SqlCommand("SELECT * FROM gss_vTrans_Mapping_Prod WHERE iSessionCode = " & iSessionCode & " ORDER BY iSessionCode, Ligne", db_connection)
        datareader = db_command.ExecuteReader
        db_command.Dispose()
        While datareader.Read
            CurRow += 1
            style = sl.CreateStyle
            style.Font.FontName = "Calibri"
            style.Font.FontSize = 11
            style.Font.Bold = False
            sl.SetCellStyle(CurRow, 1, CurRow, 11, style)
            style.FormatCode = "yyyy-mm-dd HH:MM:ss"
            sl.SetCellStyle(CurRow, 12, CurRow, 12, style)
            sl.SetCellValue(CurRow, 1, Extract_DataReader(datareader, "sDepName"))
            sl.SetCellValue(CurRow, 2, Extract_DataReader(datareader, "sDistShortName"))
            sl.SetCellValue(CurRow, 3, Extract_DataReader(datareader, "Fichier"))
            sl.SetCellValue(CurRow, 4, Extract_DataReader(datareader, "Ligne"))
            sl.SetCellValue(CurRow, 5, Extract_DataReader(datareader, "sMessage"))
            sl.SetCellValue(CurRow, 6, Extract_DataReader(datareader, "noProduit"))
            sl.SetCellValue(CurRow, 7, Extract_DataReader(datareader, "UPC"))
            sl.SetCellValue(CurRow, 8, Extract_DataReader(datareader, "Produit"))
            sl.SetCellValue(CurRow, 9, Extract_DataReader(datareader, "sProdNo"))
            sl.SetCellValue(CurRow, 10, Extract_DataReader(datareader, "sProdName_F"))
            sl.SetCellValue(CurRow, 11, Extract_DataReader(datareader, "sProdUPC"))
            sl.SetCellValue(CurRow, 12, Extract_DataReader(datareader, "Date"))
        End While

        sl.SaveAs(FileName)
        sl.Dispose()
        datareader.Close()
        db_connection.Close()
        db_connection.Dispose()
        JampCruncher.btnStatus.Text = ""
        Application.DoEvents()
        If CurRow > 1 Then Process.Start(Application.StartupPath & "/" & FileName)
    End Sub

End Module
