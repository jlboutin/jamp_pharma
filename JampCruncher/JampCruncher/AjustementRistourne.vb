﻿Imports System.Data.SqlClient

Public Class AjustementRistourne

    Dim dtTransactions As DataTable
    Dim dtTransOriginal As New DataTable
    Private Sub AjustementRistourne_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        btnSave.Visible = False
        Dim Month = Date.Now.AddMonths(-1).Month
        cbMonth.SelectedItem = MonthDesc(Month - 1)
        cbYear.Items.Clear()
        For year As Integer = Date.Now.Year - 1 To Date.Now.Year
            cbYear.Items.Add(year.ToString)
        Next
        cbYear.SelectedItem = YearActive.ToString
        cbYear.Visible = True
        cbYear.Enabled = True
        cbMonth.Visible = True
        cbMonth.Enabled = True

    End Sub

    Private Sub SearchTrans()
        Dim ds As New DataSet
        Dim sDate As String = cbYear.SelectedItem & "-" & (101 + cbMonth.SelectedIndex).ToString.Substring(1, 2) & "-15"
        dtTransOriginal.Clear()
        Using con As New SqlConnection(GSS_Connection)
            Dim Query As String = "SELECT * FROM vTransForMod WHERE PharmaID = '" & tbPharmaID.Text & "' AND Date = '" & sDate & "' ORDER BY Date"
            Using sda As New SqlDataAdapter(Query, con)
                sda.Fill(ds, "Transactions")
                sda.Fill(dtTransOriginal)
            End Using
        End Using
        dtTransactions = ds.Tables("Transactions")
        dgvTransactions.DataSource = dtTransactions
        dgvTransactions.AutoResizeColumns()
        dgvTransactions.Columns(0).Visible = False
        dgvTransactions.Columns(1).ReadOnly = True
        dgvTransactions.Columns(2).ReadOnly = True
        dgvTransactions.Columns(3).ReadOnly = True
        dgvTransactions.Columns(3).HeaderText = "Code Produit"
        dgvTransactions.Columns(4).ReadOnly = True
        dgvTransactions.Columns(4).HeaderText = "Molécule"
        dgvTransactions.Columns(5).ReadOnly = True
        dgvTransactions.Columns(5).HeaderText = "Qtée"
        dgvTransactions.Columns(6).ReadOnly = True
        dgvTransactions.Columns(6).HeaderText = "Prix"
        dgvTransactions.Columns(7).ReadOnly = True
        dgvTransactions.Columns(7).HeaderText = "Montant"
        dgvTransactions.Columns(8).ReadOnly = True
        dgvTransactions.Columns(8).HeaderText = "Ristourne"
        dgvTransactions.Columns(9).HeaderText = "% ristourne"
        btnSave.Visible = True
    End Sub

    Private Sub tbPharmaID_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbPharmaID.KeyPress
        Dim tb As TextBox
        tb = CType(sender, TextBox)

        If Char.IsControl(e.KeyChar) Then
            If e.KeyChar.Equals(Chr(Keys.Return)) Then
                e.Handled = True
                SearchTrans()
            End If
        End If
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim db_connection = New SqlConnection(GSS_Connection)
        db_connection.Open()
        For xloop As Integer = 0 To dtTransactions.Rows.Count - 1
            If dtTransactions.Rows(xloop)("PercentRistourne") <> dtTransOriginal.Rows(xloop)("PercentRistourne") Then
                DB_Exec_NonQuery("Update Trans SET Ristourne = " & dtTransactions.Rows(xloop)("PercentRistourne") & ", Overwrite = 1 WHERE TransactionID = " & dtTransactions.Rows(xloop)("TransactionID"), db_connection)
            End If
        Next
        db_connection.Close()
        db_connection.Dispose()
    End Sub
End Class