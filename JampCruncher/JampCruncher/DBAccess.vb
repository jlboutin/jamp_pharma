﻿Imports System.Data.SqlClient
Imports MySql.Data.MySqlClient

Module DBAccess

    Function DB_Connector(DB As String) As String
        Dim ConnectionString As String = My.Settings(DB & "ConnectionString")
        If My.Computer.Name = "CONNEXXIUM" Then ConnectionString = My.Settings(DB & "ConnectionStringConnexxium")
        'Dim CipherText() As Byte = Obviex.CipherLite.Encoder.HexDecode(ConnectionString)
        'ConnectionString = Obviex.CipherLite.Rijndael.Decrypt(CipherText, Application.CompanyName, "", 256, 1, "", "MD5")
        Return ConnectionString
    End Function

    Function DB_Exec_Scalar(Query As String, db_connection As SqlConnection) As Object
        Dim db_command As SqlCommand = New SqlCommand(Query, db_connection)
        DB_Exec_Scalar = db_command.ExecuteScalar()
        If DB_Exec_Scalar Is Nothing Then DB_Exec_Scalar = ""
        db_command.Dispose()
    End Function

    Function DB_Exec_Scalar_MySQL(Query As String, db_connection As MySqlConnection) As Object
        Dim db_command As MySqlCommand = New MySqlCommand(Query, db_connection)
        DB_Exec_Scalar_MySQL = db_command.ExecuteScalar()
        If DB_Exec_Scalar_MySQL Is Nothing Then DB_Exec_Scalar_MySQL = ""
        db_command.Dispose()
    End Function

    Sub DB_Exec_NonQuery(Query As String, db_connection As SqlConnection)
        If Query.Substring(0, 6) = "Select" Then
            Query = "Trouble"
        End If
        Dim db_command As SqlCommand = New SqlCommand(Query, db_connection)
        db_command.ExecuteNonQuery()
        db_command.Dispose()
    End Sub

    Sub DB_Exec_NonQuery_MySQL(Query As String, db_connection As MySqlConnection)
        If Query.Substring(0, 6) = "Select" Then
            Query = "Trouble"
        End If
        Dim db_command As MySqlCommand = New MySqlCommand(Query, db_connection)
        db_command.ExecuteNonQuery()
        db_command.Dispose()
    End Sub

    Function Extract_DataReader(DataReader As SqlDataReader, ByRef Field As String)
        Extract_DataReader = ""
        Try
            If Not (DataReader(Field) Is Nothing Or IsDBNull(DataReader(Field))) Then
                Extract_DataReader = DataReader(Field).ToString.Trim
            End If
        Catch
        End Try
    End Function

    Function Extract_DataField(ByRef DataLine As DataRow, ByRef Field As String) As String
        Extract_DataField = ""
        Try
            If Not (DataLine(Field) Is Nothing Or IsDBNull(DataLine(Field))) Then
                Extract_DataField = DataLine(Field).ToString.Trim
            End If
        Catch
            If Not DataLine.Table.Columns.Contains(Field) Then
                Dim Result As DialogResult
                Result = MessageBox.Show("Undefined Field " & Field, Extract_DataField, MessageBoxButtons.OKCancel)
                If Result = System.Windows.Forms.DialogResult.Cancel Then Environment.Exit(0)
            End If
        End Try
    End Function

    Function Count_Row(db_connection As SqlConnection, Query As String) As Integer
        Count_Row = CInt(DB_Exec_Scalar("SELECT COUNT(*) " &
                                         Query.Substring(Query.IndexOf("FROM")), db_connection))
    End Function

    Function Count_Row_MySQL(db_connection As MySqlConnection, Query As String) As Integer
        Count_Row_MySQL = CInt(DB_Exec_Scalar_MySQL("SELECT COUNT(*) " &
                                         Query.Substring(Query.ToUpper.IndexOf("FROM")), db_connection))
    End Function

    Function Count_Count_Row(db_connection As SqlConnection, Query As String) As Integer
        Count_Count_Row = CInt(DB_Exec_Scalar("SELECT Count(*) FROM (SELECT COUNT(*) As cnt " &
                                         Query.Substring(Query.IndexOf("FROM")) &
                                         ") AS Counts", db_connection))
    End Function

    Sub Load_Array(Query As String, db_connection As SqlConnection, ByRef arList As ArrayList)
        Dim db_command As SqlCommand
        Dim datareader As SqlDataReader
        db_command = New SqlCommand(Query, db_connection)
        datareader = db_command.ExecuteReader()
        db_command.Dispose()
        While datareader.Read()
            Dim dict As New Dictionary(Of String, Object)
            For count As Integer = 0 To (datareader.FieldCount - 1)
                dict.Add(datareader.GetName(count), datareader(count))
            Next
            arList.Add(dict)
        End While
        datareader.Close()
    End Sub

    Function Load_Pos(arList As ArrayList, Index As String) As String()
        Dim Pos(arList.Count - 1) As String
        For LoopCount = 0 To arList.Count - 1
            Pos(LoopCount) = arList(LoopCount)(Index)
        Next
        Return Pos
    End Function

    Function DataTable_Transfert(dtSource As DataTable, Destination_table As String, db As String, Truncate As Boolean) As Boolean
        Dim db_connection_Destination As SqlConnection
        Dim Done As Boolean = False
        Dim nbRecordSource As Integer
        Dim nbRecordDestination As Integer = 0
        Dim nbRecordTransfert As Integer = 0
        Dim nbRetry As Integer = 0

        db_connection_Destination = New SqlConnection(DB_Connector(db))
        db_connection_Destination.Open()
        If Truncate = True Then
            DB_Exec_NonQuery("TRUNCATE TABLE " & Destination_table, db_connection_Destination)
        End If
        nbRecordSource = dtSource.Rows.Count
        nbRecordDestination = Count_Row(db_connection_Destination, "SELECT * FROM " & Destination_table)

        While Done = False
            If nbRecordSource Then
                Using bulkCopy As SqlBulkCopy = New SqlBulkCopy(DB_Connector(db))
                    bulkCopy.DestinationTableName = Destination_table
                    bulkCopy.BatchSize = 1000
                    Try
                        ' try to Write from the source to the destination.
                        bulkCopy.WriteToServer(dtSource)
                        DataTable_Transfert = True
                    Catch ex As Exception
                        MsgBox("Problème de mise à jour" & vbLf & ex.Message, MsgBoxStyle.Critical)
                    End Try
                End Using
            Else
                DataTable_Transfert = True
            End If
            nbRecordTransfert = Count_Row(db_connection_Destination, "SELECT * FROM " & Destination_table)
            If nbRecordSource = nbRecordTransfert - nbRecordDestination Then Done = True
            nbRetry += 1
            If nbRetry = 10 Then
                Done = True
                MsgBox("Impossible de synchroniser", MsgBoxStyle.Critical)
            End If
        End While
        DataTable_Transfert = False
        db_connection_Destination.Close()
        db_connection_Destination.Dispose()
    End Function

    Function Data_Transfert(Query As String, db_connection_Source As SqlConnection, Destination_table As String, db_connection_Destination_String As String) As Boolean
        Dim db_connection_Destination As SqlConnection
        Dim Done As Boolean = False
        Dim nbRecordSource As Integer
        Dim nbRecordDestination As Integer = 0
        Dim nbRecordTransfert As Integer = 0
        Dim nbRetry As Integer = 0

        db_connection_Destination = New SqlConnection(db_connection_Destination_String)
        db_connection_Destination.Open()
        nbRecordSource = Count_Row(db_connection_Source, Query)
        nbRecordDestination = Count_Row(db_connection_Destination, "SELECT * FROM " & Destination_table)

        While Done = False
            Dim db_command As SqlCommand
            Dim datareader As SqlDataReader

            ' Get data from the source table as a SqlDataReader.
            db_command = New SqlCommand(Query, db_connection_Source)
            datareader = db_command.ExecuteReader
            db_command.Dispose()
            If datareader.HasRows Then
                Using bulkCopy As SqlBulkCopy = New SqlBulkCopy(db_connection_Destination_String)
                    bulkCopy.DestinationTableName = Destination_table
                    bulkCopy.BatchSize = 1000
                    Try
                        ' try to Write from the source to the destination.
                        bulkCopy.WriteToServer(datareader)
                        Data_Transfert = True
                    Catch ex As Exception
                        MsgBox("Problème de mise à jour" & vbLf & ex.Message, MsgBoxStyle.Critical)
                    End Try
                End Using
            Else
                Data_Transfert = True
            End If
            datareader.Close()
            nbRecordTransfert = Count_Row(db_connection_Destination, "SELECT * FROM " & Destination_table)
            If nbRecordSource = nbRecordTransfert - nbRecordDestination Then Done = True
            nbRetry += 1
            If nbRetry = 10 Then
                Done = True
                MsgBox("Impossible de synchroniser", MsgBoxStyle.Critical)
            End If
        End While
        Data_Transfert = False
    End Function

End Module


