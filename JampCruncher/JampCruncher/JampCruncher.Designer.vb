﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class JampCruncher
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.BtnImportDist = New System.Windows.Forms.Button()
        Me.lbCompany = New System.Windows.Forms.ListBox()
        Me.lblCompagny = New System.Windows.Forms.Label()
        Me.txtRecordInfo = New System.Windows.Forms.TextBox()
        Me.lblDistributeur = New System.Windows.Forms.Label()
        Me.cbMonth = New System.Windows.Forms.ComboBox()
        Me.cbYear = New System.Windows.Forms.ComboBox()
        Me.btnStatus = New System.Windows.Forms.Button()
        Me.lblCurrentMonth = New System.Windows.Forms.Label()
        Me.lvDist = New System.Windows.Forms.ListView()
        Me.SuspendLayout()
        '
        'BtnImportDist
        '
        Me.BtnImportDist.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnImportDist.Location = New System.Drawing.Point(19, 58)
        Me.BtnImportDist.Name = "BtnImportDist"
        Me.BtnImportDist.Size = New System.Drawing.Size(208, 55)
        Me.BtnImportDist.TabIndex = 1
        Me.BtnImportDist.Text = "Importation des ventes"
        Me.BtnImportDist.UseVisualStyleBackColor = True
        '
        'lbCompany
        '
        Me.lbCompany.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbCompany.FormattingEnabled = True
        Me.lbCompany.HorizontalScrollbar = True
        Me.lbCompany.ItemHeight = 20
        Me.lbCompany.Location = New System.Drawing.Point(325, 178)
        Me.lbCompany.Name = "lbCompany"
        Me.lbCompany.Size = New System.Drawing.Size(260, 104)
        Me.lbCompany.TabIndex = 2
        Me.lbCompany.Visible = False
        '
        'lblCompagny
        '
        Me.lblCompagny.AutoSize = True
        Me.lblCompagny.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompagny.Location = New System.Drawing.Point(346, 19)
        Me.lblCompagny.Name = "lblCompagny"
        Me.lblCompagny.Size = New System.Drawing.Size(147, 29)
        Me.lblCompagny.TabIndex = 3
        Me.lblCompagny.Text = "Compagnie"
        Me.lblCompagny.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblCompagny.Visible = False
        '
        'txtRecordInfo
        '
        Me.txtRecordInfo.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtRecordInfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRecordInfo.Location = New System.Drawing.Point(240, 143)
        Me.txtRecordInfo.Name = "txtRecordInfo"
        Me.txtRecordInfo.Size = New System.Drawing.Size(533, 23)
        Me.txtRecordInfo.TabIndex = 8
        Me.txtRecordInfo.Text = "Pharmacie"
        Me.txtRecordInfo.Visible = False
        '
        'lblDistributeur
        '
        Me.lblDistributeur.AutoSize = True
        Me.lblDistributeur.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDistributeur.Location = New System.Drawing.Point(347, 98)
        Me.lblDistributeur.Name = "lblDistributeur"
        Me.lblDistributeur.Size = New System.Drawing.Size(103, 20)
        Me.lblDistributeur.TabIndex = 12
        Me.lblDistributeur.Text = "Distributeur"
        Me.lblDistributeur.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblDistributeur.Visible = False
        '
        'cbMonth
        '
        Me.cbMonth.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbMonth.FormattingEnabled = True
        Me.cbMonth.ItemHeight = 20
        Me.cbMonth.Items.AddRange(New Object() {"Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"})
        Me.cbMonth.Location = New System.Drawing.Point(268, 61)
        Me.cbMonth.Name = "cbMonth"
        Me.cbMonth.Size = New System.Drawing.Size(146, 28)
        Me.cbMonth.TabIndex = 16
        '
        'cbYear
        '
        Me.cbYear.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbYear.FormattingEnabled = True
        Me.cbYear.Location = New System.Drawing.Point(437, 60)
        Me.cbYear.Name = "cbYear"
        Me.cbYear.Size = New System.Drawing.Size(121, 28)
        Me.cbYear.TabIndex = 17
        '
        'btnStatus
        '
        Me.btnStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStatus.Location = New System.Drawing.Point(268, 121)
        Me.btnStatus.Name = "btnStatus"
        Me.btnStatus.Size = New System.Drawing.Size(290, 70)
        Me.btnStatus.TabIndex = 18
        Me.btnStatus.UseVisualStyleBackColor = True
        '
        'lblCurrentMonth
        '
        Me.lblCurrentMonth.AutoSize = True
        Me.lblCurrentMonth.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrentMonth.Location = New System.Drawing.Point(12, 19)
        Me.lblCurrentMonth.Name = "lblCurrentMonth"
        Me.lblCurrentMonth.Size = New System.Drawing.Size(45, 18)
        Me.lblCurrentMonth.TabIndex = 27
        Me.lblCurrentMonth.Text = "Mois"
        '
        'lvDist
        '
        Me.lvDist.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me.lvDist.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvDist.FullRowSelect = True
        Me.lvDist.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvDist.Location = New System.Drawing.Point(240, 174)
        Me.lvDist.Name = "lvDist"
        Me.lvDist.Size = New System.Drawing.Size(499, 301)
        Me.lvDist.TabIndex = 4
        Me.lvDist.UseCompatibleStateImageBehavior = False
        Me.lvDist.Visible = False
        '
        'JampCruncher
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(785, 567)
        Me.Controls.Add(Me.lblCurrentMonth)
        Me.Controls.Add(Me.btnStatus)
        Me.Controls.Add(Me.cbYear)
        Me.Controls.Add(Me.cbMonth)
        Me.Controls.Add(Me.lblDistributeur)
        Me.Controls.Add(Me.txtRecordInfo)
        Me.Controls.Add(Me.lvDist)
        Me.Controls.Add(Me.lblCompagny)
        Me.Controls.Add(Me.lbCompany)
        Me.Controls.Add(Me.BtnImportDist)
        Me.Location = New System.Drawing.Point(20, 20)
        Me.Name = "JampCruncher"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Jamp Cruncher 1.0.0"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BtnImportDist As System.Windows.Forms.Button
    Friend WithEvents lbCompany As System.Windows.Forms.ListBox
    Friend WithEvents lblCompagny As System.Windows.Forms.Label
    Friend WithEvents CompagnieBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents txtRecordInfo As System.Windows.Forms.TextBox
    Friend WithEvents lblDistributeur As System.Windows.Forms.Label
    Friend WithEvents cbMonth As System.Windows.Forms.ComboBox
    Friend WithEvents cbYear As System.Windows.Forms.ComboBox
    Friend WithEvents btnStatus As System.Windows.Forms.Button
    Friend WithEvents lblCurrentMonth As System.Windows.Forms.Label
    Friend WithEvents lvDist As ListView
End Class
