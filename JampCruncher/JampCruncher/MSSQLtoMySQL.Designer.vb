﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MSSQLtoMySQL
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btn_Exit = New System.Windows.Forms.Button()
        Me.btn_Start = New System.Windows.Forms.Button()
        Me.btn_BV_CME = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btn_Exit
        '
        Me.btn_Exit.Enabled = False
        Me.btn_Exit.Location = New System.Drawing.Point(326, 310)
        Me.btn_Exit.Name = "btn_Exit"
        Me.btn_Exit.Size = New System.Drawing.Size(75, 23)
        Me.btn_Exit.TabIndex = 8
        Me.btn_Exit.Text = "Exit"
        Me.btn_Exit.UseVisualStyleBackColor = True
        '
        'btn_Start
        '
        Me.btn_Start.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Start.Location = New System.Drawing.Point(77, 50)
        Me.btn_Start.Name = "btn_Start"
        Me.btn_Start.Size = New System.Drawing.Size(324, 97)
        Me.btn_Start.TabIndex = 7
        Me.btn_Start.Text = "Start"
        Me.btn_Start.UseVisualStyleBackColor = True
        '
        'btn_BV_CME
        '
        Me.btn_BV_CME.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_BV_CME.Location = New System.Drawing.Point(77, 177)
        Me.btn_BV_CME.Name = "btn_BV_CME"
        Me.btn_BV_CME.Size = New System.Drawing.Size(324, 97)
        Me.btn_BV_CME.TabIndex = 9
        Me.btn_BV_CME.Text = "BV - CME"
        Me.btn_BV_CME.UseVisualStyleBackColor = True
        '
        'MSSQLtoMySQL
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(485, 381)
        Me.Controls.Add(Me.btn_BV_CME)
        Me.Controls.Add(Me.btn_Exit)
        Me.Controls.Add(Me.btn_Start)
        Me.Name = "MSSQLtoMySQL"
        Me.Text = "MSSQL to MySQL"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btn_Exit As System.Windows.Forms.Button
    Friend WithEvents btn_Start As System.Windows.Forms.Button
    Friend WithEvents btn_BV_CME As Button
End Class
