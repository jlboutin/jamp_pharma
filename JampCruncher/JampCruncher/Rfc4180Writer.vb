﻿Public Class Rfc4180Writer

    Public Shared Sub WriteDataTable(sourceTable As DataTable, writer As IO.TextWriter, includeHeaders As Boolean)
        Dim BinaryField As New List(Of Integer)
        Dim HexString As String = ""

        If includeHeaders Then
            Dim headerValues As IEnumerable(Of [String]) = sourceTable.Columns.OfType(Of DataColumn)().[Select](Function(column) QuoteValue(column.ColumnName))

            writer.WriteLine([String].Join(",", headerValues))
        End If

        Dim st As DataTable = sourceTable.Clone
        For xloop As Integer = 0 To st.Columns.Count - 1
            If st.Columns(xloop).DataType.Name = "Byte[]" Then
                st.Columns(xloop).DataType = GetType(String)
                st.Columns(xloop).ReadOnly = False
                BinaryField.Add(xloop)
            End If
        Next
        If BinaryField.Count Then
            For Each row As DataRow In sourceTable.Rows
                st.ImportRow(row)
            Next
            For yloop As Integer = 0 To st.Rows.Count - 1
                For xloop As Integer = 0 To BinaryField.Count - 1
                    HexString = "0x"
                    For zloop = 0 To sourceTable.Rows(yloop)(BinaryField(xloop)).length - 1
                        HexString &= CInt(sourceTable.Rows(yloop)(BinaryField(xloop))(zloop)).ToString("X2")
                    Next
                    st.Rows(yloop)(BinaryField(xloop)) = HexString
                Next
            Next
        End If

        Dim items As IEnumerable(Of [String]) = Nothing

        For Each row As DataRow In IIf(BinaryField.Count, st.Rows, sourceTable.Rows)
            items = row.ItemArray.[Select](Function(o) QuoteValue(o.ToString()))
            writer.WriteLine([String].Join(",", items))
        Next

        writer.Flush()
    End Sub

    Private Shared Function QuoteValue(ByVal value As String) As String
        If value.ToUpper = "FALSE" Then
            value = "0"
        End If
        If value.ToUpper = "TRUE" Then
            value = "1"
        End If
        Return String.Concat("""", value.Replace("""", """"""), """")
    End Function

End Class
