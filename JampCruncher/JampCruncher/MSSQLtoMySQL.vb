﻿Imports System.Data.SqlClient
Imports MySql.Data.MySqlClient
Imports System.IO

Public Class MSSQLtoMySQL

    Private Sub MSSQLtoMySQL_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = "MSSQL to MySQL Conversion" & My.Application.Info.Version.ToString
        Me.Show()
    End Sub

    Private Sub btn_Exit_Click(sender As Object, e As EventArgs) Handles btn_Exit.Click
        Application.Exit()
    End Sub

    Private Sub btn_Start_Click(sender As Object, e As EventArgs) Handles btn_Start.Click
        Dim AutoIncrement As String = ""
        Dim iAutoIncrement As Integer = 0
        Dim db_connection As SqlConnection
        Dim db_command As SqlCommand
        Dim datareader As SqlDataReader
        Dim Query As String
        Dim TableName As String
        Dim db_connection_MySQL As New MySqlConnection(My.Settings.DestinationConnectionString)
        db_connection_MySQL.Open()
        db_connection = New SqlConnection(DB_Connector("Source"))
        db_connection.Open()
        db_command = New SqlCommand("SELECT Name FROM sys.Tables WHERE Type_desc = 'USER_TABLE' ORDER BY Name", db_connection)
        datareader = db_command.ExecuteReader
        db_command.Dispose()
        While datareader.Read
            TableName = Extract_DataReader(datareader, "Name")
            If TableName <> "sysdiagrams" And TableName.Substring(0, 2) <> "sc" And
                ((TableName.Length > 3 AndAlso TableName.Substring(0, 4) <> "sec_") Or TableName.Length < 4) Then
                If TableName = "ProduitFamille" Or TableName = "ProduitMarque" Then
                    CreateTable(TableName, db_connection, db_connection_MySQL)
                    LoadTable(TableName, db_connection, db_connection_MySQL)
                    Query = "SELECT c.name FROM [sys].[tables] as t " _
                      & "LEFT JOIN [giq].[sys].[columns] as c ON t.object_id = c.object_id " _
                      & "WHERE is_identity = 1 and t.name = '" & TableName & "'"
                    AutoIncrement = DB_Exec_Scalar(Query, db_connection)
                    If AutoIncrement.Length > 0 Then
                        iAutoIncrement = DB_Exec_Scalar("SELECT IDENT_CURRENT('" & TableName & "')", db_connection) + 1
                        DB_Exec_NonQuery_MySQL("ALTER TABLE " & TableName & " MODIFY `" & AutoIncrement & "` int(10) AUTO_INCREMENT PRIMARY KEY", db_connection_MySQL)
                        DB_Exec_NonQuery_MySQL("ALTER TABLE " & TableName & " AUTO_INCREMENT=" & iAutoIncrement, db_connection_MySQL)
                    End If
                End If
            End If
        End While
        db_connection.Close()
        db_connection.Dispose()
        db_connection_MySQL.Close()
        db_connection_MySQL.Dispose()
        Application.Exit()
    End Sub

    Private Sub CreateTable(TableName As String, db_connection As SqlConnection, db_connection_MySQL As MySqlConnection)
        Dim db_command As SqlCommand
        Dim datareader As SqlDataReader
        Dim Query As String
        Dim ColumnDef As String
        Dim CurCol As Integer = 0

        btn_Start.Text = TableName
        Application.DoEvents()
        DB_Exec_NonQuery_MySQL("DROP TABLE IF EXISTS `" & TableName & "`;", db_connection_MySQL)
        Query = "SELECT * FROM " & My.Settings.DatabaseName & ".INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" & TableName & "' order by ordinal_position"
        db_command = New SqlCommand(Query, db_connection)
        datareader = db_command.ExecuteReader
        db_command.Dispose()
        Query = "CREATE TABLE IF NOT EXISTS `" & TableName & "` ("
        While datareader.Read
            btn_Start.Text = TableName & vbLf & Extract_DataReader(datareader, "COLUMN_NAME") & " - " & Extract_DataReader(datareader, "ORDINAL_POSITION")
            Application.DoEvents()
            If CurCol > 0 Then Query &= "," & vbLf
            CurCol += 1
            ColumnDef = ComposeColumn(Extract_DataReader(datareader, "COLUMN_NAME"),
                                      Extract_DataReader(datareader, "IS_NULLABLE"),
                                      Extract_DataReader(datareader, "DATA_TYPE"),
                                      Extract_DataReader(datareader, "CHARACTER_MAXIMUM_LENGTH"),
                                      Extract_DataReader(datareader, "NUMERIC_PRECISION"),
                                      Extract_DataReader(datareader, "NUMERIC_SCALE"),
                                      Extract_DataReader(datareader, "COLLATION_NAME"),
                                      Extract_DataReader(datareader, "COLUMN_DEFAULT"))
            Query &= ColumnDef
        End While
        Query &= ")" & vbLf & "ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        DB_Exec_NonQuery_MySQL(Query, db_connection_MySQL)

    End Sub

    Private Function ComposeColumn(ColumnName As String, IsNullable As String, DataType As String, CharLength As String, Precision As String, Scale As String, Collation As String, sDefault As String) As String
        If sDefault = "(getdate())" Then sDefault = ""
        ComposeColumn = ""
        Select Case DataType
            Case "bigint"
                ComposeColumn = DataType
            Case "binary"
                ComposeColumn = "varchar(" & (CharLength * 2 + 2) & ")"
                'Case "bit"
                '    ComposeColumn = "bit(1)"
                '    sDefault = "b'" & sDefault & "'"
            Case "bit"
                ComposeColumn = "boolean"
            Case "date"
                ComposeColumn = DataType
            Case "datetime"
                ComposeColumn = DataType
            Case "decimal"
                ComposeColumn = "numeric(" & Precision & "," & Scale & ")"
            Case "float"
                ComposeColumn = DataType
            Case "int"
                ComposeColumn = "int(" & Precision & ")"
            Case "money"
                ComposeColumn = "numeric(15,2)"
            Case "ntext"
                ComposeColumn = "text"
            Case "nvarchar"
                ComposeColumn = "nvarchar("
                If CharLength = -1 Or CharLength > 254 Then
                    ComposeColumn = "text"
                Else
                    ComposeColumn &= CharLength & ")"
                End If
            Case "real"
                ComposeColumn = "float"
            Case "smallint"
                ComposeColumn = DataType
            Case "text"
                ComposeColumn = DataType
            Case "timestamp"
                ComposeColumn = "varchar(18)"
            Case "tinyint"
                ComposeColumn = DataType
            Case "uniqueidentifier"
                ComposeColumn = "nvarchar(36)"
            Case "varchar"
                ComposeColumn = "varchar("
                If CharLength = -1 Or CharLength > 254 Then
                    ComposeColumn = "text"
                Else
                    ComposeColumn &= CharLength & ")"
                End If
            Case Else
                MsgBox("Undefined DataType  -->  " & DataType)
        End Select

        If ComposeColumn.Length > 0 Then
            ComposeColumn = "`" & ColumnName & "` " & ComposeColumn
            ComposeColumn &= IIf(IsNullable = "YES", " NULL", " NOT NULL")
            If sDefault.Length > 0 Then ComposeColumn &= " DEFAULT " & sDefault.Replace("(", "").Replace(")", "")
        End If

    End Function

    Private Sub LoadTable(TableName As String, db_connection As SqlConnection, db_connection_MySQL As MySqlConnection)
        Dim daTrans As New SqlDataAdapter("Select TOP 1000000 * From " & TableName, db_connection)
        Dim dsTrans As New DataSet("Transfert")
        Dim dtTrans As New DataTable

        btn_Start.Text = TableName & vbLf & " Loading MSSQL Dataset"
        Application.DoEvents()
        daTrans.FillSchema(dsTrans, SchemaType.Source, "Transfert")
        daTrans.Fill(dsTrans, "Transfert")
        dtTrans = dsTrans.Tables(0)

        Dim db_command As New MySqlCommand("TRUNCATE TABLE " & TableName, db_connection_MySQL)
        db_command.ExecuteNonQuery()
        db_command.Dispose()

        btn_Start.Text = TableName & vbLf & " Writing csv"
        Application.DoEvents()

        Dim tempCsvFileSpec As String = "dump.csv"
        Using writer As New StreamWriter(tempCsvFileSpec, False, System.Text.Encoding.UTF8)
            Rfc4180Writer.WriteDataTable(dtTrans, writer, True)
        End Using

        btn_Start.Text = TableName & vbLf & " Uploading MySQL Table"
        Application.DoEvents()
        Dim msbl = New MySqlBulkLoader(db_connection_MySQL)
        msbl.TableName = TableName
        msbl.FileName = tempCsvFileSpec
        msbl.FieldTerminator = ","
        msbl.LineTerminator = "\r\n"
        msbl.FieldQuotationCharacter = """"c
        msbl.CharacterSet = "utf8"
        msbl.NumberOfLinesToSkip = 1
        msbl.Load()
        System.IO.File.Delete(tempCsvFileSpec)
    End Sub

    Private Sub btn_BV_CME_Click(sender As Object, e As EventArgs) Handles btn_BV_CME.Click
        Dim ConnectionString = My.Settings.DestinationConnectionTechnoString
        Dim db_connection As New MySqlConnection(ConnectionString)
        Dim FilePath As String
        Dim FileName As String
        Dim Annee As Integer
        Dim Mois As Integer
        Dim PharmacieID As Integer
        Dim Rep_Mapping() As DataRow
        Dim RepID As Integer

        db_connection.Open()
        Dim dtCompte As New DataTable
        Using cmd As New MySqlCommand("SELECT PharmacieID, CME_Directory FROM Pharmacie", db_connection)
            Using sda As New MySqlDataAdapter(cmd)
                sda.Fill(dtCompte)
            End Using
        End Using

        Dim dtRep As New DataTable
        Using cmd As New MySqlCommand("SELECT * FROM RepBanGrp_Mapping", db_connection)
            Using sda As New MySqlDataAdapter(cmd)
                sda.Fill(dtRep)
            End Using
        End Using
        For Each Row In dtCompte.Rows
            FilePath = "C:\Users\jlbou\Desktop\bm\" & Row("CME_Directory")
            If System.IO.Directory.Exists(FilePath) Then
                Dim Dir As String() = Directory.GetFiles(FilePath)
                If Dir.Length > 0 Then
                    PharmacieID = Row("PharmacieID")
                    btn_BV_CME.Text = PharmacieID
                    Application.DoEvents()
                    Rep_Mapping = dtRep.Select("PharmacieID = " & PharmacieID)
                    For xloop = 0 To Dir.Length - 1
                        FileName = Path.GetFileName(Dir(xloop))
                        If FileName.StartsWith("BV_2") Then
                            Annee = CInt(FileName.Substring(3, 4))
                            If Count_Row_MySQL(db_connection, "SELECT * From Bilan WHERE PharmacieID = " & PharmacieID & " AND Année = " & Annee) = 0 Then
                                DB_Exec_NonQuery_MySQL("INSERT INTO Bilan (PharmacieID, Année, Filename) VALUES (" & PharmacieID & ", " & Annee & ", '" & FileName & "')", db_connection)
                            End If
                        End If
                        If FileName.StartsWith("CME") Then
                            Annee = CInt(FileName.Substring(4, 4))
                            Mois = CInt(FileName.Substring(9, 2))
                            If Annee = 2016 Then
                                RepID = Rep_Mapping(0)("RepID_Mois_1")
                            End If
                            If Annee = 2017 Then
                                RepID = Rep_Mapping(0)("RepID_Mois_" & Mois)
                            End If
                            If Count_Row_MySQL(db_connection, "SELECT * From Ristourne WHERE PharmacieID = " & PharmacieID & " AND Année = " & Annee & " AND Mois = " & Mois) = 0 Then
                                DB_Exec_NonQuery_MySQL("INSERT INTO Ristourne (PharmacieID, Année, Mois, RepID, Filename) VALUES (" & PharmacieID & ", " & Annee & ", " & Mois & ", " & RepID & ", '" & FileName & "')", db_connection)
                            End If
                        End If
                    Next
                End If
            End If
        Next
        Application.Exit()
    End Sub
End Class
