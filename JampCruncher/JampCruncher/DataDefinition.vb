﻿Imports PdfSharp.Pdf
Imports PdfSharp.Drawing

Module DataDefinition

    Public dtClientD As New DataTable
    Public dvClientD As DataView
    Public dtClientC As New DataTable
    Public dvClientC As DataView
    Public dtProdM As New DataTable
    Public dvProdM As DataView
    Public dtProdP As New DataTable
    Public dvProdP As DataView
    Public dtMapClient As New DataTable
    Public dtMapProd As New DataTable
    Public drMap() As DataRow
    Public dtDep As New DataTable
    Public dvDep As DataView
    Public dtTerr As New DataTable
    Public dvTerr As DataView
    Public dSkip As New Dictionary(Of String, Integer)

    Public UpdateGrabb As Boolean = False
    Public Find_Pharm_location As New Point(0, 0)
    Public Connexxium_DEV As Boolean = False
    Public CompagnyID As Integer = 1
    Public CompagnyName As String = ""
    Public MonthActive As Integer = 0
    Public YearActive As Integer = 0
    Public DateActive As New Date(2000, 1, 1)
    Public MonthDesc() As String = {"Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"}
    Public iDistCode As Integer = 0
    Public iSessionCode As Integer
    Public ImportFile As String = ""
    Public ImportSales As Boolean = False
    Public DistributorName As String = ""
    Public UpdateTimeStamp As Boolean = True
    Public MonthToUpdate = Date.Now.AddMonths(-1).Month 'Toggle update at the middle of the Month
    Public PreviousMonth = Date.Now.AddMonths(-2).Month 'Copy Price from last month to current month
    Public NextMonth = Date.Now.Month 'Copy Price from last month to current month
    'Public Document As PdfDocument
    'Public Page As PdfPage
    'Public GFX As XGraphics
    'Public fontArial7 As XFont = New XFont("Arial", 7, XFontStyle.Regular)
    'Public fontArial7B As XFont = New XFont("Arial", 7, XFontStyle.Bold)
    'Public fontArial8 As XFont = New XFont("Arial", 8, XFontStyle.Regular)
    'Public fontArial8B As XFont = New XFont("Arial", 8, XFontStyle.Bold)
    'Public fontArial8I As XFont = New XFont("Arial", 8, XFontStyle.Italic)
    'Public fontArial9B As XFont = New XFont("Arial", 9, XFontStyle.Bold)
    'Public fontArial10 As XFont = New XFont("Arial", 10, XFontStyle.Regular)
    'Public fontArial10B As XFont = New XFont("Arial", 10, XFontStyle.Bold)
    'Public fontArial12B As XFont = New XFont("Arial", 12, XFontStyle.Bold)
    'Public fontArial14 As XFont = New XFont("Arial", 14, XFontStyle.Regular)
    'Public fontArial16B As XFont = New XFont("Arial", 16, XFontStyle.Bold)
    'Public fontTime8IB As XFont = New XFont("Times New Roman", 8, XFontStyle.BoldItalic)
    'Public fontTime10IB As XFont = New XFont("Times New Roman", 10, XFontStyle.BoldItalic)
    'Public fontTime12IB As XFont = New XFont("Times New Roman", 12, XFontStyle.BoldItalic)
    'Public PenLine As New Pen(Brushes.Black, 0.5)
    'Public HalfPenLine As New Pen(Brushes.Lavender, 0.25)
    'Public ColWidth As Integer = 28
    'Public ExtraWidth As Integer = 0
    'Public TPS As Decimal = 0
    'Public TVQ As Decimal = 0
    'Public AllItems(12) As Integer
    'Public AllItems1(12) As Integer
    'Public AllItems2(12) As Integer
    'Public AllTotalSales As Decimal
    'Public AllTotalItemLast1 As Integer
    'Public alltotalitemlast2 As Integer
    'Public AllTotalItem As Integer
    'Public MoisDesc() As String = {"JAN", "FEV", "MAR", "AVR", "MAI", "JUN", "JUL", "AOU", "SEP", "OCT", "NOV", "DEC"}
    'Public Filigrane As XImage
    'Public CieColor As XBrush = XBrushes.DarkRed
    'Public CieColors As Color = Color.DarkRed
    'Public BV_Produit As ArrayList
    Public DistFile As FieldLayout
    Public DistData As FieldExtract
    Public lstUPC As New List(Of String)
    Public lstUPCOther As New List(Of String)
    Public skipProduct As New List(Of String)
    Public skipPharma As New List(Of String)

    Structure FieldLayout
        Public MultiCie As Integer
        Public Cie As String
        Public FichierClient As String
        Public FileNameStart As String
        Public FormatFichier As Integer
        Public PremiereLigne As Integer
        Public ConserveColonne As Integer
        Public NoPharmaPos As Integer
        Public NoPharmaLen As Integer
        Public NoProduitPos As Integer
        Public NoProduitLen As Integer
        Public NoProduitPrefixe As String
        Public DatePos As Integer
        Public DateLen As Integer
        Public DateFormat As String
        Public BannierePos As Integer
        Public BanniereLen As Integer
        Public BanniereOverride1 As String
        Public banniereOverride2 As String
        Public PharmaciePos As Integer
        Public Pharmacie2Pos As Integer
        Public PharmacieLen As Integer
        Public AdressePos As Integer
        Public AdresseLen As Integer
        Public VillePos As Integer
        Public VilleLen As Integer
        Public CodePostalPos As Integer
        Public CodePostalLen As Integer
        Public UPCClientPos As Integer
        Public UPCClientLen As Integer
        Public UPCProduitPos As Integer
        Public UPCProduitLen As Integer
        Public ProduitPos As Integer
        Public Produit2Pos As Integer
        Public ProduitLen As Integer
        Public QtePos As Integer
        Public QteLen As Integer
        Public MontantPos As Integer
        Public MontantLen As Integer
        Public Diviseur As Integer
        Public FacturePos As Integer
        Public FactureLen As Integer
    End Structure

    Structure FieldExtract
        Public NoPharma As String
        Public noProduit As String
        Public TransDate As String
        Public Banniere As String
        Public Pharmacie As String
        Public Adresse As String
        Public Ville As String
        Public CodePostal As String
        Public UPCClient As String
        Public UPCProduit As String
        Public Produit As String
        Public Qte As Double
        Public Montant As Decimal
        Public NoFacture As String
    End Structure
End Module
