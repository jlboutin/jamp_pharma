﻿Imports System.IO
Imports System.Data.OleDb
Imports SpreadsheetLight

Module ExcelAcess
    Public Function getDataTable(FirstRow As Integer, Date_Column As Integer, Optional ByVal Path As String = "", Optional ByVal Filter As String = "") As DataTable
        Dim fileDialog As New OpenFileDialog
        Dim result As DialogResult
        Dim strConn As String = ""
        Dim Output As New System.Data.DataTable
        Dim ReadOk As Boolean = False
        Dim ErrorMessage As String = ""
        Dim ColTitle As String = ""
        Dim nbCol As Integer = 0
        Dim dr As DataRow

        getDataTable = Output
        If Path.Length > 0 Then
            fileDialog.InitialDirectory = Path
        End If
        If Filter.Length > 0 Then
            fileDialog.Filter = "Excel Files (" & Filter & "*.xlsx)|" & Filter & "*.xlsx|All Files (*.*)|*.*"
        Else
            fileDialog.Filter = "Excel Files (*.xlsx)|*.xlsx|All Files (*.*)|*.*"
        End If
        result = fileDialog.ShowDialog()
        If fileDialog.FileName.Length = 0 Then Exit Function
        If Filter.Length > 0 Then
            If Not fileDialog.SafeFileName.ToUpper.StartsWith(Filter.ToUpper) Then
                If MsgBox("Le fichier n'a pas le bon nom, désirez-vous l'importer quand même?", MsgBoxStyle.OkCancel, "Mauvais nom de fichier") = MsgBoxResult.Cancel Then
                    Exit Function
                End If
            End If
        End If

        ImportFile = fileDialog.FileName

        Using sl As New SLDocument(ImportFile)
            For xloop = 1 To sl.GetWorksheetStatistics.EndColumnIndex
                ColTitle = sl.GetCellValueAsString(FirstRow, xloop)
                If ColTitle.Length > 0 Then
                    If xloop = Date_Column Then
                        Output.Columns.Add(sl.GetCellValueAsDateTime(FirstRow, xloop), GetType(String))
                    Else
                        Output.Columns.Add(sl.GetCellValueAsString(FirstRow, xloop), GetType(String))
                    End If

                    nbCol += 1
                Else
                    Exit For
                End If
            Next
            For zloop = FirstRow + 1 To sl.GetWorksheetStatistics.EndRowIndex
                If sl.GetCellValueAsString(zloop, 1).Length > 0 Then
                    dr = Output.NewRow
                    For xloop = 1 To nbCol
                        dr(xloop - 1) = sl.GetCellValueAsString(zloop, xloop)
                        If xloop = Date_Column AndAlso IsNumeric(dr(xloop - 1)) Then
                            dr(xloop - 1) = DateTime.FromOADate(CDbl(dr(xloop - 1))).ToShortDateString
                        End If
                    Next
                    Output.Rows.Add(dr)
                Else
                    Exit For
                End If

            Next
        End Using

        If Output.Rows.Count < FirstRow + 1 Then
            Dim reply As DialogResult = MessageBox.Show("Désolé, fichier illisible" & vbLf & ErrorMessage,
                        "Fichier illisible", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
        Return Output
    End Function

    Public Function getDataset(Optional ByVal Path As String = "", Optional ByVal Filter As String = "") As DataSet
        Dim fileDialog As New OpenFileDialog
        Dim result As DialogResult
        Dim strConn As String = ""
        Dim Output As New System.Data.DataSet()
        Dim ReadOk As Boolean = False
        Dim ErrorMessage As String = ""

        getDataset = Output
        If Path.Length > 0 Then
            fileDialog.InitialDirectory = Path
        End If
        If Filter.Length > 0 Then
            fileDialog.Filter = "Excel Files (" & Filter & "*.xls*)|" & Filter & "*.xls*|All Files (*.*)|*.*"
        Else
            fileDialog.Filter = "Excel Files (*.xls*)|*.xls*|All Files (*.*)|*.*"
        End If
        result = fileDialog.ShowDialog()
        If fileDialog.FileName.Length = 0 Then Exit Function
        If Filter.Length > 0 Then
            If Not fileDialog.SafeFileName.ToUpper.StartsWith(Filter.ToUpper) Then
                If MsgBox("Le fichier n'a pas le bon nom, désirez-vous l'importer quand même?", MsgBoxStyle.OkCancel, "Mauvais nom de fichier") = MsgBoxResult.Cancel Then
                    Exit Function
                End If
            End If
        End If

        ImportFile = fileDialog.SafeFileName
        Try
            If fileDialog.FileName.Substring(fileDialog.FileName.LastIndexOf("."c)).ToLower() = ".xlsx" Then
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & fileDialog.FileName & ";Extended Properties=""Excel 12.0 Xml;HDR=YES;IMEX=1"";Mode=Read"
            Else
                If fileDialog.FileName.Substring(fileDialog.FileName.LastIndexOf("."c)).ToLower() = ".xls" Then
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & fileDialog.FileName & ";Extended Properties=""Excel 12.0 Xml;HDR=YES;IMEX=1"";Mode=Read"
                End If
            End If
            If strConn <> "" Then
                Using conn As New System.Data.OleDb.OleDbConnection(strConn)
                    conn.Open()
                    Dim schemaTable As System.Data.DataTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, New Object() {Nothing, Nothing, Nothing, "TABLE"})
                    For Each schemaRow As DataRow In schemaTable.Rows
                        Dim sheet As String = schemaRow("TABLE_NAME").ToString()
                        If Not sheet.EndsWith("_") Then
                            Try
                                Dim cmd As New OleDbCommand("SELECT * FROM [" & sheet & "]", conn) With {
                                    .CommandType = CommandType.Text
                                }
                                Dim outputTable As New System.Data.DataTable(sheet)
                                Output.Tables.Add(outputTable)
                                Dim adapter As OleDbDataAdapter = New OleDbDataAdapter(cmd)
                                adapter.Fill(outputTable)
                                ReadOk = True
                            Catch ex As Exception
                                ErrorMessage = ex.Message
                            End Try
                            Exit For 'Only need first sheet
                        End If
                    Next
                    conn.Close()
                End Using
            End If
        Catch ex As Exception
            ErrorMessage = ex.Message
        End Try
        If Not ReadOk Then
            Dim reply As DialogResult = MessageBox.Show("Désolé, fichier illisible" & vbLf & ErrorMessage,
                        "Fichier illisible", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
        Return Output
    End Function

    Public Function getDatasetCSV(Optional ByVal Path As String = "", Optional ByVal Filter As String = "") As DataSet
        Dim fileDialog As New OpenFileDialog
        Dim result As DialogResult
        Dim strConn As String = ""
        Dim Output As New System.Data.DataSet()
        Dim ReadOk As Boolean = False
        Dim ErrorMessage As String = ""

        getDatasetCSV = Output
        If Path.Length > 0 Then
            fileDialog.InitialDirectory = Path
        End If
        If Filter.Length > 0 Then
            fileDialog.Filter = "CSV Files (" & Filter & "*.csv)|" & Filter & "*.csv|All Files (*.*)|*.*"
        Else
            fileDialog.Filter = "CSV Files (*.csv)|*.csv|All Files (*.*)|*.*"
        End If
        result = fileDialog.ShowDialog()
        If fileDialog.FileName.Length = 0 Then Exit Function
        If Filter.Length > 0 Then
            If Not fileDialog.SafeFileName.ToUpper.StartsWith(Filter.ToUpper) Then
                If MsgBox("Le fichier n'a pas le bon nom, désirez-vous l'importer quand même?", MsgBoxStyle.OkCancel, "Mauvais nom de fichier") = MsgBoxResult.Cancel Then
                    Exit Function
                End If
            End If
        End If

        ImportFile = fileDialog.SafeFileName
        If fileDialog.FileName.Substring(fileDialog.FileName.LastIndexOf("."c)).ToLower() = ".csv" Then
            Dim SR As StreamReader = New StreamReader(fileDialog.FileName, System.Text.Encoding.UTF7)
            Dim line As String = SR.ReadLine()
            Dim SplitChar As Char = ","c
            If line.Contains("|") Then
                SplitChar = "|"c
            End If
            If line.Contains(";") Then
                SplitChar = ";"c
            End If

            Dim strArray As String() = line.Split(SplitChar)
            Dim dt As DataTable = New DataTable()
            Dim row As DataRow
            Dim nbCol As Integer = 0
            Dim curpos As Integer
            Dim quotepos As Integer

            For Each s As String In strArray
                dt.Columns.Add(New DataColumn())
                nbCol += 1
            Next
            Do
                If Not line = String.Empty Then
                    If SplitChar <> "|"c Then
                        'Clean CSV file
                        curpos = 0
                        quotepos = line.IndexOf("""", 0)
                        If quotepos = -1 Then quotepos = line.Length + 1
                        While curpos < line.Length
                            curpos = line.IndexOf(SplitChar, curpos + 1)
                            If curpos = -1 Then Exit While
                            If curpos > quotepos Then
                                quotepos = line.IndexOf("""", quotepos + 1)
                                curpos = line.IndexOf(SplitChar, quotepos + 1)
                                quotepos = line.IndexOf("""", curpos + 1)
                                If quotepos = -1 Then quotepos = line.Length + 1
                            End If
                            line = line.Substring(0, curpos) & "|" & line.Substring(curpos + 1)
                        End While
                    End If
                    strArray = line.Split("|"c)
                    If strArray.Count = nbCol Then
                        row = dt.NewRow
                        For Col As Integer = 0 To nbCol - 1
                            strArray(Col) = strArray(Col).Replace("""", "").Trim
                        Next
                        row.ItemArray = strArray
                        dt.Rows.Add(row)
                    Else
                        Dim skip = True
                    End If
                Else
                    Exit Do
                End If
                line = SR.ReadLine
            Loop
            Output.Tables.Add(dt)
            ReadOk = True
        End If
        Return Output
    End Function

    Function Extract_Data_Flat(Source As String, Beginning As Integer, Len As Integer) As String
        Extract_Data_Flat = ""
        Try
            If Beginning > 0 And Beginning + Len < Source.Length Then
                Extract_Data_Flat = Source.Substring(Beginning - 1, Len)
            End If
        Catch
        End Try

    End Function

    Sub Extract_Data_Excel(Source As DataRow, Position As Integer, ByRef Destination As String)
        Dim result As String = ""
        If Position > 0 Then
            If Position <= Source.ItemArray.Length AndAlso Not (Source(Position - 1) Is Nothing) Then
                result = Source(Position - 1).ToString
            Else
                result = ""
            End If
        End If
        If result.Length > 0 Or Position > DistFile.ConserveColonne Or Position = 0 Then
            Destination = result
        Else
            If Destination Is Nothing Then Destination = ""
        End If
    End Sub
End Module
