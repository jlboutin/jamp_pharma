﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Find_Pharm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblCompanie = New System.Windows.Forms.Label()
        Me.btnQuit = New System.Windows.Forms.Button()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tbCode = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.tbBan = New System.Windows.Forms.TextBox()
        Me.tbPharm = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblDistri = New System.Windows.Forms.Label()
        Me.tbAdress = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.tbVille = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.tbCodePostal = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.tbPharmaID = New System.Windows.Forms.TextBox()
        Me.btUse = New System.Windows.Forms.Button()
        Me.tbSearchPharm1 = New System.Windows.Forms.TextBox()
        Me.tbSearchBan1 = New System.Windows.Forms.TextBox()
        Me.tbSearchAdress1 = New System.Windows.Forms.TextBox()
        Me.tbSearchVille1 = New System.Windows.Forms.TextBox()
        Me.tbSearchCodePostal1 = New System.Windows.Forms.TextBox()
        Me.tbSearchPharmaID1 = New System.Windows.Forms.TextBox()
        Me.tbSearchPharmaID2 = New System.Windows.Forms.TextBox()
        Me.tbSearchCodePostal2 = New System.Windows.Forms.TextBox()
        Me.tbSearchVille2 = New System.Windows.Forms.TextBox()
        Me.tbSearchAdress2 = New System.Windows.Forms.TextBox()
        Me.tbSearchBan2 = New System.Windows.Forms.TextBox()
        Me.tbSearchPharm2 = New System.Windows.Forms.TextBox()
        Me.tbSearchPharmaID3 = New System.Windows.Forms.TextBox()
        Me.tbSearchCodePostal3 = New System.Windows.Forms.TextBox()
        Me.tbSearchVille3 = New System.Windows.Forms.TextBox()
        Me.tbSearchAdress3 = New System.Windows.Forms.TextBox()
        Me.tbSearchBan3 = New System.Windows.Forms.TextBox()
        Me.tbSearchPharm3 = New System.Windows.Forms.TextBox()
        Me.tbSearchPharmaID4 = New System.Windows.Forms.TextBox()
        Me.tbSearchCodePostal4 = New System.Windows.Forms.TextBox()
        Me.tbSearchVille4 = New System.Windows.Forms.TextBox()
        Me.tbSearchAdress4 = New System.Windows.Forms.TextBox()
        Me.tbSearchBan4 = New System.Windows.Forms.TextBox()
        Me.tbSearchPharm4 = New System.Windows.Forms.TextBox()
        Me.tbSearchPharmaID5 = New System.Windows.Forms.TextBox()
        Me.tbSearchCodePostal5 = New System.Windows.Forms.TextBox()
        Me.tbSearchVille5 = New System.Windows.Forms.TextBox()
        Me.tbSearchAdress5 = New System.Windows.Forms.TextBox()
        Me.tbSearchBan5 = New System.Windows.Forms.TextBox()
        Me.tbSearchPharm5 = New System.Windows.Forms.TextBox()
        Me.tbSearchPharmaID6 = New System.Windows.Forms.TextBox()
        Me.tbSearchCodePostal6 = New System.Windows.Forms.TextBox()
        Me.tbSearchVille6 = New System.Windows.Forms.TextBox()
        Me.tbSearchAdress6 = New System.Windows.Forms.TextBox()
        Me.tbSearchBan6 = New System.Windows.Forms.TextBox()
        Me.tbSearchPharm6 = New System.Windows.Forms.TextBox()
        Me.cb1 = New System.Windows.Forms.CheckBox()
        Me.cb2 = New System.Windows.Forms.CheckBox()
        Me.cb3 = New System.Windows.Forms.CheckBox()
        Me.cb4 = New System.Windows.Forms.CheckBox()
        Me.cb5 = New System.Windows.Forms.CheckBox()
        Me.cb6 = New System.Windows.Forms.CheckBox()
        Me.tbSearchCodePostal0 = New System.Windows.Forms.TextBox()
        Me.tbSearchVille0 = New System.Windows.Forms.TextBox()
        Me.tbSearchAdress0 = New System.Windows.Forms.TextBox()
        Me.tbSearchBan0 = New System.Windows.Forms.TextBox()
        Me.tbSearchPharm0 = New System.Windows.Forms.TextBox()
        Me.tbSearchPharmaID0 = New System.Windows.Forms.TextBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cbSkip = New System.Windows.Forms.CheckBox()
        Me.lblLigne = New System.Windows.Forms.Label()
        Me.tbSearchPharmS0 = New System.Windows.Forms.TextBox()
        Me.tbSearchPharmS1 = New System.Windows.Forms.TextBox()
        Me.tbSearchPharmS2 = New System.Windows.Forms.TextBox()
        Me.tbSearchPharmS3 = New System.Windows.Forms.TextBox()
        Me.tbSearchPharmS4 = New System.Windows.Forms.TextBox()
        Me.tbSearchPharmS5 = New System.Windows.Forms.TextBox()
        Me.tbSearchPharmS6 = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'lblCompanie
        '
        Me.lblCompanie.AutoSize = True
        Me.lblCompanie.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompanie.Location = New System.Drawing.Point(12, 8)
        Me.lblCompanie.Name = "lblCompanie"
        Me.lblCompanie.Size = New System.Drawing.Size(99, 20)
        Me.lblCompanie.TabIndex = 8
        Me.lblCompanie.Text = "Compagnie"
        '
        'btnQuit
        '
        Me.btnQuit.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnQuit.Location = New System.Drawing.Point(471, 267)
        Me.btnQuit.Name = "btnQuit"
        Me.btnQuit.Size = New System.Drawing.Size(74, 29)
        Me.btnQuit.TabIndex = 9
        Me.btnQuit.Text = "Passer"
        Me.btnQuit.UseVisualStyleBackColor = True
        '
        'btnSearch
        '
        Me.btnSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSearch.Location = New System.Drawing.Point(252, 268)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(110, 29)
        Me.btnSearch.TabIndex = 7
        Me.btnSearch.Text = "Rechercher"
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(229, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(169, 20)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Recherche de client"
        '
        'tbCode
        '
        Me.tbCode.Enabled = False
        Me.tbCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCode.Location = New System.Drawing.Point(16, 66)
        Me.tbCode.Name = "tbCode"
        Me.tbCode.Size = New System.Drawing.Size(230, 20)
        Me.tbCode.TabIndex = 10
        Me.tbCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(264, 64)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(74, 17)
        Me.Label4.TabIndex = 15
        Me.Label4.Text = "No Client"
        '
        'tbBan
        '
        Me.tbBan.Enabled = False
        Me.tbBan.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbBan.Location = New System.Drawing.Point(16, 136)
        Me.tbBan.Name = "tbBan"
        Me.tbBan.Size = New System.Drawing.Size(230, 20)
        Me.tbBan.TabIndex = 14
        Me.tbBan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tbPharm
        '
        Me.tbPharm.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbPharm.Location = New System.Drawing.Point(16, 101)
        Me.tbPharm.Name = "tbPharm"
        Me.tbPharm.Size = New System.Drawing.Size(230, 20)
        Me.tbPharm.TabIndex = 12
        Me.tbPharm.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(265, 136)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(73, 17)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "Bannière"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(259, 101)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 17)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Pharmacie"
        '
        'lblDistri
        '
        Me.lblDistri.AutoSize = True
        Me.lblDistri.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDistri.Location = New System.Drawing.Point(12, 32)
        Me.lblDistri.Name = "lblDistri"
        Me.lblDistri.Size = New System.Drawing.Size(103, 20)
        Me.lblDistri.TabIndex = 16
        Me.lblDistri.Text = "Distributeur"
        '
        'tbAdress
        '
        Me.tbAdress.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbAdress.Location = New System.Drawing.Point(16, 159)
        Me.tbAdress.Name = "tbAdress"
        Me.tbAdress.Size = New System.Drawing.Size(230, 20)
        Me.tbAdress.TabIndex = 18
        Me.tbAdress.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(268, 160)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(67, 17)
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "Adresse"
        '
        'tbVille
        '
        Me.tbVille.Enabled = False
        Me.tbVille.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbVille.Location = New System.Drawing.Point(16, 182)
        Me.tbVille.Name = "tbVille"
        Me.tbVille.Size = New System.Drawing.Size(230, 20)
        Me.tbVille.TabIndex = 20
        Me.tbVille.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(282, 184)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(39, 17)
        Me.Label6.TabIndex = 19
        Me.Label6.Text = "Ville"
        '
        'tbCodePostal
        '
        Me.tbCodePostal.Enabled = False
        Me.tbCodePostal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCodePostal.Location = New System.Drawing.Point(16, 205)
        Me.tbCodePostal.Name = "tbCodePostal"
        Me.tbCodePostal.Size = New System.Drawing.Size(230, 20)
        Me.tbCodePostal.TabIndex = 21
        Me.tbCodePostal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(254, 208)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(95, 17)
        Me.Label7.TabIndex = 22
        Me.Label7.Text = "Code Postal"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(168, 235)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(96, 17)
        Me.Label8.TabIndex = 23
        Me.Label8.Text = "Code Client:"
        '
        'tbPharmaID
        '
        Me.tbPharmaID.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbPharmaID.Location = New System.Drawing.Point(264, 234)
        Me.tbPharmaID.MaxLength = 10
        Me.tbPharmaID.Name = "tbPharmaID"
        Me.tbPharmaID.Size = New System.Drawing.Size(91, 21)
        Me.tbPharmaID.TabIndex = 24
        '
        'btUse
        '
        Me.btUse.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btUse.Location = New System.Drawing.Point(373, 268)
        Me.btUse.Name = "btUse"
        Me.btUse.Size = New System.Drawing.Size(90, 29)
        Me.btUse.TabIndex = 25
        Me.btUse.Text = "Utiliser"
        Me.btUse.UseVisualStyleBackColor = True
        '
        'tbSearchPharm1
        '
        Me.tbSearchPharm1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchPharm1.Location = New System.Drawing.Point(736, 90)
        Me.tbSearchPharm1.Name = "tbSearchPharm1"
        Me.tbSearchPharm1.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchPharm1.TabIndex = 26
        '
        'tbSearchBan1
        '
        Me.tbSearchBan1.Enabled = False
        Me.tbSearchBan1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchBan1.Location = New System.Drawing.Point(736, 136)
        Me.tbSearchBan1.Name = "tbSearchBan1"
        Me.tbSearchBan1.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchBan1.TabIndex = 27
        '
        'tbSearchAdress1
        '
        Me.tbSearchAdress1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchAdress1.Location = New System.Drawing.Point(736, 159)
        Me.tbSearchAdress1.Name = "tbSearchAdress1"
        Me.tbSearchAdress1.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchAdress1.TabIndex = 28
        '
        'tbSearchVille1
        '
        Me.tbSearchVille1.Enabled = False
        Me.tbSearchVille1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchVille1.Location = New System.Drawing.Point(736, 182)
        Me.tbSearchVille1.Name = "tbSearchVille1"
        Me.tbSearchVille1.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchVille1.TabIndex = 29
        '
        'tbSearchCodePostal1
        '
        Me.tbSearchCodePostal1.Enabled = False
        Me.tbSearchCodePostal1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchCodePostal1.Location = New System.Drawing.Point(736, 205)
        Me.tbSearchCodePostal1.Name = "tbSearchCodePostal1"
        Me.tbSearchCodePostal1.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchCodePostal1.TabIndex = 30
        '
        'tbSearchPharmaID1
        '
        Me.tbSearchPharmaID1.Enabled = False
        Me.tbSearchPharmaID1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchPharmaID1.Location = New System.Drawing.Point(736, 65)
        Me.tbSearchPharmaID1.MaxLength = 7
        Me.tbSearchPharmaID1.Name = "tbSearchPharmaID1"
        Me.tbSearchPharmaID1.Size = New System.Drawing.Size(188, 21)
        Me.tbSearchPharmaID1.TabIndex = 31
        '
        'tbSearchPharmaID2
        '
        Me.tbSearchPharmaID2.Enabled = False
        Me.tbSearchPharmaID2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchPharmaID2.Location = New System.Drawing.Point(930, 65)
        Me.tbSearchPharmaID2.MaxLength = 7
        Me.tbSearchPharmaID2.Name = "tbSearchPharmaID2"
        Me.tbSearchPharmaID2.Size = New System.Drawing.Size(188, 21)
        Me.tbSearchPharmaID2.TabIndex = 37
        '
        'tbSearchCodePostal2
        '
        Me.tbSearchCodePostal2.Enabled = False
        Me.tbSearchCodePostal2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchCodePostal2.Location = New System.Drawing.Point(930, 205)
        Me.tbSearchCodePostal2.Name = "tbSearchCodePostal2"
        Me.tbSearchCodePostal2.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchCodePostal2.TabIndex = 36
        '
        'tbSearchVille2
        '
        Me.tbSearchVille2.Enabled = False
        Me.tbSearchVille2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchVille2.Location = New System.Drawing.Point(930, 182)
        Me.tbSearchVille2.Name = "tbSearchVille2"
        Me.tbSearchVille2.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchVille2.TabIndex = 35
        '
        'tbSearchAdress2
        '
        Me.tbSearchAdress2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchAdress2.Location = New System.Drawing.Point(930, 159)
        Me.tbSearchAdress2.Name = "tbSearchAdress2"
        Me.tbSearchAdress2.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchAdress2.TabIndex = 34
        '
        'tbSearchBan2
        '
        Me.tbSearchBan2.Enabled = False
        Me.tbSearchBan2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchBan2.Location = New System.Drawing.Point(930, 136)
        Me.tbSearchBan2.Name = "tbSearchBan2"
        Me.tbSearchBan2.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchBan2.TabIndex = 33
        '
        'tbSearchPharm2
        '
        Me.tbSearchPharm2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchPharm2.Location = New System.Drawing.Point(930, 90)
        Me.tbSearchPharm2.Name = "tbSearchPharm2"
        Me.tbSearchPharm2.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchPharm2.TabIndex = 32
        '
        'tbSearchPharmaID3
        '
        Me.tbSearchPharmaID3.Enabled = False
        Me.tbSearchPharmaID3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchPharmaID3.Location = New System.Drawing.Point(1124, 65)
        Me.tbSearchPharmaID3.MaxLength = 7
        Me.tbSearchPharmaID3.Name = "tbSearchPharmaID3"
        Me.tbSearchPharmaID3.Size = New System.Drawing.Size(188, 21)
        Me.tbSearchPharmaID3.TabIndex = 43
        '
        'tbSearchCodePostal3
        '
        Me.tbSearchCodePostal3.Enabled = False
        Me.tbSearchCodePostal3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchCodePostal3.Location = New System.Drawing.Point(1124, 204)
        Me.tbSearchCodePostal3.Name = "tbSearchCodePostal3"
        Me.tbSearchCodePostal3.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchCodePostal3.TabIndex = 42
        '
        'tbSearchVille3
        '
        Me.tbSearchVille3.Enabled = False
        Me.tbSearchVille3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchVille3.Location = New System.Drawing.Point(1124, 181)
        Me.tbSearchVille3.Name = "tbSearchVille3"
        Me.tbSearchVille3.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchVille3.TabIndex = 41
        '
        'tbSearchAdress3
        '
        Me.tbSearchAdress3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchAdress3.Location = New System.Drawing.Point(1124, 158)
        Me.tbSearchAdress3.Name = "tbSearchAdress3"
        Me.tbSearchAdress3.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchAdress3.TabIndex = 40
        '
        'tbSearchBan3
        '
        Me.tbSearchBan3.Enabled = False
        Me.tbSearchBan3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchBan3.Location = New System.Drawing.Point(1124, 135)
        Me.tbSearchBan3.Name = "tbSearchBan3"
        Me.tbSearchBan3.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchBan3.TabIndex = 39
        '
        'tbSearchPharm3
        '
        Me.tbSearchPharm3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchPharm3.Location = New System.Drawing.Point(1124, 89)
        Me.tbSearchPharm3.Name = "tbSearchPharm3"
        Me.tbSearchPharm3.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchPharm3.TabIndex = 38
        '
        'tbSearchPharmaID4
        '
        Me.tbSearchPharmaID4.Enabled = False
        Me.tbSearchPharmaID4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchPharmaID4.Location = New System.Drawing.Point(1318, 65)
        Me.tbSearchPharmaID4.MaxLength = 7
        Me.tbSearchPharmaID4.Name = "tbSearchPharmaID4"
        Me.tbSearchPharmaID4.Size = New System.Drawing.Size(188, 21)
        Me.tbSearchPharmaID4.TabIndex = 49
        '
        'tbSearchCodePostal4
        '
        Me.tbSearchCodePostal4.Enabled = False
        Me.tbSearchCodePostal4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchCodePostal4.Location = New System.Drawing.Point(1318, 205)
        Me.tbSearchCodePostal4.Name = "tbSearchCodePostal4"
        Me.tbSearchCodePostal4.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchCodePostal4.TabIndex = 48
        '
        'tbSearchVille4
        '
        Me.tbSearchVille4.Enabled = False
        Me.tbSearchVille4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchVille4.Location = New System.Drawing.Point(1318, 182)
        Me.tbSearchVille4.Name = "tbSearchVille4"
        Me.tbSearchVille4.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchVille4.TabIndex = 47
        '
        'tbSearchAdress4
        '
        Me.tbSearchAdress4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchAdress4.Location = New System.Drawing.Point(1318, 159)
        Me.tbSearchAdress4.Name = "tbSearchAdress4"
        Me.tbSearchAdress4.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchAdress4.TabIndex = 46
        '
        'tbSearchBan4
        '
        Me.tbSearchBan4.Enabled = False
        Me.tbSearchBan4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchBan4.Location = New System.Drawing.Point(1318, 136)
        Me.tbSearchBan4.Name = "tbSearchBan4"
        Me.tbSearchBan4.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchBan4.TabIndex = 45
        '
        'tbSearchPharm4
        '
        Me.tbSearchPharm4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchPharm4.Location = New System.Drawing.Point(1318, 90)
        Me.tbSearchPharm4.Name = "tbSearchPharm4"
        Me.tbSearchPharm4.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchPharm4.TabIndex = 44
        '
        'tbSearchPharmaID5
        '
        Me.tbSearchPharmaID5.Enabled = False
        Me.tbSearchPharmaID5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchPharmaID5.Location = New System.Drawing.Point(1512, 64)
        Me.tbSearchPharmaID5.MaxLength = 7
        Me.tbSearchPharmaID5.Name = "tbSearchPharmaID5"
        Me.tbSearchPharmaID5.Size = New System.Drawing.Size(188, 21)
        Me.tbSearchPharmaID5.TabIndex = 55
        '
        'tbSearchCodePostal5
        '
        Me.tbSearchCodePostal5.Enabled = False
        Me.tbSearchCodePostal5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchCodePostal5.Location = New System.Drawing.Point(1512, 204)
        Me.tbSearchCodePostal5.Name = "tbSearchCodePostal5"
        Me.tbSearchCodePostal5.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchCodePostal5.TabIndex = 54
        '
        'tbSearchVille5
        '
        Me.tbSearchVille5.Enabled = False
        Me.tbSearchVille5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchVille5.Location = New System.Drawing.Point(1512, 181)
        Me.tbSearchVille5.Name = "tbSearchVille5"
        Me.tbSearchVille5.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchVille5.TabIndex = 53
        '
        'tbSearchAdress5
        '
        Me.tbSearchAdress5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchAdress5.Location = New System.Drawing.Point(1512, 158)
        Me.tbSearchAdress5.Name = "tbSearchAdress5"
        Me.tbSearchAdress5.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchAdress5.TabIndex = 52
        '
        'tbSearchBan5
        '
        Me.tbSearchBan5.Enabled = False
        Me.tbSearchBan5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchBan5.Location = New System.Drawing.Point(1512, 135)
        Me.tbSearchBan5.Name = "tbSearchBan5"
        Me.tbSearchBan5.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchBan5.TabIndex = 51
        '
        'tbSearchPharm5
        '
        Me.tbSearchPharm5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchPharm5.Location = New System.Drawing.Point(1512, 89)
        Me.tbSearchPharm5.Name = "tbSearchPharm5"
        Me.tbSearchPharm5.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchPharm5.TabIndex = 50
        '
        'tbSearchPharmaID6
        '
        Me.tbSearchPharmaID6.Enabled = False
        Me.tbSearchPharmaID6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchPharmaID6.Location = New System.Drawing.Point(1706, 64)
        Me.tbSearchPharmaID6.MaxLength = 7
        Me.tbSearchPharmaID6.Name = "tbSearchPharmaID6"
        Me.tbSearchPharmaID6.Size = New System.Drawing.Size(188, 21)
        Me.tbSearchPharmaID6.TabIndex = 61
        '
        'tbSearchCodePostal6
        '
        Me.tbSearchCodePostal6.Enabled = False
        Me.tbSearchCodePostal6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchCodePostal6.Location = New System.Drawing.Point(1706, 204)
        Me.tbSearchCodePostal6.Name = "tbSearchCodePostal6"
        Me.tbSearchCodePostal6.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchCodePostal6.TabIndex = 60
        '
        'tbSearchVille6
        '
        Me.tbSearchVille6.Enabled = False
        Me.tbSearchVille6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchVille6.Location = New System.Drawing.Point(1706, 181)
        Me.tbSearchVille6.Name = "tbSearchVille6"
        Me.tbSearchVille6.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchVille6.TabIndex = 59
        '
        'tbSearchAdress6
        '
        Me.tbSearchAdress6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchAdress6.Location = New System.Drawing.Point(1706, 158)
        Me.tbSearchAdress6.Name = "tbSearchAdress6"
        Me.tbSearchAdress6.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchAdress6.TabIndex = 58
        '
        'tbSearchBan6
        '
        Me.tbSearchBan6.Enabled = False
        Me.tbSearchBan6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchBan6.Location = New System.Drawing.Point(1706, 135)
        Me.tbSearchBan6.Name = "tbSearchBan6"
        Me.tbSearchBan6.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchBan6.TabIndex = 57
        '
        'tbSearchPharm6
        '
        Me.tbSearchPharm6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchPharm6.Location = New System.Drawing.Point(1706, 89)
        Me.tbSearchPharm6.Name = "tbSearchPharm6"
        Me.tbSearchPharm6.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchPharm6.TabIndex = 56
        '
        'cb1
        '
        Me.cb1.AutoSize = True
        Me.cb1.Location = New System.Drawing.Point(821, 239)
        Me.cb1.Name = "cb1"
        Me.cb1.Size = New System.Drawing.Size(15, 14)
        Me.cb1.TabIndex = 68
        Me.cb1.UseVisualStyleBackColor = True
        '
        'cb2
        '
        Me.cb2.AutoSize = True
        Me.cb2.Location = New System.Drawing.Point(1014, 239)
        Me.cb2.Name = "cb2"
        Me.cb2.Size = New System.Drawing.Size(15, 14)
        Me.cb2.TabIndex = 69
        Me.cb2.UseVisualStyleBackColor = True
        '
        'cb3
        '
        Me.cb3.AutoSize = True
        Me.cb3.Location = New System.Drawing.Point(1210, 239)
        Me.cb3.Name = "cb3"
        Me.cb3.Size = New System.Drawing.Size(15, 14)
        Me.cb3.TabIndex = 70
        Me.cb3.UseVisualStyleBackColor = True
        '
        'cb4
        '
        Me.cb4.AutoSize = True
        Me.cb4.Location = New System.Drawing.Point(1404, 241)
        Me.cb4.Name = "cb4"
        Me.cb4.Size = New System.Drawing.Size(15, 14)
        Me.cb4.TabIndex = 71
        Me.cb4.UseVisualStyleBackColor = True
        '
        'cb5
        '
        Me.cb5.AutoSize = True
        Me.cb5.Location = New System.Drawing.Point(1602, 241)
        Me.cb5.Name = "cb5"
        Me.cb5.Size = New System.Drawing.Size(15, 14)
        Me.cb5.TabIndex = 72
        Me.cb5.UseVisualStyleBackColor = True
        '
        'cb6
        '
        Me.cb6.AutoSize = True
        Me.cb6.Location = New System.Drawing.Point(1793, 241)
        Me.cb6.Name = "cb6"
        Me.cb6.Size = New System.Drawing.Size(15, 14)
        Me.cb6.TabIndex = 73
        Me.cb6.UseVisualStyleBackColor = True
        '
        'tbSearchCodePostal0
        '
        Me.tbSearchCodePostal0.Enabled = False
        Me.tbSearchCodePostal0.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchCodePostal0.Location = New System.Drawing.Point(357, 205)
        Me.tbSearchCodePostal0.Name = "tbSearchCodePostal0"
        Me.tbSearchCodePostal0.Size = New System.Drawing.Size(315, 20)
        Me.tbSearchCodePostal0.TabIndex = 78
        '
        'tbSearchVille0
        '
        Me.tbSearchVille0.Enabled = False
        Me.tbSearchVille0.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchVille0.Location = New System.Drawing.Point(357, 182)
        Me.tbSearchVille0.Name = "tbSearchVille0"
        Me.tbSearchVille0.Size = New System.Drawing.Size(315, 20)
        Me.tbSearchVille0.TabIndex = 77
        '
        'tbSearchAdress0
        '
        Me.tbSearchAdress0.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchAdress0.Location = New System.Drawing.Point(357, 159)
        Me.tbSearchAdress0.Name = "tbSearchAdress0"
        Me.tbSearchAdress0.Size = New System.Drawing.Size(315, 20)
        Me.tbSearchAdress0.TabIndex = 76
        '
        'tbSearchBan0
        '
        Me.tbSearchBan0.Enabled = False
        Me.tbSearchBan0.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchBan0.Location = New System.Drawing.Point(357, 136)
        Me.tbSearchBan0.Name = "tbSearchBan0"
        Me.tbSearchBan0.Size = New System.Drawing.Size(315, 20)
        Me.tbSearchBan0.TabIndex = 75
        '
        'tbSearchPharm0
        '
        Me.tbSearchPharm0.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchPharm0.Location = New System.Drawing.Point(357, 90)
        Me.tbSearchPharm0.Name = "tbSearchPharm0"
        Me.tbSearchPharm0.Size = New System.Drawing.Size(315, 20)
        Me.tbSearchPharm0.TabIndex = 74
        '
        'tbSearchPharmaID0
        '
        Me.tbSearchPharmaID0.Enabled = False
        Me.tbSearchPharmaID0.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchPharmaID0.Location = New System.Drawing.Point(357, 66)
        Me.tbSearchPharmaID0.MaxLength = 7
        Me.tbSearchPharmaID0.Name = "tbSearchPharmaID0"
        Me.tbSearchPharmaID0.Size = New System.Drawing.Size(315, 21)
        Me.tbSearchPharmaID0.TabIndex = 79
        '
        'ToolTip1
        '
        Me.ToolTip1.AutomaticDelay = 100
        Me.ToolTip1.AutoPopDelay = 10000
        Me.ToolTip1.InitialDelay = 100
        Me.ToolTip1.ReshowDelay = 20
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(13, 272)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(120, 18)
        Me.Label9.TabIndex = 80
        Me.Label9.Text = "Ne pas traiter :"
        Me.Label9.Visible = False
        '
        'cbSkip
        '
        Me.cbSkip.AutoSize = True
        Me.cbSkip.Location = New System.Drawing.Point(139, 276)
        Me.cbSkip.Name = "cbSkip"
        Me.cbSkip.Size = New System.Drawing.Size(15, 14)
        Me.cbSkip.TabIndex = 81
        Me.cbSkip.UseVisualStyleBackColor = True
        Me.cbSkip.Visible = False
        '
        'lblLigne
        '
        Me.lblLigne.AutoSize = True
        Me.lblLigne.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLigne.Location = New System.Drawing.Point(439, 32)
        Me.lblLigne.Name = "lblLigne"
        Me.lblLigne.Size = New System.Drawing.Size(53, 20)
        Me.lblLigne.TabIndex = 83
        Me.lblLigne.Text = "Ligne"
        '
        'tbSearchPharmS0
        '
        Me.tbSearchPharmS0.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchPharmS0.Location = New System.Drawing.Point(357, 113)
        Me.tbSearchPharmS0.Name = "tbSearchPharmS0"
        Me.tbSearchPharmS0.Size = New System.Drawing.Size(315, 20)
        Me.tbSearchPharmS0.TabIndex = 84
        '
        'tbSearchPharmS1
        '
        Me.tbSearchPharmS1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchPharmS1.Location = New System.Drawing.Point(736, 113)
        Me.tbSearchPharmS1.Name = "tbSearchPharmS1"
        Me.tbSearchPharmS1.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchPharmS1.TabIndex = 85
        '
        'tbSearchPharmS2
        '
        Me.tbSearchPharmS2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchPharmS2.Location = New System.Drawing.Point(930, 113)
        Me.tbSearchPharmS2.Name = "tbSearchPharmS2"
        Me.tbSearchPharmS2.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchPharmS2.TabIndex = 86
        '
        'tbSearchPharmS3
        '
        Me.tbSearchPharmS3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchPharmS3.Location = New System.Drawing.Point(1124, 112)
        Me.tbSearchPharmS3.Name = "tbSearchPharmS3"
        Me.tbSearchPharmS3.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchPharmS3.TabIndex = 87
        '
        'tbSearchPharmS4
        '
        Me.tbSearchPharmS4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchPharmS4.Location = New System.Drawing.Point(1318, 113)
        Me.tbSearchPharmS4.Name = "tbSearchPharmS4"
        Me.tbSearchPharmS4.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchPharmS4.TabIndex = 88
        '
        'tbSearchPharmS5
        '
        Me.tbSearchPharmS5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchPharmS5.Location = New System.Drawing.Point(1512, 112)
        Me.tbSearchPharmS5.Name = "tbSearchPharmS5"
        Me.tbSearchPharmS5.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchPharmS5.TabIndex = 89
        '
        'tbSearchPharmS6
        '
        Me.tbSearchPharmS6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearchPharmS6.Location = New System.Drawing.Point(1706, 112)
        Me.tbSearchPharmS6.Name = "tbSearchPharmS6"
        Me.tbSearchPharmS6.Size = New System.Drawing.Size(188, 20)
        Me.tbSearchPharmS6.TabIndex = 90
        '
        'Find_Pharm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 18.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1900, 315)
        Me.ControlBox = False
        Me.Controls.Add(Me.tbSearchPharmS6)
        Me.Controls.Add(Me.tbSearchPharmS5)
        Me.Controls.Add(Me.tbSearchPharmS4)
        Me.Controls.Add(Me.tbSearchPharmS3)
        Me.Controls.Add(Me.tbSearchPharmS2)
        Me.Controls.Add(Me.tbSearchPharmS1)
        Me.Controls.Add(Me.tbSearchPharmS0)
        Me.Controls.Add(Me.lblLigne)
        Me.Controls.Add(Me.cbSkip)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.tbSearchPharmaID0)
        Me.Controls.Add(Me.tbSearchCodePostal0)
        Me.Controls.Add(Me.tbSearchVille0)
        Me.Controls.Add(Me.tbSearchAdress0)
        Me.Controls.Add(Me.tbSearchBan0)
        Me.Controls.Add(Me.tbSearchPharm0)
        Me.Controls.Add(Me.cb6)
        Me.Controls.Add(Me.cb5)
        Me.Controls.Add(Me.cb4)
        Me.Controls.Add(Me.cb3)
        Me.Controls.Add(Me.cb2)
        Me.Controls.Add(Me.cb1)
        Me.Controls.Add(Me.tbSearchPharmaID6)
        Me.Controls.Add(Me.tbSearchCodePostal6)
        Me.Controls.Add(Me.tbSearchVille6)
        Me.Controls.Add(Me.tbSearchAdress6)
        Me.Controls.Add(Me.tbSearchBan6)
        Me.Controls.Add(Me.tbSearchPharm6)
        Me.Controls.Add(Me.tbSearchPharmaID5)
        Me.Controls.Add(Me.tbSearchCodePostal5)
        Me.Controls.Add(Me.tbSearchVille5)
        Me.Controls.Add(Me.tbSearchAdress5)
        Me.Controls.Add(Me.tbSearchBan5)
        Me.Controls.Add(Me.tbSearchPharm5)
        Me.Controls.Add(Me.tbSearchPharmaID4)
        Me.Controls.Add(Me.tbSearchCodePostal4)
        Me.Controls.Add(Me.tbSearchVille4)
        Me.Controls.Add(Me.tbSearchAdress4)
        Me.Controls.Add(Me.tbSearchBan4)
        Me.Controls.Add(Me.tbSearchPharm4)
        Me.Controls.Add(Me.tbSearchPharmaID3)
        Me.Controls.Add(Me.tbSearchCodePostal3)
        Me.Controls.Add(Me.tbSearchVille3)
        Me.Controls.Add(Me.tbSearchAdress3)
        Me.Controls.Add(Me.tbSearchBan3)
        Me.Controls.Add(Me.tbSearchPharm3)
        Me.Controls.Add(Me.tbSearchPharmaID2)
        Me.Controls.Add(Me.tbSearchCodePostal2)
        Me.Controls.Add(Me.tbSearchVille2)
        Me.Controls.Add(Me.tbSearchAdress2)
        Me.Controls.Add(Me.tbSearchBan2)
        Me.Controls.Add(Me.tbSearchPharm2)
        Me.Controls.Add(Me.tbSearchPharmaID1)
        Me.Controls.Add(Me.tbSearchCodePostal1)
        Me.Controls.Add(Me.tbSearchVille1)
        Me.Controls.Add(Me.tbSearchAdress1)
        Me.Controls.Add(Me.tbSearchBan1)
        Me.Controls.Add(Me.tbSearchPharm1)
        Me.Controls.Add(Me.btUse)
        Me.Controls.Add(Me.tbPharmaID)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.tbCodePostal)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.tbVille)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.tbAdress)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lblDistri)
        Me.Controls.Add(Me.tbCode)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.tbBan)
        Me.Controls.Add(Me.tbPharm)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblCompanie)
        Me.Controls.Add(Me.btnQuit)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "Find_Pharm"
        Me.Text = "Recherche de client"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblCompanie As System.Windows.Forms.Label
    Friend WithEvents btnQuit As System.Windows.Forms.Button
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tbCode As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tbBan As System.Windows.Forms.TextBox
    Friend WithEvents tbPharm As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblDistri As System.Windows.Forms.Label
    Friend WithEvents tbAdress As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tbVille As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tbCodePostal As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents tbPharmaID As System.Windows.Forms.TextBox
    Friend WithEvents btUse As System.Windows.Forms.Button
    Friend WithEvents tbSearchPharm1 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchBan1 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchAdress1 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchVille1 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchCodePostal1 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchPharmaID1 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchPharmaID2 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchCodePostal2 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchVille2 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchAdress2 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchBan2 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchPharm2 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchPharmaID3 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchCodePostal3 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchVille3 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchAdress3 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchBan3 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchPharm3 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchPharmaID4 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchCodePostal4 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchVille4 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchAdress4 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchBan4 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchPharm4 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchPharmaID5 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchCodePostal5 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchVille5 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchAdress5 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchBan5 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchPharm5 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchPharmaID6 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchCodePostal6 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchVille6 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchAdress6 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchBan6 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchPharm6 As System.Windows.Forms.TextBox
    Friend WithEvents cb1 As System.Windows.Forms.CheckBox
    Friend WithEvents cb2 As System.Windows.Forms.CheckBox
    Friend WithEvents cb3 As System.Windows.Forms.CheckBox
    Friend WithEvents cb4 As System.Windows.Forms.CheckBox
    Friend WithEvents cb5 As System.Windows.Forms.CheckBox
    Friend WithEvents cb6 As System.Windows.Forms.CheckBox
    Friend WithEvents tbSearchCodePostal0 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchVille0 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchAdress0 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchBan0 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchPharm0 As System.Windows.Forms.TextBox
    Friend WithEvents tbSearchPharmaID0 As System.Windows.Forms.TextBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cbSkip As System.Windows.Forms.CheckBox
    Friend WithEvents lblLigne As Label
    Friend WithEvents tbSearchPharmS0 As TextBox
    Friend WithEvents tbSearchPharmS1 As TextBox
    Friend WithEvents tbSearchPharmS2 As TextBox
    Friend WithEvents tbSearchPharmS3 As TextBox
    Friend WithEvents tbSearchPharmS4 As TextBox
    Friend WithEvents tbSearchPharmS5 As TextBox
    Friend WithEvents tbSearchPharmS6 As TextBox
End Class
