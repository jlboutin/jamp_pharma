﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Security.Cryptography
Imports System.Text
Imports MySql.Data.MySqlClient

Public Class JampPharmaSynchro


    Private Sub Btn_Exit_Click(sender As Object, e As EventArgs) Handles btn_Exit.Click
        Application.Exit()
    End Sub

    Private Sub Btn_Start_Click(sender As Object, e As EventArgs) Handles btn_Start.Click
        Dim Jamp_con As New SqlConnection(My.Settings.Jamp_CS)
        Dim GSS_con As New SqlConnection(My.Settings.GSS_CS)
        Dim GRABB_con As New SqlConnection(My.Settings.GRABB_CS)
        Dim CRM_con As New MySqlConnection(My.Settings.CRM_CS)

        Dim Query As String
        Dim QueryIN As String
        Dim Index As Integer
        Dim IndexD As Integer
        Dim IndexC As Integer
        Dim IndexP As Integer
        Dim IndexU As Integer
        Dim sLastUpdate As String
        Dim LastUpdate As DateTime
        Dim CurUpdate As DateTime = Now
        Dim Count As Integer
        Dim UpdateData As Boolean = False
        Dim UpdateCheckGSS As DateTime = Now
        Dim DataString As String
        Dim sNewSHA512 As String
        Dim sSHA512 As String

        Dim iCieCode As Integer
        Dim iFamCode As Integer
        Dim iRepCode As Integer
        Dim iClientCode As Integer
        Dim iRegCode As Integer
        Dim iActive As Integer
        Dim iLangue As Integer

        btn_Start.Enabled = False

        Jamp_con.Open()
        GSS_con.Open()
        GRABB_con.Open()
        CRM_con.Open()

        '*******************************************************
        'Get Last Update Info
        '*******************************************************

        Query = "SELECT dLastUpdMisc FROM gss_Config WHERE iCnfgCode = 1"
        sLastUpdate = DB_Exec_Scalar(Query, GSS_con)
        Date.TryParse(sLastUpdate, LastUpdate)

#Region "Attribut Clients"
        '*******************************************************
        'Load Attribut Client
        '*******************************************************

        Dim dtAttCL As New DataTable 'Get Data From Fidelio
        Query = "SELECT id, description, modified_date FROM re_clients_att WHERE parent_id = 'ROOT' AND modified_date >= '" & LastUpdate & "'"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtAttCL)
            End Using
        End Using

        Dim dtGSSAttCL As New DataTable 'Get Data From GSS
        Query = "SELECT iAttCode, sAttName, sAttGuid FROM gss_AttCL"
        Using cmd As New SqlCommand(Query, GSS_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtGSSAttCL)
            End Using
        End Using
        Dim dvGSSAttCL = New DataView(dtGSSAttCL)

        Count = 1
        Index = 0
        For Each Row In dtAttCL.Rows
            btn_Start.Text = "Attributs Client" & vbLf & Count
            Application.DoEvents()
            Count += 1

            dvGSSAttCL.RowFilter = "sAttGuid = '" & Row("id") & "'"
            If dvGSSAttCL.Count = 0 Then
                If Index = 0 Then
                    Index = DB_Get_Next_Index("iAttCode", "gss_AttCL", GSS_con)
                Else
                    Index += 1
                End If
                Query = "INSERT INTO gss_AttCL VALUES (" _
                            & Index & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("id")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GSS_con)
                UpdateData = True
            Else
                If dvGSSAttCL(0)("sAttName") <> Row("description").ToString.Trim Then
                    Query = "UPDATE gss_AttCL SET " _
                                & "sAttName = " & SFilter(Row("description")) & ", " _
                                & "dAttUpdate = " & SFilter(Row("modified_date")) & " " _
                                & "WHERE iAttCode = " & dvGSSAttCL(0)("iAttCode")
                    DB_Exec_NonQuery(Query, GSS_con)
                    UpdateData = True
                End If
            End If
        Next

        If UpdateData = True Then
            dtGSSAttCL.Reset()
            Query = "SELECT iAttCode, sAttName, sAttGuid FROM gss_AttCL"
            Using cmd As New SqlCommand(Query, GSS_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtGSSAttCL)
                End Using
            End Using
            dvGSSAttCL.Dispose()
            dvGSSAttCL = New DataView(dtGSSAttCL)
            UpdateData = False
        End If
#End Region

#Region "Attribut Molécules"
        '*******************************************************
        'Load Attribut Molécule
        '*******************************************************

        Dim dtAttMO As New DataTable 'Get Data FROM Fidelio
        Query = "SELECT id, description, modified_date FROM in_items_att WHERE parent_id = 'ROOT' AND modified_date >= '" & LastUpdate & "'"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtAttMO)
            End Using
        End Using

        Dim dtGSSAttMO As New DataTable 'Get Data From GSS
        Query = "SELECT iAttCode, sAttName, sAttGuid FROM gss_AttMO"
        Using cmd As New SqlCommand(Query, GSS_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtGSSAttMO)
            End Using
        End Using
        Dim dvGSSAttMO = New DataView(dtGSSAttMO)

        Count = 1
        Index = 0
        For Each Row In dtAttMO.Rows
            btn_Start.Text = "Attributs Molécule" & vbLf & Count
            Application.DoEvents()
            Count += 1

            dvGSSAttMO.RowFilter = "sAttGuid = '" & Row("id") & "'"
            If dvGSSAttMO.Count = 0 Then
                If Index = 0 Then
                    Index = DB_Get_Next_Index("iAttCode", "gss_AttMO", GSS_con)
                Else
                    Index += 1
                End If
                Query = "INSERT INTO gss_AttMO VALUES (" _
                            & Index & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("id")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GSS_con)
                UpdateData = True
            Else
                If dvGSSAttMO(0)("sAttName") <> Row("description").ToString.Trim Then
                    Query = "UPDATE gss_AttMO SET " _
                                & "sAttName = " & SFilter(Row("description")) & ", " _
                                & "dAttUpdate = " & SFilter(Row("modified_date")) & " " _
                                & "WHERE iAttCode = " & dvGSSAttMO(0)("iAttCode")
                    DB_Exec_NonQuery(Query, GSS_con)
                    UpdateData = True
                End If
            End If
        Next

        If UpdateData = True Then
            dtGSSAttMO.Reset()
            Query = "SELECT iAttCode, sAttName, sAttGuid FROM gss_AttMO"
            Using cmd As New SqlCommand(Query, GSS_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtGSSAttMO)
                End Using
            End Using
            dvGSSAttMO.Dispose()
            dvGSSAttMO = New DataView(dtGSSAttMO)
            UpdateData = False
        End If
#End Region

#Region "Provinces"
        '*******************************************************
        'Load Prov
        '*******************************************************

        Dim dtProv As New DataTable 'Get Data From Fidelio
        Query = "SELECT id, description, modified_date, xx_pays_id, xx_taxes_id FROM xx_provinces " _
                    & "WHERE (xx_pays_id = 'USA' OR xx_pays_id = 'CANADA') AND modified_date >= '" & LastUpdate & "'"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtProv)
            End Using
        End Using

        Dim dtGSSProv As New DataTable 'Get Data From GSS
        Query = "SELECT iProvCode, sProvName, sProvShortName, sProvTax, sProvCountry, iProvPrix FROM gss_Prov"
        Using cmd As New SqlCommand(Query, GSS_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtGSSProv)
            End Using
        End Using
        Dim dvGSSProv = New DataView(dtGSSProv)

        Count = 1
        Index = 0
        For Each Row In dtProv.Rows
            btn_Start.Text = "Provinces" & vbLf & Count
            Application.DoEvents()
            Count += 1

            dvGSSProv.RowFilter = "sProvName = '" & Row("id") & "'"
            If dvGSSProv.Count = 0 Then
                If Index = 0 Then
                    Index = DB_Get_Next_Index("iProvCode", "gss_Prov", GSS_con)
                Else
                    Index += 1
                End If

                Query = "INSERT INTO gss_Prov VALUES (" _
                            & Index & ", " _
                            & SFilter(Row("id")) & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("xx_taxes_id")) & ", " _
                            & SFilter(Row("xx_pays_id")) & ", " _
                            & "0" & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GSS_con)
                DB_Exec_NonQuery(Query, GRABB_con)
                DB_Exec_NonQuery_MySQL(Query, CRM_con)

                UpdateData = True
            Else
                If dvGSSProv(0)("sProvName") <> Row("id").ToString.Trim Or
                   dvGSSProv(0)("sProvShortName") <> Row("description").ToString.Trim Or
                   dvGSSProv(0)("sProvTax") <> Row("xx_taxes_id").ToString.Trim Or
                   dvGSSProv(0)("sProvCountry") <> Row("xx_pays_id").ToString.Trim Then

                    Query = "UPDATE gss_Prov SET " _
                                & "sProvName = " & SFilter(Row("id")) & ", " _
                                & "sProvShortName = " & SFilter(Row("description")) & ", " _
                                & "sProvTax = " & SFilter(Row("xx_taxes_id")) & ", " _
                                & "sProvCountry = " & SFilter(Row("xx_pays_id")) & ", " _
                                & "dProvUpdate = " & SFilter(Row("modified_date")) & " " _
                                & "WHERE iProvCode = " & dvGSSProv(0)("iProvCode")
                    DB_Exec_NonQuery(Query, GSS_con)
                    DB_Exec_NonQuery(Query, GRABB_con)
                    DB_Exec_NonQuery_MySQL(Query, CRM_con)

                    UpdateData = True
                End If
            End If
        Next

        If UpdateData = True Then
            dtGSSProv.Reset()
            Query = "SELECT iProvCode, sProvName, sProvShortName, sProvTax, sProvCountry, iProvPrix FROM gss_Prov"
            Using cmd As New SqlCommand(Query, GSS_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtGSSProv)
                End Using
            End Using
            dvGSSProv.Dispose()
            dvGSSProv = New DataView(dtGSSProv)
            UpdateData = False
        End If
#End Region

#Region "Compagnies"
        '*******************************************************
        'Load Cie
        '*******************************************************

        Dim dtCie As New DataTable 'Get Data From Fidelio
        Query = "SELECT id, description, modified_date FROM xx_compagnies WHERE modified_date >= '" & LastUpdate & "'"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtCie)
            End Using
        End Using

        Dim dtGSSCie As New DataTable 'Get Data From GSS
        Query = "SELECT iCieCode, sCieID, sCieName, sCieDepGuid FROM gss_Cie"
        Using cmd As New SqlCommand(Query, GSS_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtGSSCie)
            End Using
        End Using
        Dim dvGSSCie = New DataView(dtGSSCie)

        Dim dtDepCie As New DataTable
        Query = "SELECT id, description, parent_id, modified_date FROM re_clients_att"
        'Query = "SELECT id, description, parent_id, modified_date FROM ve_vendeurs_att"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtDepCie)
            End Using
        End Using
        Dim dvDepCie = New DataView(dtDepCie)
        Dim sCieDepGuid As String

        dvDepCie.RowFilter = "description = 'Territoire' and parent_id = 'ROOT'"
        If dvDepCie.Count > 0 Then
            dvDepCie.RowFilter = "parent_id = '" & dvDepCie(0)("id") & "'"

            Count = 1
            Index = 0
            For Each Row In dtCie.Rows
                btn_Start.Text = "Compagnies" & vbLf & Count
                Application.DoEvents()
                Count += 1

                sCieDepGuid = ""
                For xloop = 0 To dvDepCie.Count - 1
                    If Row("description").ToString.ToUpper.StartsWith(dvDepCie(xloop)("description").ToString.Substring(0, 3).ToUpper) Then
                        sCieDepGuid = dvDepCie(xloop)("id")
                        Exit For
                    End If
                Next

                dvGSSCie.RowFilter = "sCieID = '" & Row("id").ToString.Trim & "'"
                If dvGSSCie.Count = 0 Then
                    If Index = 0 Then
                        Index = DB_Get_Next_Index("iCieCode", "gss_Cie", GSS_con)
                    Else
                        Index += 1
                    End If
                    Query = "INSERT INTO gss_Cie VALUES (" _
                            & Index & ", " _
                            & SFilter(Row("id")) & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(sCieDepGuid) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                    DB_Exec_NonQuery(Query, GSS_con)

                    Query = "INSERT INTO gss_Cie VALUES (" _
                            & Index & ", " _
                            & SFilter(Row("id")) & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                    DB_Exec_NonQuery(Query, GRABB_con)
                    DB_Exec_NonQuery_MySQL(Query, CRM_con)

                    UpdateData = True
                Else
                    If dvGSSCie(0)("sCieID") <> Row("id").ToString.Trim Or
                   dvGSSCie(0)("sCieName").toupper <> Row("description").ToString.Trim.ToUpper Or
                   dvGSSCie(0)("sCieDepGuid") <> sCieDepGuid Then

                        Query = "UPDATE gss_Cie SET " _
                                & "sCieID = " & SFilter(Row("id")) & ", " _
                                & "sCieName = " & SFilter(Row("description")) & ", " _
                                & "sCieDepGuid = " & SFilter(sCieDepGuid) & ", " _
                                & "dCieUpdate = " & SFilter(Row("modified_date")) & " " _
                                & "WHERE iCieCode = " & dvGSSCie(0)("iCieCode")
                        DB_Exec_NonQuery(Query, GSS_con)

                        Query = "UPDATE gss_Cie SET " _
                                & "sCieID = " & SFilter(Row("id")) & ", " _
                                & "sCieName = " & SFilter(Row("description")) & ", " _
                                & "dCieUpdate = " & SFilter(Row("modified_date")) & " " _
                                & "WHERE iCieCode = " & dvGSSCie(0)("iCieCode")
                        DB_Exec_NonQuery(Query, GRABB_con)
                        DB_Exec_NonQuery_MySQL(Query, CRM_con)

                        UpdateData = True
                    End If
                End If
            Next
        End If


        If UpdateData = True Then
            dtGSSCie.Reset()
            Query = "SELECT iCieCode, sCieID, sCieName, sCieDepGuid FROM gss_Cie"
            Using cmd As New SqlCommand(Query, GSS_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtGSSCie)
                End Using
            End Using
            dvGSSCie.Dispose()
            dvGSSCie = New DataView(dtGSSCie)
            UpdateData = False
        End If
#End Region

#Region "Classes de clients"
        '*******************************************************
        'Load ClientClass
        '*******************************************************

        dvGSSAttCL.RowFilter = "sAttName = 'Produits autorisés'"
        Dim sClientClassGuid As String = dvGSSAttCL(0)("sAttGuid")

        Dim dtClientClass As New DataTable
        Query = "SELECT id, description, modified_date FROM re_clients_att WHERE parent_id = '" & sClientClassGuid & "' AND modified_date >= '" & LastUpdate & "' ORDER BY description"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtClientClass)
            End Using
        End Using

        Dim dtGSSClientClass As New DataTable
        Query = "SELECT iClientClassCode, sClientClassName, sClientClassGuid FROM gss_Client_Class"
        Using cmd As New SqlCommand(Query, GSS_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtGSSClientClass)
            End Using
        End Using
        Dim dvGSSClientClass = New DataView(dtGSSClientClass)

        Count = 1
        Index = 0
        For Each Row In dtClientClass.Rows
            btn_Start.Text = "ClientClass" & vbLf & Count
            Application.DoEvents()
            Count += 1

            dvGSSClientClass.RowFilter = "sClientClassGuid = '" & Row("id") & "'"
            If dvGSSClientClass.Count = 0 Then
                If Index = 0 Then
                    Index = DB_Get_Next_Index("iClientClassCode", "gss_Client_Class", GSS_con)
                Else
                    Index += 1
                End If

                Query = "INSERT INTO gss_Client_Class VALUES (" _
                            & Index & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("id")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GSS_con)

                Query = "INSERT INTO gss_Client_Class VALUES (" _
                            & Index & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GRABB_con)
                DB_Exec_NonQuery_MySQL(Query, CRM_con)

                UpdateData = True
            Else
                If dvGSSClientClass(0)("sClientClassName") <> Row("description").ToString.Trim Then
                    Query = "UPDATE gss_Client_Class SET " _
                                & "sClientClassName = " & SFilter(Row("description")) & ", " _
                                & "dClientClassUpdate = " & SFilter(Row("modified_date")) & " " _
                                & "WHERE iClientClassCode = " & dvGSSClientClass(0)("iClientClassCode")
                    DB_Exec_NonQuery(Query, GSS_con)
                    DB_Exec_NonQuery(Query, GRABB_con)
                    DB_Exec_NonQuery_MySQL(Query, CRM_con)

                    UpdateData = True
                End If
            End If
        Next

        If UpdateData = True Then
            dtGSSClientClass.Reset()
            Query = "SELECT iClientClassCode, sClientClassName, sClientClassGuid FROM gss_Client_Class"
            Using cmd As New SqlCommand(Query, GSS_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtGSSClientClass)
                End Using
            End Using
            dvGSSClientClass.Dispose()
            dvGSSClientClass = New DataView(dtGSSClientClass)
            UpdateData = False
        End If
#End Region

#Region "Classes de molécules"
        '*******************************************************
        'Load Class1
        '*******************************************************

        dvGSSAttMO.RowFilter = "sAttName = 'Type'"
        Dim sClass1Guid As String = dvGSSAttMO(0)("sAttGuid")

        Dim dtClass1 As New DataTable
        Query = "SELECT id, description, modified_date FROM in_items_att WHERE parent_id = '" & sClass1Guid & "' AND modified_date >= '" & LastUpdate & "' ORDER BY description"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtClass1)
            End Using
        End Using

        Dim dtGSSClass1 As New DataTable
        Query = "SELECT iClass1Code, sClass1Name, sClass1Guid FROM gss_Class1"
        Using cmd As New SqlCommand(Query, GSS_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtGSSClass1)
            End Using
        End Using
        Dim dvGSSClass1 = New DataView(dtGSSClass1)

        Count = 1
        Index = 0
        For Each Row In dtClass1.Rows
            btn_Start.Text = "Class1" & vbLf & Count
            Application.DoEvents()
            Count += 1

            dvGSSClass1.RowFilter = "sClass1Guid = '" & Row("id") & "'"
            If dvGSSClass1.Count = 0 Then
                If Index = 0 Then
                    Index = DB_Get_Next_Index("iClass1Code", "gss_Class1", GSS_con)
                Else
                    Index += 1
                End If

                Query = "INSERT INTO gss_Class1 VALUES (" _
                            & Index & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("id")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GSS_con)

                Query = "INSERT INTO gss_Class1 VALUES (" _
                            & Index & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GRABB_con)
                DB_Exec_NonQuery_MySQL(Query, CRM_con)

                UpdateData = True
            Else
                If dvGSSClass1(0)("sClass1Name") <> Row("description").ToString.Trim Then
                    Query = "UPDATE gss_Class1 SET " _
                                & "sClass1Name = " & SFilter(Row("description")) & ", " _
                                & "dClass1Update = " & SFilter(Row("modified_date")) & " " _
                                & "WHERE iClass1Code = " & dvGSSClass1(0)("iClass1Code")
                    DB_Exec_NonQuery(Query, GSS_con)
                    DB_Exec_NonQuery(Query, GRABB_con)
                    DB_Exec_NonQuery_MySQL(Query, CRM_con)

                    UpdateData = True
                End If
            End If
        Next

        If UpdateData = True Then
            dtGSSClass1.Reset()
            Query = "SELECT iClass1Code, sClass1Name, sClass1Guid FROM gss_Class1"
            Using cmd As New SqlCommand(Query, GSS_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtGSSClass1)
                End Using
            End Using
            dvGSSClass1.Dispose()
            dvGSSClass1 = New DataView(dtGSSClass1)
            UpdateData = False
        End If
#End Region

#Region "Exclusivités"
        '*******************************************************
        'Load Excl
        '*******************************************************

        dvGSSAttMO.RowFilter = "sAttName = 'Statut Marche'"
        Dim sExclGuid As String = dvGSSAttMO(0)("sAttGuid")

        Dim dtExcl As New DataTable
        Query = "SELECT id, description, modified_date FROM in_items_att WHERE parent_id = '" & sExclGuid & "' AND modified_date >= '" & LastUpdate & "' ORDER BY description"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtExcl)
            End Using
        End Using

        Dim dtGSSExcl As New DataTable
        Query = "SELECT iExclCode, sExclName, sExclGuid FROM gss_Excl"
        Using cmd As New SqlCommand(Query, GSS_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtGSSExcl)
            End Using
        End Using
        Dim dvGSSExcl = New DataView(dtGSSExcl)

        Count = 1
        Index = 0
        For Each Row In dtExcl.Rows
            btn_Start.Text = "Excl" & vbLf & Count
            Application.DoEvents()
            Count += 1

            dvGSSExcl.RowFilter = "sExclGuid = '" & Row("id") & "'"
            If dvGSSExcl.Count = 0 Then
                If Index = 0 Then
                    Index = DB_Get_Next_Index("iExclCode", "gss_Excl", GSS_con)
                Else
                    Index += 1
                End If

                Query = "INSERT INTO gss_Excl VALUES (" _
                            & Index & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("id")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GSS_con)

                Query = "INSERT INTO gss_Excl VALUES (" _
                            & Index & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GRABB_con)
                DB_Exec_NonQuery_MySQL(Query, CRM_con)

                UpdateData = True
            Else
                If dvGSSExcl(0)("sExclName") <> Row("description").ToString.Trim Then
                    Query = "UPDATE gss_Excl SET " _
                                & "sExclName = " & SFilter(Row("description")) & ", " _
                                & "dExclUpdate = " & SFilter(Row("modified_date")) & " " _
                                & "WHERE iExclCode = " & dvGSSExcl(0)("iExclCode")
                    DB_Exec_NonQuery(Query, GSS_con)
                    DB_Exec_NonQuery(Query, GRABB_con)
                    DB_Exec_NonQuery_MySQL(Query, CRM_con)

                    UpdateData = True
                End If
            End If
        Next

        If UpdateData = True Then
            dtGSSExcl.Reset()
            Query = "SELECT iExclCode, sExclName, sExclGuid FROM gss_Excl"
            Using cmd As New SqlCommand(Query, GSS_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtGSSExcl)
                End Using
            End Using
            dvGSSExcl.Dispose()
            dvGSSExcl = New DataView(dtGSSExcl)
            UpdateData = False
        End If
#End Region

#Region "Couleurs"
        '*******************************************************
        'Load Coul
        '*******************************************************

        dvGSSAttMO.RowFilter = "sAttName = 'Couleur'"
        Dim sCoulGuid As String = dvGSSAttMO(0)("sAttGuid")

        Dim dtCoul As New DataTable
        Query = "SELECT id, description, modified_date FROM in_items_att WHERE parent_id = '" & sCoulGuid & "' AND modified_date >= '" & LastUpdate & "' ORDER BY description"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtCoul)
            End Using
        End Using

        Dim dtGSSCoul As New DataTable
        Query = "SELECT iCoulCode, sCoulName, sCoulGuid FROM gss_Coul"
        Using cmd As New SqlCommand(Query, GSS_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtGSSCoul)
            End Using
        End Using
        Dim dvGSSCoul = New DataView(dtGSSCoul)

        Count = 1
        Index = 0
        For Each Row In dtCoul.Rows
            btn_Start.Text = "Coul" & vbLf & Count
            Application.DoEvents()
            Count += 1

            dvGSSCoul.RowFilter = "sCoulGuid = '" & Row("id") & "'"
            If dvGSSCoul.Count = 0 Then
                If Index = 0 Then
                    Index = DB_Get_Next_Index("iCoulCode", "gss_Coul", GSS_con)
                Else
                    Index += 1
                End If

                Query = "INSERT INTO gss_Coul VALUES (" _
                            & Index & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("id")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GSS_con)

                Query = "INSERT INTO gss_Coul VALUES (" _
                            & Index & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GRABB_con)
                DB_Exec_NonQuery_MySQL(Query, CRM_con)

                UpdateData = True
            Else
                If dvGSSCoul(0)("sCoulName") <> Row("description").ToString.Trim Then
                    Query = "UPDATE gss_Coul SET " _
                                & "sCoulName = " & SFilter(Row("description")) & ", " _
                                & "dCoulUpdate = " & SFilter(Row("modified_date")) & " " _
                                & "WHERE iCoulCode = " & dvGSSCoul(0)("iCoulCode")
                    DB_Exec_NonQuery(Query, GSS_con)
                    DB_Exec_NonQuery(Query, GRABB_con)
                    DB_Exec_NonQuery_MySQL(Query, CRM_con)

                    UpdateData = True
                End If
            End If
        Next

        If UpdateData = True Then
            dtGSSCoul.Reset()
            Query = "SELECT iCoulCode, sCoulName, sCoulGuid FROM gss_Coul"
            Using cmd As New SqlCommand(Query, GSS_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtGSSCoul)
                End Using
            End Using
            dvGSSCoul.Dispose()
            dvGSSCoul = New DataView(dtGSSCoul)
            UpdateData = False
        End If
#End Region

#Region "Saveurs"
        '*******************************************************
        'Load Sav
        '*******************************************************

        dvGSSAttMO.RowFilter = "sAttName = 'Saveur'"
        Dim sSavGuid As String = dvGSSAttMO(0)("sAttGuid")

        Dim dtSav As New DataTable
        Query = "SELECT id, description, modified_date FROM in_items_att WHERE parent_id = '" & sSavGuid & "' AND modified_date >= '" & LastUpdate & "' ORDER BY description"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtSav)
            End Using
        End Using

        Dim dtGSSSav As New DataTable
        Query = "SELECT iSavCode, sSavName, sSavGuid FROM gss_Sav"
        Using cmd As New SqlCommand(Query, GSS_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtGSSSav)
            End Using
        End Using
        Dim dvGSSSav = New DataView(dtGSSSav)

        Count = 1
        Index = 0
        For Each Row In dtSav.Rows
            btn_Start.Text = "Sav" & vbLf & Count
            Application.DoEvents()
            Count += 1

            dvGSSSav.RowFilter = "sSavGuid = '" & Row("id") & "'"
            If dvGSSSav.Count = 0 Then
                If Index = 0 Then
                    Index = DB_Get_Next_Index("iSavCode", "gss_Sav", GSS_con)
                Else
                    Index += 1
                End If

                Query = "INSERT INTO gss_Sav VALUES (" _
                            & Index & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("id")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GSS_con)

                Query = "INSERT INTO gss_Sav VALUES (" _
                            & Index & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GRABB_con)
                DB_Exec_NonQuery_MySQL(Query, CRM_con)

                UpdateData = True
            Else
                If dvGSSSav(0)("sSavName") <> Row("description").ToString.Trim Then
                    Query = "UPDATE gss_Sav SET " _
                                & "sSavName = " & SFilter(Row("description")) & ", " _
                                & "dSavUpdate = " & SFilter(Row("modified_date")) & " " _
                                & "WHERE iSavCode = " & dvGSSSav(0)("iSavCode")
                    DB_Exec_NonQuery(Query, GSS_con)
                    DB_Exec_NonQuery(Query, GRABB_con)
                    DB_Exec_NonQuery_MySQL(Query, CRM_con)

                    UpdateData = True
                End If
            End If
        Next

        If UpdateData = True Then
            dtGSSSav.Reset()
            Query = "SELECT iSavCode, sSavName, sSavGuid FROM gss_Sav"
            Using cmd As New SqlCommand(Query, GSS_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtGSSSav)
                End Using
            End Using
            dvGSSSav.Dispose()
            dvGSSSav = New DataView(dtGSSSav)
            UpdateData = False
        End If
#End Region

#Region "Emballages"
        '*******************************************************
        'Load Emb
        '*******************************************************

        dvGSSAttMO.RowFilter = "sAttName = 'Emballage'"
        Dim sEmbGuid As String = dvGSSAttMO(0)("sAttGuid")

        Dim dtEmb As New DataTable
        Query = "SELECT id, description, modified_date FROM in_items_att WHERE parent_id = '" & sEmbGuid & "' AND modified_date >= '" & LastUpdate & "' ORDER BY description"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtEmb)
            End Using
        End Using

        Dim dtGSSEmb As New DataTable
        Query = "SELECT iEmbCode, sEmbName, sEmbGuid FROM gss_Emb"
        Using cmd As New SqlCommand(Query, GSS_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtGSSEmb)
            End Using
        End Using
        Dim dvGSSEmb = New DataView(dtGSSEmb)

        Count = 1
        Index = 0
        For Each Row In dtEmb.Rows
            btn_Start.Text = "Emb" & vbLf & Count
            Application.DoEvents()
            Count += 1

            dvGSSEmb.RowFilter = "sEmbGuid = '" & Row("id") & "'"
            If dvGSSEmb.Count = 0 Then
                If Index = 0 Then
                    Index = DB_Get_Next_Index("iEmbCode", "gss_Emb", GSS_con)
                Else
                    Index += 1
                End If

                Query = "INSERT INTO gss_Emb VALUES (" _
                            & Index & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("id")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GSS_con)

                Query = "INSERT INTO gss_Emb VALUES (" _
                            & Index & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GRABB_con)
                DB_Exec_NonQuery_MySQL(Query, CRM_con)

                UpdateData = True
            Else
                If dvGSSEmb(0)("sEmbName") <> Row("description").ToString.Trim Then
                    Query = "UPDATE gss_Emb SET " _
                                & "sEmbName = " & SFilter(Row("description")) & ", " _
                                & "dEmbUpdate = " & SFilter(Row("modified_date")) & " " _
                                & "WHERE iEmbCode = " & dvGSSEmb(0)("iEmbCode")
                    DB_Exec_NonQuery(Query, GSS_con)
                    DB_Exec_NonQuery(Query, GRABB_con)
                    DB_Exec_NonQuery_MySQL(Query, CRM_con)

                    UpdateData = True
                End If
            End If
        Next

        If UpdateData = True Then
            dtGSSEmb.Reset()
            Query = "SELECT iEmbCode, sEmbName, sEmbGuid FROM gss_Emb"
            Using cmd As New SqlCommand(Query, GSS_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtGSSEmb)
                End Using
            End Using
            dvGSSEmb.Dispose()
            dvGSSEmb = New DataView(dtGSSEmb)
            UpdateData = False
        End If
#End Region

#Region "Familles de produits"
        '*******************************************************
        'Load Fam
        '*******************************************************

        dvGSSAttMO.RowFilter = "sAttName = 'Molecule'"
        Dim sFamGuid As String = dvGSSAttMO(0)("sAttGuid")

        Dim dtFam As New DataTable
        Query = "SELECT id, description, modified_date FROM in_items_att WHERE parent_id = '" & sFamGuid & "' AND modified_date >= '" & LastUpdate & "'"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtFam)
            End Using
        End Using

        Dim dtGSSFam As New DataTable
        Query = "SELECT iFamCode, sFamName, sFamGuid FROM gss_Fam"
        Using cmd As New SqlCommand(Query, GSS_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtGSSFam)
            End Using
        End Using
        Dim dvGSSFam As New DataView(dtGSSFam)

        Count = 1
        Index = 0
        For Each Row In dtFam.Rows
            btn_Start.Text = "Famille Produit" & vbLf & Count
            Application.DoEvents()
            Count += 1

            dvGSSFam.RowFilter = "sFamGuid = '" & Row("id") & "'"
            If dvGSSFam.Count = 0 Then
                If Index = 0 Then
                    Index = DB_Get_Next_Index("iFamCode", "gss_Fam", GSS_con)
                Else
                    Index += 1
                End If

                Query = "INSERT INTO gss_Fam VALUES (" _
                            & Index & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("id")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GSS_con)

                Query = "INSERT INTO gss_Fam VALUES (" _
                            & Index & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GRABB_con)
                DB_Exec_NonQuery_MySQL(Query, CRM_con)

                UpdateData = True
            Else
                If dvGSSFam(0)("sFamName") <> Row("description").ToString.Trim Then
                    Query = "UPDATE gss_Fam SET " _
                                & "sFamName = " & SFilter(Row("description")) & ", " _
                                & "dFamUpdate = " & SFilter(Row("modified_date")) & " " _
                                & "WHERE iFamCode = " & dvGSSFam(0)("iFamCode")
                    DB_Exec_NonQuery(Query, GSS_con)
                    DB_Exec_NonQuery(Query, GRABB_con)
                    DB_Exec_NonQuery_MySQL(Query, CRM_con)

                    UpdateData = True
                End If
            End If
        Next

        If UpdateData = True Then
            dtGSSFam.Reset()
            Query = "SELECT iFamCode, sFamName, sFamGuid FROM gss_Fam"
            Using cmd As New SqlCommand(Query, GSS_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtGSSFam)
                End Using
            End Using
            dvGSSFam.Dispose()
            dvGSSFam = New DataView(dtGSSFam)
            UpdateData = False
        End If
#End Region

#Region "Catégories de molécules"
        '*******************************************************
        'Load Mol Cat
        '*******************************************************

        dvGSSAttMO.RowFilter = "sAttName = 'Categorie'"
        Dim sProdCatGuid As String = dvGSSAttMO(0)("sAttGuid")

        Dim dtProdCat As New DataTable
        Query = "SELECT id, description, modified_date FROM in_items_att WHERE parent_id = '" & sProdCatGuid & "' AND modified_date >= '" & LastUpdate & "'"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtProdCat)
            End Using
        End Using

        Dim dtGSSProdCat As New DataTable
        Query = "SELECT iProdCatCode, sProdCatName, sProdCatGuid FROM gss_Prod_Cat"
        Using cmd As New SqlCommand(Query, GSS_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtGSSProdCat)
            End Using
        End Using
        Dim dvGSSProdCat As New DataView(dtGSSProdCat)

        Count = 1
        Index = 0
        For Each Row In dtProdCat.Rows
            btn_Start.Text = "Prod Cat Produit" & vbLf & Count
            Application.DoEvents()
            Count += 1

            dvGSSProdCat.RowFilter = "sProdCatGuid = '" & Row("id") & "'"
            If dvGSSProdCat.Count = 0 Then
                If Index = 0 Then
                    Index = DB_Get_Next_Index("iProdCatCode", "gss_Prod_Cat", GSS_con)
                Else
                    Index += 1
                End If

                Query = "INSERT INTO gss_Prod_Cat VALUES (" _
                            & Index & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("id")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GSS_con)

                Query = "INSERT INTO gss_Prod_Cat VALUES (" _
                            & Index & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GRABB_con)
                DB_Exec_NonQuery_MySQL(Query, CRM_con)

                UpdateData = True
            Else
                If dvGSSProdCat(0)("sProdCatName") <> Row("description").ToString.Trim Then
                    Query = "UPDATE gss_Prod_Cat SET " _
                                & "sProdCatName = " & SFilter(Row("description")) & ", " _
                                & "dProdCatUpdate = " & SFilter(Row("modified_date")) & " " _
                                & "WHERE iProdCatCode = " & dvGSSProdCat(0)("iProdCatCode")
                    DB_Exec_NonQuery(Query, GSS_con)
                    DB_Exec_NonQuery(Query, GRABB_con)
                    DB_Exec_NonQuery_MySQL(Query, CRM_con)

                    UpdateData = True
                End If
            End If
        Next

        If UpdateData = True Then
            dtGSSProdCat.Reset()
            Query = "SELECT iProdCatCode, sProdCatName, sProdCatGuid FROM gss_Prod_Cat"
            Using cmd As New SqlCommand(Query, GSS_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtGSSProdCat)
                End Using
            End Using
            dvGSSProdCat.Dispose()
            dvGSSProdCat = New DataView(dtGSSProdCat)
            UpdateData = False
        End If
#End Region

#Region "Catégories de molécules CJ"
        '*******************************************************
        'Load Mol Cat CJ
        '*******************************************************

        dvGSSAttMO.RowFilter = "sAttName = 'Categorie_CJ'"
        Dim sProdCatCJGuid As String = dvGSSAttMO(0)("sAttGuid")

        Dim dtProdCatCJ As New DataTable
        Query = "SELECT id, description, modified_date FROM in_items_att WHERE parent_id = '" & sProdCatCJGuid & "' AND modified_date >= '" & LastUpdate & "'"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtProdCatCJ)
            End Using
        End Using

        Dim dtGSSProdCatCJ As New DataTable
        Query = "SELECT iProdCatCJCode, sProdCatCJName, sProdCatCJGuid FROM gss_Prod_CatCJ"
        Using cmd As New SqlCommand(Query, GSS_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtGSSProdCatCJ)
            End Using
        End Using
        Dim dvGSSProdCatCJ As New DataView(dtGSSProdCatCJ)

        Count = 1
        Index = 0
        For Each Row In dtProdCatCJ.Rows
            btn_Start.Text = "Prod Cat Produit" & vbLf & Count
            Application.DoEvents()
            Count += 1

            dvGSSProdCatCJ.RowFilter = "sProdCatCJGuid = '" & Row("id") & "'"
            If dvGSSProdCatCJ.Count = 0 Then
                If Index = 0 Then
                    Index = DB_Get_Next_Index("iProdCatCJCode", "gss_Prod_CatCJ", GSS_con)
                Else
                    Index += 1
                End If

                Query = "INSERT INTO gss_Prod_CatCJ VALUES (" _
                            & Index & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("id")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GSS_con)

                Query = "INSERT INTO gss_Prod_CatCJ VALUES (" _
                            & Index & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GRABB_con)
                DB_Exec_NonQuery_MySQL(Query, CRM_con)

                UpdateData = True
            Else
                If dvGSSProdCatCJ(0)("sProdCatCJName") <> Row("description").ToString.Trim Then
                    Query = "UPDATE gss_Prod_CatCJ SET " _
                                & "sProdCatCJName = " & SFilter(Row("description")) & ", " _
                                & "dProdCatCJUpdate = " & SFilter(Row("modified_date")) & " " _
                                & "WHERE iProdCatCJCode = " & dvGSSProdCatCJ(0)("iProdCatCJCode")
                    DB_Exec_NonQuery(Query, GSS_con)
                    DB_Exec_NonQuery(Query, GRABB_con)
                    DB_Exec_NonQuery_MySQL(Query, CRM_con)

                    UpdateData = True
                End If
            End If
        Next

        If UpdateData = True Then
            dtGSSProdCatCJ.Reset()
            Query = "SELECT iProdCatCJCode, sProdCatCJName, sProdCatCJGuid FROM gss_Prod_CatCJ"
            Using cmd As New SqlCommand(Query, GSS_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtGSSProdCatCJ)
                End Using
            End Using
            dvGSSProdCatCJ.Dispose()
            dvGSSProdCatCJ = New DataView(dtGSSProdCatCJ)
            UpdateData = False
        End If
#End Region

#Region "Départements"
        '*******************************************************
        'Load Dep
        '*******************************************************

        dvGSSAttMO.RowFilter = "sAttName = 'Departement'"
        Dim sDepGuid As String = dvGSSAttMO(0)("sAttGuid")

        Dim dtDepGuid As New DataTable
        Query = "SELECT id, description FROM in_items_att WHERE parent_id = '" & sDepGuid & "'"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtDepGuid)
            End Using
        End Using
        Dim dvDepGuid = New DataView(dtDepGuid)

        Dim sDepName As String
        Dim sDepGuidValue As String
        Dim sDepNo As String

        Dim dtDep As New DataTable
        Query = "SELECT Id, description, modified_date FROM ve_types"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtDep)
            End Using
        End Using

        Dim dtGSSDep As New DataTable
        Query = "SELECT iDepCode, iCieCode, sDepNo, sDepName, sDepGuid FROM gss_Dep"
        Using cmd As New SqlCommand(Query, GSS_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtGSSDep)
            End Using
        End Using
        Dim dvGSSDep = New DataView(dtGSSDep)

        Count = 1
        Index = 0
        For Each Row In dtDep.Rows
            btn_Start.Text = "Département" & vbLf & Count
            Application.DoEvents()
            Count += 1

            sDepNo = Row("id")
            sDepName = Row("description").ToString.Trim.Replace("'", "''")
            sDepGuidValue = ""
            dvDepGuid.RowFilter = "description = '" & sDepNo & "'"
            If dvDepGuid.Count > 0 Then
                sDepGuidValue = dvDepGuid(0)("id")
            End If
            iCieCode = 0
            For yloop = 0 To dtGSSCie.Rows.Count - 1
                If dtGSSCie(yloop)("sCieName").ToString.ToUpper.StartsWith(sDepName.Substring(0, 3).ToUpper) Then
                    iCieCode = dtGSSCie(yloop)("iCieCode")
                    Exit For
                End If
            Next

            dvGSSDep.RowFilter = "sDepNo = '" & sDepNo & "'"
            If dvGSSDep.Count = 0 Then
                If Index = 0 Then
                    Index = DB_Get_Next_Index("iDepCode", "gss_Dep", GSS_con)
                Else
                    Index += 1
                End If

                Query = "INSERT INTO gss_Dep VALUES (" _
                            & Index & ", " _
                            & iCieCode & ", " _
                            & SFilter(Row("id")) & ", " _
                            & SFilter(sDepName) & ", " _
                            & SFilter(sDepGuidValue) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GSS_con)

                Query = "INSERT INTO gss_Dep VALUES (" _
                            & Index & ", " _
                            & iCieCode & ", " _
                            & SFilter(Row("id")) & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GRABB_con)
                DB_Exec_NonQuery_MySQL(Query, CRM_con)

                'Not need in CRM

                UpdateData = True
            Else
                If dvGSSDep(0)("iCieCode") <> iCieCode Or
                   dvGSSDep(0)("sDepNo") <> Row("id").ToString.Trim Or
                   dvGSSDep(0)("sDepName") <> Row("description").ToString.Trim Then

                    Query = "UPDATE gss_Dep SET " _
                                & "iCieCode = " & iCieCode & ", " _
                                & "sDepNo = " & SFilter(Row("id")) & ", " _
                                & "sDepName = " & SFilter(sDepName) & ", " _
                                & "dDepUpdate = " & SFilter(Row("modified_date")) & " " _
                                & "WHERE iDepCode = " & dvGSSDep(0)("iDepCode")
                    DB_Exec_NonQuery(Query, GSS_con)
                    DB_Exec_NonQuery(Query, GRABB_con)
                    DB_Exec_NonQuery_MySQL(Query, CRM_con)

                    UpdateData = True
                End If
            End If
        Next

        If UpdateData = True Then
            dtGSSDep.Reset()
            Query = "SELECT iDepCode, iCieCode, sDepNo, sDepName, sDepGuid FROM gss_Dep"
            Using cmd As New SqlCommand(Query, GSS_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtGSSDep)
                End Using
            End Using
            dvGSSDep.Dispose()
            dvGSSDep = New DataView(dtGSSDep)
            UpdateData = False
        End If
#End Region

#Region "Molécules"
        '*******************************************************
        'Load Molécule
        '*******************************************************

        UpdateCheckGSS = Now.AddMinutes(-1)
        QueryIN = "SELECT DISTINCT in_items.id FROM in_items " _
                            & "LEFT OUTER JOIN in_items_attributs On in_items.id = in_items_attributs.parent_id " _
                            & "LEFT OUTER JOIN in_items_um On in_items.id = in_items_um.in_items_id " _
                            & "WHERE (in_items.modified_date >= '" & LastUpdate & "' OR " _
                            & "in_items_attributs.modified_date >= '" & LastUpdate & "' OR " _
                            & "in_items_um.modified_date >= '" & LastUpdate & "')"

        Dim dtMol As New DataTable
        Query = "SELECT id, description, desc_autre, InfosSupp, champ1, champ3, champ19, champ20, statut, modified_date FROM in_items WHERE id In (" & QueryIN & ")"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtMol)
            End Using
        End Using

        If dtMol.Rows.Count > 0 Then
            Dim dtMolUM As New DataTable
            Query = "SELECT description, in_items_id, prix1, prix2, prix3, prix4, prix5, prix6, prix7, prix8, prix9, prix10, modified_date " _
                                & "FROM in_items_um WHERE in_items_id IN (" & QueryIN & ") AND in_items_um.isdef = 1"
            Using cmd As New SqlCommand(Query, Jamp_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtMolUM)
                End Using
            End Using
            Dim dvMolUM = New DataView(dtMolUM)

            Dim dtMolAtt As New DataTable
            Query = "SELECT Parent_id, att_id, att_parent_id, modified_date FROM in_items_attributs WHERE parent_id IN (" & QueryIN & ")"
            Using cmd As New SqlCommand(Query, Jamp_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtMolAtt)
                End Using
            End Using
            Dim dvMolAtt = New DataView(dtMolAtt)

            Dim dtGSSUniv As New DataTable
            Query = "SELECT iUnivCode, iCieCode, sUnivName FROM gss_Univ"
            Using cmd As New SqlCommand(Query, GSS_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtGSSUniv)
                End Using
            End Using
            Dim dvGSSUniv = New DataView(dtGSSUniv)

            Dim dtGSSProvPrix As New DataTable
            Query = "SELECT iProvCode, iProvPrix FROM gss_Prov WHERE iprovprix > 0"
            Using cmd As New SqlCommand(Query, GSS_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtGSSProvPrix)
                End Using
            End Using
            Dim dvGSSProvPrix = New DataView(dtGSSProvPrix)

            Dim dtGSSMol As New DataTable
            Query = "SELECT iProdCode, iCieCode, iUnivCode, iFamCode, iCatCode, iCatCJCode, iClass1Code, iClass2Code, iEmbCode, iCoulCode, iSavCode, " _
                        & "sProdNo, sProdUPC, sProdName_F, sProdName_E, sProdInfos, sProd_DIN_NPN, sRAMQCode, dDeactivationDate, iActive, sSHA512 FROM gss_ProdM"
            Using cmd As New SqlCommand(Query, GSS_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtGSSMol)
                End Using
            End Using
            Dim dvGSSMol = New DataView(dtGSSMol)

            Dim dtGSSMolPrix As New DataTable
            Query = "SELECT iProdCode, iProdProv, fProdSalesPrice, fFfsMax, iExclusivityCode FROM gss_ProdP WHERE iCurrentInfoP = 1"
            Using cmd As New SqlCommand(Query, GSS_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtGSSMolPrix)
                End Using
            End Using
            Dim dvGSSMolPrix = New DataView(dtGSSMolPrix)

            Dim iUnivCode As Integer
            Dim iClass1Code As Integer
            Dim iClass2Code As Integer = 0
            Dim iCatCode As Integer
            Dim iCatCJCode As Integer
            Dim iEmbCode As Integer
            Dim iCoulCode As Integer
            Dim iSavCode As Integer
            Dim iExclCode As Integer
            Dim sProdNo As String
            Dim sProdUPC As String
            Dim sProdName_F As String
            Dim sProdName_E As String
            Dim sProdInfos As String
            Dim sProd_DIN_NPN As String
            Dim sRAMQCode As String
            Dim dDeActivation As Date

            Dim Prov(dtGSSProvPrix.Rows.Count - 1) As Integer
            Dim Price(dtGSSProvPrix.Rows.Count - 1) As Decimal
            Dim fFfsMax(dtGSSProvPrix.Rows.Count - 1) As String
            Dim sfFfsmax() As String = {""}
            Dim iExcl(dtGSSProvPrix.Rows.Count - 1) As Integer
            Dim PriceUpdate As Date
            Dim UniversalCode As String

            For xloop = 0 To dtGSSProvPrix.Rows.Count - 1
                Prov(xloop) = dtGSSProvPrix(xloop)("iProvCode")
            Next
            Count = 1
            Index = 0
            IndexD = 0
            IndexP = 0
            Dim IndexUniv = 0
            For Each Row In dtMol.Rows
                btn_Start.Text = "Molécules" & vbLf & Count
                Application.DoEvents()
                Count += 1

                'Calculate SHA512 to detect if date change on the server
                DataString = ""
                For xloop = 0 To dtMol.Columns.Count - 1
                    DataString &= Row(xloop)
                Next

                dvMolUM.RowFilter = "in_items_id = '" & Row("id") & "'"
                If dvMolUM.Count > 0 Then
                    For xloop = 0 To dtMolUM.Columns.Count - 1
                        DataString &= dvMolUM(0)(xloop)
                    Next
                End If

                dvMolAtt.RowFilter = "parent_id = '" & Row("id") & "'"
                For xloop = 0 To dvMolAtt.Count - 1
                    For yloop = 0 To dtMolAtt.Columns.Count - 1
                        DataString &= dvMolAtt(xloop)(yloop)
                    Next
                Next

                sNewSHA512 = GenerateSHA512String(DataString)

                sSHA512 = ""

                dvGSSMol.RowFilter = "sProdUPC = '" & Row("id") & "'"
                If dvGSSMol.Count > 0 Then sSHA512 = dvGSSMol(0)("sSHA512")

                If sSHA512 <> sNewSHA512 Then
                    iCieCode = 0
                    dvMolAtt.RowFilter = "parent_id = '" & Row("id") & "' AND att_parent_id = '" & sDepGuid & "'"
                    If dvMolAtt.Count > 0 Then
                        dvGSSDep.RowFilter = "sDepGuid = '" & dvMolAtt(0)("att_id") & "'"
                        If dvGSSDep.Count > 0 Then
                            iCieCode = dvGSSDep(0)("iCieCode")
                        End If
                    End If

                    Date.TryParse(Row("champ19"), dDeActivation)
                    If dDeActivation < "2010-01-01" Then dDeActivation = "2099-12-31"

                    UniversalCode = Row("champ3")
                    If UniversalCode.Length < 1 Then
                        'If no Universal Code, use UPC instead
                        UniversalCode = Row("id")
                    End If
                    dvGSSUniv.RowFilter = "iCieCode = " & iCieCode & " AND sUnivName = '" & UniversalCode & "'"
                    If dvGSSUniv.Count = 0 Then
                        If IndexUniv = 0 Then
                            IndexUniv = DB_Get_Next_Index("iUnivCode", "gss_Univ", GSS_con)
                        Else
                            IndexUniv += 1
                        End If
                        Query = "INSERT INTO gss_Univ VALUES (" _
                                    & IndexUniv & ", " _
                                    & iCieCode & ", " _
                                    & SFilter(UniversalCode) & ")"
                        DB_Exec_NonQuery(Query, GSS_con)
                        DB_Exec_NonQuery(Query, GRABB_con)
                        DB_Exec_NonQuery_MySQL(Query, CRM_con)

                        iUnivCode = IndexUniv
                        Dim R = dtGSSUniv.NewRow
                        R("iUnivCode") = iUnivCode
                        R("iCieCode") = iCieCode
                        R("sUnivName") = UniversalCode
                        dtGSSUniv.Rows.Add(R)
                        dvGSSUniv.Dispose()
                        dvGSSUniv = New DataView(dtGSSUniv)
                    Else
                        iUnivCode = dvGSSUniv(0)("iUnivCode")
                    End If

                    iFamCode = 0
                    dvMolAtt.RowFilter = "parent_id = '" & Row("id") & "' AND att_parent_id = '" & sFamGuid & "'"
                    If dvMolAtt.Count > 0 Then
                        dvGSSFam.RowFilter = "sFamGuid = '" & dvMolAtt(0)("att_id") & "'"
                        If dvGSSFam.Count > 0 Then
                            iFamCode = dvGSSFam(0)("iFamCode")
                        End If
                    End If

                    iCatCode = 0
                    dvMolAtt.RowFilter = "parent_id = '" & Row("id") & "' AND att_parent_id = '" & sProdCatGuid & "'"
                    If dvMolAtt.Count > 0 Then
                        dvGSSProdCat.RowFilter = "sProdCatGuid = '" & dvMolAtt(0)("att_id") & "'"
                        If dvGSSProdCat.Count > 0 Then
                            iCatCode = dvGSSProdCat(0)("iProdCatCode")
                        End If
                    End If

                    iCatCJCode = 0
                    dvMolAtt.RowFilter = "parent_id = '" & Row("id") & "' AND att_parent_id = '" & sProdCatCJGuid & "'"
                    If dvMolAtt.Count > 0 Then
                        dvGSSProdCatCJ.RowFilter = "sProdCatCJGuid = '" & dvMolAtt(0)("att_id") & "'"
                        If dvGSSProdCatCJ.Count > 0 Then
                            iCatCJCode = dvGSSProdCatCJ(0)("iProdCatCJCode")
                        End If
                    End If

                    iClass1Code = 0
                    dvMolAtt.RowFilter = "parent_id = '" & Row("id") & "' AND att_parent_id = '" & sClass1Guid & "'"
                    If dvMolAtt.Count > 0 Then
                        dvGSSClass1.RowFilter = "sClass1Guid = '" & dvMolAtt(0)("att_id") & "'"
                        If dvGSSClass1.Count > 0 Then
                            iClass1Code = dvGSSClass1(0)("iClass1Code")
                        End If
                    End If

                    iClass2Code = 0

                    iEmbCode = 0
                    dvMolAtt.RowFilter = "parent_id = '" & Row("id") & "' AND att_parent_id = '" & sEmbGuid & "'"
                    If dvMolAtt.Count > 0 Then
                        dvGSSEmb.RowFilter = "sEmBGuid = '" & dvMolAtt(0)("att_id") & "'"
                        If dvGSSEmb.Count > 0 Then
                            iEmbCode = dvGSSEmb(0)("iEmBCode")
                        End If
                    End If

                    iCoulCode = 0
                    dvMolAtt.RowFilter = "parent_id = '" & Row("id") & "' AND att_parent_id = '" & sCoulGuid & "'"
                    If dvMolAtt.Count > 0 Then
                        dvGSSCoul.RowFilter = "sCoulGuid = '" & dvMolAtt(0)("att_id") & "'"
                        If dvGSSCoul.Count > 0 Then
                            iCoulCode = dvGSSCoul(0)("iCoulCode")
                        End If
                    End If

                    iSavCode = 0
                    dvMolAtt.RowFilter = "parent_id = '" & Row("id") & "' AND att_parent_id = '" & sSavGuid & "'"
                    If dvMolAtt.Count > 0 Then
                        dvGSSSav.RowFilter = "sSavGuid = '" & dvMolAtt(0)("att_id") & "'"
                        If dvGSSSav.Count > 0 Then
                            iSavCode = dvGSSSav(0)("iSavCode")
                        End If
                    End If

                    iExclCode = 0
                    dvMolAtt.RowFilter = "parent_id = '" & Row("id") & "' AND att_parent_id = '" & sExclGuid & "'"
                    If dvMolAtt.Count > 0 Then
                        dvGSSExcl.RowFilter = "sExclGuid = '" & dvMolAtt(0)("att_id") & "'"
                        If dvGSSExcl.Count > 0 Then
                            iExclCode = dvGSSExcl(0)("iExclCode")
                        End If
                    End If

                    sProdNo = Row("id")
                    sProdUPC = Row("id")
                    sProdName_F = Row("description")
                    sProdName_E = Row("desc_autre")
                    sProdInfos = Row("InfosSupp")
                    sProd_DIN_NPN = Row("champ1")
                    sRAMQCode = ""
                    iActive = Row("statut")
                    PriceUpdate = New Date(2000, 1, 1)

                    For xloop = 0 To dtGSSProvPrix.Rows.Count - 1
                        Price(xloop) = 0
                        fFfsMax(xloop) = ""
                        iExcl(xloop) = iExclCode
                    Next
                    For xloop = 0 To sfFfsmax.Count - 1
                        sfFfsmax(xloop) = ""
                    Next

                    If Row("champ20").ToString.Length > 0 Then
                        Try
                            sfFfsmax = Row("champ20").ToString.Split(",")
                        Catch
                        End Try
                    End If
                    dvMolUM.RowFilter = "in_items_id = '" & Row("id") & "'"
                    If dvMolUM.Count > 0 Then
                        PriceUpdate = dvMolUM(0)("modified_date")
                        For xloop = 0 To dtGSSProvPrix.Rows.Count - 1
                            Price(xloop) = dvMolUM(0)("prix" & dtGSSProvPrix(xloop)("iProvPrix"))
                            If sfFfsmax.Count = 10 Then
                                fFfsMax(xloop) = sfFfsmax(dtGSSProvPrix(xloop)("iProvPrix") - 1)
                            End If
                        Next
                    End If

                    If dvGSSMol.Count = 0 Then
                        If Index = 0 Then
                            Index = DB_Get_Next_Index("iProdCode", "gss_ProdM", GSS_con)
                        Else
                            Index += 1
                        End If

                        Query = "INSERT INTO gss_ProdM VALUES (" _
                                    & Index & ", " _
                                    & iCieCode & ", " _
                                    & iUnivCode & ", " _
                                    & iFamCode & ", " _
                                    & iCatCode & ", " _
                                    & iCatCJCode & ", " _
                                    & iClass1Code & ", " _
                                    & iClass2Code & ", " _
                                    & iEmbCode & ", " _
                                    & iCoulCode & ", " _
                                    & iSavCode & ", " _
                                    & SFilter(sProdNo) & ", " _
                                    & SFilter(sProdUPC) & ", " _
                                    & SFilter(sProdName_F) & ", " _
                                    & SFilter(sProdName_E) & ", " _
                                    & SFilter(sProdInfos) & ", " _
                                    & SFilter(sProd_DIN_NPN) & ", " _
                                    & SFilter(sRAMQCode) & ", " _
                                    & SFilter(dDeActivation) & ", " _
                                    & iActive & ", " _
                                    & SFilter(sNewSHA512) & ")"
                        DB_Exec_NonQuery(Query, GSS_con)

                        Query = "INSERT INTO gss_ProdM VALUES (" _
                                    & Index & ", " _
                                    & iCieCode & ", " _
                                    & iUnivCode & ", " _
                                    & iFamCode & ", " _
                                    & iCatCode & ", " _
                                    & iCatCJCode & ", " _
                                    & iClass1Code & ", " _
                                    & iClass2Code & ", " _
                                    & iEmbCode & ", " _
                                    & iCoulCode & ", " _
                                    & iSavCode & ", " _
                                    & SFilter(sProdNo) & ", " _
                                    & SFilter(sProdUPC) & ", " _
                                    & SFilter(sProdName_F) & ", " _
                                    & SFilter(sProdName_E) & ", " _
                                    & SFilter(sProdInfos) & ", " _
                                    & SFilter(sProd_DIN_NPN) & ", " _
                                    & SFilter(sRAMQCode) & ", " _
                                    & SFilter(dDeActivation) & ", " _
                                    & iActive & ")"
                        DB_Exec_NonQuery(Query, GRABB_con)

                        Query = "INSERT INTO gss_ProdM VALUES (" _
                                    & Index & ", " _
                                    & iCieCode & ", " _
                                    & iUnivCode & ", " _
                                    & iFamCode & ", " _
                                    & iCatCode & ", " _
                                    & iCatCJCode & ", " _
                                    & iClass1Code & ", " _
                                    & iClass2Code & ", " _
                                    & iEmbCode & ", " _
                                    & iCoulCode & ", " _
                                    & iSavCode & ", " _
                                    & SFilter(sProdNo) & ", " _
                                    & SFilter(sProdUPC) & ", " _
                                    & SFilter(sProdName_F) & ", " _
                                    & SFilter(sProdName_E) & ", " _
                                    & SFilter(sProdInfos) & ", " _
                                    & SFilter(sProd_DIN_NPN) & ", " _
                                    & SFilter(sRAMQCode) & ", " _
                                    & SFilter(dDeActivation) & ", " _
                                    & iActive & ", " _
                                    & "0" & "," _
                                    & SFilter(Row("modified_date")) & ")"
                        DB_Exec_NonQuery_MySQL(Query, CRM_con)

                        If IndexD = 0 Then
                            IndexD = DB_Get_Next_Index("iProdDetailID", "gss_ProdD", GSS_con)
                        Else
                            IndexD += 1
                        End If
                        Query = "INSERT INTO gss_ProdD VALUES (" _
                                    & IndexD & ", " _
                                    & Index & ", " _
                                    & SFilter(Row("modified_date")) & ", " _
                                    & "0" & ", " _
                                    & "1" & ")"
                        DB_Exec_NonQuery(Query, GSS_con)
                        DB_Exec_NonQuery(Query, GRABB_con)

                        For xloop = 0 To dtGSSProvPrix.Rows.Count - 1
                            If IndexP = 0 Then
                                IndexP = DB_Get_Next_Index("iProdPrixID", "gss_ProdP", GSS_con)
                            Else
                                IndexP += 1
                            End If

                            Query = "INSERT INTO gss_ProdP VALUES (" _
                                        & IndexP & ", " _
                                        & Index & ", " _
                                        & Prov(xloop) & ", " _
                                        & SFilter(PriceUpdate) & ", " _
                                        & Price(xloop) & ", " _
                                        & SFilter(fFfsMax(xloop)) & ", " _
                                        & "NULL" & ", " _
                                        & iExcl(xloop) & ", " _
                                        & "1" & ")"
                            DB_Exec_NonQuery(Query, GSS_con)
                            DB_Exec_NonQuery(Query, GRABB_con)

                            Query = "INSERT INTO gss_ProdP VALUES (" _
                                        & IndexP & ", " _
                                        & Index & ", " _
                                        & Prov(xloop) & ", " _
                                        & SFilter(PriceUpdate) & ", " _
                                        & Price(xloop) & ", " _
                                        & SFilter(fFfsMax(xloop)) & ", " _
                                        & "NULL" & ", " _
                                        & iExcl(xloop) & ")"
                            DB_Exec_NonQuery_MySQL(Query, CRM_con)

                        Next
                        UpdateData = True
                    Else
                        IndexU = dvGSSMol(0)("iProdCode")
                        If dvGSSMol(0)("iCieCode") <> iCieCode Or
                           dvGSSMol(0)("iUnivCode") <> iUnivCode Or
                           dvGSSMol(0)("iFamCode") <> iFamCode Or
                           dvGSSMol(0)("iCatCode") <> iCatCode Or
                           dvGSSMol(0)("iCatCJCode") <> iCatCJCode Or
                           dvGSSMol(0)("iClass1Code") <> iClass1Code Or
                           dvGSSMol(0)("iClass2Code") <> iClass2Code Or
                           dvGSSMol(0)("iEmbCode") <> iEmbCode Or
                           dvGSSMol(0)("iCoulCode") <> iCoulCode Or
                           dvGSSMol(0)("iSavCode") <> iSavCode Or
                           dvGSSMol(0)("sProdNo") <> sProdNo.ToString.Trim Or
                           dvGSSMol(0)("sProdUPC") <> sProdUPC.ToString.Trim Or
                           dvGSSMol(0)("sProdName_F") <> sProdName_F.ToString.Trim Or
                           dvGSSMol(0)("sProdName_E") <> sProdName_E.ToString.Trim Or
                           dvGSSMol(0)("sProdInfos") <> sProdInfos.ToString.Trim Or
                           dvGSSMol(0)("sProd_DIN_NPN") <> sProd_DIN_NPN.ToString.Trim Or
                           dvGSSMol(0)("sRAMQCode") <> sRAMQCode.ToString.Trim Or
                           dvGSSMol(0)("dDeActivationDate") <> dDeActivation Or
                           dvGSSMol(0)("iActive") <> iActive Then

                            Query = "UPDATE gss_ProdM SET " _
                                   & "iCieCode = " & iCieCode & ", " _
                                   & "iUnivCode = " & iUnivCode & ", " _
                                   & "iFamCode = " & iFamCode & ", " _
                                   & "iCatCode = " & iCatCode & ", " _
                                   & "iCatCJCode = " & iCatCJCode & ", " _
                                   & "iClass1Code = " & iClass1Code & ", " _
                                   & "iClass2Code = " & iClass2Code & ", " _
                                   & "iEmbCode = " & iEmbCode & ", " _
                                   & "iCoulCode = " & iCoulCode & ", " _
                                   & "iSavCode = " & iSavCode & ", " _
                                   & "sProdNo = " & SFilter(sProdNo) & ", " _
                                   & "sProdUPC = " & SFilter(sProdUPC) & ", " _
                                   & "sProdName_F = " & SFilter(sProdName_F) & ", " _
                                   & "sProdName_E = " & SFilter(sProdName_E) & ", " _
                                   & "sProdInfos = " & SFilter(sProdInfos) & ", " _
                                   & "sProd_DIN_NPN = " & SFilter(sProd_DIN_NPN) & ", " _
                                   & "sRAMQCode = " & SFilter(sRAMQCode) & ", " _
                                   & "dDeactivationDate = " & SFilter(dDeActivation) & ", " _
                                   & "iActive = " & iActive & " " _
                                   & "WHERE iProdCode = " & IndexU
                            DB_Exec_NonQuery(Query, GSS_con)
                            DB_Exec_NonQuery(Query, GRABB_con)

                            Query = "UPDATE gss_ProdM SET " _
                                   & "iCieCode = " & iCieCode & ", " _
                                   & "iUnivCode = " & iUnivCode & ", " _
                                   & "iFamCode = " & iFamCode & ", " _
                                   & "iCatCode = " & iCatCode & ", " _
                                   & "iCatCJCode = " & iCatCJCode & ", " _
                                   & "iClass1Code = " & iClass1Code & ", " _
                                   & "iClass2Code = " & iClass2Code & ", " _
                                   & "iEmbCode = " & iEmbCode & ", " _
                                   & "iCoulCode = " & iCoulCode & ", " _
                                   & "iSavCode = " & iSavCode & ", " _
                                   & "sProdNo = " & SFilter(sProdNo) & ", " _
                                   & "sProdUPC = " & SFilter(sProdUPC) & ", " _
                                   & "sProdName_F = " & SFilter(sProdName_F) & ", " _
                                   & "sProdName_E = " & SFilter(sProdName_E) & ", " _
                                   & "sProdInfos = " & SFilter(sProdInfos) & ", " _
                                   & "sProd_DIN_NPN = " & SFilter(sProd_DIN_NPN) & ", " _
                                   & "sRAMQCode = " & SFilter(sRAMQCode) & ", " _
                                   & "dDeactivationDate = " & SFilter(dDeActivation) & ", " _
                                   & "iActive = " & iActive & ", " _
                                   & "iBackOrderInCompetitor = " & 0 & ", " _
                                   & "dProdUpdate = " & SFilter(Row("modified_date")) & " " _
                                   & "WHERE iProdCode = " & IndexU
                            DB_Exec_NonQuery_MySQL(Query, CRM_con)

                        End If
                        For xloop = 0 To dtGSSProvPrix.Rows.Count - 1
                            dvGSSMolPrix.RowFilter = "iProdCode = " & IndexU & " AND iProdProv = " & Prov(xloop)
                            If dvGSSMolPrix.Count > 0 Then
                                If dvGSSMolPrix(0)("fProdSalesPrice") <> Price(xloop) Or
                                   dvGSSMolPrix(0)("fFfsMax") <> fFfsMax(xloop) Or
                                   dvGSSMolPrix(0)("iExclusivityCode") <> iExcl(xloop) Then

                                    Query = "UPDATE gss_ProdP SET " _
                                           & "iCurrentInfoP = 0 " _
                                           & "WHERE iProdCode = " & IndexU & " And iProdProv = " & Prov(xloop)
                                    DB_Exec_NonQuery(Query, GSS_con)
                                    DB_Exec_NonQuery(Query, GRABB_con)

                                    If IndexP = 0 Then
                                        IndexP = DB_Get_Next_Index("iProdPrixID", "gss_ProdP", GSS_con)
                                    Else
                                        IndexP += 1
                                    End If

                                    Query = "INSERT INTO gss_ProdP VALUES (" _
                                            & IndexP & ", " _
                                            & IndexU & ", " _
                                            & Prov(xloop) & ", " _
                                            & SFilter(PriceUpdate) & ", " _
                                            & Price(xloop) & ", " _
                                            & SFilter(fFfsMax(xloop)) & ", " _
                                            & "NULL" & ", " _
                                            & iExcl(xloop) & ", " _
                                            & "1" & ")"
                                    DB_Exec_NonQuery(Query, GSS_con)
                                    DB_Exec_NonQuery(Query, GRABB_con)

                                    Query = "UPDATE gss_ProdP Set " _
                                           & "dProdUpdate = " & SFilter(PriceUpdate) & ", " _
                                           & "fProdSalesPrice = " & Price(xloop) & ", " _
                                           & "fFfsMax = " & SFilter(fFfsMax(xloop)) & ", " _
                                           & "dStartApp = " & "NULL" & ", " _
                                           & "iExclusivityCode = " & iExcl(xloop) & " " _
                                           & "WHERE iProdCode = " & IndexU & " And iProdProv = " & Prov(xloop)
                                    DB_Exec_NonQuery_MySQL(Query, CRM_con)

                                End If
                            End If
                        Next
                        Query = "UPDATE gss_ProdM Set sSHA512 = '" & sNewSHA512 & "' WHERE iProdCode = " & IndexU
                        DB_Exec_NonQuery(Query, GSS_con)
                    End If
                End If

            Next
        End If
#End Region

#Region "Types de clients"
        '*******************************************************
        'Load ClientType
        '*******************************************************

        dvGSSAttCL.RowFilter = "sAttName = 'Type'"
        Dim sClientTypeGuid As String = dvGSSAttCL(0)("sAttGuid")

        Dim dtClientType As New DataTable
        Query = "SELECT id, description, modified_date FROM re_clients_att WHERE parent_id = '" & sClientTypeGuid & "' AND modified_date >= '" & LastUpdate & "'"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtClientType)
            End Using
        End Using

        Dim dtGSSClientType As New DataTable
        Query = "SELECT iClientTypeCode, sClientTypeName, sClientTypeGuid FROM gss_Client_Type"
        Using cmd As New SqlCommand(Query, GSS_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtGSSClientType)
            End Using
        End Using
        Dim dvGSSClientType = New DataView(dtGSSClientType)

        Count = 1
        Index = 0
        For Each Row In dtClientType.Rows
            btn_Start.Text = "Types Client" & vbLf & Count
            Application.DoEvents()
            Count += 1

            dvGSSClientType.RowFilter = "sClientTypeGuid = '" & Row("id") & "'"
            If dvGSSClientType.Count = 0 Then
                If Index = 0 Then
                    Index = DB_Get_Next_Index("iClientTypeCode", "gss_Client_Type", GSS_con)
                Else
                    Index += 1
                End If

                Query = "INSERT INTO gss_Client_Type VALUES (" _
                            & Index & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("id")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GSS_con)

                Query = "INSERT INTO gss_Client_Type VALUES (" _
                            & Index & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GRABB_con)
                DB_Exec_NonQuery_MySQL(Query, CRM_con)

                UpdateData = True
            Else
                If dvGSSClientType(0)("sClientTypeName") <> Row("description").ToString.Trim Then
                    Query = "UPDATE gss_Client_Type SET " _
                                & "sClientTypeName = " & SFilter(Row("description")) & ", " _
                                & "dClientTypeUpdate = " & SFilter(Row("modified_date")) & " " _
                                & "WHERE iClientTypeCode = " & dvGSSClientType(0)("iClientTypeCode")
                    DB_Exec_NonQuery(Query, GSS_con)
                    DB_Exec_NonQuery(Query, GRABB_con)
                    DB_Exec_NonQuery_MySQL(Query, CRM_con)
                    UpdateData = True
                End If
            End If
        Next

        If UpdateData = True Then
            dtGSSClientType.Reset()
            Query = "SELECT iClientTypeCode, sClientTypeName, sClientTypeGuid FROM gss_Client_Type"
            Using cmd As New SqlCommand(Query, GSS_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtGSSClientType)
                End Using
            End Using
            dvGSSClientType.Dispose()
            dvGSSClientType = New DataView(dtGSSClientType)
            UpdateData = False
        End If
#End Region

#Region "Regions"
        '*******************************************************
        'Load Regions
        '*******************************************************

        dvGSSAttCL.RowFilter = "sAttName = 'Region Admin'"
        Dim sRegGuid As String = dvGSSAttCL(0)("sAttGuid")

        Dim dtReg As New DataTable
        Query = "SELECT id, description, modified_date FROM re_clients_att WHERE parent_id = '" & sRegGuid & "' AND modified_date >= '" & LastUpdate & "'"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtReg)
            End Using
        End Using

        Dim dtGSSReg As New DataTable
        Query = "SELECT iRegCode, sRegName, sRegGuid FROM gss_Reg"
        Using cmd As New SqlCommand(Query, GSS_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtGSSReg)
            End Using
        End Using
        Dim dvGSSReg = New DataView(dtGSSReg)

        Count = 1
        Index = 0
        For Each Row In dtReg.Rows
            btn_Start.Text = "Region Administrative" & vbLf & Count
            Application.DoEvents()
            Count += 1

            dvGSSReg.RowFilter = "sRegGuid = '" & Row("id") & "'"
            If dvGSSReg.Count = 0 Then
                If Index = 0 Then
                    Index = DB_Get_Next_Index("iRegCode", "gss_Reg", GSS_con)
                Else
                    Index += 1
                End If

                Query = "INSERT INTO gss_Reg VALUES (" _
                            & Index & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("id")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GSS_con)

                Query = "INSERT INTO gss_Reg VALUES (" _
                            & Index & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GRABB_con)
                DB_Exec_NonQuery_MySQL(Query, CRM_con)

                UpdateData = True
            Else
                If dvGSSReg(0)("sRegName") <> Row("description").ToString.Trim Then
                    Query = "UPDATE gss_Reg SET " _
                                & "sRegName = " & SFilter(Row("description")) & ", " _
                                & "dRegUpdate = " & SFilter(Row("modified_date")) & " " _
                                & "WHERE iRegCode = " & dvGSSReg(0)("iRegCode")
                    DB_Exec_NonQuery(Query, GSS_con)
                    DB_Exec_NonQuery(Query, GRABB_con)
                    DB_Exec_NonQuery_MySQL(Query, CRM_con)
                    UpdateData = True
                End If
            End If
        Next

        If UpdateData = True Then
            dtGSSReg.Reset()
            Query = "SELECT iRegCode, sRegName, sRegGuid FROM gss_Reg"
            Using cmd As New SqlCommand(Query, GSS_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtGSSReg)
                End Using
            End Using
            dvGSSReg.Dispose()
            dvGSSReg = New DataView(dtGSSReg)
            UpdateData = False
        End If
#End Region

#Region "Bannières"
        '*******************************************************
        'Load Banniere
        '*******************************************************

        dvGSSAttCL.RowFilter = "sAttName = 'Bannière'"
        Dim sBanGUid As String = dvGSSAttCL(0)("sAttGuid")

        dvGSSClientType.RowFilter = "sClientTypeName = 'Bannière'"
        Dim sBanClientTypeGuid As String = dvGSSClientType(0)("sClientTypeGuid")
        Dim sBanClientTypeCode As Integer = dvGSSClientType(0)("iClientTypeCode")

        Dim dtBan As New DataTable
        Query = "SELECT id, description, modified_date FROM re_clients_att WHERE parent_id = '" & sBanGUid & "'"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtBan)
            End Using
        End Using

        Dim dtGSSBan As New DataTable
        Query = "SELECT iBanCode, iClientCode, sBanName, sBanGuid FROM gss_Ban"
        Using cmd As New SqlCommand(Query, GSS_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtGSSBan)
            End Using
        End Using
        Dim dvGSSBan = New DataView(dtGSSBan)

        Dim dtBanAccount As New DataTable
        Query = "SELECT parent_id, att_id FROM re_clients_attributs WHERE parent_id IN " _
                    & "(SELECT parent_id FROM re_clients_attributs WHERE att_id = '" & sBanClientTypeGuid & "') " _
                    & "AND att_parent_id = '" & sBanGUid & "'"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtBanAccount)
            End Using
        End Using
        Dim dvBanAccount = New DataView(dtBanAccount)

        Dim dtBanAcc As New DataTable
        Query = "SELECT iClientCode, sClientGuid FROM gss_ClientM WHERE iClientTypeCode = " & sBanClientTypeCode
        Using cmd As New SqlCommand(Query, GSS_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtBanAcc)
            End Using
        End Using
        Dim dvBanAcc = New DataView(dtBanAcc)

        Dim sClientCode As String

        Count = 1
        Index = 0
        For Each Row In dtBan.Rows
            btn_Start.Text = "Bannières" & vbLf & Count
            Application.DoEvents()
            Count += 1

            iClientCode = 0
            dvBanAccount.RowFilter = "att_id = '" & Row("id") & "'"
            If dvBanAccount.Count > 0 Then
                sClientCode = dvBanAccount(0)("parent_id").ToString.Trim
                dvBanAcc.RowFilter = "sClientGuid = '" & sClientCode & "'"
                If dvBanAcc.Count > 0 Then
                    iClientCode = dvBanAcc(0)("iClientCode")
                End If
            End If

            dvGSSBan.RowFilter = "sBanGuid = '" & Row("id") & "'"
            If dvGSSBan.Count = 0 Then
                If Index = 0 Then
                    Index = DB_Get_Next_Index("iBanCode", "gss_Ban", GSS_con)
                Else
                    Index += 1
                End If

                Query = "INSERT INTO gss_Ban VALUES (" _
                            & Index & ", " _
                            & iClientCode & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("id")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GSS_con)

                Query = "INSERT INTO gss_Ban VALUES (" _
                            & Index & ", " _
                            & iClientCode & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GRABB_con)
                DB_Exec_NonQuery_MySQL(Query, CRM_con)

                UpdateData = True
            Else
                If dvGSSBan(0)("sBanName") <> Row("description").ToString.Trim Or
                   dvGSSBan(0)("iClientCode") <> iClientCode Then

                    Query = "UPDATE gss_Ban SET " _
                               & "iClientCode = " & iClientCode & ", " _
                               & "sBanName = " & SFilter(Row("description")) & ", " _
                               & "dBanUpdate = " & SFilter(Row("modified_date")) & " " _
                               & "WHERE iBanCode = " & dvGSSBan(0)("iBanCode")
                    DB_Exec_NonQuery(Query, GSS_con)
                    DB_Exec_NonQuery(Query, GRABB_con)
                    DB_Exec_NonQuery_MySQL(Query, CRM_con)

                    UpdateData = True
                End If
            End If
        Next

        If UpdateData = True Then
            dtGSSBan.Reset()
            Query = "SELECT iBanCode, iClientCode, sBanName, sBanGuid FROM gss_Ban"
            Using cmd As New SqlCommand(Query, GSS_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtGSSBan)
                End Using
            End Using
            dvGSSBan.Dispose()
            dvGSSBan = New DataView(dtGSSBan)
            UpdateData = False
        End If
#End Region

#Region "Groupes"
        '*******************************************************
        'Load Groupe
        '*******************************************************

        dvGSSAttCL.RowFilter = "sAttName = 'Groupe Achat'"
        Dim sGrpGUid As String = dvGSSAttCL(0)("sAttGuid")

        dvGSSClientType.RowFilter = "sClientTypeName = 'Groupe'"
        Dim sGrpClientTypeGuid As String = dvGSSClientType(0)("sClientTypeGuid")
        Dim sGrpClientTypeCode As Integer = dvGSSClientType(0)("iClientTypeCode")

        Dim dtGrp As New DataTable
        Query = "SELECT id, description, modified_date FROM re_clients_att WHERE parent_id = '" & sGrpGUid & "'"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtGrp)
            End Using
        End Using

        Dim dtGSSGrp As New DataTable
        Query = "SELECT iGrpCode, iClientCode, sGrpName, sGrpGuid FROM gss_Grp"
        Using cmd As New SqlCommand(Query, GSS_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtGSSGrp)
            End Using
        End Using
        Dim dvGSSGrp As New DataView(dtGSSGrp)

        Dim dtGrpAccount As New DataTable
        Query = "SELECT parent_id, att_id FROM re_clients_attributs WHERE parent_id IN " _
                    & "(SELECT parent_id FROM re_clients_attributs WHERE att_id = '" & sGrpClientTypeGuid & "') " _
                    & "AND att_parent_id = '" & sGrpGUid & "'"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtGrpAccount)
            End Using
        End Using
        Dim dvGrpAccount = New DataView(dtGrpAccount)

        Dim dtGrpAcc As New DataTable
        Query = "SELECT iClientCode, sClientGuid FROM gss_ClientM WHERE iClientTypeCode = " & sGrpClientTypeCode
        Using cmd As New SqlCommand(Query, GSS_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtGrpAcc)
            End Using
        End Using
        Dim dvGrpAcc = New DataView(dtGrpAcc)

        Count = 1
        Index = 0
        For Each Row In dtGrp.Rows
            btn_Start.Text = "Groupes" & vbLf & Count
            Application.DoEvents()
            Count += 1

            iClientCode = 0
            dvGrpAccount.RowFilter = "att_id = '" & Row("id") & "'"
            If dvGrpAccount.Count > 0 Then
                sClientCode = dvGrpAccount(0)("parent_id").ToString.Trim
                dvGrpAcc.RowFilter = "sClientGuid = '" & sClientCode & "'"
                If dvGrpAcc.Count > 0 Then
                    iClientCode = dvGrpAcc(0)("iClientCode")
                End If
            End If

            dvGSSGrp.RowFilter = "sGrpGuid = '" & Row("id") & "'"
            If dvGSSGrp.Count = 0 Then
                If Index = 0 Then
                    Index = DB_Get_Next_Index("iGrpCode", "gss_Grp", GSS_con)
                Else
                    Index += 1
                End If

                Query = "INSERT INTO gss_Grp VALUES (" _
                            & Index & ", " _
                            & iClientCode & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("id")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GSS_con)

                Query = "INSERT INTO gss_Grp VALUES (" _
                            & Index & ", " _
                            & iClientCode & ", " _
                            & SFilter(Row("description")) & ", " _
                            & SFilter(Row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GRABB_con)
                DB_Exec_NonQuery_MySQL(Query, CRM_con)

                UpdateData = True
            Else
                If dvGSSGrp(0)("sGrpName") <> Row("description").ToString.Trim Or
                   dvGSSGrp(0)("iClientCode") <> iClientCode Then

                    Query = "UPDATE gss_Grp SET " _
                                & "iClientCode = " & iClientCode & ", " _
                                & "sGrpName = " & SFilter(Row("description")) & ", " _
                                & "dGrpUpdate = " & SFilter(Row("modified_date")) & " " _
                                & "WHERE iGrpCode = " & dvGSSGrp(0)("iGrpCode")
                    DB_Exec_NonQuery(Query, GSS_con)
                    DB_Exec_NonQuery(Query, GRABB_con)
                    DB_Exec_NonQuery_MySQL(Query, CRM_con)

                    UpdateData = True
                End If
            End If
        Next

        If UpdateData = True Then
            dtGSSGrp.Reset()
            Query = "SELECT iGrpCode, iClientCode, sGrpName, sGrpGuid FROM gss_Grp"
            Using cmd As New SqlCommand(Query, GSS_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtGSSGrp)
                End Using
            End Using
            dvGSSGrp.Dispose()
            dvGSSGrp = New DataView(dtGSSGrp)
            UpdateData = False
        End If
#End Region

#Region "Représentants"
        '*******************************************************
        'Load Rep FROM Fidelio
        '*******************************************************

        Dim dtRep As New DataTable
        Query = "SELECT id, description, Statut, Email, iscoordonnateur, Ve_Types_Id, langue, modified_date FROM ve_vendeurs WHERE modified_date >= '" & LastUpdate & "'"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtRep)
            End Using
        End Using

        Dim dtGSSRep As New DataTable
        Query = "SELECT iRepCode, iCieCode, sRepName, sRepEmail, iIsCoor, iIsGest, iLangue, iActive, sRepGuid FROM gss_Rep"
        Using cmd As New SqlCommand(Query, GSS_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtGSSRep)
            End Using
        End Using
        Dim dvGSSRep = New DataView(dtGSSRep)

        Dim sRepName As String
        Dim sRepEmail As String
        Dim iIsCoor As Integer
        Dim iIsGest As Integer
        Dim sRepGuid As String

        Count = 1
        Index = 0
        For Each row In dtRep.Rows
            btn_Start.Text = "Représentants" & vbLf & Count
            Application.DoEvents()
            Count += 1

            iCieCode = 0
            dvGSSDep.RowFilter = "sDepNo = '" & row("Ve_Types_Id").ToString.Trim & "'"
            If dvGSSDep.Count = 1 Then
                iCieCode = dvGSSDep(0)("iCieCode")
            End If

            iIsGest = 0

            sRepName = row("description")
            sRepEmail = row("Email")
            iIsCoor = IIf(row("iscoordonnateur") = vbTrue, 1, 0)
            iLangue = row("langue")
            iActive = row("Statut")
            sRepGuid = row("id")

            dvGSSRep.RowFilter = "sRepGuid = '" & row("id").ToString.Trim & "'"
            If dvGSSRep.Count = 0 Then
                If Index = 0 Then
                    Index = DB_Get_Next_Index("iRepCode", "gss_Rep", GSS_con)
                Else
                    Index += 1
                End If

                Query = "INSERT INTO gss_Rep VALUES (" _
                            & Index & ", " _
                            & iCieCode & ", " _
                            & SFilter(sRepName) & ", " _
                            & SFilter(sRepEmail) & ", " _
                            & iIsCoor & ", " _
                            & iIsGest & ", " _
                            & iLangue & ", " _
                            & iActive & ", " _
                            & SFilter(sRepGuid) & ", " _
                            & SFilter(row("modified_date")) & ")"
                DB_Exec_NonQuery(Query, GSS_con)
                DB_Exec_NonQuery(Query, GRABB_con)
                DB_Exec_NonQuery_MySQL(Query, CRM_con)

                UpdateData = True
            Else
                If dvGSSRep(0)("iCieCode") <> iCieCode Or
                   dvGSSRep(0)("sRepName") <> sRepName.ToString.Trim Or
                   dvGSSRep(0)("sRepEmail") <> sRepEmail.ToString.Trim Or
                   dvGSSRep(0)("iIsCoor") <> iIsCoor Or
                   dvGSSRep(0)("iIsGest") <> iIsGest Or
                   dvGSSRep(0)("iLangue") <> iLangue Or
                   dvGSSRep(0)("iActive") <> iActive Then

                    Query = "UPDATE gss_Rep Set " _
                                & "iCieCode = " & iCieCode & ", " _
                                & "sRepName = " & SFilter(sRepName) & ", " _
                                & "sRepEmail = " & SFilter(sRepEmail) & ", " _
                                & "iIsCoor = " & iIsCoor & ", " _
                                & "iLangue = " & iLangue & ", " _
                                & "iActive = " & iActive & ", " _
                                & "dRepUpdate = " & SFilter(row("modified_date")) & " " _
                                & "WHERE iRepCode = " & dvGSSRep(0)("iRepCode")

                    'Query = "UPDATE gss_Rep Set " _
                    '            & "iCieCode = " & iCieCode & ", " _
                    '            & "sRepName = " & SFilter(sRepName) & ", " _
                    '            & "sRepEmail = " & SFilter(sRepEmail) & ", " _
                    '            & "iIsCoor = " & iIsCoor & ", " _
                    '            & "iIsGest = " & iIsGest & ", " _
                    '            & "iLangue = " & iLangue & ", " _
                    '            & "iActive = " & iActive & ", " _
                    '            & "dRepUpdate = " & SFilter(row("modified_date")) & " " _
                    '            & "WHERE iRepCode = " & dvGSSRep(0)("iRepCode")
                    DB_Exec_NonQuery(Query, GSS_con)
                    DB_Exec_NonQuery(Query, GRABB_con)
                    DB_Exec_NonQuery_MySQL(Query, CRM_con)

                    UpdateData = True
                End If
            End If
        Next

        If UpdateData = True Then
            dtGSSRep.Reset()
            Query = "SELECT iRepCode, iCieCode, sRepName, sRepEmail, iIsCoor, iIsGest, iLangue, iActive, sRepGuid FROM gss_Rep"
            Using cmd As New SqlCommand(Query, GSS_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtGSSRep)
                End Using
            End Using
            dvGSSRep.Dispose()
            dvGSSRep = New DataView(dtGSSRep)
            UpdateData = False
        End If
#End Region

#Region "Territoires"
        '*******************************************************
        'Load Terr 
        '*******************************************************

        Dim dtTerr As New DataTable
        Query = "SELECT id, description, parent_id, modified_date FROM ve_vendeurs_att ORDER BY description"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtTerr)
            End Using
        End Using
        Dim dvTerrDep = New DataView(dtTerr)
        Dim dvterr = New DataView(dtTerr)

        Dim dtTerrRep As New DataTable
        Query = "SELECT parent_id, att_id FROM ve_vendeurs_attributs"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtTerrRep)
            End Using
        End Using
        Dim dvTerrRep = New DataView(dtTerrRep)

        Dim dtGSSTerr As New DataTable
        Query = "SELECT M.iTerrCode, D.iTerrDetailID, M.iCieCode, D.iRepCode, D.iCoorCode, D.iGestCode, M.sTerrName, D.sTerrDescription, M.sTerrGuid " _
                   & "FROM gss_TerrD D RIGHT OUTER JOIN gss_TerrM M ON D.iTerrCode = M.iTerrCode WHERE (D.iCurrentInfo = 1)"
        Using cmd As New SqlCommand(Query, GSS_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtGSSTerr)
            End Using
        End Using
        Dim dvGSSTerr = New DataView(dtGSSTerr)

        Dim TerrGuid As String = ""
        Dim sTerrName As String = ""
        Dim sTerrDescription As String = ""
        Dim iCoorCode As Integer
        Dim iGestCode As Integer
        Dim iRep As Integer

        Count = 1
        Index = 0
        IndexD = 0
        dvTerrDep.RowFilter = "description = 'Territoire' AND parent_id = 'ROOT'"
        If dvTerrDep.Count > 0 Then
            TerrGuid = dvTerrDep(0)("id")
        End If

        dvTerrDep.RowFilter = "parent_id = '" & TerrGuid & "'"
        For xloop = 0 To dvTerrDep.Count - 1
            TerrGuid = dvTerrDep(xloop)("id")
            iCieCode = 0
            For yloop = 0 To dtGSSDep.Rows.Count - 1
                If dtGSSDep(yloop)("sDepName").ToString.ToUpper.Contains(dvTerrDep(xloop)("description").ToString.ToUpper.Trim) Then
                    iCieCode = dtGSSDep(yloop)("iCieCode")
                    Exit For
                End If
            Next
            dvterr.RowFilter = "parent_id = '" & TerrGuid & "'"
            For yloop = 0 To dvterr.Count - 1
                btn_Start.Text = "Territoires" & vbLf & Count & " - " & iCieCode
                Application.DoEvents()
                Count += 1

                sTerrDescription = dvterr(yloop)("description").ToString.Trim
                sTerrName = sTerrDescription.Substring(0, 4)
                iRepCode = 0
                iCoorCode = 0
                iGestCode = 0
                dvTerrRep.RowFilter = "att_id = '" & dvterr(yloop)("id") & "'"
                For zloop = 0 To dvTerrRep.Count - 1
                    dvGSSRep.RowFilter = "sRepGuid = '" & dvTerrRep(zloop)("parent_id").ToString.Trim & "'"
                    If dvGSSRep.Count > 0 Then
                        iRep = dvGSSRep(0)("iRepCode")
                        If dvGSSRep(0)("iIsCoor") = 1 Then
                            If dvGSSRep(0)("iIsGest") = 1 Then
                                iGestCode = iRep
                            Else
                                iCoorCode = iRep
                            End If
                        Else
                            iRepCode = iRep
                        End If
                    End If
                Next

                dvGSSTerr.RowFilter = "sTerrName = '" & sTerrName & "'"
                If dvGSSTerr.Count = 0 Then
                    If Index = 0 Then
                        Index = DB_Get_Next_Index("iTerrCode", "gss_TerrM", GSS_con)
                    Else
                        Index += 1
                    End If

                    Query = "INSERT INTO gss_TerrM VALUES (" _
                                & Index & ", " _
                                & iCieCode & ", " _
                                & SFilter(sTerrName) & ", " _
                                & SFilter(dvterr(yloop)("id")) & ")"
                    DB_Exec_NonQuery(Query, GSS_con)

                    Query = "INSERT INTO gss_TerrM VALUES (" _
                                & Index & ", " _
                                & iCieCode & ", " _
                                & SFilter(sTerrName) & ")"
                    DB_Exec_NonQuery(Query, GRABB_con)
                    DB_Exec_NonQuery_MySQL(Query, CRM_con)

                    If IndexD = 0 Then
                        IndexD = DB_Get_Next_Index("iTerrDetailID", "gss_TerrD", GSS_con)
                    Else
                        IndexD += 1
                    End If

                    Query = "INSERT INTO gss_TerrD VALUES (" _
                                & IndexD & ", " _
                                & Index & ", " _
                                & SFilter(dvterr(yloop)("modified_date")) & ", " _
                                & iRepCode & ", " _
                                & iCoorCode & ", " _
                                & iGestCode & ", " _
                                & SFilter(sTerrDescription) & ", " _
                                & "1" & ")"
                    DB_Exec_NonQuery(Query, GSS_con)
                    DB_Exec_NonQuery(Query, GRABB_con)
                    DB_Exec_NonQuery_MySQL(Query, CRM_con)

                    UpdateData = True
                Else
                    If dvGSSTerr(0)("iCieCode") <> iCieCode Or
                       dvGSSTerr(0)("sTerrName") <> sTerrName.ToString.Trim Then

                        Query = "UPDATE gss_TerrM SET " _
                                    & "iCieCode = " & iCieCode & ", " _
                                    & "sTerrName = " & SFilter(sTerrName) & " " _
                                    & "WHERE iTerrCode = " & dvGSSTerr(0)("iTerrCode")
                        DB_Exec_NonQuery(Query, GSS_con)
                        DB_Exec_NonQuery(Query, GRABB_con)
                        DB_Exec_NonQuery_MySQL(Query, CRM_con)

                        UpdateData = True
                    End If

                    If dvGSSTerr(0)("iRepCode") <> iRepCode Or
                       dvGSSTerr(0)("iCoorCode") <> iCoorCode Or
                       dvGSSTerr(0)("iGestCode") <> iGestCode Or
                       dvGSSTerr(0)("sTerrDescription") <> sTerrDescription.ToString.Trim Then

                        Query = "UPDATE gss_TerrD SET " _
                                    & "iCurrentInfo = 0 " _
                                    & "WHERE iTerrCode = " & dvGSSTerr(0)("iTerrCode")
                        DB_Exec_NonQuery(Query, GSS_con) 'Remove CurrentInfoFlag from other entry
                        DB_Exec_NonQuery(Query, GRABB_con)

                        If IndexD = 0 Then
                            IndexD = DB_Get_Next_Index("iTerrDetailID", "gss_TerrD", GSS_con)
                        Else
                            IndexD += 1
                        End If

                        Query = "INSERT INTO gss_TerrD VALUES (" _
                                    & IndexD & ", " _
                                    & dvGSSTerr(0)("iTerrCode") & ", " _
                                    & SFilter(dvterr(yloop)("modified_date")) & ", " _
                                    & iRepCode & ", " _
                                    & iCoorCode & ", " _
                                    & iGestCode & ", " _
                                    & SFilter(sTerrDescription) & ", " _
                                    & "1" & ")"
                        DB_Exec_NonQuery(Query, GSS_con)
                        DB_Exec_NonQuery(Query, GRABB_con)

                        Query = "UPDATE gss_TerrD SET " _
                                    & "dTerrUpdate = " & SFilter(dvterr(yloop)("modified_date")) & ", " _
                                    & "iRepCode = " & iRepCode & ", " _
                                    & "iCoorCode = " & iCoorCode & ", " _
                                    & "iGestCode = " & iGestCode & ", " _
                                    & "sTerrDescription = " & SFilter(sTerrDescription) & " " _
                                    & "WHERE iTerrCode = " & dvGSSTerr(0)("iTerrCode")
                        DB_Exec_NonQuery_MySQL(Query, CRM_con)

                        UpdateData = True
                    End If
                End If
            Next
        Next

        If UpdateData = True Then
            dtGSSTerr.Reset()
            Query = "SELECT M.iTerrCode, D.iTerrDetailID, M.iCieCode, D.iRepCode, D.iCoorCode, D.iGestCode, M.sTerrName, D.sTerrDescription, M.sTerrGuid " _
                                       & "FROM gss_TerrD D RIGHT OUTER JOIN gss_TerrM M ON D.iTerrCode = M.iTerrCode WHERE (D.iCurrentInfo = 1)"
            Using cmd As New SqlCommand(Query, GSS_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtGSSTerr)
                End Using
            End Using
            dvGSSTerr.Dispose()
            dvGSSTerr = New DataView(dtGSSTerr)
            UpdateData = False
        End If
#End Region

#Region "Mapping Products"
        ''        '*******************************************************
        ''        'Load Mapping Products
        ''        '*******************************************************


        ''Query = "TRUNCATE TABLE gss_MapProd"
        ''DB_Exec_NonQuery(Query, GSS_con)
        ''DB_Exec_NonQuery(Query, GRABB_con)
        ''DB_Exec_NonQuery_MySQL(Query, CRM_con)

        'Dim dtGSSDistMap As New DataTable
        'Query = "SELECT iDistCode, sDistShortName FROM gss_Dist WHERE LEN(sDistShortName) > 0"
        'Using cmd As New SqlCommand(Query, GSS_con)
        '    Using sda As New SqlDataAdapter(cmd)
        '        sda.Fill(dtGSSDistMap)
        '    End Using
        'End Using
        'Dim dvGSSDistMap As New DataView(dtGSSDistMap)

        'Dim Dist As String = ""
        'Dim Mapping As String
        'Dim aMap() As String
        'Dim bMap() As String
        'Dim iDistCode As Integer
        'Dim iProdCode As Integer
        'Dim iNoProduit As Integer

        'Dim GrossMol(5) As Integer
        'For xloop = 0 To GrossMol.Count - 1
        '    Select Case xloop
        '        Case 0
        '            Dist = "MCK"
        '        Case 1
        '            Dist = "MCM"
        '        Case 2
        '            Dist = "DP+"
        '        Case 3
        '            Dist = "FAM"
        '        Case 4
        '            Dist = "PJC"
        '        Case 5
        '            Dist = "K&F"
        '    End Select
        '    dvGSSDistMap.RowFilter = "sDistShortName = '" & Dist & "'"
        '    If dvGSSDistMap.Count > 0 Then
        '        GrossMol(xloop) = dvGSSDistMap(0)("iDistCode")
        '    Else
        '        GrossMol(xloop) = 0
        '    End If
        'Next

        'Dim dtProdMap As New DataTable
        'Query = "SELECT id, champ15, champ16, modified_date FROM in_items WHERE modified_date >= '" & LastUpdate & "'"
        'Using cmd As New SqlCommand(Query, Jamp_con)
        '    Using sda As New SqlDataAdapter(cmd)
        '        sda.Fill(dtProdMap)
        '    End Using
        'End Using

        'Dim dtGSSProdMap As New DataTable
        'Query = "SELECT iDistCode, iProdCode, NoProduit FROM gss_MapProd"
        'Using cmd As New SqlCommand(Query, GSS_con)
        '    Using sda As New SqlDataAdapter(cmd)
        '        sda.Fill(dtGSSProdMap)
        '    End Using
        'End Using
        'Dim dvGSSProdMap As New DataView(dtGSSProdMap)

        'Dim dtGSSProdGuid As New DataTable
        'Query = "SELECT iProdCode, sProdNo FROM gss_ProdM"
        'Using cmd As New SqlCommand(Query, GSS_con)
        '    Using sda As New SqlDataAdapter(cmd)
        '        sda.Fill(dtGSSProdGuid)
        '    End Using
        'End Using
        'Dim dvGSSProdGuid As New DataView(dtGSSProdGuid)

        'Count = 1
        'Index = 0
        'For Each Row In dtProdMap.Rows
        '    btn_Start.Text = "Mapping produit" & vbLf & Count
        '    Application.DoEvents()
        '    Count += 1

        '    Mapping = Row("champ15") & "," & Row("champ16")
        '    aMap = Mapping.Split(",")
        '    For xLoop = 0 To aMap.Count - 1
        '        If aMap(xLoop).Length > 0 Then
        '            iDistCode = GrossMol(xLoop)
        '            iProdCode = 0
        '            dvGSSProdMap.RowFilter = "iDistCode = '" & iDistCode & "' AND NoProduit = '" & aMap(xLoop) & "'"
        '            If dvGSSProdMap.Count = 0 Then
        '                dvGSSProdGuid.RowFilter = "sProdNo = '" & Row("id") & "'"
        '                If dvGSSProdGuid.Count > 0 Then
        '                    iProdCode = dvGSSProdGuid(0)("iProdCode")
        '                    If Index = 0 Then
        '                        Index = DB_Get_Next_Index("iMapProdCode", "gss_MapProd", GSS_con)
        '                    Else
        '                        Index += 1
        '                    End If

        '                    Integer.TryParse(aMap(xLoop), iNoProduit)
        '                    Query = "INSERT INTO gss_MapProd VALUES (" _
        '                                    & Index & ", " _
        '                                    & iDistCode & ", " _
        '                                    & iProdCode & ", " _
        '                                    & iNoProduit & ", " _
        '                                    & SFilter(aMap(xLoop)) & ", " _
        '                                    & "0" & ", " _
        '                                    & SFilter(Row("modified_date")) & ")"
        '                    DB_Exec_NonQuery(Query, GSS_con)
        '                    DB_Exec_NonQuery(Query, GRABB_con)
        '                    DB_Exec_NonQuery_MySQL(Query, CRM_con)

        '                End If
        '            End If
        '        End If
        '    Next
        'Next

#End Region

#Region "Mapping Clients"
        '*******************************************************
        'Load Mapping Clients
        '*******************************************************

        ''Query = "TRUNCATE TABLE gss_MapClient"
        ''DB_Exec_NonQuery(Query, GSS_con)
        ''DB_Exec_NonQuery(Query, GRABB_con)
        ''DB_Exec_NonQuery_MySQL(Query, CRM_con)

        'Dim dtClientMap As New DataTable
        'Query = "SELECT id, champ4, champ5, champ6, modified_date FROM re_clients WHERE modified_date >= '" & LastUpdate & "'"
        'Using cmd As New SqlCommand(Query, Jamp_con)
        '    Using sda As New SqlDataAdapter(cmd)
        '        sda.Fill(dtClientMap)
        '    End Using
        'End Using

        'Dim dtGSSClientMap As New DataTable
        'Dim dvGSSClientMap As New DataView(dtGSSClientMap)

        'Dim dtGSSClientGuid As New DataTable
        'Query = "SELECT iClientCode, sClientGuid FROM gss_ClientM"
        'Using cmd As New SqlCommand(Query, GSS_con)
        '    Using sda As New SqlDataAdapter(cmd)
        '        sda.Fill(dtGSSClientGuid)
        '    End Using
        'End Using
        'Dim dvGSSClientGuid As New DataView(dtGSSClientGuid)

        'Dim DistShort As String
        'Dim iNoClient As Integer

        'Index = 0
        'For zLoop = 0 To 8
        '    DistShort = ""
        '    iDistCode = 0
        '    Count = 1
        '    Select Case zLoop
        '        Case 0
        '            DistShort = "MCK"
        '            iDistCode = 53
        '        Case 1
        '            DistShort = "MCKRx"
        '            iDistCode = 53
        '        Case 2
        '            DistShort = "MCKOTC"
        '            iDistCode = 53
        '        Case 3
        '            DistShort = "FAM"
        '            iDistCode = 6
        '        Case 4
        '            DistShort = "MCM"
        '            iDistCode = 59
        '        Case 5
        '            DistShort = "PJC"
        '            iDistCode = 8
        '        Case 6
        '            DistShort = "K&F"
        '            iDistCode = 15
        '        Case 7
        '            DistShort = "DP+"
        '            iDistCode = 3
        '        Case 8
        '            DistShort = "JSYS"
        '            iDistCode = 0
        '    End Select

        '    dtGSSClientMap.Reset()
        '    Query = "SELECT iDistCode, iClientCode, iNoClient, NoClient FROM gss_MapClient WHERE iDistCode = " & iDistCode
        '    Using cmd As New SqlCommand(Query, GSS_con)
        '        Using sda As New SqlDataAdapter(cmd)
        '            sda.Fill(dtGSSClientMap)
        '        End Using
        '    End Using
        '    dvGSSClientMap.Dispose()
        '    dvGSSClientMap = New DataView(dtGSSClientMap)

        '    For Each Row In dtClientMap.Rows
        '        btn_Start.Text = "Mapping client (" & DistShort & ")" & vbLf & Count
        '        Application.DoEvents()
        '        Count += 1

        '        If Not Row("champ4").ToString.EndsWith(",") Then Row("champ4") &= ","
        '        If Not Row("champ5").ToString.EndsWith(",") Then Row("champ5") &= ","
        '        Mapping = Row("champ4") & Row("champ5") & Row("champ6")
        '        aMap = Mapping.Split(",")
        '        bMap = {}
        '        If aMap(0) <> "#N/A" Then
        '            If aMap.Count > zLoop AndAlso aMap(zLoop).StartsWith(DistShort) Then
        '                bMap = aMap(zLoop).Split(":")
        '            Else
        '                For xloop = 0 To aMap.Count - 1
        '                    If aMap(xloop).StartsWith(DistShort) Then
        '                        bMap = aMap(xloop).Split(":")
        '                        Exit For
        '                    End If
        '                Next
        '            End If

        '            If bMap.Count > 1 AndAlso bMap(1).Length > 0 Then
        '                dvGSSClientMap.RowFilter = "NoClient = '" & bMap(1).ToString.Trim & "'"
        '                If dvGSSClientMap.Count = 0 Then
        '                    dvGSSClientGuid.RowFilter = "sClientGuid = '" & Row("id") & "'"
        '                    If dvGSSClientGuid.Count > 0 Then
        '                        iClientCode = dvGSSClientGuid(0)("iclientcode")
        '                        If Index = 0 Then
        '                            Index = DB_Get_Next_Index("iMapClientCode", "gss_MapClient", GSS_con)
        '                        Else
        '                            Index += 1
        '                        End If
        '                        Integer.TryParse(bMap(1), iNoClient)
        '                        Query = "INSERT INTO gss_MapClient VALUES (" _
        '                        & Index & ", " _
        '                        & iDistCode & ", " _
        '                        & iClientCode & ", " _
        '                        & iNoClient & ", " _
        '                        & SFilter(bMap(1)) & ", " _
        '                        & "0" & ", " _
        '                        & SFilter(Row("modified_date")) & ")"
        '                        DB_Exec_NonQuery(Query, GSS_con)
        '                        DB_Exec_NonQuery(Query, GRABB_con)
        '                        DB_Exec_NonQuery_MySQL(Query, CRM_con)
        '                    End If
        '                End If
        '            End If
        '        End If
        '    Next
        'Next

#End Region

#Region "Clients"
        '*******************************************************
        'Load Client FROM Fidelio
        '*******************************************************

        QueryIN = "SELECT DISTINCT re_clients.id FROM re_clients " _
                             & "LEFT OUTER JOIN re_clients_attributs ON re_clients.id = re_clients_attributs.parent_id " _
                             & "LEFT OUTER JOIN re_clients_adresses ON re_clients.id = re_clients_adresses.re_clients_id " _
                             & "WHERE re_clients.modified_date >= " & SFilter(LastUpdate) & " OR " _
                             & "re_clients_attributs.modified_date >= " & SFilter(LastUpdate) & " OR " _
                             & "re_clients_adresses.modified_date >= " & SFilter(LastUpdate)
        Dim dtClient As New DataTable
        Query = "SELECT id, description, etat, langue, champ1, champ2, champ3, modified_date FROM re_clients WHERE id IN (" & QueryIN & ")"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtClient)
            End Using
        End Using

        dvGSSAttCL.RowFilter = "sAttName = 'Grossistes du client'"
        Dim sDistGuid As String = dvGSSAttCL(0)("sAttGuid")
        Dim dtDist As New DataTable
        Query = "SELECT id, description, parent_id FROM re_clients_att"
        Using cmd As New SqlCommand(Query, Jamp_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtDist)
            End Using
        End Using
        Dim dvDist As New DataView(dtDist)

        Dim dtGSSDist As New DataTable
        Query = "SELECT iDistCode, iClientCode, sDistName, sDistGrossName, sDistShortName, sDistGuid FROM gss_Dist"
        Using cmd As New SqlCommand(Query, GSS_con)
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(dtGSSDist)
            End Using
        End Using
        Dim dvGSSDist As New DataView(dtGSSDist)

        If dtClient.Rows.Count > 0 Then
            Dim dtClientAdr As New DataTable
            Query = "SELECT adresse, ville, xx_provinces_id, xx_pays_id, code_postal, re_clients_id, telephonebur, fax, email, telephonesf, modified_date " _
                        & "FROM re_clients_adresses WHERE re_clients_id IN (" & QueryIN & ")"
            Using cmd As New SqlCommand(Query, Jamp_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtClientAdr)
                End Using
            End Using
            Dim dvClientAdr = New DataView(dtClientAdr)

            Dim dtClientAtt As New DataTable
            Dim dvClientAtt = New DataView(dtClientAtt)

            Dim dtGSSCli As New DataTable
            Query = "SELECT sClientGuid, M.iClientCode, iClientTypeCode, sPreviousCode, iLangue, iActive, iDistPCode, iDistSCode, dClientUpdate, " _
                                   & "sClientName, sClientSocialReason, sClientAddress, sClientCity, iProvCode, sClientCountry, iRegCode, sClientPC, " _
                                   & "sClientTel, sClientFax, sClientEmail, iClientClassCode, sSHA512 FROM gss_ClientD D " _
                                   & "RIGHT OUTER JOIN gss_ClientM M ON D.iClientCode = M.iClientCode " _
                                   & "WHERE iCurrentInfo = 1 ORDER BY sClientGuid"
            Using cmd As New SqlCommand(Query, GSS_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtGSSCli)
                End Using
            End Using
            Dim dvGSSCli = New DataView(dtGSSCli)
            'dtGSSCli.PrimaryKey = New DataColumn() {dtGSSCli.Columns("sClientGuid")}

            Dim dtGSSCliC As New DataTable
            Query = "SELECT iClientCode, iCieCode, iTerrCode, iBanCode, iGrpCode FROM gss_ClientC WHERE iCurrentInfoC = 1"
            Using cmd As New SqlCommand(Query, GSS_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtGSSCliC)
                End Using
            End Using
            Dim dvGSSCliC = New DataView(dtGSSCliC)
            'dtGSSCli.PrimaryKey = New DataColumn() {dtGSSCliC.Columns("iClientCode"), dtGSSCliC.Columns("iCieCode")}

            Dim iClientTypeCode As Integer
            Dim sPreviousCode As String
            Dim dClientUpdate As DateTime
            Dim sClientName As String
            Dim sClientSocialReason As String
            Dim sClientAddress As String
            Dim sClientCity As String
            Dim sProv As String
            Dim iProvCode As Integer
            Dim sClientCountry As String
            Dim sClientPC As String
            Dim sClientTel As String
            Dim sClientFax As String
            Dim sClientEmail As String
            Dim Cie(dtGSSCie.Rows.Count - 1) As Integer
            Dim Terr(dtGSSCie.Rows.Count - 1) As Integer
            Dim Ban(dtGSSCie.Rows.Count - 1) As Integer
            Dim Grp(dtGSSCie.Rows.Count - 1) As Integer
            Dim iBan As Integer
            Dim iGrp As Integer
            Dim iCliClass As Integer
            Dim iDistP As Integer
            Dim iDistS As Integer
            Dim sTerrGuid As String

            Count = 1
            Index = 0
            IndexD = 0
            IndexC = 0
            For Each Row In dtClient.Rows
                btn_Start.Text = "Clients" & vbLf & Count
                Application.DoEvents()
                Count += 1

                'Calculate SHA512 to detect if date change on the server
                DataString = ""
                For xloop = 0 To dtClient.Columns.Count - 1
                    DataString &= Row(xloop)
                Next

                dvClientAdr.RowFilter = "re_clients_id = '" & Row("id") & "'"
                If dvClientAdr.Count > 0 Then
                    For xloop = 0 To dtClientAdr.Columns.Count - 1
                        DataString &= dvClientAdr(0)(xloop)
                    Next
                End If

                'Use SQL instead of Dataview, too many records
                dtClientAtt.Reset()
                Query = "SELECT att_id, att_parent_id FROM re_clients_attributs WHERE parent_id = '" & Row("id") & "' ORDER BY id"
                Using cmd As New SqlCommand(Query, Jamp_con)
                    Using sda As New SqlDataAdapter(cmd)
                        sda.Fill(dtClientAtt)
                    End Using
                End Using
                dvClientAtt = New DataView(dtClientAtt)

                For xloop = 0 To dvClientAtt.Count - 1
                    For yloop = 0 To dtClientAtt.Columns.Count - 1
                        DataString &= dvClientAtt(xloop)(yloop)
                    Next
                Next

                sNewSHA512 = GenerateSHA512String(DataString)

                sSHA512 = ""

                'Dim drRow = dtGSSCli.Rows.Find(Row("id"))

                dvGSSCli.RowFilter = "sClientGuid = '" & Row("id") & "'"
                If dvGSSCli.Count > 0 Then sSHA512 = dvGSSCli(0)("sSHA512")

                If sSHA512 <> sNewSHA512 Then
                    iClientTypeCode = 0
                    dvClientAtt.RowFilter = "att_parent_id = '" & sClientTypeGuid & "'"
                    If dvClientAtt.Count > 0 Then
                        dvGSSClientType.RowFilter = "sClientTypeGuid = '" & dvClientAtt(0)("att_id") & "'"
                        If dvGSSClientType.Count > 0 Then
                            iClientTypeCode = dvGSSClientType(0)("iClientTypeCode")
                        End If
                    End If
                    sPreviousCode = Row("Champ2")
                    iLangue = Row("Langue")
                    iActive = Row("etat")
                    dClientUpdate = New DateTime(2000, 1, 1)
                    sClientName = Row("Champ3").ToString.Trim
                    sClientSocialReason = Row("description")
                    If sClientName.Length = 0 Then sClientName = sClientSocialReason
                    sClientAddress = ""
                    sClientCity = ""
                    iProvCode = 0
                    sClientCountry = ""
                    iRegCode = 0
                    sClientPC = ""
                    sClientTel = ""
                    sClientFax = ""
                    sClientEmail = ""

                    dvClientAdr.RowFilter = "re_clients_id = '" & Row("id") & "'"
                    If dvClientAdr.Count > 0 Then
                        sProv = dvClientAdr(0)("xx_provinces_id")
                        dClientUpdate = dvClientAdr(0)("modified_date").ToString.Trim
                        sClientAddress = dvClientAdr(0)("adresse").ToString.Trim
                        sClientCity = dvClientAdr(0)("ville").ToString.Trim
                        sClientCountry = dvClientAdr(0)("xx_pays_id").ToString.Trim
                        sClientPC = dvClientAdr(0)("code_postal").ToString.Trim.Replace("O", "0")
                        If sClientPC.Length = 6 Then sClientPC = sClientPC.Substring(0, 3) & " " & sClientPC.Substring(3, 3)
                        sClientTel = dvClientAdr(0)("telephonebur").ToString.Trim
                        sClientFax = dvClientAdr(0)("fax").ToString.Trim
                        sClientEmail = dvClientAdr(0)("email").ToString.Trim
                        sProv = dvClientAdr(0)("xx_provinces_id")

                        dvGSSProv.RowFilter = "sProvName = '" & sProv & "'"
                        If dvGSSProv.Count > 0 Then
                            iProvCode = dvGSSProv(0)("iProvcode")
                            sClientCountry = dvGSSProv(0)("sProvCountry").ToString.Trim
                        Else
                            'Patch, inconsistance dans Fidelio, pas de ID pour la province
                            If sProv = "Colombie Britannique" Then sProv = "COLOMBIE-BRITANNIQUE"
                            If sProv = "Nouveau Brunswick" Then sProv = "NOUVEAU-BRUNSWICK"
                            If sProv = "Nouvelle Écosse" Then sProv = "NOUVELLE-ÉCOSSE"
                            If sProv = "Territoires du Nord-Ouest" Then sProv = "NORTHWEST TERRITORIES"
                            If sProv = "Île du Prince-Édouard" Then sProv = "ÎLE DU PRINCE ÉDOUARD"
                            dvGSSProv.RowFilter = "sProvName = '" & sProv & "'"
                            If dvGSSProv.Count > 0 Then
                                iProvCode = dvGSSProv(0)("iProvcode")
                                sClientCountry = dvGSSProv(0)("sProvCountry").ToString.Trim
                            End If
                        End If

                    End If

                    dvClientAtt.RowFilter = "att_parent_id = '" & sRegGuid & "'"
                    If dvClientAtt.Count > 0 Then
                        dvGSSReg.RowFilter = "sRegGuid = '" & dvClientAtt(0)("att_id") & "'"
                        If dvGSSReg.Count > 0 Then
                            iRegCode = dvGSSReg(0)("iRegCode")
                        End If
                    End If

                    iCliClass = 0
                    dvClientAtt.RowFilter = "att_parent_id = '" & sClientClassGuid & "'"
                    If dvClientAtt.Count > 0 Then
                        dvGSSClientClass.RowFilter = "sClientClassGuid = '" & dvClientAtt(0)("att_id") & "'"
                        If dvGSSClientClass.Count > 0 Then
                            iCliClass = dvGSSClientClass(0)("iClientClassCode")
                        End If
                    End If

                    iDistP = 0
                    iDistS = 0
                    dvClientAtt.RowFilter = "att_parent_id = '" & sDistGuid & "'"
                    If dvClientAtt.Count > 0 Then
                        dvDist.RowFilter = "id = '" & dvClientAtt(0)("att_id") & "'"
                        If dvDist.Count > 0 Then
                            dvGSSDist.RowFilter = "sDistGrossName = '" & dvDist(0)("description").ToString.Trim & "'"
                            If dvGSSDist.Count > 0 Then
                                iDistP = dvGSSDist(0)("iDistCode")
                            End If
                        End If
                        dvClientAtt.RowFilter = "att_parent_id = '" & dvClientAtt(0)("att_id") & "'"
                        If dvClientAtt.Count > 0 Then
                            dvDist.RowFilter = "id = '" & dvClientAtt(0)("att_id") & "'"
                            If dvDist.Count > 0 Then
                                dvGSSDist.RowFilter = "sDistGrossName = '" & dvDist(0)("description").ToString.Trim & "'"
                                If dvGSSDist.Count > 0 Then
                                    iDistS = dvGSSDist(0)("iDistCode")
                                End If
                            End If
                        End If
                    End If

                    iBan = 0
                    dvClientAtt.RowFilter = "att_parent_id = '" & sBanGUid & "'"
                    If dvClientAtt.Count > 0 Then
                        dvGSSBan.RowFilter = "sBanGuid = '" & dvClientAtt(0)("att_id") & "'"
                        If dvGSSBan.Count > 0 Then
                            iBan = dvGSSBan(0)("iBanCode")
                        End If
                    End If

                    iGrp = 0
                    dvClientAtt.RowFilter = "att_parent_id = '" & sGrpGUid & "'"
                    If dvClientAtt.Count > 0 Then
                        dvGSSGrp.RowFilter = "sGrpGuid = '" & dvClientAtt(0)("att_id") & "'"
                        If dvGSSGrp.Count > 0 Then
                            iGrp = dvGSSGrp(0)("iGrpCode")
                        End If
                    End If

                    For xloop = 0 To dtGSSCie.Rows.Count - 1
                        Cie(xloop) = 0
                        Ban(xloop) = iBan
                        Grp(xloop) = iGrp
                        Terr(xloop) = 0
                        sTerrGuid = dtGSSCie.Rows(xloop)("sCieDepGuid") 'Get GUID for Cie Territory
                        If sTerrGuid.Length > 0 Then
                            dvClientAtt.RowFilter = "att_parent_id = '" & dtGSSCie.Rows(xloop)("sCieDepGuid") & "'"
                            If dvClientAtt.Count > 0 Then
                                dvDepCie.RowFilter = "id = '" & dvClientAtt(0)("att_id") & "'"
                                If dvDepCie.Count > 0 Then
                                    dvGSSTerr.RowFilter = "sTerrName = '" & dvDepCie(0)("description").ToString.Substring(0, 4) & "'"
                                    If dvGSSTerr.Count > 0 Then
                                        Terr(xloop) = dvGSSTerr(0)("iTerrCode")
                                        Cie(xloop) = dvGSSTerr(0)("iCieCode")
                                    End If
                                End If
                            End If
                        End If
                    Next

                    If dvGSSCli.Count = 0 Then
                        If Index = 0 Then
                            Index = DB_Get_Next_Index("iClientCode", "gss_ClientM", GSS_con)
                        Else
                            Index += 1
                        End If

                        Query = "INSERT INTO gss_ClientM VALUES (" _
                                    & Index & ", " _
                                    & iClientTypeCode & ", " _
                                    & SFilter(sPreviousCode) & ", " _
                                    & SFilter(Row("id")) & ", " _
                                    & iLangue & ", " _
                                    & iActive & ", " _
                                    & SFilter(UpdateCheckGSS) & ")"
                        DB_Exec_NonQuery(Query, GSS_con)

                        Query = "INSERT INTO gss_ClientM VALUES (" _
                                    & Index & ", " _
                                    & iClientTypeCode & ", " _
                                    & SFilter(sPreviousCode) & ", " _
                                    & SFilter(Row("id")) & ", " _
                                    & iLangue & ", " _
                                    & iActive & ")"
                        DB_Exec_NonQuery(Query, GRABB_con)
                        DB_Exec_NonQuery_MySQL(Query, CRM_con)

                        If IndexD = 0 Then
                            IndexD = DB_Get_Next_Index("iClientDetailCode", "gss_ClientD", GSS_con)
                        Else
                            IndexD += 1
                        End If

                        Query = "INSERT INTO gss_ClientD VALUES (" _
                                    & IndexD & ", " _
                                    & Index & ", " _
                                    & iCliClass & ", " _
                                    & iDistP & ", " _
                                    & iDistS & ", " _
                                    & SFilter(dClientUpdate) & ", " _
                                    & SFilter(sClientName) & ", " _
                                    & SFilter(sClientSocialReason) & ", " _
                                    & SFilter(sClientAddress) & ", " _
                                    & SFilter(sClientCity) & ", " _
                                    & iProvCode & ", " _
                                    & SFilter(sClientCountry) & ", " _
                                    & iRegCode & ", " _
                                    & SFilter(sClientPC) & ", " _
                                    & SFilter(sClientTel) & ", " _
                                    & SFilter(sClientFax) & ", " _
                                    & SFilter(sClientEmail) & ", " _
                                    & "1" & ")"
                        DB_Exec_NonQuery(Query, GSS_con)
                        DB_Exec_NonQuery(Query, GRABB_con)
                        DB_Exec_NonQuery_MySQL(Query, CRM_con)

                        For xloop = 0 To dtGSSCie.Rows.Count - 1
                            If Terr(xloop) > 0 Then
                                If IndexC = 0 Then
                                    IndexC = DB_Get_Next_Index("iClientcieCode", "gss_ClientC", GSS_con)
                                Else
                                    IndexC += 1
                                End If
                                Query = "Insert INTO gss_ClientC VALUES (" _
                                            & IndexC & ", " _
                                            & Index & ", " _
                                            & Cie(xloop) & ", " _
                                            & SFilter(dClientUpdate) & ", " _
                                            & Terr(xloop) & ", " _
                                            & Ban(xloop) & ", " _
                                            & Grp(xloop) & ", " _
                                            & "1" & ")"
                                DB_Exec_NonQuery(Query, GSS_con)
                                DB_Exec_NonQuery(Query, GRABB_con)
                                DB_Exec_NonQuery_MySQL(Query, CRM_con)
                            End If
                        Next

                    Else
                        IndexU = dvGSSCli(0)("iClientCode")
                        If dvGSSCli(0)("iClientTypeCode") <> iClientTypeCode Or
                           dvGSSCli(0)("sPreviousCode") <> sPreviousCode Or
                           dvGSSCli(0)("iLangue") <> iLangue Or
                           dvGSSCli(0)("iActive") <> iActive Then

                            Query = "UPDATE gss_ClientM SET " _
                                        & "iClientTypeCode = " & iClientTypeCode & ", " _
                                        & "sPreviousCode = " & SFilter(sPreviousCode) & ", " _
                                        & "iLangue = " & iLangue & ", " _
                                        & "iActive = " & iActive & " " _
                                        & "WHERE iClientCode = " & IndexU
                            DB_Exec_NonQuery(Query, GSS_con)
                            DB_Exec_NonQuery(Query, GRABB_con)
                            DB_Exec_NonQuery_MySQL(Query, CRM_con)
                        End If

                        If dvGSSCli(0)("sClientName") <> sClientName.ToString.Trim Or
                           dvGSSCli(0)("sClientSocialReason") <> sClientSocialReason.ToString.Trim Or
                           dvGSSCli(0)("sClientAddress") <> sClientAddress.ToString.Trim Or
                           dvGSSCli(0)("sClientCity") <> sClientCity.ToString.Trim Or
                           dvGSSCli(0)("iProvCode") <> iProvCode Or
                           dvGSSCli(0)("sClientCountry") <> sClientCountry.ToString.Trim Or
                           dvGSSCli(0)("iRegCode") <> iRegCode Or
                           dvGSSCli(0)("iDistPCode") <> iDistP Or
                           dvGSSCli(0)("iDistSCode") <> iDistS Or
                           dvGSSCli(0)("iClientClassCode") <> iCliClass Or
                           dvGSSCli(0)("sClientPC") <> sClientPC.ToString.Trim Or
                           dvGSSCli(0)("sClientTel") <> sClientTel.ToString.Trim Or
                           dvGSSCli(0)("sClientFax") <> sClientFax.ToString.Trim Or
                           dvGSSCli(0)("sClientEmail") <> sClientEmail.ToString.Trim Then

                            Query = "UPDATE gss_ClientD SET " _
                                        & "iCurrentInfo = 0 " _
                                        & "WHERE iClientCode = " & IndexU
                            DB_Exec_NonQuery(Query, GSS_con)
                            DB_Exec_NonQuery(Query, GRABB_con)

                            If IndexD = 0 Then
                                IndexD = DB_Get_Next_Index("iClientDetailCode", "gss_ClientD", GSS_con)
                            Else
                                IndexD += 1
                            End If

                            Query = "INSERT INTO gss_ClientD VALUES (" _
                                        & IndexD & ", " _
                                        & IndexU & ", " _
                                        & iCliClass & ", " _
                                        & iDistP & ", " _
                                        & iDistS & ", " _
                                        & SFilter(dClientUpdate) & ", " _
                                        & SFilter(sClientName) & ", " _
                                        & SFilter(sClientSocialReason) & ", " _
                                        & SFilter(sClientAddress) & ", " _
                                        & SFilter(sClientCity) & ", " _
                                        & iProvCode & ", " _
                                        & SFilter(sClientCountry) & ", " _
                                        & iRegCode & ", " _
                                        & SFilter(sClientPC) & ", " _
                                        & SFilter(sClientTel) & ", " _
                                        & SFilter(sClientFax) & ", " _
                                        & SFilter(sClientEmail) & ", " _
                                        & "1" & ")"
                            DB_Exec_NonQuery(Query, GSS_con)
                            DB_Exec_NonQuery(Query, GRABB_con)

                            Query = "UPDATE gss_ClientD SET " _
                                        & "iDistPCode = " & iDistP & ", " _
                                        & "iDistSCode = " & iDistS & ", " _
                                        & "iClientClassCode = " & iCliClass & ", " _
                                        & "sClientName = " & SFilter(sClientName) & ", " _
                                        & "sClientSocialReason = " & SFilter(sClientSocialReason) & ", " _
                                        & "sClientAddress = " & SFilter(sClientAddress) & ", " _
                                        & "sClientCity = " & SFilter(sClientCity) & ", " _
                                        & "iProvCode = " & iProvCode & ", " _
                                        & "sClientCountry = " & SFilter(sClientCountry) & ", " _
                                        & "iRegCode = " & iRegCode & ", " _
                                        & "sClientPC = " & SFilter(sClientPC) & ", " _
                                        & "sClientTel = " & SFilter(sClientTel) & ", " _
                                        & "sClientFax = " & SFilter(sClientFax) & ", " _
                                        & "sClientEmail = " & SFilter(sClientEmail) & " " _
                                        & "WHERE iClientCode = " & IndexU
                            DB_Exec_NonQuery_MySQL(Query, CRM_con)

                        End If

                        For xloop = 0 To dtGSSCie.Rows.Count - 1
                            If Cie(xloop) > 0 Then
                                dvGSSCliC.RowFilter = "iClientCode = " & IndexU & " And iCieCode = " & Cie(xloop)
                                If dvGSSCliC.Count > 0 Then
                                    If dvGSSCliC(0)("iTerrCode") <> Terr(xloop) Or
                                       dvGSSCliC(0)("iBanCode") <> Ban(xloop) Or
                                       dvGSSCliC(0)("iGrpCode") <> Grp(xloop) Then

                                        UpdateData = True
                                    End If
                                Else
                                    If Terr(xloop) > 0 Then UpdateData = True
                                End If

                                If UpdateData = True Then
                                    If dvGSSCliC.Count > 0 Then
                                        Query = "UPDATE gss_ClientC SET " _
                                                    & "iCurrentInfoC = 0 " _
                                                    & "WHERE iClientCode = " & IndexU & " And iCieCode = " & Cie(xloop)

                                        DB_Exec_NonQuery(Query, GSS_con)
                                        DB_Exec_NonQuery(Query, GRABB_con)
                                        DB_Exec_NonQuery_MySQL(Query, CRM_con)
                                    End If

                                    If IndexC = 0 Then
                                        IndexC = DB_Get_Next_Index("iClientcieCode", "gss_ClientC", GSS_con)
                                    Else
                                        IndexC += 1
                                    End If

                                    Query = "Insert INTO gss_ClientC VALUES (" _
                                                & IndexC & ", " _
                                                & IndexU & ", " _
                                                & Cie(xloop) & ", " _
                                                & SFilter(dClientUpdate) & ", " _
                                                & Terr(xloop) & ", " _
                                                & Ban(xloop) & ", " _
                                                & Grp(xloop) & ", " _
                                                & "1" & ")"
                                    DB_Exec_NonQuery(Query, GSS_con)
                                    DB_Exec_NonQuery(Query, GRABB_con)

                                    Query = "UPDATE gss_ClientC SET " _
                                            & "iCieCode = " & Cie(xloop) & ", " _
                                            & "dClientUpdateC = " & SFilter(dClientUpdate) & ", " _
                                            & "iTerrCode = " & Terr(xloop) & ", " _
                                            & "iBanCode = " & Ban(xloop) & ", " _
                                            & "iGrpCode = " & Grp(xloop) & " " _
                                            & "WHERE iClientCode = " & IndexU
                                    DB_Exec_NonQuery_MySQL(Query, CRM_con)

                                    UpdateData = False
                                End If
                            End If
                        Next
                        Query = "UPDATE gss_ClientM SET sSHA512 = '" & sNewSHA512 & "' WHERE iClientCode = " & IndexU
                        DB_Exec_NonQuery(Query, GSS_con)
                    End If
                End If
            Next
        End If
#End Region

#Region "Distributeurs"
        '*******************************************************
        'Load Dist FROM Client
        '*******************************************************

        dvGSSClientType.RowFilter = "sClientTypeName = 'Grossiste'"
        If dvGSSClientType.Count > 0 Then

            Dim GrossisteType As Integer = dvGSSClientType(0)("iClientTypeCode")
            Dim dtGSSDistCli As New DataTable
            Query = "SELECT M.iClientCode, iClientTypeCode, sPreviousCode, sClientGuid, iLangue, iActive, dClientUpdate, sClientName, " _
                       & "sClientSocialReason, sClientAddress, sClientCity, iProvCode, sClientCountry, sClientPC, sClientTel, sClientFax, " _
                       & "sClientEmail, sClientGuid FROM gss_ClientD D RIGHT OUTER JOIN gss_ClientM M ON D.iClientCode = M.iClientCode " _
                       & "WHERE iCurrentInfo = 1 AND iClientTypeCode = " & GrossisteType
            Using cmd As New SqlCommand(Query, GSS_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtGSSDistCli)
                End Using
            End Using

            Dim dtDistShort As New DataTable
            Query = "SELECT id, champ4 FROM re_clients WHERE len(champ4) < 10 AND champ4 <> '#N/A' AND len(champ4) > 0"
            Using cmd As New SqlCommand(Query, Jamp_con)
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dtDistShort)
                End Using
            End Using
            Dim dvDistShort As New DataView(dtDistShort)

            Dim sDistName As String
            Dim sDistGrossName As String
            Dim sDistShortName As String

            Count = 1
            Index = 0
            For Each Row In dtGSSDistCli.Rows
                btn_Start.Text = "Distributeurs" & vbLf & Count
                Application.DoEvents()
                Count += 1

                iClientCode = Row("iClientCode")
                sDistName = Row("sClientName")
                sDistGuid = Row("sClientGuid")

                sDistShortName = ""
                dvDistShort.RowFilter = "id = '" & sDistGuid & "'"
                If dvDistShort.Count > 0 Then
                    sDistShortName = dvDistShort(0)("champ4").ToString.Trim
                    If sDistShortName = "MCKRX" Then sDistShortName = "MCK"
                    If sDistShortName = "MCKOTC" Then sDistShortName = ""
                End If

                sDistGrossName = ""
                If sDistShortName = "DP+" Then sDistGrossName = "Distribution Pplus"
                If sDistShortName = "FAM" Then sDistGrossName = "Familiprix"
                If sDistShortName = "PJC" Then sDistGrossName = "Pharmacies Jean-Coutu"
                If sDistShortName = "IMP-AB" Then sDistGrossName = "Imperial Distributors-AB"
                If sDistShortName = "IMP-BC" Then sDistGrossName = "Imperial Distributors-BC"
                If sDistShortName = "K&F" Then sDistGrossName = "Kohl & Frisch"
                If sDistShortName = "LAW" Then sDistGrossName = ""
                If sDistShortName = "LOB" Then sDistGrossName = ""
                If sDistShortName = "MCK" Then sDistGrossName = "McKesson"
                If sDistShortName = "MCM" Then sDistGrossName = "McMahon"
                If sDistShortName = "NUQ" Then sDistGrossName = "Nu-Quest"
                If sDistShortName = "SOB" Then sDistGrossName = ""
                If sDistShortName = "UNI" Then sDistGrossName = "Unipharm"

                dvGSSDist.RowFilter = "sDistGuid = '" & sDistGuid & "'"
                If dvGSSDist.Count = 0 Then
                    If Index = 0 Then
                        Index = DB_Get_Next_Index("iDistCode", "gss_Dist", GSS_con)
                    Else
                        Index += 1
                    End If

                    Query = "INSERT INTO gss_Dist VALUES (" _
                                & Index & ", " _
                                & iClientCode & ", " _
                                & SFilter(sDistName) & ", " _
                                & SFilter(sDistGrossName) & "' " _
                                & SFilter(sDistShortName) & ", " _
                                & SFilter(sDistGuid) & ", " _
                                & SFilter(Row("dClientUpdate")) & ")"
                    DB_Exec_NonQuery(Query, GSS_con)
                    DB_Exec_NonQuery(Query, GRABB_con)
                    DB_Exec_NonQuery_MySQL(Query, CRM_con)

                    UpdateData = True
                Else
                    If dvGSSDist(0)("iClientCode") <> iClientCode Or
                       dvGSSDist(0)("sDistName") <> sDistName Or
                       dvGSSDist(0)("sDistGrossName") <> sDistGrossName Or
                       dvGSSDist(0)("sDistShortName") <> sDistShortName Then

                        Query = "UPDATE gss_Dist SET " _
                                    & "iClientCode = " & iClientCode & ", " _
                                    & "sDistName = " & SFilter(sDistName) & ", " _
                                    & "sDistGrossName = " & SFilter(sDistGrossName) & ", " _
                                    & "sDistShortName = " & SFilter(sDistShortName) & ", " _
                                    & "dDistUpdate = " & SFilter(Row("dClientUpdate")) & " " _
                                    & "WHERE iDistCode = " & dvGSSDist(0)("iDistCode")
                        DB_Exec_NonQuery(Query, GSS_con)
                        DB_Exec_NonQuery(Query, GRABB_con)
                        DB_Exec_NonQuery_MySQL(Query, CRM_con)

                        UpdateData = True
                    End If
                End If
            Next
        End If
#End Region

        CRM_con.Close()
        CRM_con.Dispose()
        GRABB_con.Close()
        GRABB_con.Dispose()
        GSS_con.Close()
        GSS_con.Dispose()
        Jamp_con.Close()
        Jamp_con.Dispose()
        Application.Exit()
    End Sub

    Public Function GenerateSHA512String(ByVal inputString) As String
        Dim sha512 As SHA512 = SHA512Managed.Create()
        Dim bytes As Byte() = Encoding.UTF8.GetBytes(inputString)
        Dim hash As Byte() = sha512.ComputeHash(bytes)
        Dim stringBuilder As New StringBuilder()

        For i As Integer = 0 To hash.Length - 1
            stringBuilder.Append(hash(i).ToString("X2"))
        Next

        Return stringBuilder.ToString()
    End Function

End Class
